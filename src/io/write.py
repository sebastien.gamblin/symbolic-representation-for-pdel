
import os
import pickle
import subprocess
import time

from src.io.colors import *

__default_path = "./output/"

def getDefaultOutputPath():
    # Check whether the specified path exists or not
    isExist = os.path.exists(__default_path)

    if not isExist:
      # Create a new directory because it does not exist
      os.makedirs(__default_path)
      print(f">>> The {__default_path} is created.")
    return __default_path

def verifyPath(path):
    if not os.path.exists(path):
        os.makedirs(path)

def stringToFile(filename: str, string: str, extension="txt", location=None):

    loc = __default_path if location == None else __default_path + location

    verifyPath(loc)

    name_path = loc + "{}.{}".format(filename, extension.replace(".", ""))

    with open(name_path, "w") as text_file:
        # print("  >> Writing in", name_path)
        print(string, file=text_file)
    return name_path


def remove(filename, location=None, show=False):
    loc = __default_path if location == None else __default_path + location
    verifyPath(loc)
    name_path = loc + filename

    if os.path.exists(name_path):
        if show:
            print("Remove " + name_path)
        os.remove(name_path)
    else:
        if show:
            print(name_path + " doesn't exists.")

def dotFileToSvg(file_path: str, name: str, location=None, timeout=None):

    loc = __default_path if location == None else __default_path + location
    verifyPath(loc)

    if timeout is None:
        sub = subprocess.Popen(["dot",
                          file_path,
                          "-Tsvg", "-o",
                          "{}".format(loc) + name + ".svg"],
                         stdout=subprocess.PIPE)
    else:
        assert isinstance(timeout, int)
        sub = subprocess.Popen(["timeout", str(timeout), "dot",
                                file_path,
                                "-Tsvg", "-o",
                                "{}".format(loc) + name + ".svg"],
                               stdout=subprocess.PIPE)

    print(Color.get(f"  >> Writing in {loc}{name}.svg", Color.CBOLD))

    # print("New file : {}{}.svg written.".format(loc, name))

    #liste = f"dot {file_path} -Tsvg -o {loc}{name}.svg"
    #l2 = ["dot", file_path, "-Tsvg", "-o", "{}".format(loc) + name + ".svg"]
    #popen = subprocess.Popen(l2)

    #(out, err) = popen.communicate()

    #print("RETURN", out, err)

    """
    command = f"dot {file_path} -Tsvg -o {loc}{name}.svg"
    print(command)
    try:
        subprocess.check_call(command)
    except subprocess.CalledProcessError:
        print(f"OUPS, error in dot {name}.dot")
    # print("RES", res)
    """


def stringToSvg(filename: str, string: str, location=None, timeout=60, datetime=False):
    assert isinstance(string, str), "Second argument must be a string for .dot. It's a {}".format(type(string).__name__)
    if datetime:
        from datetime import datetime
        now = datetime.now()
        # dd/mm/YY H:M:S
        dt_string = now.strftime("%d_%m_%Y %H:%M:%S")
        filename = filename + dt_string
    res = stringToFile(filename, string, extension="dot", location=location)
    dotFileToSvg(res, filename, location=location, timeout=timeout)
    return res.replace(".dot", ".svg")

def stringToPNG(filename: str, string: str, location=None):
    assert isinstance(string, str), "Second argument must be a string for .dot. It's a {}".format(type(string).__name__)
    res = stringToFile(filename, string, extension="png", location=location)
    dotFileToSvg(res, filename, location=location)
    return res.replace(".dot", ".svg")


def clearFilesWithExtension(extension: str, location: str):

    loc = __default_path if location == None else __default_path + location
    os.system("rm {}*.{}".format(loc, extension))


"""
IT'S A TRY TO PUT SOME DOT IN ONLY ONE
def clusterOfDot(dots, dotGraph, title, location=None):

    loc = __default_path if location == None else __default_path + location

    digraph = 'digraph {graph_name} {{\n' \
              '\tcompound = true;\n' \
              '\tedge[dir = forward];\n' \
              '\tnode[shape = plaintext];\n\n' \
              '\t{list_subgraphs}\n\n' \
              '\tlabelloc="t";\n' \
              '\tlabel = {titre};\n\n\t' \
              '{list_arcs}\n' \
              '}}\n'.format(
        graph_name="GMaster",
        list_arcs="\n\t".join(["{g1} -> {g2} [ltail=cluster{g1}, lhead=cluster{g2}];".format(g1=rel1, g2=rel2) for rel1, rel2 in dotGraph]),
        titre=title,
        list_subgraphs="\n\t".join([dot.replace("digraph ", "subgraph " + "cluster") for dotName, dot in dots.items()])
    )

    stringToSvg("master", digraph, location=location)
"""


def save_object(obj, name, location=None):
    loc = __default_path if location == None else __default_path + location
    verifyPath(loc)
    name = name + ".pickle" if ".pickle" not in name else name
    print("save_object", loc + name)
    pickle.dump(obj, open(loc + name, 'wb'))


def read_object(name, location=None):
    loc = __default_path if location == None else __default_path + location
    name = name + ".pickle" if ".pickle" not in name else name
    print("read_object", loc + name)
    f = open(loc + name, 'rb')
    return pickle.load(f)


def plotCSV(csv_file, location=None):
    # gnuplot -c afficher.gnu data1.csv 'output.png'
    path = __default_path + location
    command = "gnuplot -c {}afficher.gnu {}{}.csv '{}{}.png'".format("data/scripts/", path, csv_file, path, csv_file)
    # os.system(command)
    p = subprocess.Popen(command, shell=True,
                         stdout=subprocess.PIPE, stdin=subprocess.PIPE, stderr=subprocess.PIPE)

def tableToCSV(filename, matrix, separator=";", location=None):
    file_string = ""
    for col in matrix:
        for line in col:
            file_string += str(line) + separator
        file_string += "\n"

    return stringToFile(filename, file_string, extension="csv", location=location)

import sys, os

# Disable
def blockPrint():
    sys.stdout = open(os.devnull, 'w')

# Restore
def enablePrint():
    sys.stdout = sys.__stdout__

class BlockPrint:

    def __init__(self, block=True, showtime=False):
        self.block = block
        self.showtime = showtime

    def __enter__(self):
        if self.showtime:
            self.begin = time.time()
        if self.block:
            blockPrint()

    def __exit__(self, *args):
        if self.block:
            enablePrint()
        if self.showtime:
            self.time = time.time() - self.begin
            print(Color.get('Total time : %2.4f sec' % (self.time), Color.CBOLD, Color.CGREEN))
