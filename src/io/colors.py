from enum import Enum

class Color(Enum):

    """
    Color is usefull to print in Unix terminal.
    By default, it's disable and do nothing.
    """
    DISABLE = False

    CEND      = '\33[0m'
    CBOLD     = '\33[1m'
    CITALIC   = '\33[3m'
    CUNDERLINE= '\33[4m'
    CBLINK    = '\33[5m'
    CBLINK2   = '\33[6m'
    CSELECTED = '\33[7m'

    CBLACK  = '\33[30m'
    CRED    = '\33[31m'
    CGREEN  = '\33[32m'
    CYELLOW = '\33[33m'
    CBLUE   = '\33[34m'
    CVIOLET = '\33[35m'
    CBEIGE  = '\33[36m'
    CWHITE  = '\33[37m'

    CBLACKBG  = '\33[40m'
    CREDBG    = '\33[41m'
    CGREENBG  = '\33[42m'
    CYELLOWBG = '\33[43m'
    CBLUEBG   = '\33[44m'
    CVIOLETBG = '\33[45m'
    CBEIGEBG  = '\33[46m'
    CWHITEBG  = '\33[47m'

    CGREY    = '\33[90m'
    CRED2    = '\33[91m'
    CGREEN2  = '\33[92m'
    CYELLOW2 = '\33[93m'
    CBLUE2   = '\33[94m'
    CVIOLET2 = '\33[95m'
    CBEIGE2  = '\33[96m'
    CWHITE2  = '\33[97m'

    CGREYBG    = '\33[100m'
    CREDBG2    = '\33[101m'
    CGREENBG2  = '\33[102m'
    CYELLOWBG2 = '\33[103m'
    CBLUEBG2   = '\33[104m'
    CVIOLETBG2 = '\33[105m'
    CBEIGEBG2  = '\33[106m'
    CWHITEBG2  = '\33[107m'

    @classmethod
    def disable(cls):
        cls.DISABLE = True

    @classmethod
    def enable(cls):
        cls.DISABLE = False

    @staticmethod
    def test():
        x = 0
        for i in range(24):
            colors = ""
            for j in range(5):
                code = str(x+j)
                colors = colors + "\33[" + code + "m\\33[" + code + "m\033[0m "
            print(colors)
            x = x+5

    @staticmethod
    def get(text, *args):
        """
        Retourne une string avec les couleurs données en *args
        :param args: couleurs de Color.CXXXX
        :return: str
        """
        if isinstance(text, tuple):
            text = ",".join([str(e) for e in text])
        if not Color.DISABLE.value:
            return "".join([str(c.value) for c in args]) + str(text) + Color.CEND.value
        return text

def pprint(text, *args):
    return Color.get(text, *args)

if __name__ == "__main__":

    Color.test()

    print(Color.get("Bla", Color.CRED))
    print(Color.get("Bla", Color.CRED2))
    print(Color.get("Bla", Color.CGREEN, Color.CBOLD))
    print(Color.get("Bla", Color.CGREY, Color.CBOLD))

