

import csv



def openCSV(csvfile, limit=";"):

    # '#' if for comment

    with open(csvfile) as csv_file:
        csv_reader = csv.reader(csv_file, delimiter=limit)
        line_count = 0
        datas = []
        for row in csv_reader:
            line_count += 1
            if len(row) == 0:
                continue
            if not row[0][0] == "#":
                datas.append(row)
        return datas


