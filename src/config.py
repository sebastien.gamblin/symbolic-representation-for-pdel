
"""
Centralize all gestionof variablesfor uniformation of use.
"""

#BDD_TYPE = "autoref"
BDD_TYPE = "cudd"
# other : "autoref"

def getPrimeSymbol():
    return "_p"

def getAfterSymbol():
    return "°" #"+"

def getBeforeSymbol():
    return "-"

def isPrimed(var):
    return var.endswith(getPrimeSymbol())

def getPrimedVariable(var: str):
    return "{}{}".format(var, getPrimeSymbol())

def getAfterVariable(var:str) -> str:
    return "{}{}".format(var, getAfterSymbol())

def getBeforeVariable(var:str) -> str:
    return "{}{}".format(var, getBeforeSymbol())

def getVariantOfVariable(var: str):
    return var, getAfterVariable(var), getPrimedVariable(var), getPrimedVariable(getAfterVariable(var))

def getVariantOfVariables(vars: [str]):
    return vars, [getAfterVariable(v) for v in vars], [getPrimedVariable(v) for v in vars], [getPrimedVariable(getAfterVariable(v)) for v in vars]

def cleanVariable(var):
    return var.replace(getAfterSymbol(), "").replace(getPrimeSymbol(), "")

def getMyListOfVariables(initial_variables) -> [[str]]:

    all_variables = []
    before_variables = []
    after_variables = []
    primes_variables = []

    for var in initial_variables:
        all_variables.append(var)

        prime = getPrimedVariable(var)
        primes_variables.append(prime)
        all_variables.append(prime)

        b = getBeforeVariable(var)
        before_variables.append(b)
        all_variables.append(b)

        a = getAfterVariable(var)
        after_variables.append(a)
        all_variables.append(a)

        bp = getPrimedVariable(b)
        before_variables.append(bp)
        primes_variables.append(bp)
        all_variables.append(bp)

        ap = getPrimedVariable(a)
        after_variables.append(ap)
        primes_variables.append(ap)
        all_variables.append(ap)

    print("Initial variables :", initial_variables)
    print("Before variables :", before_variables)
    print("After variables :", after_variables)
    print("Primes variables :", primes_variables)
    print("All variables :", all_variables)

    return before_variables, after_variables, primes_variables, all_variables



