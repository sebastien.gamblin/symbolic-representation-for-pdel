import unittest
import random
import time
import itertools
import math

from src.model.datastructure.real_function import PseudoBooleanFunctionManager, PseudoBooleanFunction
from src.model.datastructure.proba_table.proba_table import ProbaTableManager
from src.model.datastructure.proba_table.proba_table import ProbaTable

from src.model.datastructure.add.add_real_function import ADDManager

from src.model.epistemiclogic.epistemicmodel.world import Valuation
from src.model.epistemiclogic.formula.formula import *
from src.io.colors import *
from src.io.write import *

from src.model.epistemiclogic.epistemicmodel.explicit.explicit_proba_epistemic_model import ExplicitProbaEpistemicModel
from src.utils.memoize import memoize

from src.utils.timer import MemoryTimer, Timer

DEBUG = False

"""
USE :

manager: PseudoBooleanFunctionManager = ProbaTableManager | SLDDManager | ADDManager

run_tests(manager)
run_one_test(manager, test_name) # "test_exaclty_simple", etc
"""

little_test_cache = {}


def debug(*args, **kwargs):
    if DEBUG:
        print(*args, **kwargs)


def print_real_function(real_function, table=False, notZero=True):
    debug()
    if notZero:
        for x, y in real_function.support():
            debug([int(v) for k, v in x.items()] if table else x, y)
    else:
        for x, y in real_function.browse():
            debug([int(v) for k, v in x.items()] if table else x, y)

class PseudoBooleanFunctionTests(unittest.TestCase):

    repetition_number: int = 10
    nb_variables = 8
    print_time = False
    manager_type: PseudoBooleanFunctionManager = None
    memory = MemoryTimer(time.time())

    def setUp(self):
         if PseudoBooleanFunctionTests.print_time:
             self.startTime = time.time()

    def tearDown(self):
         if PseudoBooleanFunctionTests.print_time:
            t = time.time() - self.startTime
            #print(str(self.id), t)
            PseudoBooleanFunctionTests.memory.add(str(self.id()), t)

    def getPrimedVariable(self, v):
        return f"{v}_p"

    def myAssert(self, real_function, results, eps=10**(-15), all=True):
        for res_i, valuation in enumerate([list(i) for i in itertools.product([False, True], repeat=len(real_function.getVariables()))]):
            val = {x: valuation[i] for i, x in enumerate(real_function.getVariables())}
            #print(valuation, real_function.getValue(val), results[res_i])
            self.assertAlmostEquals(real_function.getValue(val), results[res_i])

    def myCompare(self, real_function, real_function2, eps=10**(-15), all=True):
        assert set(real_function.getVariables()) == set(real_function2.getVariables())
        for valuation in [list(i) for i in itertools.product([True, False], repeat=len(real_function.getVariables()))]:
            val = {x: valuation[i] for i, x in enumerate(real_function.getVariables())}
            self.assertAlmostEquals(real_function.getValue(val), real_function2.getValue(Valuation(val)))


    """
    Tests on specific formulas
    """

    def test_exactly_simple(self):

        n = PseudoBooleanFunctionTests.nb_variables

        vars = ["x{}".format(i) for i in range(1, 2 * n)]

        manager = self.manager_type.create()

        xs1 = vars[0:n]
        xs2 = vars[n - 1:2 * n - 1]

        exactly1 = And(Exactly(1, xs1), Or(Atom(vars[n]), Not(Atom(vars[n]))))
        exaclty2 = Exactly(1, xs2)
        debug(f"Test exaclty 1 parmi {xs1} x 1 parmi {xs2} =?= 1 parmi {xs1} || 1 parmi {xs2}")

        show = False
        with Timer("1", show=show):
            real_function1 = ProbaTableManager().from_formula(exactly1, vars=vars)
        with Timer("2", show=show):
            real_function2 = manager.from_formula(exactly1, vars=vars)
        with Timer("3", show=show):
            assert PseudoBooleanFunctionManager.compare(real_function1, real_function2, show=False)

        with Timer("3", show=show):
            real_function1 = ProbaTableManager().from_formula(And(exactly1, exaclty2), vars=vars)
        with Timer("4", show=show):
            real_function2 = manager.from_formula(And(exactly1, exaclty2), vars=vars)
        with Timer("5", show=show):
            assert PseudoBooleanFunctionManager.compare(real_function1, real_function2, show=False)



    def test_exactly(self):

        n = PseudoBooleanFunctionTests.nb_variables

        manager = self.manager_type.create()

        vars = [f"x{i}" for i in range(0, n)]

        for step in range(0, PseudoBooleanFunctionTests.repetition_number):
            i = int(random.uniform(1, len(vars)))
            form = Exactly(i, vars)
            assert PseudoBooleanFunctionManager.compare(
                ProbaTableManager().from_formula(formula=form, vars=vars),
                manager.from_formula(form, vars=vars),
                show=False
            )


    """
    Test fonctionnalities
    """

    def test_basic_real_function(self):

        manager = self.manager_type.create()

        a, b = Atom("a"), Atom("b")
        formulas = [Not(a), a, And(a, b), Or(a, b), Implies(a, b), Equiv(a, b), Not(b), b, Not(And(a, b))]

        results = [
            [1.0, 0.0, 1.0, 0.0],  # -a
            [0.0, 1.0, 0.0, 1.0],  # a
            [0.0, 0.0, 0.0, 1.0],  # a and b
            [0.0, 1.0, 1.0, 1.0],  # a or b
            [1.0, 0.0, 1.0, 1.0],  # a -> b
            [1.0, 0.0, 0.0, 1.0],  # a <-> b
            [1.0, 1.0, 0.0, 0.0],  # - b
            [0.0, 0.0, 1.0, 1.0],  #  b
            [1.0, 1.0, 1.0, 0.0],  # - ( a and b )
        ]

        for i, formula in enumerate(formulas):

            real_function = manager.from_formula(formula, vars=formula.getVariables())
            res = [real_function.getValue(Valuation(val)) for val in [{"a":0, "b":0}, {"a":1, "b":0}, {"a":0, "b":1}, {"a":1, "b":1}]]

            if debug:
                debug(formula, ":", res, results[i])

            assert results[i] == res


    def test_marginalization_test1(self):
        """
        Marginalisation on simple fonction
        :return: None
        """

        manager = self.manager_type.create()

        formula = And(Or(Atom("zap"), Atom("zbp")), And(Atom("a"), Atom("b")))

        real_function = manager.from_formula(formula)
        real_function2 = ProbaTableManager().from_formula(formula)

        #real_function.print()
        #print()
        #real_function2.print()

        #from src.io.write import stringToSvg
        #stringToSvg(f"ADD_test", real_function.toDot(style=True))

        #print("Compare :")
        assert PseudoBooleanFunctionManager.compare(real_function, real_function2, show=False)

        #print(">> MARG", "zap", "zbp")

        liste = ["zap"] # "zap", "zbp"]
        real_function3 = real_function.marginalization(liste)
        real_function4 = real_function2.marginalization(liste)

        #real_function3.print()
        #print(real_function3.pointer)
        #print()
        #real_function4.print()

        #print("Compare :")
        assert PseudoBooleanFunctionManager.compare(real_function3, real_function4, show=False)


    def get_small_test(self, order=None):
        """
        Return a simple real_function
        :return: SLDDPseudoBooleanFunction
        """

        @memoize(little_test_cache)
        def inside(order):

            if self.manager_type == ADDManager:
                manager = self.manager_type.create(order=order)
            else:
                manager = self.manager_type.create()

            vars = ["x", "y", "z"]
            tab = [
                [0, 0, 0, 0.1],
                [0, 0, 1, 0.3],
                [0, 1, 1, 0.6],
                [1, 0, 0, 0.8],
                [1, 0, 1, 0.9]
            ]

            liste = list()
            for ligne in tab:
                dic = {vars[i]: ligne[i] for i in range(0, len(vars))}
                liste.append((AndOfVarList(dic), ligne[-1]))

            return ProbaTableManager.create().exclusiveOrOfFormulasWithValues(liste, vars=vars), \
                   manager.exclusiveOrOfFormulasWithValues(liste, vars=vars)

        return inside(tuple(order) if order is not None else None)


    def test_marginalisation_test2(self):
        """
        Marginalisation on simple example
        :return: None
        """

        test_pt, other = self.get_small_test()

        for marg in [["z"], ["y", "z"], ["y", "z", "x"]]:
            assert PseudoBooleanFunctionManager.compare(
            test_pt.marginalization(marg),
            other.marginalization(marg), show=False)


    def test_conditionning(self):
        """
        Basic conditionning
        :return:
        """

        test_pt, other = self.get_small_test()

        for cond in [{"z": 0}, {"y": 1}, {"x": 0}]:
            assert PseudoBooleanFunctionManager.compare(
                test_pt.conditionning(cond),
                other.conditionning(cond))





    # TODO : SLDD TEST
    # def test_worst_add(self):
    #     """
    #     Create the best real_function with pow [1,2,4,8,16,...] and add 1 to get
    #     [2,3,5,9,17,...]
    #     :param n: number of variables in real_function
    #     :return:
    #     """
    #
    #     n = 5
    #
    #     #manager = SLDDManager.create()
    #     real_function = manager.getPerfectSLDD(n)
    #
    #     print_real_function(real_function)
    #
    #     results = [2.0**i for i in range(0, 2**(n))]
    #     debug(results, len(results))
    #     self.myAssert(real_function, results)
    #
    #     #stringToSvg("best", real_function.toDot(style="simple"), location="real_function_tests/")
    #
    #     one = manager.getConstant(1)
    #     self.myAssert(one, [1.0])
    #
    #     res = one.apply("+", real_function)
    #
    #     #stringToSvg("oups", res.toDot(style="simple"), location="real_function_tests/")
    #     debug(" + 1 =")
    #     results = [2.0 ** i + 1 for i in range(0, 2 ** (n))]
    #
    #     self.myAssert(res, results)
    #
    #     print_real_function(res)


    def get_add_real_function_example(self, a, b, c, d):

        manager = self.manager_type.create()

        liste1 = [(AndOfVarList({a: 0, b: 0}), 1),
                 (AndOfVarList({a: 0, b: 1}), 2),
                 (AndOfVarList({a: 1, b: 0}), 3),
                 (AndOfVarList({a: 1, b: 1}), 4)]

        liste2 = [(AndOfVarList({c: 0, d: 0}), 5),
                 (AndOfVarList({c: 0, d: 1}), 6),
                 (AndOfVarList({c: 1, d: 0}), 7),
                 (AndOfVarList({c: 1, d: 1}), 8)]

        real_function1 = manager.exclusiveOrOfFormulasWithValues(liste1, vars=[a, b])
        real_function2 = manager.exclusiveOrOfFormulasWithValues(liste2, vars=[c, d])
        real_function3 = ProbaTableManager().exclusiveOrOfFormulasWithValues(liste1, vars=[a, b])
        real_function4 = ProbaTableManager().exclusiveOrOfFormulasWithValues(liste2, vars=[c, d])

        return real_function3, real_function4, real_function1, real_function2


    def test_add_simple(self):

        for vars in [("a", "b", "a", "b"), ("a", "b", "a", "c"), ("a", "b", "c", "d")]:
            real_function1, real_function2, real_function3, real_function4 = self.get_add_real_function_example(*vars)

            assert PseudoBooleanFunctionManager.compare(
                real_function1.apply("+", real_function2),
                real_function3.apply("+", real_function4), show=False)


    def test_marginalisation_kripke(self):

        manager = self.manager_type.create()

        rename = {"a": "zap", "b": "zbp"}

        world1 = {"a": True, "b":False}
        world2 = {"a": False, "b":True}

        f1, f2 = AndOfVarList(world1), AndOfVarList(world2)
        f1p, f2p = f1.rename(rename), f2.rename(rename)

        # |w1 <-> w2  (|=reflexive)
        kripke = manager.exclusiveOrOfFormulasWithValues([
            (And(f1, f1p), 1),
            (And(f1, f2p), 1),
            (And(f2, f1p), 1),
            (And(f2, f2p), 1)
        ], vars=["a", "zap", "b", "zbp"])

        print_real_function(kripke)

        marg1 = kripke.marginalization(["zap", "zbp"])

        PseudoBooleanFunctionManager.compare(marg1, show=False)
        self.myAssert(marg1, [0.0, 2.0, 2.0, 0.0])


    def test_marginalisation_in_middle1(self):

        manager = self.manager_type.create()
        real_function = manager.getConstant(1)

        real_function.setValue({"a": 0, "b": 0}, 4)
        real_function.setValue({"a": 0, "b": 1}, 2)
        real_function.setValue({"a": 1, "b": 0}, 0.5)
        real_function.setValue({"a": 1, "b": 1}, 0.25)

        marg1 = real_function.marginalization(["a"])

        self.myAssert(marg1, [4.5, 2.25])

        marg2 = real_function.marginalization(["b"])
        self.myAssert(marg2, [6.0, 0.75])


    def test_marginalisation_in_middle2(self):

        real_function, real_function2 = self.get_small_test()

        PseudoBooleanFunctionManager.compare(real_function.marginalization(["x", "y"]), real_function2.marginalization(["x", "y"]), show=False)
        PseudoBooleanFunctionManager.compare(real_function.marginalization(["y"]), real_function2.marginalization(["y"]), show=False)
        PseudoBooleanFunctionManager.compare(real_function.marginalization(["x"]), real_function2.marginalization(["x"]), show=False)


    def test_marginalisation_in_fonction_small_example(self):

        show = False

        real_function1, real_function2 = self.get_small_test()

        for op in float.__add__, float.__mul__, min, max:

            for i in range(1, 4):
                for to_marg in itertools.permutations(["x", "y", "z"], i):

                    marg1 = real_function2.marginalization(to_marg, fonction=op)
                    marg1pt = real_function1.marginalization(to_marg, fonction=op)
                    assert PseudoBooleanFunctionManager.compare(marg1, marg1pt, show=show)

    def __test_marginalisation_with_exclusive(self, voc, liste_tuple, name="output_name"):

        manager = self.manager_type.create(voc)
        pt_manager = ProbaTableManager.create()

        add = manager.exclusiveOrOfFormulasWithValues(liste_tuple, vars=voc)
        pt = pt_manager.exclusiveOrOfFormulasWithValues(liste_tuple, vars=voc)

        stringToSvg(name, add.toDot(atoms=True))

        print_res = PseudoBooleanFunctionManager.compare(add, pt, show=False, string=False)

        stringToFile(name, print_res, extension="txt")

        for op in float.__add__, min, max, float.__mul__,:

            for i in range(1, len(voc)+1):
                for to_marg in itertools.permutations(voc, i):
                    # to_marg = ["x0"]
                    marg_add = add.marginalization(to_marg, fonction=op)
                    marg_pt = pt.marginalization(to_marg, fonction=op)
                    assert PseudoBooleanFunctionManager.compare(marg_add, marg_pt, show=False)


    def test_marginalisation_in_fonction_hard_example(self):

        n = 4
        voc = [f"x{i}" for i in range(n)]

        begin = Atom("x0")
        vus = [begin]
        liste_tuple = [(begin, 1)]
        for i in range(1, n):
            formula = And(Atom(f"x{i}"), MAnd(*[Not(a) for a in vus]))
            vus.append(formula)
            liste_tuple.append((formula, i+1))

        self.__test_marginalisation_with_exclusive(voc, liste_tuple, name="ex1")


    def test_marginalisation_in_fonction_hard_example2(self):

        n = 5
        voc = [f"x{i}" for i in range(1, n+1)]
        gls = {a : Atom(a) for a in voc }

        liste_tuple =[
            (parseExpr("(x1 & x3 & x4)", globals=gls), 13),
            (parseExpr("(x1 & x3 & -x4 & x5)", globals=gls), 11),
            (parseExpr("(x1 & x3 & -x4 & -x5)", globals=gls), 7),
            (parseExpr("(x1 & -x3 & x4)", globals=gls), 3),
            (parseExpr("(-x1 & x3)", globals=gls), 3),
            (parseExpr("(-x1 & -x3 )", globals=gls), 2)
        ]

        self.__test_marginalisation_with_exclusive(voc, liste_tuple, name="ex2")


    def operator_test(self, op):

        for _ in range(0, PseudoBooleanFunctionTests.repetition_number):
            n = PseudoBooleanFunctionTests.nb_variables
            m = PseudoBooleanFunctionTests.nb_variables // 2 if PseudoBooleanFunctionTests.nb_variables % 2 == 0 else (
                                                                                                                              PseudoBooleanFunctionTests.nb_variables - 2) // 2

            manager = self.manager_type.create()

            real_function1 = manager.getRandom(n, m, fill_percent=0.7)
            real_function2 = manager.getRandom(n, m, fill_percent=0.7)

            pt1 = ProbaTable.init_with_real_function(real_function1)
            pt2 = ProbaTable.init_with_real_function(real_function2)

            real_function_res = real_function1.apply(op, real_function2)
            proba_res = pt1.apply(op, pt2)

            PseudoBooleanFunctionManager.compare(real_function1, real_function2, real_function_res, proba_res, show=False)

            assert PseudoBooleanFunctionManager.compare(real_function_res, proba_res, show=False)

    def compare_test(self, function, *args, **kwargs):

        for i in range(0, PseudoBooleanFunctionTests.repetition_number):

            n = PseudoBooleanFunctionTests.nb_variables
            m = PseudoBooleanFunctionTests.nb_variables // 2 if PseudoBooleanFunctionTests.nb_variables % 2 == 0 else (
                                                                                                                              PseudoBooleanFunctionTests.nb_variables - 2) // 2
            manager = PseudoBooleanFunctionTests.manager_type.create()
            real_function1 = manager.getRandom(n, m, fill_percent=0.7, **kwargs)
            pt1 = ProbaTable.init_with_real_function(real_function1)

            try:
                func1 = getattr(real_function1, function)
                real_function_res = func1()
                func2 = getattr(pt1, function)
                proba_res = func2()
            except AttributeError:
                print("dostuff not found")

            iterator1 = list(real_function_res.browse())
            iterator2 = list(proba_res.browse())

            for i in range(0, len(iterator1)):
                self.assertAlmostEquals(iterator1[i][1], iterator2[i][1])


    def test_random_add(self):
        self.operator_test("+")

    def test_random_times(self):
        self.operator_test("x")

    def test_random_substract(self):
        self.operator_test("-")

    def test_random_divide(self):
        self.operator_test("/")

    def test_substract(self):
        # self.operator_test("-")
        real_function, real_function2 = self.get_small_test()

        assert PseudoBooleanFunctionManager.compare(
            real_function.apply("-", ProbaTableManager().getConstant(1)),
            real_function2.apply("-", real_function2.manager.getConstant(1)))


    def test_divide(self):

        manager = PseudoBooleanFunctionTests.manager_type.create()
        real_function, real_function2 = self.get_small_test()

        real_function_pt = real_function.apply("/", ProbaTableManager().getConstant(2))
        real_function3 = real_function2.apply("/", manager.getConstant(2))

        assert PseudoBooleanFunctionManager.compare(real_function, real_function2, show=False)
        assert PseudoBooleanFunctionManager.compare(real_function_pt, real_function3, show=False)

        self.myAssert(real_function3, [0.05, 0.15, 0.0, 0.3, 0.4, 0.45, 0.0, 0.0])

        # real_function.print(notZero=False)
        # print()
        # real_function2.print(notZero=False)

        # TODO: Obtain 2 ?
        # real_function.apply("/", real_function2).print()

        #self.operator_test("/")

    def test_not(self):

        real_function, real_function2 = self.get_small_test()

        assert PseudoBooleanFunctionManager.compare(
            real_function.booleanization().apply("not", None),
            real_function2.booleanization().apply("not", None),
            show=False
        )
        #self.myAssert(real_function2, [1.0, 1.0])

    def test_random_not(self):

        i = 0

        while i < PseudoBooleanFunctionTests.repetition_number:

            seed = None
            seed = random.randrange(100000000) if seed is None else seed
            n, m = PseudoBooleanFunctionTests.nb_variables, None
            manager = PseudoBooleanFunctionTests.manager_type.create()
            real_function1 = manager.getRandom(n, m, seed=seed)
            pt1 = ProbaTable.init_with_real_function(real_function1)

            real_function2 = real_function1.apply("not")
            pt2 = pt1.apply("not")

            assert PseudoBooleanFunctionManager.compare(pt2, real_function2, show=False)

            i +=1


    def test_random_conditionning(self):

        for _ in range(0, PseudoBooleanFunctionTests.repetition_number):
            n = PseudoBooleanFunctionTests.nb_variables
            m = PseudoBooleanFunctionTests.nb_variables // 2 if PseudoBooleanFunctionTests.nb_variables % 2 == 0 else (
                                                                                                                              PseudoBooleanFunctionTests.nb_variables - 2) // 2
            debug("====\nTEST ADD : RANDOM \n")

            manager = PseudoBooleanFunctionTests.manager_type.create()

            real_function1 = manager.getRandom(n, m)

            pt1 = ProbaTable.init_with_real_function(real_function1)

            # manager.compare_real_functions(real_function1, pt1, show=True)

            real_function_vars = real_function1.getVariables()
            to_take = int(random.uniform(1, len(real_function_vars)))
            sample = random.sample(real_function_vars, to_take)
            cond_vars = {v : random.getrandbits(1) for v in sample}

            cond_real_function = real_function1.conditionning(cond_vars)
            cond_proba = pt1.conditionning(cond_vars)

            # manager.compare_real_functions(cond_real_function, cond_proba, show=True)

            assert PseudoBooleanFunctionManager.compare(cond_real_function, cond_proba, show=False)


    def test_random_marginalisation(self):

        ratio = 0
        i = 0
        while i < PseudoBooleanFunctionTests.repetition_number:

            debug(f"{'=='*15} Test {i} {'=='*15}")

            seed = None # 55193576 # 67349898  # 66353532 # 52714377 #98757636 #74052081 #69360369 # 12815493 #5002786 # 30491565  #79144409 #2681707
            #seed = 86523249
            seed = random.randrange(100000000) if seed is None else seed

            # print(i, seed)
            n, m = PseudoBooleanFunctionTests.nb_variables, None

            manager = PseudoBooleanFunctionTests.manager_type.create()

            real_function1 = manager.getRandom(n, m, seed=seed)

            #real_function1.print()

            random.seed(seed)
            pt1 = ProbaTable.init_with_real_function(real_function1)
            real_function_vars = real_function1.getVariables()
            to_take = int(random.uniform(1, len(real_function_vars)))

            sample = random.sample(real_function_vars, to_take)
            #print(sample)
            if sample == []:
                continue

            debug(f"\nMarginalisation on {sample}")
            debug()

            # real_function1.print()

            liste = sample #[0:1]
            debug(f"Marginalisation on {liste}")
            try:
                cond_real_function = real_function1.marginalization(liste, fonction=min)
            except Exception:
                import sys, traceback
                print(Color.get("-"*50, Color.CRED))
                print(Color.get(f"Error with seed {seed}", Color.CRED))
                traceback.print_exc(file=sys.stdout)
                print(Color.get("-" * 50, Color.CRED))

            #print_real_function(cond_real_function, notZero=True)

            debug(f"\n>> {type(pt1).__name__} Marg")
            cond_proba = pt1.marginalization(liste, fonction=min)
            #print_real_function(cond_proba, notZero=True)

            if False: #let's me debugging like i want
                # if not manager.compare_real_functions(cond_real_function, cond_proba, show=False):
                manager.compare(cond_real_function, cond_proba, show=True)
                print(f"Not OK for seed={manager.last_seed}, fill_percent={manager.last_fill_percent}")
                location = "maginalisation/"
                stringToSvg(f"{seed}", real_function1.toDot(), location=location)
                stringToSvg(f"{seed}_marg_{sample}", cond_real_function.toDot(), location=location)
            else:
                ratio += 1

            i += 1
            #cond_real_function.print()
            #print()
            #cond_proba.print()
            debug(f"seed {i} : {seed}", end="")
            assert PseudoBooleanFunctionManager.compare(cond_real_function, cond_proba, show=False), f"marginalization fails for seed={seed}"
            debug()
            debug()


    def test_fixed_marg(self):

        def go(f, marg_vars):
            pt = ProbaTable.init_with_real_function(f)
            real_function_vars = f.getVariables()
            cond_real_function = f.marginalization(marg_vars)
            cond_proba = pt.marginalization(marg_vars)

            test = PseudoBooleanFunctionManager.compare(cond_real_function, cond_proba, show=False)
            if not test: #error
                print('Comparison between marginalized:')
                PseudoBooleanFunctionManager.compare(cond_real_function, cond_proba, show=True)
                print('Comparison between original functions:')
                PseudoBooleanFunctionManager.compare(f, pt, show=True)
            assert test
            #assert PseudoBooleanFunctionManager.compare(cond_real_function, cond_proba, show=False)

        x1, x2, x3, x0 = "x1", "x2", "x3", "x0"

        # first nonreg test
        """
        >>ADDPseudoBooleanFunction.print() :
        Vars=['x1', 'x2', 'x3', 'x0']
        -> x1:0, x0:0 = 1.0
        -> x1:0, x0:1 = 9.0
        -> x1:1, x2:0, x0:0 = 9.0
        -> x1:1, x2:0, x0:1 = 3.0
        -> x1:1, x2:1, x0:0 = 1.0
        -> x1:1, x2:1, x0:1 = 8.0
        <<<ADDPseudoBooleanFunction.print(). 6 lines.
        Marginalisation on ['x1', 'x2', 'x3']
        """
        manager = PseudoBooleanFunctionTests.manager_type.create(order=[x1, x3, x2, x0])
        f = manager.exclusiveOrOfFormulasWithValues([
            (AndOfVarList({x1:0, x0:0}), 1.0),
            (AndOfVarList({x1:0, x0:1}), 9.0),
            (AndOfVarList({x1:1, x2:0, x0:0}), 9.0),
            (AndOfVarList({x1:1, x2:0, x0:1}), 3.0),
            (AndOfVarList({x1:1, x2:1, x0:0}), 1.0),
            (AndOfVarList({x1:1, x2:1, x0:1}), 8.0),
        ], vars=[x1, x2, x3, x0])
        go(f, [x1, x2, x3])

        # second nonreg test
        """
        >>ADDPseudoBooleanFunction.print() :
        Vars=['x2', 'x3', 'x0', 'x1']
        -> x2:0, x0:0 = 1.0
        -> x2:0, x0:1, x1:0 = 5.0
        -> x2:0, x0:1, x1:1 = 4.0
        -> x2:1, x1:0 = 9.0
        -> x2:1, x1:1 = 1.0
        <<<ADDPseudoBooleanFunction.print(). 5 lines.

        Marginalisation on ['x0', 'x1']
        """
        manager = PseudoBooleanFunctionTests.manager_type.create(order=[x2, x3, x0, x1])
        f = manager.exclusiveOrOfFormulasWithValues([
            (AndOfVarList({x2:0, x0:0}), 1.0),
            (AndOfVarList({x2:0, x0:1, x1:0}), 5.0),
            (AndOfVarList({x2:0, x0:1, x1:1}), 4.0),
            (AndOfVarList({x2:1, x1:0}), 9.0),
            (AndOfVarList({x2:1, x1:1}), 1.0),
        ], vars=[x1, x2, x3, x0])
        #stringToSvg(f"fixed_marg", f.toDot())
        go(f, [x0, x1])


    def test_booleanization(self):

        real_function_pt, real_function_add = self.get_small_test()

        real_function_pt_bool = real_function_pt.booleanization()
        real_function_add_bool = real_function_add.booleanization()

        assert PseudoBooleanFunctionManager.compare(real_function_pt_bool, real_function_add_bool, show=False)

    def test_is_boolean(self):
        real_function, real_function3 = self.get_small_test()
        self.assertEqual(real_function.isBoolean(), False)

        self.assertEqual(real_function.booleanization().isBoolean(), True)

    # # TODO : SLDD TESTS
    # def test_reordering(self):
    #
    #     for i in range(0, TestSLDD.N):
    #
    #         nb_real_functions = 5
    #         nb_var = 8
    #
    #         liste = [f"x{i}" for i in range(0, nb_var)]
    #         ok = OrderKeyList(liste[:], reordering=True)
    #         manager = TestSLDD.manager_type.create(order_key=ok)
    #
    #         memory = dict()
    #         real_functions = dict()
    #
    #         for i_real_functions in range(0, nb_real_functions):
    #
    #             real_function = manager.getRandom(nb_var)
    #
    #             memory[i_real_functions] = ProbaTable.init_with_real_function(real_function)
    #             real_functions[i_real_functions] = real_function
    #
    #         real_function_x = real_functions[i_real_functions].apply("x", real_functions[i_real_functions-1])
    #
    #         debug("NB", len(list(real_functions.values())))
    #
    #         size1 = manager.grove.size()
    #
    #         # manager.export_to_svg(f"test_{i}_1", others=False)
    #
    #         debug(real_functions.values())
    #         manager.reorder()
    #         debug(real_functions.values())
    #
    #         # manager.export_to_svg(f"test_{i}_2", others=False)
    #
    #         for i_real_functions in range(0, nb_real_functions):
    #             debug("->", real_functions[i_real_functions])
    #             for valuation in [list(i) for i in itertools.product([True, False], repeat=len(liste))]:
    #                 val = {x: valuation[i] for i, x in enumerate(liste)}
    #                 debug(val, memory[i_real_functions].getProba(val), real_functions[i_real_functions].getProba(val))
    #                 self.assertAlmostEqual(memory[i_real_functions].getProba(val), real_functions[i_real_functions].getProba(val))
    #
    #         size2 = manager.grove.size()
    #         debug("SIZES", size1, size2)
    #         self.assertGreaterEqual(size1, size2)

    def test_cut(self):

        test_pt, other = self.get_small_test()

        #stringToSvg('get_small_test', other.toDot())

        # TODO: Test on < and <=
        for op in [">", ">=", "<=", "<"]:
            for fl in [0.3, 0.5, 0.8]:

                rv = 0.0 if op in [">", ">="] else 1.0

                test_pt2 = test_pt.cut(op, fl, rejection_value=rv)
                other2 = other.cut(op, fl, rejection_value=rv)

                assert PseudoBooleanFunctionManager.compare(test_pt2, other2, show=False), f"Error on get_small_test with {op} {fl}"

    def test_bool_cut(self):

        test_pt, other = self.get_small_test()

        #stringToSvg('get_small_test', other.toDot())

        # TODO: On random too
        for op in [">", ">=", "<=", "<"]:
            for fl in [0.3, 0.5, 0.8]:

                test_pt2 = test_pt.bool_cut(op, fl)
                other2 = other.bool_cut(op, fl)

                assert PseudoBooleanFunctionManager.compare(test_pt2, other2, show=False), f"Error on get_small_test with {op} {fl}"



    def test_cut_random(self):

        n, m = PseudoBooleanFunctionTests.nb_variables, None

        for op in [">", ">="]: #">", ">=",

            for i in range(0, PseudoBooleanFunctionTests.repetition_number):
                #print(i)

                seed = None #78344498 # 72934961 #23478620 # None
                seed = random.randrange(100000000) if seed is None else seed

                manager = PseudoBooleanFunctionTests.manager_type.create()

                real_function = manager.getRandom(n, n//2, values=float, mini=0.0, maxi=1.0, seed=seed)

                #stringToSvg(f"test_cut_{seed}", real_function.toDot())

                mini, maxi = 0.01, 1.0
                pt = ProbaTable.init_with_real_function(real_function)

                PseudoBooleanFunctionManager.compare(real_function, pt, show=False)

                rng = random.Random(seed)
                factor = 10.0 ** 4
                fl = rng.uniform(mini, maxi)
                fl = math.trunc(fl * factor) / factor

                #print("cut", op, fl)

                real_function2 = real_function.cut(op, fl, rejection_value=0.0)
                pt2 = pt.cut(op, fl, rejection_value=0.0)

                assert PseudoBooleanFunctionManager.compare(real_function2, pt2, show=False), f"Error on seed {seed}, {op} {fl}"


    def test_save_and_load(self):

        if self.manager_type != ADDManager:
            return


        # CREATE 3 PBF, save, dump, and compare.

        _, pbf1 = self.get_small_test(order=["x", "y", "z"])
        _, pbf2 = self.get_small_test(order=["z", "y", "x"])
        _, pbf3 = self.get_small_test(order=["y", "x", "z"])

        pbf1 = pbf1.apply("x", pbf1.manager.getConstant(2))
        pbf2 = pbf2.apply("x", pbf2.manager.getConstant(0.5))
        pbf3 = pbf3.apply("x", pbf3.manager.from_formula(And(Atom("x"), Not(Atom("a")))))

        """
        stringToSvg("manager1", pbf1.manager.toDot())
        stringToSvg("manager2", pbf2.manager.toDot())
        stringToSvg("manager3", pbf3.manager.toDot())
        stringToSvg("pbf1", pbf1.toDot())
        stringToSvg("pbf2", pbf2.toDot())
        stringToSvg("pbf3", pbf3.toDot())
        """

        name = "test_save"

        PseudoBooleanFunction.dump(pbf1, f"{name}-1", cache={})
        #print(f"ADD {name}-1.json saved.")
        PseudoBooleanFunction.dump(pbf2, f"{name}-2", cache={})
        #print(f"ADD {name}-2.json saved.")
        PseudoBooleanFunction.dump(pbf3, f"{name}-3", cache={})
        #print(f"ADD {name}-3.json saved.")

        pbf2_1 = PseudoBooleanFunction.load(PseudoBooleanFunctionTests.manager_type.create(), f"{name}-1")
        pbf2_2 = PseudoBooleanFunction.load(PseudoBooleanFunctionTests.manager_type.create(), f"{name}-2")
        pbf2_3 = PseudoBooleanFunction.load(PseudoBooleanFunctionTests.manager_type.create(), f"{name}-3")

        pbf4 = PseudoBooleanFunction.load(PseudoBooleanFunctionTests.manager_type.create(), f"{name}-1")

        assert PseudoBooleanFunctionManager.compare(pbf1, pbf2_1, pbf4, show=False)
        assert PseudoBooleanFunctionManager.compare(pbf2, pbf2_2, show=False)
        assert PseudoBooleanFunctionManager.compare(pbf3, pbf2_3, show=False)
        assert PseudoBooleanFunctionManager.compare(pbf4, pbf2_1, show=False)

        # SAME MANAGER BUT WITH DIFFERENT ORDERS
        def myfunc():
            manager = PseudoBooleanFunctionTests.manager_type.create()
            pbf3_1 = PseudoBooleanFunction.load(manager, f"{name}-1")
            # MUST RAISE AN ERROR : ORDERS ARE DIFFERENTS
            pbf3_2 = PseudoBooleanFunction.load(manager, f"{name}-2")

            #print(pbf3_1)
            #print(pbf3_2)
            pbf3_x = pbf3_1.apply("x", pbf3_2)
            #print(pbf3_x)

        self.assertRaises(AssertionError, myfunc)

        for i in range(1, 4):
            remove(f"{name}-{i}.json", show=True)

    def test_getProba_setProba(self):
        """
        test of getProba and setProba

        Alex 2021-09-04
        """
        manager = self.manager_type.create()
        rels = manager.getConstant(0.0)
        for val in (
                Valuation({"x": 0, "y": 1, "x_p": 0, "y_p": 1}),
                ):
            rels.setValue(val, 1.0)
        for val in (
                Valuation({"x": 0, "y": 0, "x_p": 0, "y_p": 1}),
                Valuation({"x": 0, "y": 1, "x_p": 0, "y_p": 0}),
                ):
            rels.setValue(val, 7.0)
        for val in (
                Valuation({"x": 0, "y": 0, "x_p": 1, "y_p": 0}),
                Valuation({"x": 1, "y": 0, "x_p": 0, "y_p": 0}),
                ):
            rels.setValue(val, -3.0)
        for val in (
                Valuation({"x": 0, "y": 1, "x_p": 1, "y_p": 0}),
                Valuation({"x": 1, "y": 0, "x_p": 0, "y_p": 1}),
                ):
            rels.setValue(val, -2.0)
        # we set those last 2 values again
        for val in (
                Valuation({"x": 0, "y": 1, "x_p": 1, "y_p": 0}),
                Valuation({"x": 1, "y": 0, "x_p": 0, "y_p": 1}),
                ):
            rels.setValue(val, -2.0)

        for val in (
                Valuation({"x": 1, "y": 0, "x_p": 1, "y_p": 0}),
                ):
            rels.setValue(val, 5.0)

        def index_to_valuation(index: int):
            d = dict()
            dividend = index
            for v in ("y_p", "x_p", "y", "x"):
                dividend, d[v] = divmod(dividend, 2)
            assert dividend == 0
            return Valuation(d)
        values = (
                0., 7., -3., 0.,
                7., 1., -2., 0.,
                -3., -2., 5., 0.,
                0., 0., 0., 0.,
        )
        for i, v in enumerate(values):
            valu = index_to_valuation(i)
            p = rels.getValue(valu)
            assert p == v, f'valuation {valu} should yield {v}, yielded {p}';

    def test_multi_compose(self):

        manager = PseudoBooleanFunctionTests.manager_type.create(tmp=True)

        bdd = manager.from_formula(MOr(Atom("a"), Not(Atom("b")), Atom("c")))

        a, b, c = Atom("a"), Atom("b"), Atom("c")

        res = bdd.compose(
            {
                "a": manager.from_formula(MAnd(a, b, Not(c))),
                "b": manager.from_formula(Or(b, c)),
                "c": manager.from_formula(MAnd(Not(a), b, Not(c)))
            }
        )

        nonc = manager.from_formula(Not(c))
        assert PseudoBooleanFunctionManager.compare(nonc, res)


def init(manager_type: PseudoBooleanFunctionManager, n: int=None, nb_vars: int=None, print_time:bool =False):
    """
    Initialise some parameters
    :param manager_type: type of Manager (BDDManager, SLDDManager, ADDManager...)
    :param name: name of method we want to test
    :param n: number of random tests
    :param nb_vars: number of variables used in tests
    :param print_time: print time taken by each method, or not (bool).
    :return:
    """

    if n is not None:
        PseudoBooleanFunctionTests.repetition_number = n
    if nb_vars is not None:
        PseudoBooleanFunctionTests.nb_variables = nb_vars
    if print_time is not None:
        PseudoBooleanFunctionTests.print_time = print_time

    PseudoBooleanFunctionTests.manager_type = manager_type
    assert PseudoBooleanFunctionTests.manager_type != None, "PseudoBooleanFunctionTests.manager_type must be precised."

    print(f"Run PseudoBooleanFunctionTests with {PseudoBooleanFunctionTests.repetition_number} random tests "
          f"for real_function={manager_type.__name__} and {PseudoBooleanFunctionTests.nb_variables} variables.")

    time.sleep(1)


def run_one_test(manager_type: PseudoBooleanFunctionManager, name: str, n: int=None, nb_vars: int=None, print_time:bool=False):

    init(manager_type, n=n, nb_vars=nb_vars, print_time=print_time)

    print(f"Tested function : {name}.\n")

    time.sleep(0.1)

    suite = unittest.TestSuite()
    suite.addTest(PseudoBooleanFunctionTests(name))
    unittest.TextTestRunner(verbosity=3).run(suite)


def run_tests(manager_type: PseudoBooleanFunctionManager, n:int = None, nb_vars:int = None, print_time:bool =False):

    init(manager_type, n=n, nb_vars=nb_vars, print_time=print_time)

    unittest.TextTestRunner(verbosity=3).run(
        unittest.TestLoader().loadTestsFromTestCase(PseudoBooleanFunctionTests)
    )

    if print_time:
        PseudoBooleanFunctionTests.memory.print(threshold=0.05)
