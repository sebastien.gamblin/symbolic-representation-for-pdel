import unittest

from src.model.datastructure.sldd import *
from src.model.datastructure.proba_table import *

SHOW = False

def show(*args, **kwargs):
    if SHOW:
        print(*args, **kwargs)


def print_sldd(sldd, table=False):
    show()
    for x, y in sldd.support():
        show([int(v) for k, v in x.items()] if table else x, y)


class TestProbaTable(unittest.TestCase):

    def myAssert(self, sldd, results, eps=10**(-15)):
        #print(pprint(f"Assert"), Color.CRED)
        for i, (x, y) in enumerate(sldd.support()):
            #print(y, results[i])
            self.assertAlmostEqual(y, results[i], delta=eps)

    def test_basic(self):
    
        a, b = Atom("a"), Atom("b")
        formulas = [Not(a), a, And(a, b), Or(a, b), Implies(a, b), Equiv(a, b), Not(b), b, Not(And(a, b))]

        results = [
            [1.0, 0.0, 1.0, 0.0],  # -a
            [0.0, 1.0, 0.0, 1.0],  # a
            [0.0, 0.0, 0.0, 1.0],  # a and b
            [0.0, 1.0, 1.0, 1.0],  # a or b
            [1.0, 0.0, 1.0, 1.0],  # a -> b
            [1.0, 0.0, 0.0, 1.0],  # a <-> b
            [1.0, 1.0, 0.0, 0.0],  # - b
            [0.0, 0.0, 1.0, 1.0],  #  b
            [1.0, 1.0, 1.0, 0.0],  # - ( a and b )
        ]

        for i, formula in enumerate(formulas):

            pt = ProbaTable(["a", "b"], formula=formula)
            res = [pt.getValue(Valuation(val)) for val in [{"a":0, "b":0}, {"a":1, "b":0}, {"a":0, "b":1}, {"a":1, "b":1}]]

            if show:
                show(formula, ":", res, results[i])

            assert results[i] == res

    def test_marginalization_test1(self):
        """
        Marginalisation on simple fonction
        :return: None
        """
        formula = And(Or(Atom("zap"), Atom("zbp")), And(Atom("a"), Atom("b")))

        pt = ProbaTable(["a", "b", "zap", "zbp"], formula=formula)

        for x, y in pt.support():
            show("-->", x, y)
        show()
        # stringToSvg("sldd_marg", sldd.toDot(), location="sldd_tests/")

        pt2 = pt.marginalization(["zap", "zbp"])
        #stringToSvg("sldd_after_{}".format("bp"), sldd2.toDot(), location="sldd_tests/")

        for x, y in pt2.support():
            show("-->", x, y)
        
        assert list(pt2.support())[0][1] == 3.0

    def get_small_test(self):
        """
        Return a simple ProbaTable
        :return: ProbaTable
        """

        vars = ["x", "y", "z"]
        pt = ProbaTable(vars)
        tab = [
            [0, 0, 0, 0.1],
            [0, 0, 1, 0.3],
            [0, 1, 1, 0.6],
            [1, 0, 0, 0.8],
            [1, 0, 1, 0.9]
        ]

        liste = list()
        for ligne in tab:
            dic = {vars[i]: ligne[i] for i in range(0, len(vars))}
            pt.setValue(dic, ligne[-1])

        return pt

    def test_marginalisation_test2(self):
        """
        Marginalisation on simple example
        :return: None
        """
        test = self.get_small_test()

        results = [0.4, 0.6, 1.7]
        testz = test.marginalization(["z"])
        self.myAssert(testz, results)

        results = [1.0, 1.7]
        testz = test.marginalization(["y", "z"])
        self.myAssert(testz, results)

        results = [2.7]
        testz = test.marginalization(["x", "y", "z"])
        self.myAssert(testz, results)

    def test_conditionning(self):
        """
        Basic conditionning
        :return:
        """

        sldd = self.get_small_test()
        print_sldd(sldd)

        testnz = sldd.conditionning({"z": 0})
        self.myAssert(testnz, [0.1, 0.8])
        print_sldd(testnz)

        testnz = sldd.conditionning({"z": 1})
        self.myAssert(testnz, [0.3, 0.6, 0.9])
        print_sldd(testnz)

        testnzy = sldd.conditionning({"y": 1})
        self.myAssert(testnzy, [0.6])
        print_sldd(testnzy)

        testnzny = sldd.conditionning({"x": 0})
        self.myAssert(testnzny, [0.1, 0.3, 0.6])
        print_sldd(testnzny)


    def get_add_probatable_example(self, a, b, c, d):
        
        sldd1 = ProbaTable([a, b])
        sldd1.setValue({a: 0, b: 0}, 1)
        sldd1.setValue({a: 0, b: 1}, 2)
        sldd1.setValue({a: 1, b: 0}, 3)
        sldd1.setValue({a: 1, b: 1}, 4)

        # stringToSvg("test_sldd1", sldd1.toDot(style=None), location="sldd_tests/")

        # sldd2 = manager.getConstant(2)

        sldd2 = ProbaTable([c, d])
        sldd2.setValue({c: 0, d: 0}, 5)
        sldd2.setValue({c: 0, d: 1}, 6)
        sldd2.setValue({c: 1, d: 0}, 7)
        sldd2.setValue({c: 1, d: 1}, 8)

        return sldd1, sldd2

    def test_add_simple(self):
    
        sldd1, sldd2 = self.get_add_probatable_example("a", "b", "a", "b")

        add1 = sldd1.apply("+", sldd2)

        print_sldd(sldd1)
        
        print_sldd(sldd2)
        print_sldd(add1)

        self.myAssert(add1, [6.0, 8.0, 10.0, 12.0])

    def test_add_simple2(self):

        sldd1, sldd2 = self.get_add_probatable_example("a", "b", "a", "c")

        add1 = sldd1.apply("+", sldd2)

        print_sldd(sldd1)
        print_sldd(sldd2)
        print_sldd(add1)

        self.myAssert(add1, [6.0, 7, 7, 8, 10, 11, 11, 12])

    def test_add_simple3(self):
    
        sldd1, sldd2 = self.get_add_probatable_example("a", "b", "c", "d")

        add1 = sldd1.apply("+", sldd2)

        print_sldd(sldd1)
        print_sldd(sldd2)
        print_sldd(add1)

        self.myAssert(add1, [6, 7, 8, 9, 7, 8, 9, 10, 8, 9, 10, 11, 9, 10, 11, 12])


    def test_marginalisation_kripke(self):
    
        manager = SLDDManager.create()

        rename = {"a": "zap", "b": "zbp"}

        world1 = {"a": True, "b":False}
        world2 = {"a": False, "b":True}

        f1, f2 = AndOfList(world1), AndOfList(world2)
        f1p, f2p = f1.rename(rename), f2.rename(rename)

        # |w1 <-> w2  (|=reflexive)

        kripke = ProbaTable(list(rename.keys())+list(rename.values()), formula=And(f1, And(f2, And(f1p, f2p))))

        print_sldd(kripke)

        marg1 = kripke.marginalization(["zap", "zbp"])

        print_sldd(marg1)
        self.myAssert(marg1, [2.0, 2.0])


    def get_probatable_constant(self, n):
        
        pt = ProbaTable([])
        pt.setValue(Valuation({}), n)
        pt.print()

    def test_marginalisation_in_middle1(self):
    
        sldd = ProbaTable(["a", "b"])

        sldd.setValue({"a": 0, "b": 0}, 4)
        sldd.setValue({"a": 0, "b": 1}, 2)
        sldd.setValue({"a": 1, "b": 0}, 0.5)
        sldd.setValue({"a": 1, "b": 1}, 0.25)

        #stringToSvg("simple_test_sldd", sldd.toDot(), location="sldd_tests/")

        print_sldd(sldd)

        marg1 = sldd.marginalization(["a"])

        #stringToSvg("simple_test_sldd_marg_y", sldd.toDot(), location="sldd_tests/")

        print_sldd(marg1)

        self.myAssert(marg1, [4.5, 2.25])

        marg2 = sldd.marginalization(["b"])

        print_sldd(marg2)

        self.myAssert(marg2, [6.0, 0.75])

    def test_marginalisation_in_middle2(self):
    
        sldd = self.get_small_test()

        print_sldd(sldd)

        marg1 = sldd.marginalization(["x", "y"])
        print_sldd(marg1)
        self.myAssert(marg1, [0.9, 1.8])

        marg2 = sldd.marginalization(["y"])
        print_sldd(marg2)
        self.myAssert(marg2, [0.1, 0.9, 0.8, 0.9])

        marg3 = sldd.marginalization(["x"])
        print_sldd(marg3)
        self.myAssert(marg3, [0.9, 1.2, 0.6])

def run_probatable_tests():
    
    t = unittest.TestLoader().loadTestsFromTestCase(TestProbaTable)
    unittest.TextTestRunner(verbosity=2).run(t)

if __name__ == "__main__":
    
    SHOW = True

    run_probatable_tests()
