import itertools
from prettytable import PrettyTable

from src.model.epistemiclogic.formula.formula import *
from src.model.epistemiclogic.epistemicmodel.explicit.explicit_epistemic_model import ExplicitEpistemicModel
from src.model.epistemiclogic.epistemicmodel.world import World


class TruthTable(object):

    def __init__(self, variables:[str]=None, formula:Formula=None):

        self.formula = formula
        self.variables = variables
        self.variables_places = {self.variables[i]:i for i in range(0, len(self.variables))}

        self.my_checker = ExplicitEpistemicModel("agent")

        if not formula:
            raise Exception('Formula is required')

        # generate the sets of booleans for the variables
        self.variables_conditions = list(itertools.product([False, True], repeat=len(variables)))

        self.results = []
        for condition in self.variables_conditions:
            world = World("world", {atom:condition[i] for i, atom in enumerate(self.variables)})
            self.results.append( self.my_checker.modelCheck(world, self.formula, assertion=False) )

        print(self.variables_conditions)

    def print(self, onlyTrue=False):
        t = PrettyTable(self.variables + [self.formula])
        for i, conditions_set in enumerate(self.variables_conditions):
            if onlyTrue and self.results[i]:
                t.add_row(conditions_set + (self.results[i],))
            if not onlyTrue:
                t.add_row(conditions_set + (self.results[i],))
        print(str(t))


if __name__ == '__main__':
    formula = And(Atom("x1"), Not(Atom("x2")))
    tt = TruthTable(["x{}".format(i) for i in range(1, 4)], formula)
    tt.print(onlyTrue=False)
