
import itertools
from prettytable import PrettyTable
from src.model.datastructure.real_function import PseudoBooleanFunction

from src.model.epistemiclogic.formula.formula import *
#from src.model.epistemiclogic.epistemicmodel.explicit.explicit_proba_epistemic_model import ExplicitProbaEpistemicModel
#from src.model.epistemiclogic.epistemicmodel.explicit.explicit_epistemic_model import ExplicitEpistemicModel
from src.model.epistemiclogic.epistemicmodel.world import World, Valuation

from src.model.datastructure.real_function import PseudoBooleanFunctionManager

from src.io import write
from src.config import *
from src.io.read import *
from src.io.write import *

import copy
import sys
from typing import List, Dict


class ProbaTableManager(PseudoBooleanFunctionManager):

    def __init__(self):
        pass

    @staticmethod
    def create():
        return ProbaTableManager()

    def from_formula(self, formula: Formula, vars:[str]= None, cache=None   ):
        return ProbaTable(variables=vars, formula=formula)

    def exclusiveOrOfFormulasWithValues(self, liste: List[Tuple[Formula, float]],
                                        vars: [str] = None):
        return ProbaTable.exclusiveOrOfFormulasWithValues(liste, vars=vars)

    def getConstant(self, n: float):
        return ProbaTable.exclusiveOrOfFormulasWithValues([(Top(), n)], vars=[])

    def reorder(self):
        return None

    def toDot(self):
        return ""

    def variables(self):
        return 0

    def _get_mentioned_variables_with_order(self):
        return []

    def size(self):
        return 0

class ProbaTable(PseudoBooleanFunction):

    """ Truth table with probabilities """

    def __init__(self, variables:[str]=None, formula: Formula=None):

        self.formula = formula

        self.variables = list(set(formula.getVariables())) if variables is None else variables

        # {var_1 : 0, var_2: 1, ...}
        self.variables_places = {self.variables[i]: i for i in range(0, len(self.variables))}

        # [ [False, False, ...], [False, False, ...]
        self.variables_conditions = list(itertools.product([0, 1], repeat=len(self.variables)))

        # { [False, False, ...]: 1, [False, False, ...]: 2, ...}
        self.variables_conditions_places = {}
        for i in range(0, len(self.variables_conditions)):
            self.variables_conditions_places[self.variables_conditions[i]] = i

        # [ 0.0, 0.0, ...]
        self.results = []
        for _ in self.variables_conditions:
            self.results.append(0.0)

        if formula is not None:
            res = self.__initWithFormula(formula)

    def _get_mentioned_variables_with_order(self):
        return self.getVariables()

    def get_size(self):
        return 2 ** len(self.getVariables())

    def toDot(self, *args, **kwargs):
        return ""

    def __initWithFormula(self, formula:Formula):
        for i, conditions_set in enumerate(self.variables_conditions):
            valuation = self.__conditionToValuation(conditions_set)
            res = formula.semantic(None, valuation)
            if res:
                self.setValue(valuation, 1.0)

    """
    @staticmethod
    def init_With_ExplicitProbaEpistemicModel(epem: ExplicitProbaEpistemicModel, agent):

        variables = set()
        for world in epem.getWorlds():
            for variable in world.getValuation().keys():
                variables.add(variable)

        variables = sorted(list(variables))
        print(variables)

        pt = ProbaTable(variables + [getPrimedVariable(var) for var in variables])

        for arc in epem.getArcs():
            if arc[2] == agent:
                pt.setProba(Valuation({**arc[0].getValuation(), **toPrime(arc[1].getValuation())}),
                            epem.getProba(arc[0], arc[1], agent))

        return pt
    """

    @staticmethod
    def init_with_real_function(sldd): #SLDDRealFunction

        vars = sldd.getVariables()
        vars = list(vars)
        vars.sort()
        pt = ProbaTable(vars)

        for valuation in [list(i) for i in itertools.product([True, False], repeat=len(vars))]:
            val = {x: valuation[i] for i, x in enumerate(vars)}
            pt.setValue(val, sldd.getValue(val))

        return pt

    def getVariables(self):
        return self.variables

    def __valuationToCondition(self, valuation:Valuation):
        return tuple(valuation[atom] if atom in valuation.keys() else False for atom in self.variables)

    def __conditionToValuation(self, conditions_set)-> Valuation:
        return Valuation({self.variables[vari]: conditions_set[vari] for vari in range(0, len(self.variables))})

    #def getLines(self, conditions_set, proba) -> Valuation:
    #    #print("getLineResult", conditions_set, proba, conditions_set[0], [vari for vari in range(0, len(self.variables))] )
    #    return self.conditionToValuation(conditions_set), proba

    """
    Return iterator on (real_world, float)
    """
    def browse(self) -> iter:
        for i, conditions_set in enumerate(self.variables_conditions):
            yield self.__conditionToValuation(conditions_set), self.__getResult(conditions_set)

    def setValue(self, valuation:Valuation, value:float) -> None:
        assert isinstance(valuation, Valuation) or isinstance(valuation, dict)
        line = self.__valuationToCondition(valuation)
        lineid = self.variables_conditions_places[line]
        self.results[lineid] = value

    def getValue(self, valuation:Valuation) -> float:
        assert isinstance(valuation, Valuation) or isinstance(valuation, dict), f"here, it's {type(valuation).__name__}"
        line = self.__valuationToCondition(valuation)
        lineid = self.variables_conditions_places[line]
        return self.results[lineid]

    def __getResult(self, conditions_set):
        return self.results[self.variables_conditions_places[conditions_set]]

    def isBoolean(self):
        for proba in self.results:
            if proba not in [1.0, 0.0]:
                return False
        return True

    def isFalse(self):
        return sum([r for r in self.results]) == 0.0

    def isTrue(self):
        for i, conditions_set in enumerate(self.variables_conditions):
            if self.results[i] != 1.0:
                return False
        return True

    def print(self, notZero=True, limit=10, trueAtoms=False, sort=False, string=False) -> None:
        # print(self.getVariables())
        t = PrettyTable(self.variables + ["X"])
        countLines = 0
        for i, conditions_set in enumerate(self.variables_conditions):
            if notZero and self.results[i]:
                t.add_row(conditions_set + (self.results[i],))
                countLines += 1
            if not notZero:
                t.add_row(conditions_set + (self.results[i],))
                countLines += 1
            if i>limit:
                break
        res = str(t) + '\n|{}/{} lines.\n+-------------'.format(countLines, i+1)
        if string:
            return res
        print(res)

    def marginalization(self, var_list: [str], fonction=float.__add__, cache=None):

        def list_to_bin(liste, op):
            assert len(liste)>=2
            res = op(liste[0], liste[1])
            for i in range(0, len(liste)-2):
                res = op(res, liste[i+2])
            return res
        new_variables = [var for var in self.variables if var not in var_list]
        pt = ProbaTable(new_variables)

        #for var_to_delete in var_list:
        #    if var_to_delete not in self.variables:
        #        print("/!\ During Marginalization, {} variable is unknowned in ProbaTable.".format(var_to_delete))

        index_to_delete = [self.variables_places[var] for var in var_list if var in self.getVariables()]
        new_table = {} # conditions_set : proba
        for i, conditions_set in enumerate(self.variables_conditions):
            new_conditions_set = multiDelete(conditions_set, index_to_delete)
            #print(conditions_set, "->", new_conditions_set)
            key = myhash(new_conditions_set)
            if key not in new_table.keys():
                new_table[key] = []
            new_table[key].append(self.results[i])
            #print("++", i, conditions_set, new_table[key])

        for hash_conditions_set, proba in new_table.items():
            #print("->", hash_conditions_set, proba)
            pt.setValue(
                hashToValuation(
                    hash_conditions_set,
                    new_variables),
                list_to_bin(proba, fonction)
            )
            #print("+", hashToValuation(hash_conditions_set, new_variables), proba, fonction(proba))

        return pt

    def rename(self, dico:{str:str}) -> None:

        new = self.copy()
        new_variables = []
        for var in new.variables:
            #if var not in dico.keys():
            #    raise Exception("Renaming is not intended to support renaming with common variables.")
            new_variables.append(dico[var] if var in dico.keys() else var)

        new.variables_places = {new_variables[i]: i for i in range(0, len(new.variables))}
        new.variables = new_variables

        return new

    """
    Take an other ProbaTable or a Formula or a proba for filtering.

    pt.apply("and|or", ProbaTable[Bool]|Formula);
    pt.apply("not", ProbaTable[Bool]|Formula);
    pt.apply([">", "<", "<=", ">=", "=="], float);
    pt.apply("x"|"times", ProbaTable|float);
    """
    def apply(self, op, other=None, zeroincluded=True, cache=None):
        #print("APPLY", op, other, op in ProbaTableManager.OPTABLE)
        if op in ProbaTableManager.OPTABLE:
            assert other is not None
            return self.__combine(other, op=ProbaTableManager.OPTABLE[op])

        if op in ProbaTableManager.UNARYOPTABLE:
            assert other is None, f" op = {op} and other is {type(other).__name__}"
            return self.__filtre(op=ProbaTableManager.UNARYOPTABLE[op])

        raise Exception("Error in operator or other : op={}, self.isBool={}, other_type={}, other.isBool={}.".format(op, self.isBoolean(), type(other).__name__, other.isBoolean()))

    def __filtre(self, op, zeroincluded=True):
        #print("HERE")
        npt = self.copy()
        for i, conditions_set in enumerate(npt.variables_conditions):
            #print("?", op(npt.__getResult(conditions_set)))
            npt.setValue(npt.__conditionToValuation(conditions_set), op(npt.__getResult(conditions_set)))
        return npt

    def __filter_formula(self, formula):
        new = self.copy()
        if isinstance(formula, bool):
            if formula:
                return new
            else:
                return None

        if formula.isPropositionnal():
            for i, conditions_set in enumerate(new.variables_conditions):
                valuation = new.__conditionToValuation(conditions_set)
                res = formula.semantic(None, valuation)
                if res and new.results[i] > 0.0:
                    new.setValue(valuation, new.results[i])
                else:
                    new.setValue(valuation, 0.0)
            return new

        if isinstance(formula, Not):
            return self.apply("not", new.__filter_formula(formula.inner))

        raise Exception("Error on filter_formula. {} not supported".format(type(formula).__name__))

    def copy(self):
        return copy.deepcopy(self)

    def booleanization(self):
        return self.apply(">", 0.0)

    @staticmethod
    def exclusiveOrOfFormulasWithValues(liste: List[Tuple[Formula, float]], vars: [str] = None, force=False):
        assert vars is not None, "For ProbaTable, we need list of variables!"
        pt = ProbaTable(vars)
        for formula, value in liste:
            pt.append(formula, float(value), force=force)
        return pt

    def append(self, formula:Formula, k:float, force=False):
        for var in list(set(formula.getVariables())):
            if var not in self.getVariables():
                raise Exception("Variable {} of formula {} not in ProbaTable.".format(var, formula))

        for i, conditions_set in enumerate(self.variables_conditions):
            valuation = self.__conditionToValuation(conditions_set)
            res = formula.semantic(None, valuation)
            #print(i, valuation, formula, res, len(valuation),len(conditions_set))
            if res:
                if not force and self.results[i] != 0.0:
                    raise Exception("Value {} already assigned in the probability table (here, {}) . Formula={} on world={}. All formulas must be inconsistent with each other when you use 'append().".format(k, self.results[i], formula, valuation, k))
                #self.setProba(valuation, k)
                self.results[i] = k

    @staticmethod
    def fromCSV(csvfile, force=False):

        datas = openCSV(csvfile)

        show = False
        proba_table = None

        for ligne, data in enumerate(datas):

            if data[0] in ["variables", "vars"]:
                variables = eval(datas[0][1])

            if data[0] == "formula":
                print("Initial formula : {}.".format(datas[1][1]))
                try:
                    proba_table = ProbaTable(variables, formula=eval(data[1]))
                except SyntaxError:
                    raise Exception("Error in the initial formula : {}.".format(datas[1][1]))
            elif proba_table is None:
                proba_table = ProbaTable(variables)

            if not proba_table is None and not show:
                print(">=====\n> Load description of Proba_table in {}.".format(csvfile))
                print("> Variables : {}.".format(variables))
                show = True

            if data[0] == "append":
                print("> Append float {} for formula {}.".format(eval(data[2]), eval(data[1])))
                proba_table.append(eval(data[1]), eval(data[2]), force=force)
            elif data[0] == "set":
                print("> Set float {} for real_world {}.".format(eval(data[2]), eval(data[1])))
                if not isinstance(eval(data[1]), dict):
                    raise Exception("For set a probability, you need to use a dict as Valuation.")
                proba_table.setValue(Valuation(eval(data[1])), eval(data[2]))
            elif data[0] == "show":
                print(proba_table.toString())
            else:
                #instr = data[1]
                exec(data[0])
        print("<=====")
        return proba_table

    def __eq__(self, other):
        """Overrides the default implementation"""
        if isinstance(other, ProbaTable):
            if not set(self.getVariables()) == set(other.getVariables()):
                return False

        for i, conditions_set in enumerate(self.variables_conditions):
            val = self.__conditionToValuation(conditions_set)
            result1 = self.__getResult(self.__valuationToCondition(val))
            result2 = other.__getResult(other.__valuationToCondition(val))
            if result1 != result2:
                print(i, val)
                print(self.getVariables(), other.getVariables())
                print(conditions_set, other.__valuationToCondition(val))
                print(result1, result2)
                return False

        return True

    def support(self):
        for i, conditions_set in enumerate(self.variables_conditions):
            if self.results[i] > 0.0:
                yield self.__conditionToValuation(conditions_set), self.results[i]

    def conditionning(self, assignment: dict):  #-> RealFunction
        vars = [var for var in self.getVariables() if var not in assignment.keys()]
        new = ProbaTable(vars)

        conditionning_val = Valuation(assignment)
        for i, conditions_set in enumerate(self.variables_conditions):
            valuation = self.__conditionToValuation(conditions_set)
            value = self.getValue(valuation)
            if not conditionning_val.hasContradiction(valuation):
                new.setValue(Valuation({var: valuation[var] for var in new.getVariables()}), value)

        return new

    def compose(self, dico: Dict[str, PseudoBooleanFunction]) -> PseudoBooleanFunction:
        raise NotImplementedError

    def __combine(self, other, op):


        assert callable(op), f"here op need to be callable, not {type(other).__name__}"
        new = self.copy()

        if isinstance(other, float):
            for i, conditions_set in enumerate(new.variables_conditions):
                new.setValue(new.__conditionToValuation(conditions_set), float(op(new.results[i], other)))
            return new

        left, right = self.copy(), other.copy()
        # simple case : optimization
        if left.getVariables() == right.getVariables():
            for i, conditions_set in enumerate(new.variables_conditions):
                new.setValue(new.__conditionToValuation(conditions_set), op(float(new.results[i]), float(other.results[i])))
            return new

        # else :
        new = ProbaTable(self.getVariables() + [var for var in other.getVariables() if var not in self.getVariables()])
        countLine = 0
        for i, self_conditions_set in enumerate(self.variables_conditions):
            for j, other_conditions_set in enumerate(other.variables_conditions):
                self_valuation = self.__conditionToValuation(self_conditions_set)
                other_valuation = other.__conditionToValuation(other_conditions_set)
                #print(self_valuation,other_valuation)
                if not self_valuation.hasContradiction(other_valuation):
                    combine_valuation = self_valuation.andOther(other_valuation)
                    new.setValue(combine_valuation, op(float(self.results[i]), float(other.results[j])))
                    countLine += 1

        return new

    def cut(self, op: str, threshold: float, rejection_value):  # -> RealFunction

        assert op in [">", ">=", "<", "<=", "=="], f"Unknown operator : {op}"
        realop = ProbaTableManager.OPTABLE[op]
        copy = self.copy()
        return copy.__filtre(lambda x: x if realop(x, threshold) else rejection_value)

    def bool_cut(self, op: str, threshold: float):  # -> RealFunction

        assert op in [">", ">=", "<", "<=", "=="], f"Unknown operator : {op}"
        realop = ProbaTableManager.OPTABLE[op]
        copy = self.copy()
        return copy.__filtre(lambda x: realop(x, threshold))

    def save(self, name):
        save_object(name)

    def UForget(self, liste):
        return self.marginalization(liste, PseudoBooleanFunctionManager.OPTABLE["and"])

    def EForget(self, liste):
        return self.marginalization(liste, PseudoBooleanFunctionManager.OPTABLE["or"])

    def extendScope(self, vars: List[str]) -> None:

        for v in vars:
            assert v not in self.getVariables(), f"Error in scope : '{v}' is already in scope."

        zero = ProbaTable(variables=vars, formula=Bot())
        newObj = self.apply("+", zero)
        self.__dict__.update(newObj.__dict__) # In place modification


def multiDelete(liste, list_index)->list:
    return [e for i, e in enumerate(liste) if i not in list_index]

def myhash(liste):
    return "".join([str(el) for el in liste])

def hashToValuation(hash:str, liste_var:[str]):
    return Valuation({liste_var[i]:bool(int(val)) for i, val in enumerate(hash)})

def toPrime(valuation:Valuation):
    return {getPrimedVariable(atom):val for atom, val in valuation.items()}


def andOtherFalse(myvars, vars):

    dic = {}
    for var in vars:
        if var not in myvars:
            dic[var] = False

    return AndOfList(dic)


def arcValuationToString(valuation:Valuation):
    name1 = "'" + "".join([var for var, value in valuation.items() if not getPrimeSymbol() in var and value]) + "'"
    name2 = "'" + "".join([var.replace(getPrimeSymbol(), "") for var, value in valuation.items() if getPrimeSymbol() in var and value]) + "'"
    return name1, name2

"""
A mettre dans Symbolic proba epistemic model
"""

def renameInPrime(formula):
    variables = list(set(formula.getVariables()))
    return formula.rename({var: getPrimedVariable(var) for var in variables})


if __name__ == '__main__':
    # exampleExplicit()

    #orig_stdout = sys.stdout
    #f = open('out.txt', 'w')
    #sys.stdout = f

    #print(ProbaTable.fromCSV("load.csv").toString())

    initial_vars = ["p", "s"]

    before_variables, after_variables, primes_variables, all_variables = getMyListOfVariables(initial_vars)

    rename_in_prime = {var:getPrimedVariable(var) for var in all_variables if getPrimeSymbol() not in var}
    #print("Renaming in prime", rename_in_prime)

    rename_unafter = {var:var.replace(getAfterSymbol(), "") for var in all_variables if getAfterSymbol() in var}
    #print("Renaming + into neg(+)", rename_unafter)


    # TABLE SUR LES MONDES
    pt_Pr = ProbaTable.fromCSV("../../../data/Pr_hypocondriac.csv")
    print(pt_Pr.toString())

    # TABLE D'EVENEMENTS
    pt_PrE = ProbaTable.fromCSV("../../../data/PrE_hypocondriac.csv")
    print(pt_PrE.toString())

    # TABLE DE PRECONDITION
    # precondition sur AP, evenement sur before (AP-) et after (AP+)

    pt_pre = ProbaTable.fromCSV("../../../data/pre_hypocondriac.csv")
    print(pt_pre.toString())


    print("Pr x PrE")
    res = pt_Pr.apply("x", pt_PrE)
    print(res.toString())

    print("\npre x PrE x Pr")
    res = res.apply("x", pt_pre)
    print(res.toString())

    alpha = res.marginalization(initial_vars + [getPrimedVariable(var) for var in initial_vars])
    print("Marginalisation Sum on", before_variables)
    print(initial_vars + [getPrimedVariable(var) for var in initial_vars])
    print(alpha.toString())
    print("And rename", rename_unafter)
    alpha = alpha.rename(rename_unafter)
    print("Alpha =\n", alpha.toString())

    print(alpha.toString())

    print("Denominator")
    denominator = alpha.marginalization(primes_variables)
    print(denominator.toString())

    update = alpha.apply("/", denominator)
    print("Update")
    print(update.toString())



    #sys.stdout = orig_stdout
    #f.close()
