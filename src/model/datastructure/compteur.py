

import random

from src.model.datastructure.graph.symbolic_graph import BDDManager
from src.utils.singleton import Singleton


def get_counter_varname(id_counter, i):
    return "cnt_{}.{}b".format(id_counter, i)


class CompteurFactory(metaclass=Singleton):

    def __init__(self, manager=None):

        if manager is None:
            self.manager = BDDManager().get()
        else:
            self.manager = manager
        self.all_counter = {}

    def build(self, name, nbVars):
        string = "{0:b}".format(nbVars)
        all_vars = [get_counter_varname(name, i) for i in range(0, len(string))]
        self.manager.declare(*all_vars)
        cpt = Compteur(name, all_vars, nbVars, self.manager)
        self.all_counter[name] = cpt
        return cpt

    def getCounter(self, name):
        return self.all_counter[name]


class Compteur:

    def __init__(self, name, variables):
        self.name = name
        self.variables = variables
        self.variables = list(reversed(self.variables))
        self.pointeur = {k: False for k in self.variables}
        self.max = 2**len(variables)
        # print("New compteur", self.name, self.variables, self.max)

    def __repr__(self):
        return f"Counter '{self.name}' = {self.pointeur}"

    def getName(self):
        return self.name

    def getVars(self):
        return self.variables

    def read(self, dic, value=int):
        if dic == {}:
            return -1
        binary = "".join([str(int(dic[v])) for v in self.variables])
        if value == int:
            return self._bin_to_int(binary)
        return binary

    def getValue(self, value="vars"): # int, "vars", "binary"

        binary = "".join([str(int(self.pointeur[v])) for v in self.variables])
        if value == int:
            return self._bin_to_int(binary)
        elif value == "vars":
            return self.pointeur
        else:
            return binary

    def get(self, value):
        string = self._int_to_bin(value)
        return {v: bool(int(string[i])) for i, v in enumerate(self.variables)}

    def set(self, i):
        assign = self._int_to_bin(i)
        assign = {v: bool(int(assign[i])) for i, v in enumerate(self.variables)}
        self.pointeur = assign

    def incr(self):
        tab = self.variables
        assign = self.get(value="vars")
        news = dict(assign)
        for var in reversed(tab):
            if assign[var]:
                news[var] = False
            else:
                news[var] = True
                break
        self.pointeur = news

    def decr(self):

        tab = self.variables
        assign = self.get(value="vars")
        news = dict(assign)
        for i, var in enumerate(reversed(tab)):
            if assign[var]:
                news[var] = False
                for var2 in tab[len(tab)-i:]:
                    news[var2] = not news[var2]
                break
        self.pointeur = news


    def __gt__(self, i):
        assert isinstance(i, int)
        return i < self.get(value=int)

    def __lt__(self, i):
        assert isinstance(i, int)
        return i > self.get(value=int)

    def __eq__(self, i):
        assert isinstance(i, int)
        return i == self.get(value=int)

    def _int_to_bin(self, i):
        if i > self.max:
            raise ValueError("Le compteur est prevu pour gerer des chiffres < {}".format(self.max))
        s = "{0:b}".format(i)
        t = '{0:0>{width}}'.format(s, width=len(self.variables))
        return t

    def _bin_to_int(self, string):
        return int(string, 2)


def test(cpt):
    piece = random.randint(1, 2)
    if piece == 1:
        cpt.incr()
        print("Incr", cpt.get())
    if piece == 2:
        cpt.decr()
        print("Decr", cpt.get())


if __name__ == '__main__':

    compteur1 = Compteur("cpt", ["jb1", "jb2", "jb3", "jb4"],)
    print(compteur1)

    compteur1.set(5)

    print(compteur1.get(value="binary"))

    print(compteur1 > 4)
    print(compteur1 > 10)

    print(compteur1 < 8)
    print(compteur1 < 5)

    print(compteur1 == 5)

