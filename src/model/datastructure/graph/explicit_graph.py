
from src.model.datastructure.graph.graph_interface import GraphInterface
from src.model.epistemiclogic.epistemicmodel.world import World
from src.model.epistemiclogic.eventmodel.event import Event


class Graph(GraphInterface):
    """
    Graph with successors lists
    """

    def __init__(self):
        self.neighbors = []
        self.name2node = {}
        self.node2name = []
        self.labels = {}

    def __repr__(self):
        string = f">> Nodes : {len(self.getNodes())}\n"
        string += ", ".join([str(node) for node in self.getNodes()])
        string += f"\n>> Arcs : {len(self.getArcs())}\n"
        succs = {}
        nb_arcs = {}
        for a1, a2, agent in self.getArcs():
            if agent not in succs:
                succs[agent] = {}
                nb_arcs[agent] = 0
            if a1 not in succs[agent]:
                succs[agent][a1] = []
            succs[agent][a1].append(a2.getName())
            nb_arcs[agent] += 1

        for agent in succs:
            string += f"> Agent {agent} ({nb_arcs[agent]}) :\n"
            for node1 in succs[agent]:
                string += f" - {node1.getName()} -> {succs[agent][node1]}\n"
        return string

    def __len__(self):
        return len(self.node2name)

    def __getitem__(self, v):
        return self.neighbors[v]

    def getId(self, name):
        return self.name2node[name]

    def getNodes(self):
        # print("Nodes", self.name2node.keys())
        return list((self.name2node.keys()))

    def addNode(self, name:World):
        assert name not in self.name2node, "{} already in graph.".format(name.name)
        # print("Add", name)
        self.name2node[name] = len(self.name2node)
        self.node2name.append(name)
        self.neighbors.append([])
        return self.name2node[name]

    def getSuccessors(self, name:World, label=None):

        if isinstance(name, str):
            for node in self.getNodes():
                if name == node.name:
                    name = node
                    break

        assert isinstance(name, World) or isinstance(name, Event)

        if label == None:
            return [self.node2name[i] for i in self.__getitem__(self.name2node[name])]
        succ = self.getSuccessors(name)

        return [s for s in succ if label in self.getLabel(name, s)]

    def addEdge(self, name_u:World, name_v:World, labels_uv=None):
        self.addArc(name_u, name_v, labels_uv)
        self.addArc(name_v, name_u, labels_uv)

    def addArc(self, name_u, name_v, labels_uv=None):
        assert name_u in self.name2node.keys(), "Node {} doesnt exist. Did you add it?".format(name_u)
        assert name_v in self.name2node.keys(), "Node {} doesnt exist. Did you add it?".format(name_v)
        u = self.name2node[name_u]
        v = self.name2node[name_v]
        if v not in self.neighbors[u]:
            self.neighbors[u].append(v)
        # else: print("Warning : node {} already in node {} neighbor's.".format(u, v))

        if (u, v) not in self.labels.keys():
            self.labels[(u,v)] = []
        if labels_uv not in self.labels[(u, v)]:
            self.labels[(u, v)].append(labels_uv)

    def getLabel(self, name1, name2):
        return self.labels[(self.getId(name1), self.getId(name2))]

    def removeNode(self):
        return NotImplemented

    def getArcs(self):
        arcs = []
        for node in self.name2node.keys():
            for succ in self.getSuccessors(node):
                for label in self.getLabel(node, succ):
                        arcs.append((node, succ,  label))
        return arcs

    def hasArc(self, name_u, name_v, labels_uv=None):
        # print("has arc :", name_u, "->", name_v, "l:", labels_uv, "==", self.getSuccessors(name_u))
        if labels_uv == None:
            for succ in self.getSuccessors(name_u):
                if succ == name_v:
                    return True
            return False

        for succ in self.getSuccessors(name_u):
            if succ == name_v and labels_uv in self.getLabel(name_u, name_v):
                return True
        return False

    def hasNode(self, name: World):
        return name in self.getNodes()

    def print(self):
        print(" -",  type(self).__name__)
        print("name2node", self.name2node)
        print("node2name", self.node2name)
        print("neighbors", self.neighbors)
        print("labels", self.labels)

    def toDot(self):

        list_arcs = ["{} -> {}{}; ".format('"{}.{}"'.format(node.name, self.name2node[node]) if isinstance(node, World) else node,
                                           '"{}.{}"'.format(successor.name, self.name2node[successor]) if isinstance(successor, World) else successor,
                                            "" if self.labels[self.name2node[node]][self.name2node[successor]] == None else "[label={}]".format(self.labels[self.name2node[node]][self.name2node[successor]]))
                     for node in self.name2node for successor in self.getSuccessors(node)]
        digraph = """
        digraph G {{
            edge[dir = forward]
            node[shape = plaintext]
            {0}
        }}
        """.format(
            "\n\t\t".join(
                list_arcs))

        return digraph
