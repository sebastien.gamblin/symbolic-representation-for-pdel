

from abc import ABCMeta, abstractmethod

class GraphInterface(metaclass=ABCMeta):
    # ('getSucessors', 'getNodes', 'removeNode', 'addNode', 'addEdge', 'addArc')

    @abstractmethod
    def __init__(self):
        pass

    @abstractmethod
    def getSuccessors(self, name):
        pass

    @abstractmethod
    def removeNode(self, name):
        pass

    @abstractmethod
    def addNode(self, name):
        pass

    @abstractmethod
    def getNodes(self):
        pass

    @abstractmethod
    def addEdge(self, name, name2):
        pass

    @abstractmethod
    def addArc(self, name, name2):
        pass

    @abstractmethod
    def getArcs(self):
        pass

    @abstractmethod
    def hasArc(self, name, name2, label=None):
        pass

    @abstractmethod
    def toDot(self):
        pass