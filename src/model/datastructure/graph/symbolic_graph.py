
from abc import ABCMeta, abstractmethod

from src.model.datastructure.graph.graph_interface import GraphInterface
from src.model.epistemiclogic.epistemicmodel.world import ValuationInterface
from src.model.epistemiclogic.formula.formula import *
from src.utils.singleton import Singleton

class BDDManager(metaclass=Singleton):

    def __init__(self, type):

        print(f"BDD package {BDD_TYPE} imported.")
        if type == "cudd":
            from dd import cudd as _bdd
            self.manager = _bdd.BDD()
        elif type == "autoref":
            from dd import autoref as _bdd
            self.manager = _bdd.BDD()



        # self.manager.configure(reordering=False)

    def get(self):
        return self.manager

    def reset(self, type):
        # print(f"BDD package {BDD_TYPE} imported.")
        if type == "cudd":
            from dd import cudd as _bdd
            self.manager = _bdd.BDD()
        elif type == "autoref":
            from dd import autoref as _bdd
            self.manager = _bdd.BDD()

    @staticmethod
    def create(type="autoref", **kwargs):
        return BDDManager(type)

    def from_formula(self, formula, vars=None, cache=None):
        if cache is None:
            cache = {}
        return formulaToBDD(formula, self.manager, cache=cache)

    def size(self):
        return 0

    def models(self, pointeur, limit=20, all=True):
        sols = self.manager.pick_iter(pointeur)
        for i, s in enumerate(sols):
            if i < limit:
                print(i, ", ".join(sorted(["'{}':{}".format(v, int(k)) for v, k in s.items() if all or k])), end="")
                print()
            else:
                break





class SymbolicRelation(metaclass=ABCMeta):

    @abstractmethod
    def __init__(self):
        pass

    @abstractmethod
    def toFormula(self)-> Formula:
        pass

    @abstractmethod
    def toBDD(self) -> int:
        pass


class Obs(SymbolicRelation):

    def __init__(self, varlist: list):
        self.liste = varlist

    def toFormula(self):
        formula = True
        for var in self.liste:
            formula = And(formula, Equiv(Atom(var), Atom(BDDGraph.getPrimed(var))))
        return formula

    def toBDD(self):
        mg = BDDManager().get()
        pointeur = mg.true
        for var in self.liste:
            if isinstance(var, str):
                pointeur = mg.apply("and", pointeur, mg.apply('<->', mg.var(var), mg.var(BDDGraph.getPrimed(var))))
            elif isinstance(var, Formula):
                pointeur = mg.apply("and", pointeur, mg.apply('<->', formulaToBDD(var, BDDManager().get()), formulaToBDD(FormulaToPrime(var), BDDManager().get())))
            else:
                raise Exception("{} not known to convert in BDD.".format(type(var).__name__))
        return pointeur

    def __repr__(self):
        return "Obs({})".format(self.liste)


class BDDGraph(GraphInterface):
    """
    Graph with successors lists
    """

    def __init__(self, variables: list, symbrel: SymbolicRelation, rules: Formula):

        self.manager = BDDManager().get()
        self.variables = variables
        self.primes = [self.getPrimed(var) for var in self.variables]
        self.rename = {var: self.getPrimed(var) for var in self.variables}

        self.manager.declare(*self.variables)
        self.manager.declare(*self.primes)

        self.pointeur = symbrel

        if rules != None:
            rules_p =  (rules)
            self.pointeur = self.manager.apply("and", self.pointeur, formulaToBDD(rules, self.manager))
            self.pointeur = self.manager.apply("and", self.pointeur, formulaToBDD(rules_p, self.manager))

        # self.show(self.pointeur, title="Constructor")


    def getPointed(self):
        return self.pointeur

    def setPointed(self, new):
        self.pointeur = new

    def getSuccessors(self, bdd):
        if isinstance(bdd, ValuationInterface):
            bdd = self.manager.cube(bdd)
        assert str(type(bdd)) in ["<class 'dd.cudd.Function'>", "<class 'dd.autoref.Function'>"]

        pointeur = self.manager.apply("and", bdd, self.pointeur)
        pointeur = self.manager.exist(self.variables, pointeur)
        pointeur = self.manager.let(self.rename, pointeur)
        return pointeur

    def removeNode(self, name):
        return NotImplemented

    def addNode(self, name):
        return NotImplemented

    def getNodes(self):
        p = self.manager.exist(self.primes, self.pointeur)
        return p

    def addEdge(self, name, name2):
        return NotImplemented

    def addArc(self, name, name2):
        return NotImplemented

    def getArcs(self):
        return NotImplemented

    def hasArc(self, name, name2, label=None):
        return NotImplemented

    def toDot(self):
        return NotImplemented

    @staticmethod
    def getPrimed(variable: str):
        return "{}{}".format(BDDGraph.getPrimedSymbol(), variable)

    @staticmethod
    def getPrimedSymbol():
        return "p_"

    def show(self, pointeur, limit=20, all=False, title=None, counters=None):
        sols = self.manager.pick_iter(pointeur)
        if title != None:
            print(title)
        for i, s in enumerate(sols):
            if i < limit:
                print(i, ", ".join(sorted(["'{}':{}".format(v, int(k)) for v, k in s.items() if all or k])), end="")
                if counters !=None:
                    for counter in counters:
                        for var in counter.getVars():
                            if var in s.keys():
                                print(" - Counter :", counter.name, counter.read(s))
                                break
                print()
            else:
                break

    def conditionning(self, valuation):
        return self.manager.let(valuation, self.pointeur)


def formulaToBDD(formula: Formula, manager, cache=None)-> int:

    if cache is None:
        cache = {}

    key = "bdd_" + str(hash(formula))
    if formula in cache:
        return cache[key]

    if isinstance(formula, Atom):
        if isinstance(formula.inner, list):
            for a in formula.inner:
                manager.declare(a)
            return manager.cube(formula.inner)

        else:
            manager.declare(formula.inner)
        res = manager.var(formula.inner)

    elif isinstance(formula, Top):
        res = manager.true

    elif isinstance(formula, Bot):
        res = manager.false

    elif isinstance(formula, bool):
        res = manager.true if formula else manager.false

    elif isinstance(formula, And):
        res = manager.apply("and", formulaToBDD(formula.left, manager, cache), formulaToBDD(formula.right, manager, cache))

    elif isinstance(formula, Or):
        res = manager.apply("or", formulaToBDD(formula.left, manager, cache), formulaToBDD(formula.right, manager, cache))

    elif isinstance(formula, Not):
        res = manager.apply("not", formulaToBDD(formula.inner, manager, cache))

    elif isinstance(formula, Implies):
        res = manager.apply("->", formulaToBDD(formula.left, manager, cache), formulaToBDD(formula.right, manager, cache))

    elif isinstance(formula, Equiv):
        res = manager.apply("<->", formulaToBDD(formula.left, manager, cache), formulaToBDD(formula.right, manager, cache))

    elif isinstance(formula, ite):
        res = Or(
            And(formulaToBDD(formula.x, manager, cache), formulaToBDD(formula.left, manager, cache)),
            And(formulaToBDD(Not(formula.x), manager), formulaToBDD(formula.right, manager, cache))
        )
    elif isinstance(formula, Exactly):
        res = parmi_bdd(formula.n, formula.array)
    else:
        raise Exception("Traduction Error. Instanceof {} not found ? Find {}".format(formula, formula.__class__.__name__))

    cache[key] = res
    return res


def parmi_bdd(n, xvars, manager=None, dic=None):
    if manager is None:
        manager = BDDManager().get()
    manager.declare(*xvars)
    if dic is None:
        dic = {}
    return _parmi_bdd(int(n), xvars, dic, manager)


def _parmi_bdd(n, xvars, dic, manager):

    k = hash((n, frozenset(xvars)))
    if k in dic.keys():
        return dic[k]

    if n==0:
        bdd = manager.true
        for x in xvars:
            bdd = manager.apply("and", manager.apply("not", manager.var(x)), bdd)
        dic[k] = bdd
        return bdd

    if len(xvars) == 0:
        return manager.false

    Y = xvars[:]
    x = Y[0]
    Y.remove(x)

    res = getIte(x, _parmi_bdd(n-1, Y, dic, manager), _parmi_bdd(n, Y, dic, manager), manager)
    dic[k] = res
    return res


def getIte(x, left, right, manager):
    return manager.apply("or",
                         manager.apply("and", manager.var(x), left),
                         manager.apply("and", manager.apply("not", manager.var(x)), right))


def FormulaToPrime(formula: Formula):
    if isinstance(formula, Atom):
        return Atom(BDDGraph.getPrimed(formula.inner))
    elif isinstance(formula, bool):
        return formula

    elif isinstance(formula, And):
        return And(FormulaToPrime(formula.left), FormulaToPrime(formula.right))

    elif isinstance(formula, Or):
        return Or(FormulaToPrime(formula.left), FormulaToPrime(formula.right))

    elif isinstance(formula, Not):
        return Not(FormulaToPrime(formula.inner))

    elif isinstance(formula, Implies):
        return Implies(FormulaToPrime(formula.left), FormulaToPrime(formula.right))

    elif isinstance(formula, ite):
        return ite(FormulaToPrime(formula.x), FormulaToPrime(formula.left), FormulaToPrime(formula.right))
    elif isinstance(formula, Exactly):
        return Exactly(formula.n, [BDDGraph.getPrimed(var) for var in formula.array])
    else:
        raise Exception("toPrime Error. Instanceof {} not found ? Find {}".format(formula, formula.__class__.__name__))


def BDDToPrime(bdd, rename: dict, manager):
    return manager.let(rename, bdd)