from prettytable import PrettyTable
from typing import Iterable
import itertools
import math
from typing import Dict, List

from src.model.epistemiclogic.epistemicmodel.world import Valuation
from src.model.epistemiclogic.formula.formula import *
from src.io.write import *

def almostEqual(a, b, rel_tol=1e-09, abs_tol=0.0):
    return abs(a - b) <= max(rel_tol * max(abs(a), abs(b)), abs_tol)


def RepresentsInt(s):
    try:
        int(s)
        return True
    except ValueError:
        return False


def jsonKeys2int(x):
    if isinstance(x, dict):
        return {int(k) if RepresentsInt(k) else k: v for k, v in x.items()}
    return x


class PseudoBooleanFunction:
    pass

class PseudoBooleanFunctionManager(metaclass=ABCMeta):

    OPTABLE = {">=": float.__ge__, "<=": float.__le__, "<": float.__lt__,
               ">": float.__gt__, "==": float.__eq__,
               "+": float.__add__, "-": float.__sub__,
               "x": float.__mul__, "/": float.__truediv__,
               "/norm": lambda x, y: 0.0 if y == 0.0 else x/y,
               "or": lambda x, y: float(bool(x) or bool(y)),
               "and": lambda x, y: float(bool(x) and bool(y)),
               "->": lambda x, y: float(not(bool(x)) or bool(y)),
               "xor": lambda x, y: float(bool(x) != bool(y)),
               "==": float.__eq__,
               "xnorm1": lambda beta, denom: beta * denom if denom != 0.0 else (math.inf if beta > 0.0 else -math.inf),
               "xnorm2": lambda beta, denom: beta * denom if denom != 0.0 else (math.inf if beta >= 0. else -math.inf)
        }

    UNARYOPTABLE = {"not": lambda x: float(bool(not bool(x))),
                    "neg": lambda x: -x,
                    "abs": lambda x: float(abs(float(x)))
                    }

    @abstractmethod
    def __init__(self):
        pass

    @staticmethod
    @abstractmethod
    def create():
        pass

    @abstractmethod
    def from_formula(self, formula: Formula, vars: List[str]= None, cache=None):
        pass

    @abstractmethod
    def exclusiveOrOfFormulasWithValues(self, liste: List[Tuple[Formula, float]],
                                        vars: [str] = None):
        pass

    @abstractmethod
    def getConstant(self, n: float):
        pass

    @abstractmethod
    def variables(self) -> [str]:
        pass

    @abstractmethod
    def reorder(self, randomizer=False) -> None:
        pass

    @abstractmethod
    def toDot(self, adds=None, style=False, others=True) -> str:
        pass

    @abstractmethod
    def size(self, realfunctions: [PseudoBooleanFunction]) -> int:
        pass

    @staticmethod
    def compare(*real_functions: PseudoBooleanFunction, show=False, notZero=False, order=None, latex=False, string=False):
        """
        Pretty hard comparaison of PseudoBooleanFunction (SLDDPseudoBooleanFunction ou ProbaTable, btw)
        :param real_functions: list of PseudoBooleanFunction, as *args
        :param show: print, or not, a table
        :return: bool : if all values are equals
        """

        def my_equal(value, liste):
            for v in liste:
                if not almostEqual(v, value):
                    return False
            return True

        def all_equals(liste):
            for v in liste:
                if not my_equal(v, liste):
                    return False
            return True

        global_res = True

        vars = set()
        for real_function in real_functions:
            for var in real_function.getVariables():
                vars.add(var)

        if order is None:
            vars = sorted(list(vars))
        else:
            tmp1 = list(vars).copy()
            tmp2 = order.copy()
            tmp1.sort()
            tmp2.sort()
            assert len(tmp2) >= len(tmp1), "To put an order, please use all variables in PseudoBooleanFunction."
            vars = [v for v in order if v in vars]

        if not string:
            t = PrettyTable(
                [pprint(f"{v}", Color.CBOLD) for v in vars] +
                [pprint(f"{i}-{type(real_functions[i]).__name__}".replace("PseudoBooleanFunction", ".PBF"), Color.CBOLD)
                    for i in range(0, len(real_functions))]
            )
        else:
            t = PrettyTable(
                vars +
                [f"{i}-{type(real_functions[i]).__name__}".replace("PseudoBooleanFunction", ".PBF")
                    for i in range(0, len(real_functions))]
            )

        count = 0
        for valuation in [list(i) for i in itertools.product([False, True], repeat=len(vars))]:
            val = {x: valuation[i] for i, x in enumerate(vars)}
            res = [real_function.getValue(Valuation(val)) for real_function in real_functions]
            all_equal = all_equals(res)
            color = Color.CGREEN if all_equal else Color.CRED

            if not all_equals(res):
                global_res = False

            if not notZero:
                t.add_row(
                    [int(v) for k, v in val.items()] + ([pprint(f"{r}", color) for r in res] if not string else res)
                )
                count += 1
            else:
                if sum(res) != 0.0:
                    t.add_row(
                        [int(v) for k, v in val.items()] + ([pprint(f"{r}", color) for r in res] if not string else res)
                    )
                    count += 1

        if show:
            print(t)
            print("Number of lines :", count)

        if latex:
            print(t.get_string().replace("|\n", "\\\ \n").replace("|", "&").replace("\n&", "\n"))

        if string:
            return t.get_string()

        return global_res

    def getRandom(self, nb_total_vars, nb_mentionned_vars=None, values=int, fill_percent=None, mini=1, maxi=10,
                      seed=None, rng=None):

        def var_name(i):
            return f"x{i}"

        if rng is None:
            rng = random.Random(seed)

        if seed is None:
            seed = rng.randrange(100000000)

        rng.seed(seed)

        if fill_percent is None:
            fill_percent = rng.random()

        if nb_mentionned_vars is None:
            nb_mentionned_vars = int(rng.uniform(1, nb_total_vars))

        if False:
            print(f"Seed is {seed}, for {nb_total_vars} and {nb_mentionned_vars} mentionned_variables.\nFill_percent = {fill_percent}, with {values.__name__} values between {mini} and {maxi}")

        self.last_seed = seed
        self.last_fill_percent = fill_percent

        vars = [var_name(i) for i in range(0, nb_total_vars)]

        if nb_total_vars < nb_mentionned_vars: raise ValueError(
            f"Erreur nb_mentionned_vars={nb_mentionned_vars} must be <= as nb_total_vars={nb_total_vars}")
        mentionned_vars = rng.sample(vars, nb_mentionned_vars)

        real_function1 = self.from_formula(Top(), vars=vars)

        for i in range(0, 2 ** len(mentionned_vars)):
            if rng.random() < fill_percent:
                val = rng.uniform(mini, maxi) if values == float else int(rng.uniform(mini, maxi))
                real_function1.setValue({v: rng.getrandbits(1) for v in mentionned_vars}, val)

        return real_function1




class PseudoBooleanFunction(metaclass=ABCMeta):

    """ Like a BooleanFunction but with reals """

    # Maybe add normalisation() ?

    @abstractmethod
    def __init__(self, vars: [str], formula: Formula=None):
        pass

    """ Return list of varibles used in the PBF """
    @abstractmethod
    def getVariables(self) -> [str]:
        pass

    @abstractmethod
    def get_size(self) -> int:
        pass

    """ Return an iterator on (Valuation, float)"""
    @abstractmethod
    def browse(self) -> Iterable[Tuple[Valuation, float]]:
        pass

    @abstractmethod
    def setValue(self, valuation: Valuation, value: float) -> None:
        pass

    @abstractmethod
    def getValue(self, valuation: Valuation) -> float:
        pass

    """ The PBF admit only 0 and 1 as results ?"""
    @abstractmethod
    def isBoolean(self) -> bool:
        pass

    @abstractmethod
    def isFalse(self) -> bool:
        pass

    @abstractmethod
    def marginalization(self, vars: [str], fonction=sum):  # -> PseudoBooleanFunction
        pass

    @abstractmethod
    def rename(self, dico: {str: str}):  # -> PseudoBooleanFunction
        pass

    """
        Can NOT take and,or,not with other PseudoBooleanFunction|Formula
        Can NOT take [">", "<", "<=", ">=", "=="] with float
        Can take x,/ with PseudoBooleanFunction|float
        """
    @abstractmethod
    def apply(self, op: str, other, zeroincluded:bool=True): #-> PseudoBooleanFunction
        pass

    @abstractmethod
    def copy(self): #-> PseudoBooleanFunction
        pass

    @abstractmethod
    def booleanization(self): #-> PseudoBooleanFunction
        pass

    """ Return an iterator on all valuations which has none 0 value."""
    @abstractmethod
    def support(self) -> Iterable[Valuation]: # dict as valuation
        pass

    @abstractmethod
    def append(self, formula:Formula, k:float) -> None:
        pass

    @abstractmethod
    def conditionning(self, assignment: dict):  #-> PseudoBooleanFunction
        pass

    @abstractmethod
    def save(self, name):
        pass

    @abstractmethod
    def cut(self, op: str, threshold: float, rejection_value=0.0):  #-> PseudoBooleanFunction
        pass

    def marginalize_and_normalize(self, vars: [str], cache=None):
        """
        Permit to marginalize the ADDPseudoBooleanFunction by variables in parameters,
        and to normalise with this marginalisation as denominator.

        Example :

        ADD :                  MARG on b        ADD / MARG
        +---+---+----------+  +---+----------+  +---+---+----------+
        | a | b | 0-ADD.PBF |  | a | 0-ADD.PBF |  | a | b | 0-ADD.PBF |
        +---+---+----------+  +---+----------+  +---+---+----------+
        | 0 | 0 |   0.0    |  | 0 |   1.0    |  | 0 | 0 |   0.0    |
        | 0 | 1 |   1.0    |  | 1 |   3.0    |  | 0 | 1 |   1.0    |
        | 1 | 0 |   1.0    |  +---+----------+  | 1 | 0 |   1/3    |
        | 1 | 1 |   2.0    |                    | 1 | 1 |   2/3    |
        +---+---+----------+                    +---+---+----------+

        :param vars: List of variables to marginalize
        :param cache: dict as cache of memoïze
        :return: \frac{self}{self.marginalize(vars)}
        """
        return self.apply("/norm", self.marginalization(vars, cache=cache), cache=cache)

    def scopeEquals(self, vars):
        return set(self.getVariables()) == set(vars)

    @abstractmethod
    def compose(self, dico: Dict[str, PseudoBooleanFunction]) -> PseudoBooleanFunction:
        pass

    @abstractmethod
    def isTrue(self) -> bool:
        pass

    @abstractmethod
    def isFalse(self) -> bool:
        pass

    @abstractmethod
    def extendScope(self, vars: List[str]) -> None:
        pass

    def save(self, name, location=None):
        print("write", name, location, self)
        save_object(self, name, location=location)

    def print(self, notZero=True, limit=10, trueAtoms=False, sort=False, string=False) -> None:

        def print_dico(x):
            if trueAtoms:
                return ", ".join(sorted([str(k) for k, v in x.items() if v]))
            else:
                return ", ".join([f"{k}:{int(v)}" for k, v in x.items()])

        count = 0

        iterator = self.support() if notZero else self.browse()

        res = pprint(f">>{type(self).__name__}.print() :", Color.CRED, Color.CBOLD) + "\n"
        res += f"Vars={self.getVariables()}\n"
        if limit is None:
            for x, y in iterator:
                res += f"-> {print_dico(x)} = {y}\n"
                count += 1
        else:
            assert isinstance(limit, int)
            for i, (x, y) in enumerate(self.support()):
                if i < limit:
                    res += f"-> {print_dico(x)} = {y}" + "\n"
                    count += 1
                else:
                    res += Color.get(f">>limit={limit}.", Color.CBOLD) + "\n"
                    count += 1
                    break

        res += pprint(f"<<<{type(self).__name__}.print(). {count} lines.", Color.CRED, Color.CBOLD) + "\n"

        if not string:
            print(res)
        else:
            return res

    def __repr__(self) -> str:
        return self.print(string=True)

    @classmethod
    def dumps(cls, pbf: PseudoBooleanFunction, cache=None) -> None:
        """
        Transform DD into dict.
        :param pbf: pseudo boolean function as PseudoBooleanFunction
        :return: dict
        """

        # ATTENTION FONCTIONNE QUE POUR LES ADDPseudoBooleanFunction
        # POUR L'INSTANT

        import math
        from src.utils.memoize import memoize

        if cache is None:
            cache = {}

        dico = {
            "root": pbf.pointer._id,
            "type": type(pbf.manager).__name__,
            "order": pbf.manager.getOrder()
        }

        @memoize(cache=cache)
        def rec(node):

            ilow, ihigh = None, None

            if not node._low is None:
                rec(node._low)
                ilow = node._low._id
            if not node._high is None:
                rec(node._high)
                ihigh = node._high._id

            subdict = {
                "var": pbf.get_varname(node.index) if node.index is not math.inf else None,
                "index": node.index,
                "value": node.value,
                "low": ilow,
                "high": ihigh
            }

            dico[node._id] = subdict

        rec(pbf.pointer)

        return dico

    @classmethod
    def dump(cls, pbf: PseudoBooleanFunction, filename:str=None, cache=None, indent=None) -> str:
        """
        Transform DD into string, as json.
        :param pbf: pseudo boolean function as PseudoBooleanFunction
        :return: None
        """
        import json

        from src.io.write import getDefaultOutputPath

        with open(f"{getDefaultOutputPath()}/{filename}.json", "w") as outfile:
            json.dump(cls.dumps(pbf, cache=cache), outfile, indent=indent)

    @classmethod
    def loadd(cls, manager: PseudoBooleanFunctionManager, data: dict, cache=None) -> PseudoBooleanFunction:
        """
        Load a dict into PseudoBooleanFunction
        :param manager:
        :param data:
        :param cache:
        :return:
        """

        if cache is None:
            cache = {}

        assert data["type"] == type(manager).__name__, \
            f"Type of manager given as parameter need to be {data['type']}, not {type(manager).__name__}."

        from src.utils.memoize import memoize
        from .add.add_real_function import ADDPseudoBooleanFunction
        from src.model.datastructure.add.add import ADD

        data = jsonKeys2int(data)

        manager_order = manager.getOrder()
        proposed_order = data['order']

        if manager_order != []:
            # assert the order of new added ADD is good
            for var_i in range(0, len(proposed_order)-1):
                if proposed_order[var_i] in manager_order:
                    # TO CHECK
                    #print(f"{var_i} = {proposed_order[var_i]}:{manager_order.index(proposed_order[var_i])} < {proposed_order[var_i+1]}:{manager_order.index(proposed_order[var_i+1])}")
                    assert manager_order.index(proposed_order[var_i]) < manager_order.index(proposed_order[var_i+1])
                # ELSE :
                else:
                    raise AssertionError(f"Variable '{proposed_order[var_i]}' not declared!")

        variables = set()

        @memoize(cache=cache)
        def rec(id):
            #assert id in data.keys() or int(id) in data.keys()
            #id = int(id)
            if not data[id]['var'] is None:
                var = data[id]['var']
                variables.add(var)
                manager.add_var(var)
                return ADD.build_node(manager.get_id(var), rec(data[id]["low"]), rec(data[id]["high"]))
            else:
                return ADD.constant(data[id]["value"])

        new_pointer = rec(data["root"])
        return ADDPseudoBooleanFunction(manager, new_pointer, variables)


    @classmethod
    def loads(cls, manager: PseudoBooleanFunctionManager, jsonstring: str, cache=None) -> PseudoBooleanFunction:
        """
        Load a jsonstring into PseudoBooleanFunction
        :param manager:
        :param jsonstring:
        :param cache:
        :return:
        """

        import json

        data = json.loads(jsonstring, object_hook=jsonKeys2int)

        return cls.loadd(manager, data, cache=cache)



    @classmethod
    def load(cls, manager: PseudoBooleanFunctionManager, filename: str, cache=None) -> PseudoBooleanFunction:
        """
        Load a json file into a PseudoBooleanFunction
        :param manager:
        :param filename:
        :param cache:
        :return:
        """

        assert isinstance(filename, str), f"'filename' need to be a string, not a {type(str).__name__}"

        from src.io.write import getDefaultOutputPath

        return cls.loads(manager,
                                  open(f"{getDefaultOutputPath()}/{filename}.json").read(),
                                  cache=cache)

    def __add__(self, other):
        return self.apply("+", other)

    def __sub__(self, other):
        return self.apply("-", other)

    def __mul__(self, other):
        return self.apply("x", other)

    def __truediv__(self, other):
        return self.apply("x", other)

    def __neg__(self):
        return self.apply("neg", other=None)

    def __abs__(self):
        return self.apply("abs")

    def __or__(self, other):
        return self.apply("or", other)

    def __and__(self, other):
        return self.apply("and", other)

    def __xor__(self, other):
        return self.apply("xor", other)

    def __gt__(self, other):
        return self.apply(">", other)

    def __ge__(self, other):
        return self.apply(">=", other)

    def __le__(self, other):
        return self.apply("<=", other)

    def __lt__(self, other):
        return self.apply("<", other)
