from typing import Dict

# from .pyddlib.add import ADD # from pyddlib.add import ADD
from src.model.datastructure.add.add import ADD # from pyddlib.add import ADD

from src.model.datastructure.real_function import *
from src.utils.debug import *

from src.model.datastructure.observer_pattern import *
from src.utils.memoize import memoize
import math

def getCacheKey(fct, *args):
    return (fct, *[a for a in args])


def add_bool(x, y):
    return int((x + y) > 0)



class ADDPseudoBooleanFunction:
    pass

#@for_all_methods(debug_me)
class ADDManager(PseudoBooleanFunctionManager, Subject):

    """
    allows you to create temporary variables automatically by assigning
    them an index index(temp_var) = index(var)+1
    See ADDManager.add_var()
    See ADDPseudoBooleanFunction.compose()
    """
    tmp_var = lambda v: f"{v}_tmp"

    __slots__ = ("global_variables_names", "global_variables", "tmp", "ordering_check")

    def __init__(self, order=None, tmp=False):

        #print("CREATE MANAGER")

        Subject.__init__(self)

        self.global_variables_names = []
        self.global_variables = []

        # permit to insert tmp var after all vars
        self.tmp = tmp

        # print("INIT", order)
        self.ordering_check = True
        if order is not None:
            for v in order:
                self.add_var(v)

        # print("INIT OK")

    def notify(self, new_links) -> None:
        """
        @Override of Subject
        Trigger an update in each subscriber.
        """
        for i, observer in enumerate(self.observers):
            print("notify")
            observer.update(self, new_links[i])

    @classmethod
    def create(cls, order=None, tmp=False, **kwargs):
        return cls(order=order, tmp=tmp)

    def add_var(self, var: str, tmp=False) -> None:
        """
        Create ADD.variable(id) for variable var, with new id
        :param var: variable name
        :return:
        """
        # already used and declared
        if var in self.global_variables_names:
            return

        self.global_variables_names.append(var)
        # print("ADD var", var, "at", len(self.global_variables_names)-1)
        self.global_variables.append(ADD.variable(len(self.global_variables_names)-1))

        if self.tmp and not tmp:
            #  allows you to create temporary variables automatically by assigning
            #  them an index index(temp_var) = index(var)+1
            self.add_var(ADDManager.tmp_var(var), tmp=True)

    def get_variable(self, var: str) -> ADD:
        self.add_var(var)
        return self.global_variables[self.get_id(var)]

    def get_id(self, var: str) -> int:
        """
        ADD use int as id of node. We use string as literals.
        :param var: variable name as str
        :return: id as int
        """
        return self.global_variables_names.index(var)

    def get_varname(self, id: int) -> str:
        """
        Reverse method of get_id.
        :param id: id as int
        :return: the correspondant variable as str
        """
        #print("get varname", cls.global_variables_names, id)
        return self.global_variables_names[id]

    def is_known(self, var: str) -> bool:
        return var in self.global_variables_names

    def set_order(self, new_order):

        old = self.global_variables_names

        self.global_variables_names = new_order + [v for v in old if v not in new_order]

        self.global_variables = []
        for var in self.global_variables_names:
            self.add_var(var)

    def from_formula(self, formula: Formula, vars: [str] = None, cache=None, link=True) -> ADDPseudoBooleanFunction:
        """Transform a boolean formula in ADD"""

        variables_names = formula.getVariables() if vars is None else vars
        #for var in sorted(list(set(variables_names))):
        #    cls.add_var(var)

        #print(formula)
        @memoize(cache=cache)
        def rec_from_formula(form):
            # print(self.size(), form, self.variables())
            if isinstance(form, Top):
                res = ADD.constant(1.0)
            elif isinstance(form, Bot):
                res = ADD.constant(0.0)
            elif isinstance(form, Atom):
                res = self.get_variable(form.inner)
                # res = ADD.variable(ADDPseudoBooleanFunction.get_id(form.inner))
            elif isinstance(form, Not):
                res = ADD.__invert__(rec_from_formula(form.inner), cache=cache)
            elif isinstance(form, And):
                # res = rec_from_formula(form.left) * rec_from_formula(form.right)
                left = rec_from_formula(form.left)
                right = rec_from_formula(form.right)
                res = ADD.apply(left, right, float.__mul__, cache=cache)
            elif isinstance(form, Or):
                left = rec_from_formula(form.left)
                right = rec_from_formula(form.right)

                #res = ADD.apply(left, right, float.__add__, cache=cache)
                res = ADD.apply(left, right, add_bool, cache=cache)
                #res = ADD.apply(left, right, lambda x,y : int((x+y)>0), cache=cache)

                # res = rec_from_formula(Not(And(Not(form.left), Not(form.right))))  # A /\ B = -(-A & -B)
            elif isinstance(form, Implies):
                res = rec_from_formula(Not(And(form.left, Not(form.right))))  # A -> B = -(A & -B)
            elif isinstance(form, Equiv):
                res = rec_from_formula(And(Implies(form.left, form.right), Implies(form.right, form.left)))

            elif isinstance(form, Exactly):
                res = self._parmi(form, cache=cache)
            elif isinstance(form, ite):
                res = rec_from_formula(Or(And(form.x, form.left), And(Not(form.x), form.right)))
            else:
                raise ValueError(f"Formula not implemented: {form} of type {type(form).__name__}")

            return res

        res = rec_from_formula(formula)
        return ADDPseudoBooleanFunction(self, res, variables_names, link=link)

    def _parmi(self, formula: Exactly, cache=None) -> ADDPseudoBooleanFunction:

        # print(formula)

        def _parmi_rec(n: int, xvars: [str], dic: {}):  # -> Arc

            k = getCacheKey("_parmi_rec", n, frozenset(xvars))
            if k in dic.keys():
                return dic[k]

            if n == 0:
                dic[k] = self.from_formula(AndOfVarList(xvars, pos=False), link=False).pointer
                return dic[k]

            if len(xvars) == 0:
                return self.from_formula(Bot(), link=False).pointer

            Y = xvars[:]
            x = Y[0]
            Y.remove(x)

            dic[k] = getIte(x, _parmi_rec(n - 1, Y, dic), _parmi_rec(n, Y, dic))
            return dic[k]

        def getIte(x: str, left: ADD, right: ADD) -> ADD:
            """
            Return Arc as IfThenElse of (x, left, right), i.e. Or(And(x, left), And(Not(x), right))
            :param x: variable as str
            :param left: ADD
            :param right: ADD
            :return: Arc
            """
            var_x = self.get_variable(x)
            not_var_x = ~ var_x

            and_x_left = var_x * left
            and_not_x_right = not_var_x * right

            # print("var_x", var_x)

            # A | B = -(-A & -B)
            # TODO : call with cache ? create error
            res = ADD.apply(~ and_x_left, ~ and_not_x_right, float.__mul__)
            res = ~ res
            return res

        key = getCacheKey("_parmi", formula.n, frozenset(formula.getVariables()))

        assert isinstance(formula, Exactly), "Formula to SLDD must be Exactly, not {}".format(type(formula).__name__)
        xvars = formula.getVariables()
        for x in xvars:
            assert isinstance(x,
                              str), "Exactly formula take (n:int, [variables:str]) as parameters, not {} as variables.".format(
                type(x).__name__)
        n = formula.n
        assert isinstance(n, int), "Exactly formula take (n:int, [variables:str]) as parameters, not {} as n.".format(
            type(n).__name__)

        assert n <= len(xvars)

        if cache is None:
            cache = {}
        else:
            if key in cache.keys():
                print("already calculated this _parmi", formula)

                return cache[key]

        res = _parmi_rec(n, xvars, cache)  # -> Arc
        cache[key] = res
        return res

    def exclusiveOrOfFormulasWithValues(self, liste: List[Tuple[Formula, float]],
                                        vars: [str] = None):
        """
        Caution: No verification if Formulas haven't communs models.
        :param liste: List if Tuple[Formula, float]
        :param vars: List of variables as str
        :return: The Sum(formula=n) for formula,n in liste
        """

        acc = ADD.constant(0.0)
        variables = set()
        for formula, n in liste:
            v = formula.getVariables()
            for var in v:
                variables.add(var)
            pointer = self.from_formula(formula, vars=v).pointer
            #print(formula, n)
            #ADDPseudoBooleanFunction(pointer, []).print()
            acc = acc + pointer * ADD.constant(n)
            #ADDPseudoBooleanFunction(acc, []).print()
            #print()

        if vars is not None:
            for v in variables:
                assert v in vars, f"Variables are declared, but there are another : {v}"

        return ADDPseudoBooleanFunction(self, acc, variables if vars is None else vars, link=True)

    def getConstant(self, n: float, vars=None):
        return ADDPseudoBooleanFunction(self, ADD.constant(n), [] if vars is None else vars, link=False)

    def size(self, adds=None):
        if adds is None:
            adds = [o.pointer for o in self.observers]

        seen = set()

        def rec_size(n: ADD, incr=0):
            if n in seen or n is None:
                return 0
            seen.add(n)
            return 1 + rec_size(n._low, incr=incr) + rec_size(n._high, incr=incr)

        return sum([rec_size(add, incr=-1) for add in adds])

    def variables(self):
        return self.global_variables_names


    def reorder(self, randomizer=False):
        raise NotImplementedError("Reorder need to be reimplemented. :)")


    def get_mentioned_variables_with_order(self, adds: [ADD]=None):
        if adds is None:
            adds = [o.pointer for o in self.observers]
        variables = set()
        for add in adds:
            for var in self._get_mentioned_variables_with_order_with_add(add):
                variables.add(var)
        variables = list(variables)
        return [v for v in self.global_variables_names if v in variables]
        return variables

    def _get_mentioned_variables_with_order_with_add(self, add: ADD):
        add_ids = add.get_mentioned_indices()
        # print(add_ids)
        return [ADDPseudoBooleanFunction.get_varname(id) for id in add_ids]

    def toDot(self, adds=None, style=False, others=True) -> str:
        # print("ADD dot")
        if adds is not None:
            adds = [f.pointer for f in adds]
        else:
            adds = [o.pointer for o in self.observers]

        seen = set()

        def get_dot_node(vertex):
            if vertex is None:
                return ""
            color = "red" if vertex in adds else "black"
            return f'{vertex._id} [color="{color}" label="(#{self.get_varname(vertex._index)}:id={vertex._id},ind={vertex._index})"];'

        def get_constant(vertex):
            return f'{vertex._id} [label="(#{vertex._value}:id={vertex._id},ind={vertex._index})", shape="box"];'

        def get_dot_arc(vertex1, vertex2, type):
            if vertex2 is None:
                return ""
            return f'{vertex1._id} -> {vertex2._id} {"[style=dashed]" if not type else ""};'

        ddrepr = 'digraph G {\n'

        level = {}

        for add in adds:
            for vertex in add:
                if vertex._index not in level.keys():
                    level[vertex._index] = []
                level[vertex._index].append(vertex._id)
                if not vertex.is_terminal():
                    ddrepr += get_dot_node(vertex) + '\n'
                else:
                    ddrepr += get_constant(vertex) + "\n"

                seen.add(vertex)

        for vertex in seen:
            if not vertex.is_terminal():
                ddrepr += get_dot_arc(vertex, vertex._high, True) + "\n"
                ddrepr += get_dot_arc(vertex, vertex._low, False) + "\n"

        for lvl, ids in level.items():
            ddrepr += "{ rank = same;" + ";".join([str(i) for i in ids]) + "}\n"

        return ddrepr + "\n}"

    def getOrder(self):
        return self.global_variables_names

    def save(self, name, location=None):
        print("write", name, location, self)
        self.add_real_function_gvn = self.global_variables_names
        self.add_real_function_gv = self.global_variables
        save_object(self, name, location=location)


    def savelock(self, pointeur, name):
        if not hasattr(self, "pointers"):
            self.pointers = {}
        self.pointers[name] = pointeur

    @staticmethod
    def load(name, location=None):
        print("load", name, location)
        res = read_object(name, location=location)
        return res, res.pointers



class ADDPseudoBooleanFunction:
    pass

#@for_all_methods(debug_me)
class ADDPseudoBooleanFunction(PseudoBooleanFunction, Observer):

    def __init__(self, manager: ADDManager, pointeur: ADD, vars: [str], link: bool=True):

        Observer.__init__(self)

        self.manager = manager
        self.variables_names = list(set(vars))
        self.variables_names.sort()
        self.pointer = pointeur

        for var in self.variables_names:
            self.manager.add_var(var)

        if link:
            self.manager.attach(self)


    def update(self, subject: Subject, new_link) -> None:
        """
        @Override of Observer
        Implementation of Observer
        :param subject: here, ADDManager
        :return:
        """
        old_link = self.pointer
        self.pointer = new_link
        debug(Lazy(lambda: pprint(f"I'm updated : {old_link}->{self.pointer}", Color.CBLUE, Color.CBOLD)))


    def get_varname(self, index):
        return self.manager.get_varname(index)


    ###
    # REDEFINITION OF PseudoBooleanFunction

    def getVariables(self) -> [str]:
        """ Return list of varibles used in the ADD """
        return self.variables_names

    def extendScope(self, vars: List[str]):
        for v in vars:
            assert v not in self.variables_names, f"Error in scope : '{v}' is already in scope."
            self.manager.add_var(v)
            self.variables_names.append(v)



    def _get_iterator(self) -> Iterable[Tuple[Dict[str,bool], float]]:

        def succ(add):
            return (add._low, add._high)

        def rec(node, valuation):

            if node.is_terminal():
                    yield (valuation, node._value)
            else:
                for i, succs in enumerate(succ(node)):
                    yield from rec(succs, {**valuation, **{self.manager.get_varname(node._index):i}})

        yield from rec(self.pointer, {})

    def browse(self) -> Iterable[Tuple[Valuation, float]]:
        """ Return an iterator on (Valuation, float)"""
        yield from self._get_iterator()

    def setValue(self, valuation: Valuation, value: float) -> None:
        old_value = self.getValue(valuation)
        # create the ADD with one model and one value, which is (new value - old value)
        new_constant = self.manager.from_formula(AndOfVarList(valuation)).pointer * ADD.constant(value - old_value)
        # add this ADD to the old one, effectively changing the value to the new one
        self.pointer += new_constant
        self.variables_names = list(set(self.variables_names + list(valuation.keys())))

    def getValue(self, valuation: Valuation) -> float:
        """Return the value of a specific valuation in ADD"""
        def succ(add):
            return (add._low, add._high)

        def rec(node):
            if node.is_terminal():
                return node._value
            var = self.manager.get_varname(node._index)
            if var not in valuation.keys():
                raise Exception(f"Missing variable {var} in valuation {valuation} to find value.")
            return rec(succ(node)[valuation[var]])

        return rec(self.pointer)

    def isBoolean(self) -> bool:
        """ The PBF admit only 0 and 1 as results ?"""
        for x, y in self._get_iterator():
            if y not in [0, 1]:
                return False
        return True

    def isFalse(self) -> bool:
        return self.pointer == ADD.constant(0)

    def isTrue(self) -> bool:
        return self.pointer == ADD.constant(1)

    def __eq__(self, other):
        return set(self.getVariables()) == set(other.getVariables()) and self.pointer == other.pointer

    def marginalization(self, vars: [str], fonction=float.__add__, cache=None) -> PseudoBooleanFunction:
        """ Forgetting variables var en collapse values in models with sum."""

        if cache is None:
            cache = {}

        # print("Marginalisation : liste=", liste)

        pointer = self.pointer
        #mydebug(0, "")
        #mydebug(0, Lazy(lambda: f"> ADDPseudoBooleanFunction marginalisation on {vars}"))

        if __debug__:
            for v in vars:
                assert v in self.variables_names, f" {v} is not in scope of ADDPseudoBooleanFunction: {self.variables_names}"

        all_marg_vars = vars[:]
        add_vars = self._get_mentioned_variables_with_order()
        add_marg_vars = [v for v in add_vars if v in all_marg_vars]
        other_marg_vars = [v for v in all_marg_vars if v not in add_vars]

        liste_variables = [self.manager.get_variable(var) for var in add_marg_vars]
        pointer = pointer.marginalize_liste(liste_variables, cache=cache, fonction=fonction)

        new_vars = [v for v in self.getVariables() if v not in all_marg_vars]
        if fonction is float.__add__:
            return ADDPseudoBooleanFunction(self.manager, pointer * ADD.constant(2 ** (len(other_marg_vars))), new_vars)
        elif fonction in [min, max]:
            return ADDPseudoBooleanFunction(self.manager, pointer, new_vars)
        else:
            """ float.__mul__ here, or other ? """
            res = pointer
            for i in range(len(other_marg_vars)):
                res = ADD._apply_step(res, res, fonction, cache)
            return ADDPseudoBooleanFunction(self.manager, res, new_vars)

    def rename(self, dico: {str: str}) -> ADDPseudoBooleanFunction:
        """Rename some variables names with others"""

        if __debug__:
            for p in dico:
                assert p in self.variables_names, f"Error : {p} isn't in ADDPseudoBooleanFunction scope ({self.variables_names}), you can't rename it."

        vars = [dico[var] if var in dico.keys() else var for var in self.variables_names]

        #print("RENAME", vars)
        for k, v in dico.items():
            self.manager.add_var(k)
            self.manager.add_var(v)

        dico_id = {self.manager.get_id(var1): self.manager.get_id(var2) for var1, var2 in dico.items()}

        if __debug__:
            current = -1
            for key in sorted(list(dico_id.keys())):
                assert dico_id[key] > current, f"Rename error : {dico_id[key]} !> {current} " \
                                               f"= {self.manager.get_varname(key)} > {self.manager.get_varname(current)}"
                current = dico_id[key]

        try:
            res = self.pointer.rename(dico_id)
        except AssertionError as e:
            print("ERROR in renaming")
            for k, v in dico.items():
                print(k, self.manager.get_id(k), "->", v, self.manager.get_id(v))
            print()
            print(str(e))
            print(
                self.pointer.assert_left, self.get_varname(self.pointer.assert_left),
                self.pointer.assert_right, self.get_varname(self.pointer.assert_right)
            )
            sys.exit()

        res = ADDPseudoBooleanFunction(self.manager, res, vars)
        #print("après")
        #res.print()
        return res

    def apply(self, op: str, other=None, zeroincluded:bool=True, cache=None) -> PseudoBooleanFunction:

        #print("op", op, "self", self, "other", other)
        if cache is None:
            cache = {}

        if op in ADDManager.OPTABLE:
            assert other is not None
            assert isinstance(other, PseudoBooleanFunction), f"{other} is not PseudoBooleanFunction but {type(other).__name__}"

            op = self.manager.OPTABLE[op]
            new_vars = list(set(self.getVariables() + other.getVariables()))
            return ADDPseudoBooleanFunction(self.manager, ADD.apply(self.pointer, other.pointer, op, cache=cache), new_vars)

        elif op in ADDManager.UNARYOPTABLE:
            op = self.manager.UNARYOPTABLE[op]
            #print(op, self.getVariables())
            assert other is None, f" op = {op} and other is {type(other).__name__}"
            return ADDPseudoBooleanFunction(self.manager, ADD.self_apply(self.pointer, op, cache=cache), self.getVariables())

        # Use custom lambda here
        assert callable(op), f"Here op need to be callable, not {type(other).__name__}"
        if other is None:
            return ADDPseudoBooleanFunction(self.manager, ADD.self_apply(self.pointer, op, cache=cache), self.getVariables())
        else:
            new_vars = list(set(self.getVariables() + other.getVariables()))
            return ADDPseudoBooleanFunction(self.manager, ADD.apply(self.pointer, other.pointer, op, cache=cache), new_vars)

    def copy(self) -> PseudoBooleanFunction:
        return ADDPseudoBooleanFunction(self.manager, self.pointer, self.getVariables())

    def booleanization(self, cache=None)-> PseudoBooleanFunction:
        return ADDPseudoBooleanFunction(self.manager, ADD.self_apply(self.pointer, ADD.ibool, cache=cache), self.getVariables())

    def support(self) -> Iterable[Valuation]:
        """ Return an iterator on all valuations which has none 0 value."""
        return filter(lambda x: x[1] != 0., self._get_iterator())

    def append(self,formula: Formula, k:float) -> None:
        raise NotImplementedError

    def conditionning(self, assignment: {str: bool}) -> PseudoBooleanFunction:
        if isinstance(assignment, ADDPseudoBooleanFunction):
            support = list(assignment.support())
            len_support = len(support)
            if len_support == 0:
                raise ValueError("ADDPseudoBooleanFunction as assignement need to have one support.")
            elif len_support != 1:
                raise ValueError("ADDPseudoBooleanFunction as assignement need to have only one support.")
            else:
                assignment, value = support[0]

        """Conditionning request on ADD"""
        ass = {self.manager.get_id(x): y for x, y in assignment.items()}
        new_vars = [var for var in self.variables_names if var not in assignment.keys()]
        return ADDPseudoBooleanFunction(self.manager, self.pointer.restrict(ass), new_vars)

    def cut(self, op: str, threshold: float, rejection_value) -> PseudoBooleanFunction:
        """
        op permit comparaison test [">", ">=", "<", "<=", "=="].
        If test is OK, keep values, else get rejection_value.
        :param op: str [">", ">=", "<", "<=", "=="]
        :param threshold: float
        :param rejection_value: float
        :return:
        """
        assert op in [">", ">=", "<", "<=", "=="], f"Unknown operator : {op}"

        #if op not in [">", ">="]:
        #    raise Exception("Implemented doesn't work yet for this operators : " + op)

        """
        if op in [">", ">="]:
            if rejection_value is None:
                rejection_value = 0.

        if rejection_value is None:
            rejection_value = 1.
            threshold = 1 - threshold
        """

        #print(" == ADD cut", op, threshold, rejection_value)

        realop = self.manager.OPTABLE[op]
        return ADDPseudoBooleanFunction(
            self.manager,
            ADD.self_apply(self.pointer, lambda x: x if realop(x, threshold) else rejection_value),
            self.variables_names)


    def bool_cut(self, op: str, threshold: float):

        assert op in [">", ">=", "<", "<=", "=="], f"Unknown operator : {op}"

        realop = self.manager.OPTABLE[op]
        return ADDPseudoBooleanFunction(
            self.manager,
            ADD.self_apply(self.pointer, lambda x: realop(x, threshold)),
            self.variables_names)


    def toDot(self, atoms=False):

        seen = set()

        def get_dot_node(vertex):
            if vertex is None:
                return ""
            if atoms:
                return f'{vertex._id} [label="{self.manager.get_varname(vertex._index)}"];'
            return f'{vertex._id} [label="(#{self.manager.get_varname(vertex._index)}:id={vertex._id},ind={vertex._index})"];'

        def get_constant(vertex):
            if atoms:
                return f'{vertex._id} [label="{vertex._value}", shape="box"];'
            return f'{vertex._id} [label="(#{vertex._value}:id={vertex._id},ind={vertex._index})", shape="box"];'

        def get_dot_arc(vertex1, vertex2, type):
            if vertex2 is None:
                return ""
            return f'{vertex1._id} -> {vertex2._id} {"[style=dashed]" if not type else ""};'

        ddrepr = 'digraph G {\n'

        level = {}
        for vertex in self.pointer:
            if vertex._index not in level.keys():
                level[vertex._index] = []
            level[vertex._index].append(str(vertex._id))
            if not vertex.is_terminal():
                ddrepr += get_dot_node(vertex) + '\n'
            else:
                ddrepr += get_constant(vertex) + "\n"

            seen.add(vertex)

        stack = [(self.pointer, None)]
        for vertex in self.pointer:
            if not vertex.is_terminal():
                stack.append((vertex._high, '+'))
                stack.append((vertex._low, '-'))
                ddrepr += get_dot_arc(vertex, vertex._high, True) + "\n"
                ddrepr += get_dot_arc(vertex, vertex._low, False) + "\n"

        for lvl, ids in level.items():
            ddrepr += "{ rank = same;" + ";".join(ids) + "}\n"

        return ddrepr + "\n}"


    def _get_mentioned_variables_with_order(self):
        add_ids = self.pointer.get_mentioned_indices()

        liste = []
        for var in self.manager.global_variables_names:
            id = self.manager.get_id(var)
            if id in add_ids:
                liste.append(id)

        return [self.manager.get_varname(id) for id in add_ids]

    @classmethod
    def get_mentioned_id_with_order(self, pointer):
        add_ids = pointer.get_mentioned_indices()

        liste = []
        for var in self.manager.global_variables_names:
            id = self.manager.get_id(var)
            if id in add_ids:
                liste.append(id)

        return [self.manager.get_varname(id) for id in add_ids]

    def get_size(self):

        seen = set()

        def rec_size(n: ADD):
            if n in seen or n is None:
                return 0
            seen.add(n)
            return 1 + rec_size(n._low) + rec_size(n._high)

        return rec_size(self.pointer)

    def UForget(self, vars: [str], cache=None):
        return self._forget(vars, lambda x, y: float(bool(x) and bool(y)), cache=cache)

    def EForget(self, vars: [str], cache=None):
        return self._forget(vars, lambda x, y: float(bool(x) or bool(y)), cache=cache)


    def _forget(self, vars: [str], fonction, cache=None) -> PseudoBooleanFunction:

        if cache is None:
            cache = {}

        pointer = self.pointer

        vars = [v for v in vars if v in self.getVariables()]

        all_marg_vars = vars[:]
        add_vars = self._get_mentioned_variables_with_order()
        add_marg_vars = [v for v in add_vars if v in all_marg_vars]

        liste_variables = [self.manager.get_variable(var) for var in add_marg_vars]
        pointer = pointer.forget(liste_variables, cache=cache, fonction=fonction)

        new_vars = [v for v in self.getVariables() if v not in all_marg_vars]

        return ADDPseudoBooleanFunction(self.manager, pointer, new_vars)

    def compose(self, dico: Dict[str, ADDPseudoBooleanFunction]) -> ADDPseudoBooleanFunction:
        """
        use temporary variables for dico.keys() permitting simultaneous compose
        :param dico: dictionary of variable into ADDPseudoBooleanFunction
        :return: ADDPseudoBooleanFunction
        """
        assert self.manager.tmp, "manager.tmp need to be true to do composition. Please Create Manager with 'tmp=True' in parameters."

        vars = list((set(self.getVariables())-set(dico.keys())) | set(v for g in dico.values() for v in g.getVariables()))

        to_hat = {self.manager.get_id(x): self.manager.get_id(ADDManager.tmp_var(x)) for x in dico.keys()}
        un_hat = {self.manager.get_id(ADDManager.tmp_var(x)): self.manager.get_id(x) for x in dico.keys()}

        pointer = self.pointer
        for var, g in dico.items():
            g_hat = g.pointer.rename(to_hat)
            pointer = pointer.compose(self.manager.get_id(var), g_hat)

        return ADDPseudoBooleanFunction(self.manager, pointer.rename(un_hat), vars)

    def restrict_law(self, add: ADDPseudoBooleanFunction) -> ADDPseudoBooleanFunction:
        # TODO TEST IT
        raise Exception("Need to be tested.")
        pointer = self.pointer.restrict_law(add.pointer)
        vars = self.get_mentioned_variables_with_order([pointer])
        return ADDPseudoBooleanFunction(self.manager, pointer, vars)

    def get_nodes_informations(self):
        dico = {f"(var='{self.manager.get_varname(k)}', id={k})" if isinstance(k, int) else "Terminals": v for k, v in self.pointer.get_nodes_informations().items()}
        somme = 0
        for i, nb in dico.items():
            if i != "Terminals":
                somme += nb
        dico["NoTerminals"] = somme
        return dico







if __name__ == "__main__":

    manager = ADDManager.create()

    add1 = manager.from_formula(Atom("a"))
