
"""
This is a heavily modified version of add.py of pyddlib :
https://github.com/thiagopbueno/pyddlib/blob/master/pyddlib/add.py
which is licenced under the GNU GPL
"""

from numbers import Number

from .dd import DD

from src.io.colors import *
from src.utils.debug import *
from src.utils.memoize import memoize
import math

DEBUG = False

def mydebug(incr, *msg):
    if DEBUG :
        print(Lazy(lambda: f"{'|   ' * incr}"), *msg)

class ADD:
    pass

#@for_all_methods(debug_me)
class ADD(DD):
    """
    Reduced Ordered Algebraic Decision Diagram class.
    :param index: root vertex variable index (inf if terminal vertex)
    :type  index: int
    :param low:   low child vertex of ADD (None if terminal vertex)
    :type  low:   pyddlib.ADD
    :param high:  high child vertex of ADD (None if terminal vertex)
    :type  high:  pyddlib.ADD
    :param value: terminal numeric value (None if non-terminal vertex)
    :type  type:  Number or None
    """

    __nextid = 1

    __slots__ = ('_index', '_low', '_high', '_value', '_id')

    def __init__(self, index, low, high, value, authorized_caller=False):
        if not authorized_caller:
            raise Exception("unauthorized caller")

        if __debug__:
            assert (
                    (   # Is terminal node with good value and not childs
                        value is not None and isinstance(value, Number)
                        and index == math.inf and low == high == None
                    )
                    or
                    (   # I'm a node
                        index >= 0 and None not in (low, high) and value is None
                    )
            ), f"index={index}, low={low}, high={high}, value={value}\n" \
               f"{value is not None}, {isinstance(value, Number)}, {index == math.inf}, {low == high == None}\n" \
               f"{index >= 0}, {None not in (low, high)}, {value is None}"

            # Order verification
            if not low is None:
                self.assert_left, self.assert_right = index, low._index
                assert index < low._index, f"Order error : index={index}, low={low._index}"
            if not high is None:
                self.assert_left, self.assert_right = index, high._index
                res_str = f"Assert_left: {self.assert_left}, Assert_right: {self.assert_right}"
                assert index < high._index, f"Order error : not(index={index} < high={high._index}) - {res_str}\n " \
                                            f"index={index}, value={value},\n" \
                                            f"low={str(low)}\n " \
                                            f"high={str(high)}"

        self._index = index
        self._low   = low
        self._high  = high
        self._value = float(value) if value is not None else None
        self._id    = ADD.__nextid
        ADD.__nextid += 1

    def tree_repr(self):
        """
        Return tree-like representation of pyddlib.ADD object.
        :rytpe: str
        """
        ddrepr = ''
        stack = [(self, 0, None)]
        while stack:
            (vertex, indentation, child_type) = stack.pop()
            for i in range(indentation):
                ddrepr += '|  '
            prefix = '@'
            if child_type is not None:
                prefix = child_type
            ddrepr += prefix
            if vertex.is_terminal():
                ddrepr += ' (value={}, id={})'.format(vertex._value, vertex._id) + '\n'
            else:
                ddrepr += ' (index={}, id={})'.format(vertex._index, vertex._id) + '\n'
                stack.append((vertex._high, indentation+1, '+'))
                stack.append((vertex._low,  indentation+1, '-'))
        return ddrepr

    def __repr__(self):
        if self.is_constant():
            return f"ADD(cst={self._value})"
        left = f"ADD(cst={self._low._value}" if self._low.is_constant() else f"id={self._low._id}"
        right = f"ADD(cst={self._high._value}" if self._high.is_constant() else f"id={self._high._id}"
        return f"ADD(id={self._id}, index={self._index}, high=({right}), low=({left}))"

    def __hash__(self):
        return hash(("add", self._id))

    @property
    def value(self):
        """
        Return node value.
        :rtype: Number or None
        """
        return self._value

    @property
    def index(self):
        """
        Return variable index of node.
        :rtype: int
        """
        return self._index

    def is_terminal(self):
        """
        Return True if ADD function represents a constant value.
        Otherwise, return False.
        :rtype: bool
        """
        return self._value is not None

    def is_constant(self):
        """
        Return True if ADD function represents a constant value.
        Otherwise, return False.
        :rtype: bool
        """
        return self.is_terminal()

    def is_variable(self):
        """
        Return True if ADD function represents the function
        of a single boolean variable. Otherwise, return False.
        :rtype: bool
        """
        low  = self._low
        high = self._high
        return (
                low  and low._value  == 0.0 and
                high and high._value == 1.0
        )

    def __invert__(self, cache=None):
        """
        Compute a new reduced ADD representing the negation
        of the algebraic function. Terminal values other than
        0.0 are changed to 0.0 and terminal value 0.0 is changed
        to 1.0.
        Return ~self.
        :rtype: pyddlib.ADD
        """

        return self.self_apply(
            self,
            ADD.inot,
            cache=cache
        )

    def __neg__(self, cache=None):
        """
        Compute a new ADD representing the opposite of the
        algebraic function.
        Return -self.
        :rtype: pyddlib.ADD
        """
        return self.self_apply(
            self,
            lambda v: -v,
            cache=cache
        )
        """
        if self.is_terminal():
            return self.constant(-self._value)
        return ADD.build_node(self._index, -self._low, -self._high)
        """

    def __add__(self, other, cache=None):
        """
        Compute a new ADD representing the addition of algebraic functions.
        Return self+other.
        :param other: ADD
        :type other: pyddlib.ADD
        :rtype: pyddlib.ADD
        """
        return ADD.apply(self, other, float.__add__, cache=cache)

    def __sub__(self, other, cache=None):
        """
        Compute a new ADD representing the subtraction of algebraic functions.
        Return self-other.
        :param other: ADD
        :type other: pyddlib.ADD
        :rtype: pyddlib.ADD
        """
        return ADD.apply(self, other, float.__sub__, cache=cache)

    def __mul__(self, other, cache=None):
        """
        Compute a new ADD representing the product of algebraic functions.
        Return self*other.
        :param other: ADD
        :type other: pyddlib.ADD
        :rtype: pyddlib.ADD
        """
        return ADD.apply(self, other, float.__mul__, cache=cache)

    def __truediv__(self, other, cache=None):
        """
        Compute a new ADD representing the division of algebraic functions.
        Return self/other.
        :param other: ADD
        :type other: pyddlib.ADD
        :rtype: pyddlib.ADD
        """
        return ADD.apply(self, other, float.__truediv__, cache=cache)


    def marginalize(self, variable, cache=None):#, incr=0):
        """
        FOR MULTIPLE MARGIINALISATION, PLEASE USE marginalize_liste WHICH IS MORE EFFICIENT
        Caution : here, works ony with fonction=add.

        Compute a new reduced ADD with `variable` marginalized.
        Return self.restrict({variable.index: 1}) + self.restrict({variable.index: 0})
        :param variable: ADD variable node
        :type other: pyddlib.ADD
        :rtype: pyddlib.ADD
        """
        # mydebug(incr, Lazy(lambda: pprint(f"> add.marginalize : (id={self._id},ind={self._index},{self.value}) mentionned_id={self.get_mentionned_id()}", Color.CBOLD)))
        if cache is None:
            cache = {}

        @memoize(cache)
        def __marginalize_step(self, variable):  # , incr=0):
            # mydebug(incr, Lazy(lambda: f"> marginalize_step : me={self._index} <> {variable._index}"))
            if self.is_terminal():  # self._index > variable._index
                # mydebug(incr, Lazy(lambda: f">> is_terminal = id={variable._id}, ind={variable._index}, val={variable._value}"))
                # mydebug(incr, Lazy(lambda: f">> multiply by 2."))
                return ADD.apply(self, ADD.build_terminal(2), float.__mul__, cache=cache)
            elif self._index == variable._index:
                # mydebug(incr, Lazy(lambda: f">> find = {variable._index}"))
                # mydebug(incr, Lazy(lambda: f">> {'cst='+str(self._low._value) if self._low._index == math.inf else self._low._index} + "
                #                           f"{'cst='+str(self._high._value) if self._high._index == math.inf else self._high._index}"))
                res = ADD.apply(self._low, self._high, float.__add__, cache=cache)
                coeff = 2. ** (self._index - variable._index)
                # mydebug(incr, Lazy(lambda: f">> multiply by = {coeff}"))
                return ADD.apply(res, ADD.build_terminal(coeff), float.__mul__, cache=cache)
            elif self._index < variable._index:
                # mydebug(incr, Lazy(lambda: f">> {self._index} < {variable._index} : continue ..."))
                low = self._low.__marginalize_step(variable)  # , incr=incr+1)
                high = self._high.__marginalize_step(variable)  # , incr=incr+1)
                return ADD.build_node(self._index, low, high)
            else:
                # mydebug(incr, Lazy(lambda: f">> below ?, id={variable._id}, ind={variable._index}, val={variable._value}"))
                return ADD.apply(self, ADD.build_terminal(2), float.__mul__, cache=cache)

        return self.__marginalize_step(variable)  # , incr=incr+1)



    def marginalize_liste(self, variables: [ADD], cache=None, fonction=float.__add__):#, incr=0) -> ADD:
        """
        ADDED METHOD : optimisation
        Return self.restrict({variable.index: 1}) + self.restrict({variable.index: 0}) for all variables in parameters
        :param variables: index of variables
        :param cache:
        :param fonction: marginalisation function as binary operator (float.__add__, float.__mul__, min, max...)
        :return:
        """

        if cache is None:
            cache = {}

        mentioned_indices = self.get_mentioned_indices()
        variables_index = [var._index for var in variables if var._index in mentioned_indices]
        # sort indices
        variables_index.sort()

        @memoize(cache, debug=False)
        def rec_marg(node, indices_to_forget): #, incr=0):

            if len(indices_to_forget) == 0:
                """ no more indices to marg, we return the current node"""
                res = node

            elif node._index > max(indices_to_forget):
                #mydebug(incr, f"we are below all indices : {node._index} > {indices_to_forget}")
                if fonction is float.__add__:
                    coef = 2**len(indices_to_forget)
                    res = ADD._apply_step(node, ADD.build_terminal(coef), float.__mul__, cache)
                elif fonction in [min, max]:
                    res = node
                else:
                    """
                    for all type of operators.
                    Works of add, min and max, but we avoid superfluous calculations
                    typically, here it's for ___mul__"""
                    res = node
                    for i in range(len(indices_to_forget)):
                        res = ADD._apply_step(res, res, fonction, cache)

            elif node._index in indices_to_forget:
                """mydebug(incr, Color.get(f"find {node._index} : {node._low._id} + {node._high._id}", Color.CBLUE))"""

                index = indices_to_forget.index(node._index)
                nb_skipped_variables = index
                new_variables_index = indices_to_forget[index+1:]
                # new_variables_index = indices_to_forget[indices_to_forget.index(node._index)+1:]
                low = rec_marg(node._low, new_variables_index)#, incr=incr + 1)
                high = rec_marg(node._high, new_variables_index)#, incr=incr + 1)
                res = ADD._apply_step(low, high, fonction, cache)
                # mult by coefficient
                if fonction is float.__add__:
                    coef = 2**nb_skipped_variables
                    res = ADD._apply_step(res, ADD.build_terminal(coef), float.__mul__, cache)
                elif fonction in [min, max]:
                    # do nothing, but we dont want to go to 'else'
                    res = res
                else:
                    """
                    for all type of operators.
                    Works of add, min and max, but we avoid superfluous calculations
                    typically, here it's for ___mul__"""
                    for i in range(nb_skipped_variables):
                        res = ADD._apply_step(res, res, fonction, cache)
            else:
                #mydebug(incr, "continue...")
                assert node.index not in indices_to_forget
                res = ADD.build_node(node._index,
                                     rec_marg(node._low, indices_to_forget),#, incr=incr+1),
                                     rec_marg(node._high, indices_to_forget))#, incr=incr+1))

            #mydebug(incr, f"returning res={res}")
            return res

        res = rec_marg(self, tuple(variables_index))#, incr=incr)
        return res


    @classmethod
    def constant(cls, value: float) -> ADD:
        """
        Return a terminal node with a given numeric `value`.
        :param value: numeric value
        :type value: Number
        :rtype: pyddlib.ADD
        """
        return cls.build_terminal(value)

    @classmethod
    def variable(cls, index: int) -> ADD:
        """
        Return the ADD representing the function of a
        single boolean variable with given `index`.
        :param index: variable index
        :type index: int
        :rtype: pyddlib.ADD
        """
        return ADD.build_node(index, cls.build_terminal(0.0), cls.build_terminal(1.0))


    #######
    # ADDED METHOD
    #

    def get_mentioned_indices(self) -> [int]:
        """
        # ADDED
        Return list of id which are present in ADD.
        :return: [id: int]
        """
        id_list = []
        seen = set()
        for vertex in self:
            if not vertex.is_terminal():
                if vertex._index not in seen:
                    seen.add(vertex._index)
                    id_list.append(vertex._index)
        return id_list


    def cut(self, op:str, threshold: float, rejection_value=0.0) -> ADD:
        """
        :param
        op: str[">", ">=", "<", "<=", "=="]
        :param
        threshold: float
        :param
        rejection_value: float
        :return:
        """

        fct = None
        if op == ">":
            fct = lambda x: x > threshold
        elif op == ">=":
            fct = lambda x: x >= threshold
        elif op == "<":
            fct = lambda x: x < threshold
        elif op == "<=":
            fct = lambda x: x <= threshold
        elif op == "==":
            fct = lambda x: x == threshold
        else:
            raise Exception("Unknown operator")

        rej_node = ADD.build_terminal(rejection_value)

        @memoize()
        def rec_cut(node):

            if node.is_terminal():
                if fct(node._value):
                    res = node
                else:
                    res = rej_node
            else:
                res = ADD.build_node(node._index, rec_cut(node._low), rec_cut(node._high))

            return res

        return rec_cut(self)



    def rename(self, dico_id: {int: int}):

        @memoize()
        def rec_rename(node):
            #print("rec_rename", node._id, node._index)
            if node.is_terminal():
                return node

            low = rec_rename(node._low)
            high = rec_rename(node._high)

            if node._index in dico_id.keys():
                #print("change", node._index, "into", dico_id[node._index])
                #print("low :", low._index, low._value)
                #print("high:", high._index, high._value)
                res = ADD.build_node(dico_id[node._index], low, high)
            else:
                res = ADD.build_node(node._index, low, high)

            return res

        return rec_rename(self)



    def compose(self, i: int, g: ADD, cache=None) -> ADD:
        """
        We want to compute :
        f_{|x_i = g} = f(x_1, ... x_{i-1}, g, x_{i+1}, ..., x_n)
        which is :
        f_{|x_i = g} = g . f_{x_i} + g' . f_{x'_i}

        http://www.ecs.umass.edu/ece/labs/vlsicad/ece667/reading/somenzi99bdd.pdf
        p.15 5.3.2 Function Composition

        :param i: index of variable we want to subtitute
        :param g: the function we want to substitute to i
        :return:
        """

        if cache is None:
            cache = {}

        # f_{|x_i = g} = g . f_{x_i} + g' . f_{x'_i}
        high = self.restrict({i: True}, cache=cache)
        low = self.restrict({i: False}, cache=cache)
        res = ADD.ite(g, high, low, cache=cache)

        return res

        #TODO : better implementation

        t = self._index

        print(f"Compose : t={t}, i={i}")
        print("g=", g)

        if t > i:
            print("t > i : Return f")
            return self
        elif t == i:  # x_i is top variable
            print("t == i : Return ite(t, f-; f+)")
            return ADD.build_node(t, self._low, self._high)
        #else  t < i
        # x_u : variable of least index between the top variable of f and the top variable of g

        print(f"t ({t})< i ({i})")

        xu = self._low._index if self._low._index < self._high._index else self._high._index

        print(f"xu = {xu}")

        # g_xu . (f_xu)_xi + g'_xu . (f_xu)_x'i
        leftl = ADD.apply(
                g.restrict({xu: True}),               # g_xu
                self.restrict({xu: True, i: True}),   # (f_xu)_xi
                ADD.iand
            )
        leftr = ADD.apply(
                ADD.self_apply(g.restrict({xu: True}), ADD.inot), # g'_xu
                self.restrict({xu: True, i: False}),  # (f_xu)_x'i
                ADD.iand
            )
        left = ADD.apply(leftl, leftr, ADD.ior)
        print("Leftl", leftl)
        print("leftr", leftr)
        print("Left", left)

        # g_x'u . (f_x'u)_xi + g'_x'u . (f_x'u)_x'i
        rightl = ADD.apply(
                g.restrict({xu: False}),              # g_x'u
                self.restrict({xu: False, i: True}),  # (f_x'u)_xi
                ADD.iand
            )
        rightr = ADD.apply(
                ADD.self_apply(g.restrict({xu: False}), ADD.inot), # g'_x'u
                self.restrict({xu: True, i: False}),   # (f_xu)_x'i
                ADD.iand
            )
        right = ADD.apply(rightl, rightr, ADD.ior)
        print("Rightl", rightl)
        print("Rightr", rightr)
        print("Right", right)

        # xu . left + x'u . right
        resl = ADD.apply(ADD.variable(xu), left, ADD.iand)
        resr = ADD.apply(ADD.self_apply(ADD.variable(xu), ADD.inot), right, ADD.iand)
        res = ADD.apply(resl, resr, ADD.ior)
        print("Res", res)

        @memoize(cache)
        def rec_compose(node):
            pass

        # find recursion ??   rec_compose(xu)
        return res


    def restrict_law(self, G: ADD, cache=None) -> ADD:
        """
        p.22 5.4.2 The Restrict Operator
        http://www.ecs.umass.edu/ece/labs/vlsicad/ece667/reading/somenzi99bdd.pdf
        Existential quantification of all variables in g that not appear in f

        TODO: MOVE TO BDD
        """
        #TODO Testing
        raise Exception("resttrictLaw Need to be read and tested.")
        if cache is None: cache = {}

        @memoize(cache)
        def rec_restrict(f, g):

            if (g.is_one()) or f.is_constant():
                return f
            if f is g:
                return ADD.build_terminal(1.0)
            if f is ADD.self_apply(g, ADD.inot):
                return ADD.build_terminal(0.0)
            if g.is_zero():
                return ADD.build_terminal(0.0)

            v = min(f._index, g._index)
            # if (G_v = 0): return restrict(F_v', G_v')
            if ADD.restrict(g, {v: True}).is_zero():
                return rec_restrict(ADD.restrict(f, {v: False}), ADD.restrict(g, {v: False}))
            # if (G_v' = 0): return restrict(F_v, G_v)
            if ADD.restrict(g, {v: False}).is_zero():
                return rec_restrict(ADD.restrict(f, {v: True}), ADD.restrict(g, {v: True}))

            if v != f._index:
                return rec_restrict(f, ADD.ite(ADD.restrict(g, {v: True}), ADD.build_terminal(1.0), ADD.restrict(g, {v: False})))

            return ADD.ite(ADD.build_node(v),
                           rec_restrict(f.restrict({v: True}), rec_restrict(g.restrict({v: True}))),
                           rec_restrict(f.restrict({v: False}), rec_restrict(g.restrict({v: False})))
                           )

        return rec_restrict(self, G)

    def forget(self, variables: [ADD], fonction, cache=None):

        variables_index = [var._index for var in variables]
        # print(">>", variables_index)
        mentioned_indices = self.get_mentioned_indices()
        variables_index = [v for v in variables_index if v in mentioned_indices]
        # print(">>", variables_index)
        variables_index.sort()

        if cache is None:
            cache = {}

        # print(f">>>> Forget \n>> fonction = {fonction} \n>> to_forget = {variables_index}")

        @memoize(cache, debug=False)
        def rec_forget(node, indices_to_forget):

            # print(node._id, node._index, indices_to_forget)

            # key = ("rec_forget", node._id)
            # print("Visite :", node._id)
            if node.is_terminal():
                res = node
            elif len(indices_to_forget) == 0:
                res = node
            elif node._index > max(indices_to_forget):
                res = node
            elif node._index in indices_to_forget:
                index = indices_to_forget.index(node._index)
                new_variables_index = indices_to_forget[
                                      index + 1:]  # tuple(i for i in indices_to_forget if i != node._index)
                low = rec_forget(node._low, new_variables_index)
                high = rec_forget(node._high, new_variables_index)
                res = ADD.apply(low, high, fonction, cache=cache)
            else:
                assert node.index not in indices_to_forget
                res = ADD.build_node(node._index,
                                     rec_forget(node._low, indices_to_forget),
                                     rec_forget(node._high, indices_to_forget))
            return res

        res = rec_forget(self, tuple(variables_index))
        return res
