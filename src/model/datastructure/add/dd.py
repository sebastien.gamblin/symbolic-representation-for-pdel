
"""
This is adapted from dd.py of pyddlib :
https://github.com/thiagopbueno/pyddlib/blob/master/pyddlib/dd.py
which is licenced under the GNU GPL
"""

import math
from src.io.colors import *
from src.utils.memoize import memoize

import sys
from weakref import WeakValueDictionary

from src.utils.memoize import memoize

class DD:
    pass

class DD(object):
    """ Decision Diagram abstract base class. """

    terminals_table = WeakValueDictionary()
    nodes_table = WeakValueDictionary()

    iand = lambda x, y: float(bool(x) and bool(y))
    ior = lambda x, y: float(bool(x) or bool(y))
    inot = lambda v: 0.0 if v != 0.0 else 1.0
    ibool = lambda v: 0.0 if v == 0.0 else 1.0

    @classmethod
    def build_terminal(cls, value):
        try:
            n = cls.terminals_table[value]
        except KeyError:
            cls.terminals_table[value] = n = cls(math.inf, None, None, value, authorized_caller=True)
        return n

    @classmethod
    def build_node(cls, index, low, high):
        """ builds a canonical node"""

        if low._id is high._id:
            return low

        key = (index, low._id, high._id)
        try:
            n = cls.nodes_table[key]
        except KeyError:
            # cls.terminals_table[key] = n = cls(index, low, high, None, authorized_caller=True)
            cls.nodes_table[key] = n = cls(index, low, high, None, authorized_caller=True)
        return n

    def stats(self, index=None):
        total = 0
        for n in self:
            if index is None or n._index == index:
                total += 1
        return total

    def __iter__(self):
        """
        Initialize and return an iterator for pyddlib.DD objects.
        :rtype: pyddlib.DD
        """
        self.__traversed = set()
        self.__fringe = [self]
        return self

    def __next__(self):
        """
        Implement a graph-based traversal algorithm for pyddlib.DD objects.
        Each vertex is visited exactly once. Low child is visited before
        high child. Return the next vertex in the sequence.
        :rtype: pyddlib.DD
        """
        if not self.__fringe:
            raise StopIteration()
        vertex = self.__fringe.pop()
        if not vertex.is_terminal():
            low  = vertex._low
            high = vertex._high
            if id(high) not in self.__traversed:
                self.__fringe.append(high)
                self.__traversed.add(id(high))
            if id(low) not in self.__traversed:
                self.__fringe.append(low)
                self.__traversed.add(id(low))
        return vertex

    @classmethod
    def self_apply(cls, v, op, cache=None):

        if cache is None:
            cache = {}
        #print("\nApply", op)
        return cls.self_apply_step(v, op, cache)

    @classmethod
    def self_apply_step(cls, v, op, cache):

        key = (op, v._id)
        u = cache.get(key)

        if u is not None:
            return u

        if v.is_terminal():
            val = op(v._value)
            return cls.constant(val)

        res = cls.build_node(
            v._index,
            cls.self_apply_step(v._low, op, cache),
            cls.self_apply_step(v._high, op, cache)
        )

        cache[key] = res
        return res


    @classmethod
    def apply(cls, v1, v2, op, cache=None):
        """
        Return a new canonical representation of the
        pyddlib.DD object for the result of `v1` `op` `v2`.
        :param v1: root vertex of left operand
        :type v1: pyddlib.DD
        :param v2: root vertex of right operand
        :type v2: pyddlib.DD
        :param op: a binary operator
        :type op: callable object or function
        :rtype: pyddlib.DD
        """
        if cache is None:
            cache = {}
        #print("\nApply", op)
        return cls._apply_step(v1, v2, op, cache)

    @classmethod
    def _apply_step(cls, v1, v2, op, cache): #, incr=""):
        """
        Recursively computes `v1` `op` `v2`. If the result was
        already computed as an intermediate result, it returns
        the cached result stored in `cache`.
        :param v1: root vertex of left operand
        :type v1: pyddlib.DD
        :param v2: root vertex of right operand
        :type v2: pyddlib.DD
        :param op: a binary operator
        :type op: callable object or function
        :param cache: cached intermediate results
        :type cache: dict( (int,int), pyddlib.DD )
        :rtype: pyddlib.DD
        """

        #print(inc + ("applystep\nv1 = " + str(v1) + "\nop = " + str(op) + "\nv2 = " + str(v2) + "\ncache = " + str(cache)).replace('\n', '\n' + inc))
        #print(incr, Color.get(f"In dd._apply_step :", Color.CBOLD, Color.CUNDERLINE), v1._id, v2._id)

        key = (op, v1._id, v2._id)
        u = cache.get(key)

        if u is not None:
            return u

        #print(v1._index, v2._index)

        index_min = min(v1._index, v2._index)
        if index_min == math.inf:
            result = cls.build_terminal(op(v1._value, v2._value))
        else:
            if v1._index == index_min:
                vlow1  = v1._low
                vhigh1 = v1._high
            else:
                vlow1 = vhigh1 = v1

            if v2._index == index_min:
                vlow2  = v2._low
                vhigh2 = v2._high
            else:
                vlow2 = vhigh2 = v2

            low  = cls._apply_step(vlow1, vlow2, op, cache) #, incr=incr+"|   ")
            high = cls._apply_step(vhigh1, vhigh2, op, cache) #, incr=incr+"|   ")
            result = cls.build_node(index_min, low, high)

        cache[key] = result
        #print(incr + ("|result = " + str(result)).replace('\n', '\n'+incr))
        return result

    def restrict(self, valuation, cache=None):
        """
        Return a new reduced ADD with variables in `valuation`.keys()
        restricted to `valuation`.values().
        :param valuation: mapping from variable index to boolean value
        :type valuation: dict(int,bool)
        :rtype: pyddlib.ADD
        """
        if cache is None:
            cache = {}

        @memoize(cache)
        def __restrict_step(n, current_mapping):
            """
            Return a new ADD with variables in `valuation`.keys()
            restricted to `valuation`.values().
            :param valuation: mapping from variable index to boolean value
            :type valuation: dict(int,bool)
            :rtype: pyddlib.ADD
            """

            for i, (index, val) in enumerate(current_mapping):
                if n._index == index:
                    new_mapping = current_mapping[i+1:]
                    return __restrict_step(n._high, new_mapping) if val else __restrict_step(n._low, new_mapping)

                elif index > n._index:
                    new_mapping = current_mapping[i:]
                    low = __restrict_step(n._low, new_mapping)
                    high = __restrict_step(n._high, new_mapping)
                    return self.__class__.build_node(n._index, low, high)

            return n

        mapping = list()
        for k in sorted(valuation.keys()):
            mapping.append((k, valuation[k]))
        res = __restrict_step(self, tuple(mapping))
        return res

    def __eq__(self, other):
        """
        Return True if both ADDs represent the same algebraic function.
        :param other: ADD
        :type other: pyddlib.ADD
        :rtype: bool
        """
        #result = self.apply(self, other, float.__eq__)
        #return result.is_terminal() and bool(result._value)
        return self is other
        #return result

    def __neq__(self, other):
        """
        Return True if both ADDs do not represent the same algebraic function.
        :param other: ADD
        :type other: pyddlib.ADD
        :rtype: bool
        """
        return not self is other

    def __hash__(self):
        return hash(self._id)

    def is_zero(self):
        return self._value == 0.0

    def is_one(self):
        return self._value == 1.0

    @classmethod
    def ite(cls, f, g, h, cache=None):
        """
        ite(f,g,h) = f . g + -f .h
        """
        if cache is None:
            cache = {}
        #print("f", f)
        #print("g", g)
        #print("h", h)

        left = cls.apply(f, g, cls.iand, cache=cache)
        #print("left", left)

        nf = cls.self_apply(f, cls.inot, cache=cache)
        #print("nf", nf)
        right = cls.apply(
                nf,
                h, cls.iand, cache=cache)
        #print("right", right)

        res = cls.apply(left, right, cls.ior, cache=cache)
        #print("res", res)
        return res

    def get_nodes_informations(self):
        counts = {}
        for n in self:
            if n._index not in counts.keys():
                counts[n._index] = 0
            counts[n._index] += 1
        return counts




