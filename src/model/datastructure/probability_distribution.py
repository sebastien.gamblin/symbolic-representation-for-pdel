
from src.model.epistemiclogic.formula.formula import *

class ProbabilityDistribution:

    def __init__(self, dict=None):
        self.dict = {}

        if not dict is None:
            for key, val in dict.items():
                self.addProba(key, val)

    def __repr__(self):
        return "("+ ",".join([f"{e.getName()}:{str(p)[0:5]}.." for e, p in self.dict.items()]) + ")"

    def __initVariable(self, variable) -> bool:
        if variable not in self.dict.keys():
            self.dict[variable] = 1 # by default
            return True
        return False

    def addVariable(self, variable):
        if not self.__initVariable(variable):
            raise Exception("Variable {} already in PD.")

    def addProba(self, variable, proba: float) -> None:
        self.__initVariable(variable)
        self.dict[variable] = proba

    def getProba(self, variable) -> float:
        self.__initVariable(variable)
        return self.dict[variable]

    def sumOf(self, variable):
        self.__initVariable(variable)
        return sum([val for key, val in self.dict.items()])

    def verify(self):
        for key, val in self.dict.items():
            if self.sumOf(key) != 1:
                return False
        return True



class ConditionalProbabilityDistribution():

    def __init__(self, preconditions:[Formula], pds:[ProbabilityDistribution]):
        self.preconditions = preconditions
        self.pds = pds

    def __repr__(self):
        string = "Preconditions :" + str(self.preconditions) + "\n"
        return string + "Distributions: " + str(self.pds)

    def getMyProba(self, ks, world, event):
        #print(world, event, ks.modelCheck(world, self.formula))
        find = None
        for i, formula in enumerate(self.preconditions):
            if ks.modelCheck(formula, world=world):
                #print("pd", self.pds[i].dict)
                #print("==> OK", world, event, formula, self.pds[i].getProba(event))
                if find is not None:
                    raise Exception(f"Multiples formulas in pairwise can be applyed on world {world}")
                find = self.pds[i].getProba(event)
        if find is None:
            raise Exception("No preconditions are applicable to the world {} and the event {}.".format(world, event))
        return find

    def getDatas(self):
        return [(self.preconditions[i], self.pds[i]) for i in range(0, len(self.pds))]

    def get_dict(self):
        return {self.preconditions[i]: self.pds[i].dict for i in range(0, len(self.pds))}

    def getPreconditions(self):
        return self.preconditions






class DoubleProbabilityDistribution:

    def __init__(self):
        self.dict = {}

    def __repr__(self):
        return " - " + " - ".join([f"{e.getName()}:({','.join([e2.getName()+':'+str(val)[0:6]+'..' for e2, val in p.items()])})"
                                     for e, p in self.dict.items()])

    def __initVariable(self, variable) -> bool:
        if variable not in self.dict.keys():
            self.dict[variable] = {}
            return True
        return False

    def __initSubVariable(self, variable, variable2) -> bool:
        self.__initVariable(variable)
        if variable not in self.dict[variable].keys():
            self.dict[variable][variable2] = 0
            return True
        return False

    def addVariable(self, variable):
        if not self.__initVariable(variable):
            raise Exception("Variable {} already in PD.")

    def addProba(self, variable, variable2, proba: float) -> None:
        self.__initVariable(variable)
        self.__initSubVariable(variable, variable2)
        self.dict[variable][variable2] = proba
        # print("new proba", variable, variable2, proba, self.dict[variable][variable2], self.getProba(variable, variable2))

    def getProba(self, variable, variable2) -> float:
        #self.__initVariable(variable)
        #self.__initSubVariable(variable, variable2)
        return self.dict[variable][variable2]

    def setProba(self, variable, variable2, proba: float) -> None:
        self.__initVariable(variable)
        self.__initSubVariable(variable, variable2)
        self.dict[variable][variable2] = proba

    def sumOf(self, variable):
        self.__initVariable(variable)
        return sum([val for key, val in self.dict[variable].items()])

    def check(self) -> bool:
        for variable in self.dict.keys():
            res = self.sumOf(variable)
            if not (0.99 < res < 1.01):
                raise Exception("Error in the probability distribution for variable {} : = {}".format(variable, res))
        return True

    def getSuccessors(self, variable):
        self.__initVariable(variable)
        return self.dict[variable].keys()



if __name__ == '__main__':

    pd = DoubleProbabilityDistribution()
    pd.addVariable("w1")
    pd.addProba("w1", "w2", 0.5)
    pd.addProba("w1", "w3", 0.2)

    print(pd.getProba("w1", "w2"))
    print(pd.sumOf("w1"))
