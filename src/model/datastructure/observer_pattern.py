
from abc import ABC, abstractmethod
from typing import List
from weakreflist import WeakList


class Observer:

    pass


class Subject(ABC):
    """
    The Subject interface declares a set of methods for managing subscribers.
    """

    #_observers: List[Observer] = []
    """
    List of subscribers. 
    """
    def __init__(self):
        self.observers = WeakList()

    def attach(self, observer: Observer) -> None:
        """
        Attach an observer to the subject.
        """
        # print("Subject: Attached an observer :", observer, len(self._observers))
        if observer not in self.observers:
            # print("New observer", observer)
            self.observers.append(observer)


    def detach(self, observer: Observer) -> None:
        """
        Detach an observer from the subject.
        """
        self.observers.remove(observer)

    def notify(self) -> None:
        """
        Trigger an update in each subscriber.
        """
        print("Subject: Notifying observers...")
        for observer in self.observers:
            observer.update(self)


class Observer(ABC):
    """
    The Observer interface declares the update method, used by subjects.
    """

    @abstractmethod
    def update(self, subject: Subject) -> None:
        """
        Receive update from subject.
        """
        pass


class ConcreteSubject(Subject):
    """
    Example of Subject which reimplement notify()
    """

    def some_business_logic(self) -> None:
        """
        Usually, the subscription logic is only a fraction of what a Subject can
        really do. Subjects commonly hold some important business logic, that
        triggers a notification method whenever something important is about to
        happen (or after it).
        """
        print(f"Subject: My state has just changed")
        self.notify()


class ConcreteObserverA(Observer):
    """
    Example of Observer which reimplement update()
    """
    def update(self, subject: Subject) -> None:
        print("ConcreteObserverA: Reacted to the event")


class ConcreteObserverB(Observer):
    """
    Example of Observer which reimplement update()
    """
    def update(self, subject: Subject) -> None:
        print("ConcreteObserverB: Reacted to the event")


if __name__ == "__main__":
    # The client code.

    subject = ConcreteSubject()

    observer_a = ConcreteObserverA()
    subject.attach(observer_a)

    observer_b = ConcreteObserverB()
    subject.attach(observer_b)

    subject.some_business_logic()
    subject.some_business_logic()

    subject.detach(observer_a)

    subject.some_business_logic()