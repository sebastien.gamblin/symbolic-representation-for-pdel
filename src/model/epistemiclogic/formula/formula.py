"""Modal logic formula module

This module unites all operators from propositional and modal logic.
"""

from abc import ABCMeta, abstractmethod
from src.config import *
#from src.model.epistemiclogic.eventmodel.event_model_interface import EventModelInterface

from typing import List, Tuple

import random

class Formula(metaclass=ABCMeta):

    @abstractmethod
    def __init__(self):
        pass

    @abstractmethod
    def getFormulas(self):
        pass

    def __repr__(self):
        pass


    @abstractmethod
    def isPropositionnal(self):
        pass

    @abstractmethod
    def rename(self, dico):
        pass

    """
    Take a empty list and return this list with variables
    Caution : can have duplicate variables
    """
    @abstractmethod
    def getVariables(self):
        pass

    @staticmethod
    def getRandom(agents, vars, depth, rng=None, seed:int=None, proba=True, complexity=1, S5=True):

        # ops = ["<", "<=", ">", ">="]
        ops = [">="]

        if rng is None:
            rng = random.Random(seed)
        if seed is None:
            seed = rng.randrange(100000000)
        rng.seed(seed)

        if depth == 0:
            return Formula.get_random_propositional(vars, complexity, rng=rng, seed=seed)

        safety_tries = 10
        def loop(branchs):
            nonlocal safety_tries
            #print(safety_tries)
            safety_tries -= 1
            if safety_tries == 0 or branchs == 0:
                return Formula.get_random_propositional(vars, complexity, rng=rng, seed=seed)

            c = [0, 1, 2] if proba else [0, 2]
            r = rng.choice(c)
            if r == 0:
                if S5:
                    return K(rng.choice(agents), loop(branchs - 1))
                else:
                    return Box_a(rng.choice(agents), loop(branchs - 1))
            elif r == 1:
                return Pr(
                    rng.choice(agents),
                    loop(branchs - 1),
                    rng.choice(ops),
                    #rng.choice([">", ">=", "<", "<="]),
                    rng.uniform(0, 1))
            else:
                r = rng.choice([0, 1, 2])
                if r == 0:
                    return And(Formula.get_random_propositional(vars, rng.choice([1, 2, 3]), rng=rng, seed=seed), loop(branchs - 1))
                elif r == 1:
                    return Or(Formula.get_random_propositional(vars, rng.choice([1, 2, 3]), rng=rng, seed=seed), loop(branchs - 1))
                else:
                    return Not(loop(branchs -1))

        return loop(depth)

    @staticmethod
    def get_random_propositional(vars, nb_branchs, rng=None, seed:int=None):

        if rng is None:
            rng = random.Random(seed)
        if seed is None:
            seed = rng.randrange(100000000)
        rng.seed(seed)

        safety_tries = 10
        def loop(branchs, choice_list):
            nonlocal safety_tries
            safety_tries -= 1
            #print(safety_tries)
            if safety_tries < 0 or branchs == 0:
                return Atom(rng.choice(vars))
            #print("<", safety_tries)
            r = rng.choice(choice_list)
            if r == 0:
                return And(loop(branchs - 1, [0,1,2]), loop(branchs - 1, [0,1,2]))
            elif r == 1:
                return Or(loop(branchs - 1, [0,1,2]), loop(branchs - 1, [0,1,2]))
            else:
                return Not(loop(branchs - 1, [0,1]))

        return loop(nb_branchs, [0,1,2])

    @abstractmethod
    def conditionning(self, formula, dico: dict):
        raise NotImplementedError

    @abstractmethod
    def replace_operator(self, operator, new_operator):
        raise NotImplementedError

    def __or__(self, other):
        """ a | b """
        return Or(self, other)

    def __and__(self, other):
        """ a & b """
        return And(self, other)

    def __xor__(self, other):
        """ a ^ b """
        return Xor(self, other)

    def __neg__(self):
        """ - a : inversion = not a """
        return Not(self)

    def __invert__(self):
        """ ~ a : inversion = not a """
        return Not(self)

    def __gt__(self, other):
        """ a > b : implication = a -> b """
        return Implies(self, other)

    def __lt__(self, other):
        """ a < b : implication = b -> a """
        return Implies(other, self)

    def __lshift__(self, other):
        """ a >> b : equivalence = a <-> b """
        return Equiv(self, other)

    def __rshift__(self, other):
        """ a << b : equivalence = a <-> b """
        return Equiv(self, other)

__rewrite_list = [
    ("<->", '>>'),
    ('xor', '^'),
    ('not', '~'),
    ('->', '>'),
    ('<-', '<'),
    ('and', "&"),
    ('or', "|")
]



class Atom(Formula):
    """
    This class represents propositional logic variables in modal logic formulas.
    """

    used = set()

    def __init__(self, inner:str):
        assert isinstance(inner, str), "An atom is a string. Not {}".format(type(inner))
        self.inner = inner
        super().__init__()
        Atom.used.add(inner)

    def getFormulas(self) -> str:
        return self.inner

    def __repr__(self):
        return f"'{self.inner}'"

    def __hash__(self):
        return hash(("atom", self.inner))

    def __eq__(self, other):
        return isinstance(other, Atom) and self.inner == other.inner

    def isPropositionnal(self):
        return True

    def semantic(self, ks, state_to_test):
        return bool(state_to_test[self.inner]) if self.inner in state_to_test.keys() else False

    def rename(self, dico):
        if self.inner in dico:
            return Atom(dico[self.inner])
        else:
            return Atom(self.inner)

    def getVariables(self):
        return [self.inner]

    def conditionning(self, dico: dict):
        if self.inner in dico:
            return Top() if dico[self.inner] else Bot()
        return self.conditionning(dico)

    def replace_operator(self, operator, new_operator):
        return Atom(self.inner)


class Box(Formula):
    """
    Describes box operator of modal logic formula and it's semantics for Agent a
    """

    def __init__(self, inner):
        self.inner = inner

    def getFormulas(self):
        return self.inner

    def __repr__(self):
        if isinstance(self.inner, Atom):
            return u"\u2610" + str(self.inner)
        else:
            return u"\u2610" + "(" + str(self.inner) + ")"

    def __hash__(self):
        return hash("box", self.inner)

    def __eq__(self, other):
        return isinstance(other, Box) and self.inner == other.inner

    def isPropositionnal(self):
        return False

    def rename(self, dico):
        return Box(self.inner.rename(dico))

    def getVariables(self):
        return self.inner.getVariables()

    def conditionning(self, dico: dict):
        return Box(self.inner.conditionning(dico))

    def replace_operator(self, operator, new_operator):
        if isinstance(self, operator):
            assert new_operator.__name__ in ["Diamond", "Box", "Not"]
            return new_operator(self.inner.replace_operator(operator, new_operator))
        return Box(self.inner.replace_operator(operator, new_operator))


class Box_a(Formula):
    """
    Describes box operator of modal logic formula and it's semantics for Agent a
    """

    def __init__(self, agent, inner):
        assert isinstance(inner, Formula), f"{inner} need to be a formula, not a {type(inner)}"
        assert isinstance(agent, str)
        self.inner = inner
        self.agent = agent

    def getFormulas(self):
        return self.inner

    def getAgent(self):
        return self.agent

    def __repr__(self):
        if isinstance(self.inner, Atom):
            return u"\u2610" + "[" + str(self.agent) + "]" + str(self.inner)
        else:
            return u"\u2610" + "[" + str(self.agent) + "](" + str(self.inner) + ")"

    def __hash__(self):
        return hash(("boxa", self.agent, self.inner))

    def __eq__(self, other):
        return isinstance(other, Box_a) and \
               self.agent == other.agent and self.inner == other.inner

    def isPropositionnal(self):
        return False

    def rename(self, dico):
        return Box_a(self.agent, self.inner.rename(dico))

    def getVariables(self):
        return self.inner.getVariables()

    def conditionning(self, dico: dict):
        return Box_a(self.agent, self.inner.conditionning(dico))

    def replace_operator(self, operator, new_operator):
        if isinstance(self, operator):
            assert new_operator.__name__ in ["Diamond_a", "Box_a", "K", "nK"]
            return new_operator(self.agent, self.inner.replace_operator(operator, new_operator))
        return Box_a(self.agent, self.inner.replace_operator(operator, new_operator))


"""
class K(Box_a):

    def __str__(self):
        if isinstance(self.inner, Atom):
            return "K[" + str(self.agent) + "]" + str(self.inner)
        else:
            return "K[" + str(self.agent) + "](" + str(self.inner) + ")"

    def conditionning(self, dico: dict):
        return K(self.agent, self.inner.conditionning(dico))
"""

class K(Formula):
    """
    Describes Know operator of modal logic formula and it's semantics for Agent a
    """

    def __init__(self, agent, inner):
        assert isinstance(inner, Formula)
        assert isinstance(agent, str)
        self.inner = inner
        self.agent = agent

    def getFormulas(self):
        return self.inner

    def getAgent(self):
        return self.agent

    def __str__(self):
        if isinstance(self.inner, Atom):
            return "K[" + str(self.agent) + "]" + str(self.inner)
        else:
            return "K[" + str(self.agent) + "](" + str(self.inner) + ")"

    def __hash__(self):
        return hash(("k", self.agent, self.inner))

    def __eq__(self, other):
        return isinstance(other, K) and \
               self.agent == other.agent and self.inner == other.inner

    def isPropositionnal(self):
        return False

    def rename(self, dico):
        return K(self.agent, self.inner.rename(dico))

    def getVariables(self):
        return self.inner.getVariables()

    def conditionning(self, dico: dict):
        return K(self.agent, self.inner.conditionning(dico))

    def replace_operator(self, operator, new_operator):
        if isinstance(self, operator):
            assert new_operator.__name__ in ["Diamond_a", "Box_a", "K", "nK", "K_hat"]
            return new_operator(self.agent, self.inner.replace_operator(operator, new_operator))
        return K(self.agent, self.inner.replace_operator(operator, new_operator))


class nK(Formula):

    def __init__(self, agent, inner):
        self.inner = inner
        self.agent = agent

    def getFormulas(self):
        return self.inner

    def getAgent(self):
        return self.agent

    def semantic(self, ks, state):
        i = self.agent
        p = self.inner
        return And(Not(K(i, Not(Atom(p)))), Not(K(i, Not(Not(Atom(p)))))).semantic(ks, state)

    def __str__(self):
        if isinstance(self.inner, Atom):
            return "~K[" + str(self.agent) + "]" + str(self.inner)
        else:
            return "~K[" + str(self.agent) + "](" + str(self.inner) + ")"

    def __eq__(self, other):
        return isinstance(other, nK) and self.agent == other.agent \
               and self.inner == other.inner

    def isPropositionnal(self):
        return True

    def rename(self, dico):
        return nK(self.agent, self.inner.rename(dico))

    def getVariables(self):
        return self.inner.getVariables()

    def conditionning(self, dico: dict):
        return nK(self.agent, self.inner.conditionning(dico))

    def replace_operator(self, operator, new_operator):
        if isinstance(self, operator):
            assert new_operator.__name__ in ["Diamond_a", "Box_a", "K", "nK", "K_hat"]
            return new_operator(self.agent, self.inner.replace_operator(operator, new_operator))
        return nK(self.agent, self.inner.replace_operator(operator, new_operator))


class Diamond_a(Formula):
    """
    Describes diamond operator of modal logic formula and it's semantics for Agent a
    """

    def __init__(self, agent, inner):
        self.inner = inner
        self.agent = agent

    def getFormulas(self):
        return self.inner

    def getAgent(self):
        return self.agent

    def __repr__(self):
        if isinstance(self.inner, Atom):
            return "<>[" + str(self.agent) + "]" + str(self.inner)
        else:
            return "<>[" + "[" + str(self.agent) + "](" + str(self.inner) + ")"

    def __eq__(self, other):
        return isinstance(other, Diamond_a) and self.agent == other.agent \
               and self.inner == other.inner

    def isPropositionnal(self):
        return False

    def rename(self, dico):
        return Diamond_a(self.agent, self.inner.rename(dico))

    def getVariables(self):
        return self.inner.getVariables()

    def conditionning(self, dico: dict):
        return Diamond_a(self.agent, self.inner.conditionning(dico))

    def replace_operator(self, operator, new_operator):
        if isinstance(self, operator):
            assert new_operator.__name__ in ["Diamond_a", "Box_a", "K", "nK", "K_hat"]
            return new_operator(self.agent, self.inner.replace_operator(operator, new_operator))
        return Diamond_a(self.agent, self.inner.replace_operator(operator, new_operator))


class K_hat(Formula):

    def __init__(self, agent, inner):
        self.inner = inner
        self.agent = agent

    def getFormulas(self):
        return self.inner

    def getAgent(self):
        return self.agent

    def __repr__(self):
        if isinstance(self.inner, Atom):
            return "K^[" + str(self.agent) + "]" + str(self.inner)
        else:
            return "K^[" + str(self.agent) + "](" + str(self.inner) + ")"

    def __hash__(self):
        return hash(("Kh", self.agent, self.inner))

    def __eq__(self, other):
        return isinstance(other, K_hat) and self.agent == other.agent \
               and self.inner == other.inner

    def rename(self, dico):
        return K_hat(self.agent, self.inner.rename(dico))

    def getVariables(self):
        return self.inner.getVariables()

    def isPropositionnal(self):
        return False

    def conditionning(self, dico: dict):
        return K_hat(self.agent, self.inner.conditionning(dico))

    def replace_operator(self, operator, new_operator):
        if isinstance(self, operator):
            assert new_operator.__name__ in ["Diamond_a", "Box_a", "K", "nK", "K_hat"]
            return new_operator(self.agent, self.inner.replace_operator(operator, new_operator))
        return K_hat(self.agent, self.inner.replace_operator(operator, new_operator))


def Kw(agent, formula):
    """
    Know whether
    :param agent:
    :param formula:
    :return:
    """
    return Or(K(agent, formula), K(agent, Not(formula)))

def Boxw(agent, formula):
    """
    Belief whether
    :param agent:
    :param formula:
    :return:
    """
    return Or(Box_a(agent, formula), Box_a(agent, Not(formula)))



class Diamond(Formula):
    """
    Describes diamond operator of modal logic formula and it's semantics for Agent a
    """

    def __init__(self, inner):
        self.inner = inner

    def __repr__(self):
        return self.__str__()

    def __str__(self):
        if isinstance(self.inner, Atom):
            return "<>" + str(self.inner)
        else:
            return "<>(" + str(self.inner) + ")"

    def __eq__(self, other):
        return isinstance(other, Diamond) and self.inner == other.inner

    def isPropositionnal(self):
        return False

    def rename(self, dico):
        return Diamond(self.inner.rename(dico))

    def getVariables(self):
        return self.inner.getVariables()

    def conditionning(self, dico: dict):
        return Diamond(self.inner.conditionning(dico))

    def replace_operator(self, operator, new_operator):
        if isinstance(self, operator):
            assert new_operator.__name__ in ["Diamond", "Box", "Not"]
            return new_operator(self.inner.replace_operator(operator, new_operator))
        return Diamond(self.inner.replace_operator(operator, new_operator))


class Implies(Formula):
    """
    Describes implication derived from classic propositional logic
    """

    def __init__(self, left, right):
        self.left = left
        self.right = right

    def getFormulas(self):
        return [self.left, self.right]

    def semantic(self, ks, state_to_test):
        return not self.left.semantic(ks, state_to_test) or self.right.semantic(ks, state_to_test)

    def __repr__(self):
        return "(" + self.left.__str__() + " -> " + self.right.__str__() + ")"

    def __hash__(self):
        return hash(("implies", self.left, self.right))

    def __eq__(self, other):
        return isinstance(other, Implies) and self.left == other.left \
               and self.right == other.right

    def isPropositionnal(self):
        return self.left.isPropositionnal() and self.right.isPropositionnal()

    def rename(self, dico):
        return Implies(self.left.rename(dico), self.right.rename(dico))

    def getVariables(self):
        return self.left.getVariables() + self.right.getVariables()

    def conditionning(self, dico: dict):
        return Implies(self.left.conditionning(dico), self.right.conditionning(dico))

    def replace_operator(self, operator, new_operator):
        left = self.left.replace_operator(operator, new_operator)
        right = self.right.replace_operator(operator, new_operator)
        if isinstance(self, operator):
            assert new_operator.__name__ in ["Implies", "And", "Or", "Equiv"]
            return new_operator(left, right)
        return Implies(left, right)



class Equiv(Formula):
    """
    Describes implication derived from classic propositional logic
    """

    def __init__(self, left, right):
        self.left = left
        self.right = right

    def semantic(self, ks, state_to_test):
        return And(Implies(self.left, self.right), Implies(self.right, self.left)).semantic(ks, state_to_test)

    def getFormulas(self):
        return [self.left, self.right]

    def __repr__(self):
        return "(" + self.left.__str__() + " <-> " + self.right.__str__() + ")"

    def __hash__(self):
        return hash(("equiv", self.left, self.right))

    def __eq__(self, other):
        return isinstance(other, Equiv) and self.left == other.left \
               and self.right == other.right

    def isPropositionnal(self):
        return self.left.isPropositionnal() and self.right.isPropositionnal()

    def rename(self, dico):
        return Equiv(self.left.rename(dico), self.right.rename(dico))

    def getVariables(self):
        return self.left.getVariables() + self.right.getVariables()

    def conditionning(self, dico: dict):
        return Equiv(self.left.conditionning(dico), self.right.conditionning(dico))

    def replace_operator(self, operator, new_operator):
        left = self.left.replace_operator(operator, new_operator)
        right = self.right.replace_operator(operator, new_operator)
        if isinstance(self, operator):
            assert new_operator.__name__ in ["Implies", "And", "Or", "Equiv"]
            return new_operator(left, right)
        return Equiv(left, right)


class Not(Formula):
    """
    Describes negation derived from classic propositional logic
    """

    def __init__(self, inner):
        self.inner = inner

    def semantic(self, ks, state_to_test):
        return not self.inner.semantic(ks, state_to_test)

    def getFormulas(self):
        return self.inner

    def __repr__(self):
        return u"\uFFE2" + "(" + str(self.inner) + ")"

    def __hash__(self):
        return hash(("not", self.inner))

    def __eq__(self, other):
        return isinstance(other, Not) and self.inner == other.inner

    def isPropositionnal(self):
        return self.inner.isPropositionnal()

    def rename(self, dico):
        return Not(self.inner.rename(dico))

    def getVariables(self):
        return self.inner.getVariables()

    def conditionning(self, dico: dict):
        return Not(self.inner.conditionning(dico))

    def replace_operator(self, operator, new_operator):
        if isinstance(self, operator):
            assert new_operator.__name__ in ["Diamond", "Box", "Not"]
            return new_operator(self.inner.replace_operator(operator, new_operator))
        raise Exception(f"Do you want to replace {operator.__name__} by {new_operator.__name__} ?")


class And(Formula):
    """
    Describes and derived from classic propositional logic
    """

    def __init__(self, left, right):
        self.left = left
        self.right = right

    def semantic(self, ks, state_to_test):
        return self.left.semantic(ks, state_to_test) and self.right.semantic(ks, state_to_test)

    def getFormulas(self):
        return [self.left, self.right]

    def __repr__(self):
        return "(" + self.left.__str__() + " " + u"\u2227" + " " + self.right.__str__() + ")"

    def __hash__(self):
        return hash( ("and", self.left, self.right) )

    def __eq__(self, other):
        return isinstance(other, And) and self.left == other.left \
               and self.right == other.right

    def isPropositionnal(self):
        return self.left.isPropositionnal() and self.right.isPropositionnal()

    def rename(self, dico):
        return And(self.left.rename(dico), self.right.rename(dico))

    def getVariables(self):
        l = self.left.getVariables()
        r = self.right.getVariables()
        return l + r

    def conditionning(self, dico: dict):
        return And(self.left.conditionning(dico), self.right.conditionning(dico))

    def replace_operator(self, operator, new_operator):
        left = self.left.replace_operator(operator, new_operator)
        right = self.right.replace_operator(operator, new_operator)
        if isinstance(self, operator):
            assert new_operator.__name__ in ["Implies", "And", "Or", "Equiv"]
            return new_operator(left, right)
        return And(left, right)


def MAnd(*args):
    return AndOfList(args)

def MOr(*args):
    return OrOfList(args)


class Or(Formula):
    """
    Describes or derived from classic propositional logic
    """

    def __init__(self, left, right):
        self.left = left
        self.right = right

    def getFormulas(self):
        return [self.left, self.right]

    def semantic(self, ks, state_to_test):
        return self.left.semantic(ks, state_to_test) or self.right.semantic(ks, state_to_test)

    def __repr__(self):
        return "(" + self.left.__str__() + " " + u"\u2228" + " " + self.right.__str__() + ")"

    def __hash__(self):
        return hash(("or", self.left, self.right))

    def __eq__(self, other):
        return isinstance(other, Or) and self.left == other.left \
               and self.right == other.right

    def isPropositionnal(self):
        return self.left.isPropositionnal() and self.right.isPropositionnal()

    def rename(self, dico):
        return Or(self.left.rename(dico), self.right.rename(dico))

    def getVariables(self):
        return self.left.getVariables() + self.right.getVariables()

    def conditionning(self, dico: dict):
        return Or(self.left.conditionning(dico), self.right.conditionning(dico))

    def replace_operator(self, operator, new_operator):
        left = self.left.replace_operator(operator, new_operator)
        right = self.right.replace_operator(operator, new_operator)
        if isinstance(self, operator):
            assert new_operator.__name__ in ["Implies", "And", "Or", "Equiv"]
            return new_operator(left, right)
        return Or(left, right)


class ite(Formula):

    def __init__(self, x, left, right):
        self.x = x
        self.left = left
        self.right = right

    def getFormulas(self):
        return [self.x, self.left, self.right]

    def __repr__(self):
        return "ite({}, {}, {})".format(self.x, self.left.__str__(), self.right.__str__())

    def __hash__(self):
        return hash(("ite", self.x, self.left, self.right))

    def __eq__(self, other):
        return isinstance(other, ite) and self.left == other.left \
               and self.right == other.right and self.x == other.x

    def isPropositionnal(self):
        return self.left.isPropositionnal() and self.right.isPropositionnal()

    def rename(self, dico):
        return ite(self.inner.rename(dico), self.left.rename(dico), self.right.rename(dico))

    def getVariables(self):
        return self.x.getVariables() + self.left.getVariables() + self.right.getVariables()

    def conditionning(self, dico: dict):
        return ite(self.x.conditionning(self.x), self.left.conditionning(dico), self.right.conditionning(dico))

    def replace_operator(self, operator, new_operator):
        return ite(
            self.x.replace_operator(operator, new_operator),
            self.left.replace_operator(operator, new_operator),
            self.right.replace_operator(operator, new_operator))



class Exactly(Formula):

    def __init__(self, number: int, array: [str]):
        seen = set()
        for x in array:
            assert not x in seen, f"Duplicate in list : {x}"
            seen.add(x)

        self.n = number
        self.array = array

    def getFormulas(self):
        return None

    def __repr__(self):
        return "exactly({}, {})".format(self.n, self.array)

    def __hash__(self):
        return hash(("exact", self.n, self.n))

    def __eq__(self, other):
        return isinstance(other, Exactly) and self.n == other.n \
               and sorted(self.array) == sorted(other.array)

    def getVariables(self):
        return self.array

    def isPropositionnal(self):
        return True

    def rename(self, dico):
        return Exactly(self.n, [dico[f] for f in self.array])

    def semantic(self, ks, state_to_test):
        return sum([v for k,v in state_to_test.items() if k in self.array]) == self.n

    def conditionning(self, dico: dict):
        return NotImplementedError

    def replace_operator(self, operator, new_operator):
        return Exactly(self.n, self.array)




class Pr(Formula):

    def __init__(self, agent:str, inner, op, k):
        self.agent = agent
        self.inner = inner
        self.op = op
        self.k = k

    def getFormulas(self):
        return [self.inner]

    def getAgent(self):
        return self.agent

    def __repr__(self):
        if isinstance(self.inner, Atom):
            return "Pr[" + str(self.agent) + "]" + str(self.inner) + self.op + str(self.k)
        else:
            return "Pr[" + str(self.agent) + "](" + str(self.inner) + ")" + self.op + str(self.k)

    def __hash__(self):
        return hash(("Pr", self.agent, self.inner, self.op, self.k))

    def __eq__(self, other):
        return isinstance(other, Pr) and self.agent == other.agent \
               and self.inner == other.inner and self.op == other.op and self.k == other.k

    def isPropositionnal(self):
        return False

    def rename(self, dico):
        return Pr(self.agent, self.inner.rename(dico), self.op, self.k)

    def getVariables(self):
        return self.inner.getVariables()

    def conditionning(self, dico: dict):
        return Pr(self.agent, self.inner.conditionning(dico), self.op, self.k)

    def replace_operator(self, operator, new_operator):
        return Pr(self.agent, self.inner.replace_operator(operator, new_operator), self.op, self.k)


class MPr(Formula):
    """
    For PDEL Formula as :

    a1.Pr_a(phi1) + a2.Pr_a(phi2) >= beta
    """

    def __init__(self, agent: str, inner_list: List[Tuple[float, Formula]], op, beta):

        self.agent = agent
        self.inner_list = inner_list
        for a, phi in self.inner_list:
            assert 0 <= a <= 1 and isinstance(phi, Formula)

        self.op = op
        self.beta = beta

    def __hash__(self):
        innerhash = sum([hash(i)*a for a, i in self.inner_list])
        return hash(("MPr", self.agent, innerhash, self.op, self.beta))

    def __eq__(self, other):
        return isinstance(other, MPr) and self.agent == other.agent \
               and frozenset(self.inner_list) == frozenset(other.inner_list) and self.op == other.op and self.beta == other.beta

    def getFormulas(self):
        return self.inner_list

    def __repr__(self):
        def get_inner(t):
            return f"{t[0]}xPr[{self.agent}]({t[1]})"
        return f"{'+'.join([get_inner(t) for t in self.inner_list])} {self.op} {self.beta}"


    def isPropositionnal(self):
        return False

    def rename(self, dico):
        return MPr(self.agent, [(a,b.rename(dico)) for a,b in self.inner_list], self.op, self.beta)

    def getVariables(self):
        res = []
        for a, inner in self.inner_list:
            for v in inner.getVariables():
                res += v
        return res

    def conditionning(self, dico: dict):
        return MPr(self.agent, [(a, b.conditionning(dico)) for a,b in self.inner_list], self.op, self.beta)

    def replace_operator(self, operator, new_operator):
        return Pr(self.agent,
                  [f.replace_operator(operator, new_operator) for f in self.inner_list],
                  self.op, self.beta)



class Top(Formula):

    def __init__(self):
        self.inner = True

    def getFormulas(self):
        return []

    def isPropositionnal(self):
        return True

    def rename(self, dico):
        return Top()

    def semantic(self, ks, state_to_test):
        return True

    def __hash__(self):
        return hash("T")

    def __eq__(self, other):
        return isinstance(other, Top)

    def __repr__(self):
        return "T"

    def getVariables(self):
        return []

    def conditionning(self, dico: dict):
        return Top()

    def replace_operator(self, operator, new_operator):
        return Top()


class Bot(Formula):

    def __init__(self):
        inner = False

    def getFormulas(self):
        return []

    def isPropositionnal(self):
        return True

    def rename(self, dico):
        return Bot()

    def semantic(self, ks, state_to_test):
        return False

    def __hash__(self):
        return hash("B")

    def __eq__(self, other):
        return isinstance(other, Bot)

    def __repr__(self):
        return "F"

    def getVariables(self):
        return []

    def conditionning(self, dico: dict):
        return Bot()

    def replace_operator(self, operator, new_operator):
        return Bot()


class After(Formula):

    def __init__(self, event, formula: Formula):
        self.event = event
        self.formula = formula

    def getFormulas(self):
        return [self.event, self.formula]

    def isPropositionnal(self):
        return False

    def rename(self, dico):
        return After(self.event, self.formula.rename(dico))

    def __hash__(self):
        return hash((self.event, self.formula))

    def __eq__(self, other):
        if not isinstance(other, After):
            return False
        return self.event == other.event and self.formula == other.formula

    def getVariables(self):
        return self.formula.getVariables()

    def __repr__(self):
        return f"After[{self.event}]({self.formula})"

    def conditionning(self, dico: dict):
        return After(self.event, self.formula.conditionning(dico))

    def replace_operator(self, operator, new_operator):
        return After(self.event, self.formula.replace_operator(operator, new_operator))


class Announcement(Formula):

    def __init__(self, announcement: Formula, formula: Formula, group=None):
        self.announcement = announcement
        self.formula = formula
        self.group = group

    def getFormulas(self):
        return [self.announcement, self.formula, self.group]

    def isPropositionnal(self):
        return False

    def rename(self, dico):
        return Announcement(self.announcement.rename(dico), self.formula.rename(dico), self.group)

    def __hash__(self):
        return hash((self.announcement, self.formula, self.group))

    def __eq__(self, other):
        if not isinstance(other, Announcement):
            return False
        return self.annoucement == other.annoucement and \
               self.formula == other.formula and \
               self.group == other.group

    def getVariables(self):
        return self.formula.getVariables() + self.annoucement.getVariables()

    def __repr__(self):
        grp = "" if self.group is None else f"_{self.group}"
        return f"[{self.annoucement}]{grp}({self.formula})"

    def conditionning(self, dico: dict):
        return Announcement(self.announcement, self.formula.conditionning(dico), self.group)

    def replace_operator(self, operator, new_operator):
        return Announcement(self.announcement.replace_operator(operator, new_operator),
                            self.formula.replace_operator(operator, new_operator),
                            self.group)


"""
Return list of true or false (thanks to 'pos') variables as formula
"""
def AndOfVarList(varlist: list, pos=True):

    if isinstance(varlist, list):
        formula = Top()
        for var in varlist:
            if pos:
                formula = And(formula, Atom(var))
            else:
                formula = And(formula, Not(Atom(var)))
        return formula
    if isinstance(varlist, dict):
        formula = Top()
        for var, value in varlist.items():
            f = Atom(var) if value else Not(Atom(var))
            formula = And(formula, f)
        return formula
    raise Exception(f"AndOfList not generated with {varlist}")


def OrOfVarList(varlist: list, pos=True):
    if isinstance(varlist, list):
        formula = Bot()
        for var in varlist:
            if pos:
                formula = Or(formula, Atom(var))
            else:
                formula = Or(formula, Not(Atom(var)))
        return formula
    if isinstance(varlist, dict):
        formula = Bot()
        for var, value in varlist.items():
            f = Atom(var) if value else Not(Atom(var))
            formula = Or(formula, f)
        return formula
    raise Exception("OrOfList not generated.")

def AndOfList(formulalist: list):
    if len(formulalist) == 0:
        return Top()
    elif len(formulalist) == 1:
        return formulalist[0]

    formula = formulalist[0]
    for f in formulalist[1:]:
        assert isinstance(f, Formula)
        formula = And(formula, f)
    return formula

def AndOf(*args):
    # list of formulas
    return AndOfList([arg for arg in args])

def OrOfList(formulalist: list):
    if len(formulalist) == 0:
        return Bot()
    elif len(formulalist) == 1:
        return formulalist[0]

    formula = formulalist[0]
    for f in formulalist[1:]:
        assert isinstance(f, Formula), f"Error in OrOfList : {type(f).__name__} isn't a formula."
        formula = Or(formula, f)
    return formula

def OrOf(*args):
    # list of formulas
    return OrOfList([arg for arg in args])

def renameInPrime(formula, maplist):
    vars = formula.getVariables()
    #variables = list(set(formula.getVariables([])))

    return formula.rename({var: maplist[var] for var in vars})

def Xor(x, y):
    assert isinstance(x, Formula) and isinstance(y, Formula)
    return Or(And(Not(x), y), And(x, Not(y)))

def MXor(*args):
    assert len(args) > 1
    begin = Xor(args[0], args[1])
    for arg in args[2:]:
        begin = Xor(begin, arg)
    return begin


def __get_var(i):
    return f"x{i}"

def __var_name(i, p=False):
    return getPrimedVariable(__get_var(i)) if p else __get_var(i)

def getXiequivXip(nb_vars):
    """
    type=1 : AND_i(xi <-> yi)
    type=2 : AND_i xi <-> AND_i yi
    ordonned=0: [xi, ..., yi,...]
    ordonned=1: [xi, yi, ...]
    :param nb_vars:
    :param type:
    :param ordonned:
    :return:
    """

    ordo = [__var_name(i) for i in range(nb_vars)] + [__var_name(i, p=True) for i in range(nb_vars)]
    nordo = [item for sublist in [(__var_name(i), __var_name(i, p=True)) for i in range(nb_vars)] for item in sublist]

    form = Top()
    for i in range(0, nb_vars):
        form = And(form, Equiv(Atom(__var_name(i)), Atom(__var_name(i, p=True))))

    return form, nordo, ordo

def getAndXiEquivAndXip(nb_vars):

    ordo = [__var_name(i) for i in range(nb_vars)] + [__var_name(i, p=True) for i in range(nb_vars)]
    nordo = [item for sublist in [(__var_name(i), __var_name(i, p=True)) for i in range(nb_vars)] for item in sublist]


    form = Top()
    for i in range(0, nb_vars):
        form = And(form, Atom(__var_name(i)))

    form2 = Top()
    for i in range(0, nb_vars):
        form2= And(form2, Atom(__var_name(i, p=True)))

    return Equiv(form, form2), ordo, nordo



def get_frame_formula(xvars, prime=False):

    if prime:
        fct = lambda x: Equiv(Atom(getPrimedVariable(x)), Atom(getPrimedVariable(getAfterVariable(x))))
    else:
        fct = lambda x: Equiv(Atom(x), Atom(getAfterVariable(x)))

    for var in xvars:
        assert not getAfterSymbol() in var, f"{getAfterSymbol()} in var {var}"
        assert not getPrimeSymbol() in var, f"{getPrimeSymbol()} in var {var}"

        #formula = And(formula, fct(var))
    return AndOfList([fct(var) for var in xvars])
    #return formula


def get_swap_formula(var1, var2, prime=False):
    if not prime:
        a1, a2 = Atom(var1), Atom(var2)
        na1, na2 = Not(a1), Not(a2)
        pa1, pa2 = Atom(getAfterVariable(var1)), Atom(getAfterVariable(var2))
        npa1, npa2 = Not(pa1), Not(pa2)
        return And(Equiv(a1, npa1), Equiv(na2, pa2))
    else:
        a1, a2 = Atom(getPrimedVariable(var1)), Atom(getPrimedVariable(var2))
        na1, na2 = Not(a1), Not(a2)
        pa1, pa2 = Atom(getPrimedVariable(getAfterVariable(var1))), Atom(getPrimedVariable(getAfterVariable(var2)))
        npa1, npa2 = Not(pa1), Not(pa2)
        return And(Or(And(a1, Not(a2)),And(Not(a1), a2)), And(Equiv(a1, npa1), Equiv(a2, npa2)))


import re

def multiple_replace(string, rep_dict):
    pattern = re.compile("|".join([re.escape(k) for k in sorted(rep_dict,key=len,reverse=True)]), flags=re.DOTALL)
    return pattern.sub(lambda x: rep_dict[x.group(0)], string)

def parseExpr(expr:str, globals:dict={}):
    """
    str : string as boolean expression we want to parse.

    globals: permit to deal with variables we want to use, as formulas.
    """

    _expr = expr
    for symb, rew in __rewrite_list:
        _expr = _expr.replace(symb, rew)

    """ if a variable has already been declared in the Atom constructor,
    it does not need to be passed to globals and is recognized. """
    atoms = {a :Atom(a) for a in Atom.used}

    try:
        return eval(_expr, {**globals, **{"Atom": Atom}, **atoms})
    except NameError as e:
        var = str(e).split("'")[1]
        raise NameError(f"""name '{var}' is not defined. Declare it in globals \
argument as "parseExpr(expr, globals={{'{var}': {var}_object}})", \
unless it is an atom, declare it beforehand : "{var}_object = Atom('{var}')".""")
