"""Modal logic formula module

This module unites all operators from propositional and modal logic.
"""

import itertools
import random

from .formula import *


class FormulaManager:

    def __init__(self):
        self.table = {}

    def Atom(self, inner):
        key = (Atom, inner)
        if key in self.table:
            return self.table[key]
        self.table[key] = Atom(inner)
        return self.table[key]

    def And(self, left, right):
        fs = frozenset({left, right})
        key = (And, fs)
        if key in self.table:
            return self.table[key]
        self.table[key] = And(left, right)
        return self.table[key]

    def Or(self, left, right):
        fs = frozenset({left, right})
        key = (Or, fs)
        if key in self.table:
            return self.table[key]
        self.table[key] = Or(left, right)
        return self.table[key]

    def K(self, agent, inner):
        key = (K, agent, inner)
        if key in self.table:
            return self.table[key]
        self.table[key] = K(agent, inner)
        return self.table[key]

    def K_hat(self, agent, inner):
        key = (K_hat, agent, inner)
        if key in self.table:
            return self.table[key]
        self.table[key] = K_hat(agent, inner)
        return self.table[key]

    def Implies(self, left, right):
        fs = frozenset({left, right})
        key = (Implies, fs)
        if key in self.table:
            return self.table[key]
        self.table[key] = Implies(left, right)
        return self.table[key]

    def Not(self, inner):
        key = (Not, inner)
        if key in self.table:
            return self.table[key]
        self.table[key] = Not(inner)
        return self.table[key]

    def ite(self, x, left, right):
        key = (ite, x, left, right)
        if key in self.table:
            return self.table[key]
        self.table[key] = ite(x, left, right)
        return self.table[key]

    def get_random_formula(self, vars: [str], fill_percent: float=None, seed=None):
        if seed is None:
            seed = random.randrange(100000000)
            print(seed)
        rng = random.Random(seed)

        if fill_percent is None:
            fill_percent = rng.random()

        formula = Bot()
        for valuation in [list(i) for i in itertools.product([False, True], repeat=len(vars))]:
            val = {x: valuation[i] for i, x in enumerate(vars)}
            r = rng.random()
            if r < fill_percent:
                formula = Or(formula, AndOfVarList(val))
        return formula
