

from abc import ABCMeta, abstractmethod
from typing import List

from src.model.epistemiclogic.epistemicmodel.world import ValuationInterface
from src.model.epistemiclogic.eventmodel.event import EventInterface
from src.model.epistemiclogic.epistemicmodel.epistemic_model_interface import EpistemicModelInterface
from src.model.epistemiclogic.eventmodel.event_model_interface import EventModelInterface
from src.model.epistemiclogic.formula.formula import *

class Example(metaclass=ABCMeta):

    @abstractmethod
    def __init__(self):
        pass

    @abstractmethod
    def getAgents(self) -> List[str]:
        pass

    @abstractmethod
    def getEpistemicModel(self) -> EpistemicModelInterface:
        pass

    @abstractmethod
    def getEventModels(self) -> List[EventModelInterface]:
        pass

    @abstractmethod
    def getWorldName(self, valuation: ValuationInterface) -> str:
        pass

    def product_update(self, event_model=None, world=None, first_possible=False) -> EpistemicModelInterface:
        if not world is None:
            self.getEpistemicModel().setPointedWorld(world)
        if event_model is None:
            event_model = self.getEventModels()[0]

        if first_possible:
            event_model = None
            for e_v in self.getEventModels():
                if self.getEpistemicModel().isApplicable(e_v):
                    event_model = e_v

        return self.getEpistemicModel().apply(event_model)

    @abstractmethod
    def get_formulas_to_check(self) -> List[Formula]:
        pass


