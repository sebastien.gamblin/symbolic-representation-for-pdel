
from src.model.epistemiclogic.epistemicmodel.world import ValuationInterface
from src.model.epistemiclogic.eventmodel.event import Event
from src.model.epistemiclogic.eventmodel.explicit.explicit_event_model import ExplicitEventModel
from src.model.epistemiclogic.examples.example import Example
from src.model.epistemiclogic.epistemicmodel.epistemic_model_interface import EpistemicModelInterface
from src.model.epistemiclogic.eventmodel.event_model_interface import EventModelInterface, EventModelPipeline
from src.model.epistemiclogic.epistemicmodel.explicit.explicit_epistemic_model import ExplicitEpistemicModel
from src.model.epistemiclogic.epistemicmodel.world import World
from src.model.epistemiclogic.epistemicmodel.explicit.explicit_proba_epistemic_model import ExplicitProbaEpistemicModel

from src.model.epistemiclogic.eventmodel.event_model_interface import EventModelList
from src.model.epistemiclogic.eventmodel.explicit.explicit_delp_event_model import ExplicitDELP_EventModel

from src.model.datastructure.compteur import Compteur
from src.utils.timer import for_all_methods

from src.model.epistemiclogic.formula.formula import *

from functools import wraps
import random
import time
import math
import itertools

def timeit(method):
    @wraps(method)
    def timed(*args, **kw):
        #print(method.__name__)
        ts = time.time()
        result = method(*args, **kw)
        te = time.time()
        if 'log_time' in kw and kw["log_time"] is not None:
            name = kw.get('log_name', method.__name__)
            kw['log_time'][name] = (te - ts)
        #if HanabiExample.timer:
        #    print('%r  \t\t%2.2f ms' % (method.__name__, (te - ts) * 1000))
        return result
    return timed


def need(*needed_args):
    def decorator(f):
        @wraps(f)
        def inner_need(*args, **kwargs):
            for needed in needed_args:
                if needed not in HanabiExample.check:
                    raise Exception(f"{needed} need to be called before {f.__name__}. Before : {HanabiExample.check}")
            res = f(*args, **kwargs)
            HanabiExample.check.append(f.__name__)
            #print("CHECK", f.__name__, HanabiExample.check)
            return res
        return inner_need
    return decorator


def getAgentName(i):
    return chr(ord('a') + i)

def getTurnVariable(agent_name):
    return f"turn:{agent_name}"


def getCardVariable(color, value, id, agent_name, position=None, prime=False, after=False):
    atome = f"{agent_name}-{color}{value}{id}" if position is None else f"{agent_name}{position}-{color}{value}{id}"

    if not prime and not after:
        return atome
    if prime and not after:
        return getPrimedVariable(atome)
    if after and not prime:
        return getAfterVariable(atome)
    return getPrimedVariable(getAfterVariable(atome))

def read_atome(var):
    assert not getPrimeSymbol() in var
    assert not getAfterSymbol() in var

    if not "-" in var:
        raise Exception(f"{var} isn't an atome to decompose.'")

    pos, cvi = var.split("-")
    if len(pos) == 1:
        return pos, cvi[0], cvi[1], cvi[2]
    elif len(pos) == 2:
        return pos[0], pos[1], cvi[0], cvi[1], cvi[2]
    else:
        raise Exception("Error in reading atom.")


def getTokenVariable(color, i):
    return f"({color}{i})"


def geteventId(i):
    return f"e{i}"


COLORS = ["R", "W", "B", "Y", "G"] #White, Red, Blue, Yellow, White
DISTRIBUTION = [(1, 1), (1, 2), (1, 3), (2, 1), (2, 2), (3, 1), (3, 2), (4, 1), (4, 2), (5, 1)] # (value, id)

DRAW_NAME = "D"
TABLE_NAME = "T"
DISCARD_NAME = "C"

NB_BLUE_TOKENS = 8
NB_RED_TOKENS = 3
RED_TOKEN = "R"
BLUE_TOKEN = "B"

# 2 or 3 players : 5 cards in hand
# 4 or 5 players : 4 cards in hand

def get_nb_cards_in_hands(nb_players: int):
    if nb_players in [2, 3]:
        return 5
    elif nb_players in [4, 5]:
        return 4
    else:
        raise ValueError(f"Error in nb_players={nb_players}.")


# steps :
# i color = i*10 cartes

@for_all_methods(timeit)
class HanabiExample(Example):

    check = []

    def __init__(self, nbAgents, nbCards, nbCardsInHand=None, random_pointed=False,
                 timer=False, tokens=False, centered=False):

        HanabiExample.timer = timer

        #print(HanabiExample.timer)

        self.nbAgents = nbAgents
        self.nbCards = nbCards
        self.random_pointed = random_pointed

        self.nb_cards_in_hands = nbCardsInHand if nbCardsInHand is not None else get_nb_cards_in_hands(nbAgents)

        self.tokens = tokens

        assert 2 <= nbAgents <= 5, f"2<=NbAgents<=5, not {nbAgents}"

        assert self.nb_cards_in_hands * self.nbAgents <= self.nbCards, \
            f"Error in parameters : nbCardsInHands({self.nb_cards_in_hands})*agents({self.nbAgents})={self.nb_cards_in_hands * self.nbAgents} < nbCards({self.nbCards})"


        self.agents = [getAgentName(i) for i in range(0, nbAgents)]

        self.owners = self.agents + [TABLE_NAME, DRAW_NAME, DISCARD_NAME]
        self.positions = [i for i in range(0, self.nb_cards_in_hands)]

        self.pull_cards = [(color, value, id) for color in COLORS for value, id in DISTRIBUTION]
        self.pull_cards = self.pull_cards[0:nbCards]

        self.COLORS = set(c for c, v, id in self.pull_cards)

        self.MAXVALUES = max([v for c2, v, i in self.pull_cards for c in self.COLORS if c == c2 ])


        # print(self.agents, f"nbCards={self.nbCards}, nbCardsInHand={self.nb_cards_in_hands}", self.pull_cards)

        self.player_cards_variables = [getCardVariable(color, value, id, agent, position)
                          for agent in self.agents
                          for (color, value, id) in self.pull_cards
                          for position in self.positions
                          ]

        self.player_cards_variables_dict = {agent: [getCardVariable(color, value, id, agent, position)
                                       for (color, value, id) in self.pull_cards
                                       for position in self.positions] for agent in self.agents
                                            }

        self.draw_variables = [getCardVariable(color, value, id, DRAW_NAME) for (color, value, id) in self.pull_cards]
        self.table_variables = [getCardVariable(color, value, id, TABLE_NAME) for (color, value, id) in self.pull_cards]
        self.discard_variables = [getCardVariable(color, value, id, DISCARD_NAME) for (color, value, id) in self.pull_cards]
        self.turns_variables = [getTurnVariable(agent) for agent in self.agents]

        if tokens:
            self.red_tokens_variables = [getTokenVariable(RED_TOKEN, i) for i in range(0, math.ceil(math.log(NB_RED_TOKENS))+1)]
            self.blue_tokens_variables = [getTokenVariable(BLUE_TOKEN, i) for i in range(0, math.ceil(math.log(NB_BLUE_TOKENS))+1)]

            #self.red_tokens_variables = [getTokenVariable(RED_TOKEN, i) for i in range(0, 2)]
            #self.blue_tokens_variables = [getTokenVariable(BLUE_TOKEN, i) for i in range(0, 4)]

            self.blue_compteur = Compteur("b_tok", self.blue_tokens_variables)
            self.red_compteur = Compteur("r_tok", self.red_tokens_variables)
            self.blue_compteur.set(NB_BLUE_TOKENS)
            self.red_compteur.set(0)

        self.simple_varnames = self.player_cards_variables + self.draw_variables + self.table_variables + self.discard_variables + self.turns_variables
        if tokens:
            self.simple_varnames += self.red_tokens_variables + self.blue_tokens_variables


        self.centered = centered

        liste_f = [*[Exactly(self.nb_cards_in_hands,
                      [getCardVariable(color, value, id, agent, position)
                       for position in self.positions
                       for color, value, id in self.pull_cards
                       ])
              for agent in self.agents
              ],
            Exactly(0, [getCardVariable(color, value, id, TABLE_NAME)
                        for color, value, id in self.pull_cards]),
            Exactly(0, [getCardVariable(color, value, id, DISCARD_NAME)
                        for color, value, id in self.pull_cards]),
            Atom(getTurnVariable(self.getAgents()[0]))
        ]

        if self.tokens:
            liste_f.append(AndOfVarList(self.blue_compteur.getValue()))
            liste_f.append(AndOfVarList(self.red_compteur.getValue()))

        self.centered_f = MAnd(*liste_f)


    def get_ids(self, c, v):
        return [i for (c2, v2, i) in self.pull_cards if c == c2 and v == v2]


    def get_valuation_with_card_order(self, pull):
        """
        self.real_world: Valuation = get_pointed_world()
        :return:
        """

        # cards distribution  one by one
        val = {v: False for v in self.simple_varnames}

        c = 0
        for a in self.agents:
            for pos in self.positions:
                color, value, id = pull[c]
                val[getCardVariable(color, value, id, a, pos)] = True
                c += 1

        for i in range(c, len(pull)):
            color, value, id = pull[i]
            val[getCardVariable(color, value, id, DRAW_NAME)] = True
            val[getCardVariable(color, value, id, TABLE_NAME)] = False
            val[getCardVariable(color, value, id, DISCARD_NAME)] = False

        for agent in self.agents:
            val[getTurnVariable(agent)] = False
        val[getTurnVariable(getAgentName(0))] = True

        if self.tokens:

            for k, v in self.red_compteur.getValue().items():
                val[k] = v

            for k, v in self.blue_compteur.getValue().items():
                val[k] = v



        return val

    def getAgents(self) -> List[str]:
        return self.agents

    def getWorldName(self, valuation: ValuationInterface, withid=True) -> str:

        coupe = 5 if withid else 5

        present = [var for var in self.player_cards_variables if var in valuation] #IT'S SORTED BY DEFINITION
        old = ""
        string = ""
        for var in present:
            if valuation[var]:
                here = var[0]
                if old != here:
                    string += f" {var[0]}:"
                old = var[0]
                string += var[2:coupe]

        # Turns variables
        string += f" {[i for i in self.turns_variables if valuation[i]][0]} "
        # Tokens variables
        #string += f" R:{self.red_compteur.read(valuation, value=int)} B:{self.blue_compteur.read(valuation, value=int)}"

        return string[1:]


    @abstractmethod
    def getEpistemicModel(self) -> EpistemicModelInterface:
        pass

    @abstractmethod
    def getEventModels(self) -> List[EventModelInterface]:
        pass

    def get_formulas_to_check(self, proba=False):

        colors = set([c for c, v, i in self.pull_cards])
        values = {c:set([v for c2, v, i in self.pull_cards if c==c2]) for c in colors}
        ids = {(c,v): set([i for c2, v2, i in self.pull_cards if c2==c and v2==v]) for c in colors for v in values[c]}

        def agent_has_cv_pos(agent, c, v, pos):
            return MOr(*[Atom(getCardVariable(c, v, id, agent, pos)) for id in ids[(c,v)]])

        def agent_has_c_pos(agent, color, pos):
            return MOr(*[Atom(getCardVariable(c, v, i, agent, pos)) for c, v, i in self.pull_cards if c == color])

        def agent_has_v_pos(agent, value, pos):
            return MOr(*[Atom(getCardVariable(c, v, i, agent, pos)) for c, v, i in self.pull_cards if v == value])

        succs = {a: self.getAgents()[(i+1)%len(self.getAgents())] for i, a in enumerate(self.getAgents())}

        liste = []

        for agent in ["a", "b"]:

            liste.append((f"K_a {agent}", K("a", agent_has_v_pos(agent, 1, 0))))

            liste.append((f"K_b {agent}", K("b", agent_has_v_pos(agent, 1, 0))))

            liste.append((f"K_a K_b {agent}", K("a", K("b", agent_has_v_pos(agent, 1, 0)))))

            liste.append((f"K_b K_a {agent}", K("b", K("a", agent_has_v_pos(agent, 1, 0)))))

            if proba:
                liste.append((f"Pr_a {agent}", Pr("a", agent_has_v_pos(agent, 1, 0), ">=", 0.25)))
                liste.append((f"Pr_b {agent}", Pr("b", agent_has_v_pos(agent, 1, 0), ">=", 0.25)))

                liste.append((f"Pr_b Pr_a {agent}", Pr("b", Pr("a", agent_has_v_pos(agent, 1, 0), ">=", 0.25), ">=", 0.25)))
                liste.append((f"Pr_a Pr_b {agent}", Pr("a", Pr("b", agent_has_v_pos(agent, 1, 0), ">=", 0.25), ">=", 0.25)))

                liste.append((f"Pr_b K_a {agent}", Pr("b", K("a", agent_has_v_pos(agent, 1, 0)), ">=", 0.25)))
                liste.append((f"Pr_a K_b {agent}", Pr("a", K("b", agent_has_v_pos(agent, 1, 0)), ">=", 0.25)))

                liste.append((f"K_b Pr_a {agent}", K("b", Pr("a", agent_has_v_pos(agent, 1, 0), ">=", 0.25))))
                liste.append((f"K_a Pr_b {agent}", K("a", Pr("b", agent_has_v_pos(agent, 1, 0), ">=", 0.25))))

        return liste

    def get_actions_to_apply(self, action_names):

        liste = []

        for to_find in action_names:
            for event in self.getEventModels():
                print()
                if event.name is None:
                    if to_find in str(event):
                        liste.append(event)
                else:
                    if to_find in str(event.name):
                        liste.append(event)
        return liste


@for_all_methods(timeit)
class HanabiExampleExplicit(HanabiExample):

    def __init__(self, nbAgents, nbCards, nbCardsInHand=None, random_pointed=False, timer=False, tokens=True, centered=False):
        super().__init__(nbAgents, nbCards, nbCardsInHand=nbCardsInHand, random_pointed=random_pointed, timer=timer, tokens=tokens, centered=centered)

    def get_order_with_priority_of_list(self, priority_list: [str], var_list):

        def generate(arg):
            if arg == "agents":
                return self.owners
            if arg == "cards":
                return self.pull_cards
            if arg == "position":
                return range(0, self.nb_cards_in_hands)
            if arg == "prime":
                return [False, True]
            if arg == "after":
                return [False, True]
            raise Exception(f"Unknow {arg} for sorting.")

        # agent , carte, position, primage, plusssage

        res = itertools.product(*[generate(arg) for arg in priority_list])

        index = {e: priority_list.index(e) for e in priority_list}

        for var in var_list:
            for items in res:
                print(var, items)

    def get_order_with_priority(self, priority_list: [str]):

        assert len(priority_list) == 5, f"Need 5 elements : 'cards', 'agents', 'position', 'prime', 'after', not {len(priority_list)} ({priority_list})"
        for e in ['cards', 'agents', 'position', 'prime', 'after']:
            assert e in priority_list, f"{e} not in {priority_list}"

        def generate(arg):
            if arg == "agents":
                return self.owners
            if arg == "cards":
                return self.pull_cards
            if arg == "position":
                return range(0, self.nb_cards_in_hands)
            if arg == "prime":
                return [False, True]
            if arg == "after":
                return [False, True]
            raise Exception(f"Unknow {arg} for sorting.")

        # agent , carte, position, primage, plusssage

        res = itertools.product(*[generate(arg) for arg in priority_list])

        index = {e: priority_list.index(e) for e in priority_list}

        rl = []

        # order cards variables
        for items in res:
            color, value, id = items[index["cards"]]
            prime = items[index["prime"]]
            after = items[index["after"]]
            position = items[index["position"]]
            agent = items[index["agents"]]

            var = getCardVariable(color, value, id, agent,
                                  position=position if agent in self.agents else None, prime=prime, after=after)
            rl.append(var)


        # order tokens and turns variables
        n_list = [p for p in priority_list if p in ["prime", "after"]]
        iter = list(itertools.product(*[generate(arg) for arg in n_list]))
        index = {e: n_list.index(e) for e in n_list}

        others_atoms = self.turns_variables.copy()
        if self.tokens:
            others_atoms += self.red_tokens_variables.copy() + self.blue_tokens_variables.copy()

        for var in others_atoms:
            for items in iter:
                prime = items[index["prime"]]
                after = items[index["after"]]
                if after and prime:
                    nvar = getPrimedVariable(getAfterVariable(var))
                elif prime:
                    nvar = getPrimedVariable(var)
                elif after:
                    nvar = getAfterVariable(var)
                else:
                    nvar = var
                rl.append(nvar)

        return rl

    def init_pointed_world(self, getdict=False, **kwargs):

        pull = self.pull_cards.copy() if not self.random_pointed else sorted(self.pull_cards.copy(),
                                                                             key=lambda k: random.random())

        vars = self.get_valuation_with_card_order(pull)

        return vars

    def getEpistemicModel(self, **kwargs):
        if hasattr(self, "epistemic_model"):
            return self.epistemic_model
        else:
            self.epistemic_model = self.calculate_em()
        return self.epistemic_model

    def calculate_em(self, **kwargs):

        epistemicmodel = ExplicitEpistemicModel(self.agents)

        worlds = []
        check = []

        # print("-", self.pull_cards, self.nb_cards_in_hands*self.nbAgents)

        for dist in itertools.permutations(self.pull_cards, self.nb_cards_in_hands*self.nbAgents):
            val = self.get_valuation_with_card_order(list(dist) + [card for card in self.pull_cards if card not in dist])

            name = self.getWorldName(val)

            if name in check:
                continue
            # print(name, {k:1 for k, v in val.items() if v})
            check.append(name)
            w = World(name, val)
            worlds.append(w)

        for w in worlds:
            epistemicmodel.addNode(w)

        # Created Pointed World
        print("RANDOM", self.random_pointed)
        pull = self.pull_cards.copy() if not self.random_pointed else sorted(self.pull_cards.copy(), key=lambda k: random.random())
        pointed_name = self.getWorldName(self.get_valuation_with_card_order(pull))

        print("Pointed:", pointed_name)
        pw = [w for w in epistemicmodel.getWorlds() if w.getName() == pointed_name][0]
        print(pw)
        epistemicmodel.setPointedWorld(pw)

        for ai, agent in enumerate(self.agents):
            #print("agent", agent)
            for world in epistemicmodel.getWorlds():
                for world2 in epistemicmodel.getWorlds():
                    split1 = self.getWorldName(world.getValuation(), withid=False).split(" ")
                    split2 = self.getWorldName(world2.getValuation(), withid=False).split(" ")
                    #print(split1, split2)
                    #print([x for i, x in enumerate(split1) if i!=ai], [x for i, x in enumerate(split2) if i!=ai])
                    if [x for i, x in enumerate(split1) if i!=ai] == [x for i, x in enumerate(split2) if i!=ai]:
                        #print("add", sorted([k for k, v in world.getValuation().items() if v]), sorted([k for k, v in world2.getValuation().items() if v]), agent)
                        epistemicmodel.addEdge(world, world2, agent)
            #print()

        # stringToSvg("permutations", epistemicmodel.toDot())
        return epistemicmodel

    def getEventModels(self, addArc=False, **kwargs) -> List[EventModelInterface]:
        if hasattr(self, "event_models"):
            return self.event_models
        else:
            self.event_models = self.init_eventmodel(addArc=addArc)
        return self.event_models


    def init_eventmodel(self, addArc=False, **kwargs) -> ExplicitEventModel:

        list_event_model = []

        mem_action = {}

        def change_turn():
            key = "change_turn"
            if key in mem_action.keys():
                return mem_action[key]

            event_model = ExplicitEventModel("change_turn", self.agents)

            for i, agent in enumerate(self.agents):

                pre = Atom(getTurnVariable(agent))
                post = {getTurnVariable(agent): False,
                        getTurnVariable(self.agents[(i + 1) % len(self.agents)]): True}

                event = Event(f"change_turn_{agent}->{self.agents[(i + 1) % len(self.agents)]}", pre, post)
                event_model.addNode(event)

            if addArc:
                for agent2 in self.agents:
                    for event in event_model.getEvents():
                        for event2 in event_model.getEvents():
                            event_model.addArc(event, event2, agent2)

            mem_action[key] = event_model
            return event_model

        def use_blue():

            event_model = ExplicitEventModel(f"use_blue", self.agents)

            for i in range(NB_BLUE_TOKENS, 0, -1):
                pre = AndOfVarList(self.blue_compteur.get(i))
                post = self.blue_compteur.get(i - 1)
                event = Event("use_blue{i}_{i-1}", pre, post)
                event_model.addNode(event)

            if addArc:
                for agent2 in self.agents:
                    for event in event_model.getEvents():
                        for event2 in event_model.getEvents():
                            event_model.addArc(event, event2, agent2)

            return event_model

        change = change_turn()
        #stringToSvg("change_turn", change.toDot())

        def _piocher_acvi(agent, c, v, i):

            #draw at pos 0
            pre = MAnd(
                Atom(getTurnVariable(agent)),
                *[Not(Atom(getCardVariable(c, v, i, agent, p))) for p in self.positions],
                Not(Atom(getCardVariable(c,v,i, DRAW_NAME)))
            )
            post = {
                getCardVariable(c, v, i, DRAW_NAME): False,
                getCardVariable(c, v, i, agent, position=0): True,
            }
            event = Event(f"{agent}_draw_{c},{v},{i}", pre, post)

            return event

        def piocher_a(agent):

            key = f"{agent}_draws"
            if key in mem_action.keys():
                return mem_action[key]

            event_model = ExplicitEventModel(f"{agent}_draws", self.agents)

            event_list = [_piocher_acvi(agent, c, v, i) for c, v, i in self.pull_cards]
            for event in event_list:
                event_model.addNode(event)
            if addArc:
                for event in event_list:
                    event_model.addArc(event, event, agent)
                    for event2 in event_list:
                        for agent2 in self.agents:
                            if agent2 != agent:
                                event_model.addArc(event, event2, agent2)

            event_models = [event_model.copy() for _ in event_list]
            for i, event_m in enumerate(event_models):
                # print(event_m.getEvents()[i])
                event_m.setPointedEvent(event_m.getEvents()[i])
                event_m.setName(f"{agent}_draws_{event_m.getEvents()[i].getName()}")


            draw_random_spem = EventModelList(event_models, f"{agent}_Random_draw", random_choice=True)

            mem_action[key] = draw_random_spem
            return draw_random_spem

        def _possible(color, value):
            needed = [(color2, value2, id2) for color2, value2, id2 in self.pull_cards if
                      color == color2 and value2 < value]
            formula = Top()
            for v in range(1, value):
                formula = And(formula,
                              Exactly(1, [getCardVariable(c, v2, i, TABLE_NAME) for c, v2, i in needed if v2 == v]))
            return And(formula, Not(OrOfList(
                [Atom(getCardVariable(color2, value2, id2, TABLE_NAME)) for color2, value2, id2 in self.pull_cards if
                 color == color2 and value2 == value])))

        def jouer(agent, pos):

            name = f"{agent}_plays_p{pos}"
            event_model = ExplicitEventModel(name, self.agents)

            for color, value, id in self.pull_cards:

                # OK

                is_possible = _possible(color, value)

                pre = MAnd(
                    Atom(getTurnVariable(agent)),
                    Not(Atom(getCardVariable(color, value, id, TABLE_NAME))),
                    Atom(getCardVariable(color, value, id, agent, pos)),
                    is_possible)

                post = {getCardVariable(color, value, id, TABLE_NAME): True,
                        getCardVariable(color, value, id, agent, pos): False}

                subname = f"{agent} plays {getCardVariable(color, value, id, agent, pos)} and can"

                event = Event(subname, pre, post)
                event_model.addNode(event)

                if addArc:
                    for agent2 in self.agents:
                        event_model.addArc(event, event, agent2)

                # ERROR

                post = {getCardVariable(color, value, id, DISCARD_NAME): True,
                        getCardVariable(color, value, id, agent, pos): False}

                pre = MAnd(
                    Atom(getTurnVariable(agent)),
                    Not(Atom(getCardVariable(color, value, id, TABLE_NAME))),
                    Atom(getCardVariable(color, value, id, agent, pos)),
                    Not(is_possible)
                )

                if self.tokens:
                    #print(NB_RED_TOKENS, list(range(NB_RED_TOKENS, 0, -1)))
                    for i_red in range(NB_RED_TOKENS, 0, -1):
                        pre = And(pre, AndOfVarList(self.red_compteur.get(i_red)))
                        for k, v in self.red_compteur.get(i_red-1).items():
                            post[k] = v

                        subname = f"{agent} plays {getCardVariable(color, value, id, agent, pos)} and can't. {i_red} red tokens"

                        event = Event(subname, pre, post)
                        event_model.addNode(event)
                        # print(event)
                else:

                    subname = f"{agent} plays {getCardVariable(color, value, id, agent, pos)} and can't"

                    event = Event(subname, pre, post)
                    event_model.addNode(event)

                if addArc:
                    for agent2 in self.agents:
                        event_model.addArc(event, event, agent2)

            return event_model

        def defausser(agent, pos):

            name = f"{agent}_discards_p{pos}"
            event_model = ExplicitEventModel(name, self.agents)

            for color, value, id in self.pull_cards:

                pre = MAnd(
                    Not(Atom(getCardVariable(color, value, id, DISCARD_NAME))),
                    Atom(getCardVariable(color, value, id, agent, pos)),
                    Atom(getTurnVariable(agent))
                )

                post = {getCardVariable(color, value, id, DISCARD_NAME): True,
                        getCardVariable(color, value, id, agent, pos): False}

                if self.tokens:

                    for nb_token in range(0, NB_BLUE_TOKENS+1):

                        v1, v2 = self.blue_compteur.get(nb_token), self.blue_compteur.get(nb_token+1)

                        npre = And(pre, AndOfVarList(v1))
                        npost = post.copy()
                        for k, v in v2.items():

                            npost[k] = v

                        subname = f"{agent} discards {getCardVariable(color, value, id, agent, pos)} and there are {nb_token} blue tokens."
                        event = Event(subname, npre, npost)
                        event_model.addNode(event)

                        if addArc:
                            for agent2 in self.agents:
                                event_model.addArc(event, event, agent2)

                else:

                    subname = f"{agent} discards {getCardVariable(color, value, id, agent, pos)}"

                    event = Event(subname, pre, post)
                    event_model.addNode(event)

                    if addArc:
                        for agent2 in self.agents:
                            event_model.addArc(event, event, agent2)

            list_event_model.append(event_model)
            return event_model


        def shift_card(agent, c, v, i, pos):

            subname = "shift_card_{c}{v}{i}{pos}"
            pre = And(
                Atom(getCardVariable(c, v, i, agent, pos-1)),
                MAnd(*[Not(Atom(getCardVariable(c2, v2, i2, agent, pos))) for c2, v2, i2 in self.pull_cards]),
            )
            post = {getCardVariable(c, v, i, agent, pos-1): False, getCardVariable(c, v, i, agent, pos): True}
            event = Event(subname, pre, post)
            return event

        def shift(agent, pos):

            assert pos > 0

            event_model = ExplicitEventModel(f"{agent}_shifts_p{pos}->{pos-1}", self.agents)

            # position pos is empty and we put pos-1 in it
            for c, v, i in self.pull_cards:
                event = shift_card(agent, c, v, i, pos)
                event_model.addNode(event)

            if addArc:
                for agent2 in self.agents:
                    for event in event_model.getEvents():
                        for event2 in event_model.getEvents():
                            event_model.addArc(event, event2, agent2)

            return event_model

        def announce_color(color, agent, positions:[int]):
            formula = Top()
            for position in positions:
                formula = And(formula, Exactly(1, [getCardVariable(color, v, id, agent, position) for c, v, id in self.pull_cards if c == color]))
            for position in [x for x in range(0, self.nb_cards_in_hands) if x not in positions]:
                formula = And(formula, Exactly(0, [getCardVariable(color, v, id, agent, position) for c, v, id in self.pull_cards if c == color]))

            res = And(Not(Atom(getTurnVariable(agent))), formula)
            return res

        def announce_value(value, agent, positions:[int]):
            formula = Top()
            for position in positions:
                formula = And(formula, Exactly(1, [getCardVariable(c, v, id, agent, position) for c, v, id in self.pull_cards if v == value]))
            for position in [x for x in range(0, self.nb_cards_in_hands) if x not in positions]:
                formula = And(formula, Exactly(0, [getCardVariable(c, v, id, agent, position) for c, v, id in self.pull_cards if v == value]))
            res = And(Not(Atom(getTurnVariable(agent))), formula)
            return res

        def annoncer(eq, agent, combis, ac):

            if ac :
                pre = announce_color(eq, agent, combis)
            else:
                pre = announce_value(eq, agent, combis)

            #print(formula)
            combi = [str(ic) for ic in combis]
            name = f"Announce for {agent} : {eq} at [{'.'.join(combi)}]"
            #print(name)

            event_model = ExplicitEventModel(name, self.agents)
            event = Event(name, pre)
            event_model.addNode(event)
            event_model.setPointedEvent(event)
            event_model.name = name
            #print(event_model.getPointedEvent())
            if addArc:
                for agent in self.agents:
                    event_model.addArc(event, event, agent)

            return event_model

        event_models = []

        for agent in self.agents:
            for pos in self.positions:

                event_pipeline = EventModelPipeline()
                event_pipeline.append(jouer(agent, pos))

                for np in range(pos, 0, -1):
                    event_pipeline.append(shift(agent, np))

                event_pipeline.append(piocher_a(agent))
                event_pipeline.append(change_turn())

                event_models.append(event_pipeline)

        for agent in self.agents:
            for pos in self.positions:

                event_pipeline = EventModelPipeline()
                event_pipeline.append(defausser(agent, pos))

                for np in range(pos, 0, -1):
                    event_pipeline.append(shift(agent, np))

                event_pipeline.append(piocher_a(agent))
                event_pipeline.append(change_turn())

                event_models.append(event_pipeline)

        for agent in self.agents:
            for color in self.COLORS:
                nb_per_color = min(self.nb_cards_in_hands, len([c for c, v, id in self.pull_cards]))
                for announce_c in range(1, nb_per_color+1):

                    positions = itertools.combinations([i for i in range(0, self.nb_cards_in_hands)], announce_c)
                    for pos in positions:

                        event_pipeline = EventModelPipeline()
                        event_pipeline.append(annoncer(color, agent, pos, True))
                        event_pipeline.append(use_blue())
                        event_pipeline.append(change_turn())
                        event_models.append(event_pipeline)

            for value in range(1, self.MAXVALUES):
                for announce_v in range(1, self.nb_cards_in_hands+1):

                    positions = itertools.combinations([i for i in range(0, self.nb_cards_in_hands)], announce_v)

                    for pos in positions:
                        event_pipeline = EventModelPipeline()
                        event_pipeline.append(annoncer(value, agent, pos, False))
                        event_pipeline.append(use_blue())
                        event_pipeline.append(change_turn())
                        event_models.append(event_pipeline)

        # event_models.append(change_turn())

        """
        for pip in event_models:

            for event_model in pip:

                if isinstance(event_model, ExplicitEventModel):
                    print(event_model.getName())
                    stringToSvg(event_model.getName(), event_model.toDot())
                if isinstance(event_model, EventModelList):
                    print(event_model.getName())
                    for event_m in event_model:
                        stringToSvg(event_m.getName(), event_m.toDot())
        """

        return event_models


@for_all_methods(timeit)
class HanabiExampleExplicitProba(HanabiExampleExplicit):

    def __init__(self, nbAgents, nbCards, nbCardsInHand=None, random_pointed=False, timer=False, tokens=True,
                 centered=False):
        # print(nbAgents, nbCards, nbCardsInHand)
        super().__init__(nbAgents, nbCards, nbCardsInHand=nbCardsInHand, random_pointed=random_pointed, timer=timer,
                         tokens=tokens, centered=centered)

    def getEventModels(self, **kwargs) -> List[EventModelInterface]:
        if hasattr(self, "event_models"):
            return self.event_models
        else:
            self.event_models = [ExplicitDELP_EventModel.initWithNonProba(ev) for ev in super().getEventModels()]
        return self.event_models

    def getEpistemicModel(self, **kwargs):
        if hasattr(self, "epistemic_model"):
            return self.epistemic_model
        else:
            self.epistemic_model = self.calculate_em(**kwargs)
            self.probabilisation(**kwargs)
            return self.epistemic_model

    def probabilisation(self, **kwargs):
        self.epistemic_model = ExplicitProbaEpistemicModel.initWithNonProba(self.epistemic_model)
        return self.epistemic_model



