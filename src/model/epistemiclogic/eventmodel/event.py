
from abc import ABCMeta, abstractmethod
from typing import Dict

from src.model.epistemiclogic.formula.formula import Formula


class EventInterface(metaclass=ABCMeta):

    @abstractmethod
    def __init__(self):
        pass

    @abstractmethod
    def getName(self):
        pass

    @abstractmethod
    def getPrecondition(self):
        pass

    @abstractmethod
    def getPostcondition(self):
        pass

    @abstractmethod
    def canBeTriggered(self, epistemicmodel, world):
        pass


class Event(EventInterface):

    def __init__(self, name, pre: Formula, post: Dict[str, Formula]={}):
        assert isinstance(pre, Formula)
        assert isinstance(post, dict)
        self.name = name
        self.pre = pre
        self.post = post

    def getName(self):
        return self.name

    def getPrecondition(self):
        return self.pre

    def getPostcondition(self):
        return self.post

    def canBeTriggered(self, epistemicmodel, world):
        return epistemicmodel.modelCheck(self.pre, world=world)

    def __repr__(self):
        return "E:({})".format(self.name)

    def copy(self):
        return Event(self.name, self.pre, self.post)


