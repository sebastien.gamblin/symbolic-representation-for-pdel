
from src.model.datastructure.graph.symbolic_graph import BDDGraph
from src.model.datastructure.graph.symbolic_graph import BDDManager, BDDToPrime
from src.model.datastructure.graph.symbolic_graph import formulaToBDD
from .event import Event
from src.model.epistemiclogic.eventmodel.explicit.explicit_event_model import ExplicitEventModel
from src.model.epistemiclogic.eventmodel.symbolic.symbolic_event_model import SymbolicEventModel


class ExplicitToSymbolic:

    def __init__(self):
        self.manager = BDDManager().get()

    def translate(self, explicit_em: ExplicitEventModel, agents: list, variables: list) -> SymbolicEventModel:

        symbolic_em = SymbolicEventModel("symb_"+explicit_em.getName(), agents, variables)

        event_bdd = {}
        for event in explicit_em.getEvents():
            event_bdd[event.name] = self.event_to_bdd(event)
            symbolic_em.addUniqueEvent(event.name, event_bdd[event.name])

        for agent in agents:
            for event in explicit_em.getEvents():

                action = event_bdd[event.name]

                liste = variables[:]
                for v in self.manager.support(action):
                    if not BDDGraph.getPrimedSymbol() in v and not SymbolicEventModel.getPostedSymbol() in v:
                        liste.remove(v)
                action_frame = self.manager.apply("and", action, self.frame(liste))

                pointeur = action_frame

                or_others = self.manager.false
                for succ in explicit_em.getSuccessors(event, agent=agent):

                    action_prime = BDDToPrime(event_bdd[succ.name], symbolic_em.vars_to_primed, self.manager)
                    liste = variables[:]
                    support = self.manager.support(action_prime)
                    for v in support:
                        v1 = v.replace(BDDGraph.getPrimedSymbol(), "")
                        v1 = v1.replace(SymbolicEventModel.getPostedSymbol(), "")
                        if v1 in liste:
                            liste.remove(v1)

                    action_prime_frame = self.manager.apply("and", action_prime, self.frame(liste, p=True))
                    or_others = self.manager.apply("or", or_others, action_prime_frame)

                pointeur = self.manager.apply("and", pointeur, or_others)

                symbolic_em.addPlayerEvent(event.name, agent, pointeur)


        if explicit_em.getPointedEvent() != None:
            symbolic_em.setPointedEvent(explicit_em.getPointedEvent().name)
        return symbolic_em

    def event_to_bdd(self, event: Event):
        precond = event.getPrecondition()
        postcond = event.getPostcondition()

        bdd_prec = formulaToBDD(precond, self.manager)
        if postcond == {}:
            bdd_post = self.manager.let(SymbolicEventModel.vars_to_posted(self.manager.support(bdd_prec)), bdd_prec)
        else:
            bdd_post = self.manager.cube({SymbolicEventModel.getPosted(k): v for k, v in postcond.items()})
        event = self.manager.apply("and", bdd_prec, bdd_post)
        return event

    def frame(self, xvars, p=False):
        symb = SymbolicEventModel.getPostedSymbol()

        pointeur = self.manager.true
        for var in xvars:
            # si j'ai pas de + sur var : var+ else var
            var1 = var
            if SymbolicEventModel.getPostedSymbol() in var:
                var1.remove(symb)

            var2 = SymbolicEventModel.getPosted(var)

            if p: # if primed : var1 = var1' , var2 = var2'
                var1 = BDDGraph.getPrimed(var)
                var2 = BDDGraph.getPrimed(var2)

            equiv = self.manager.apply("<->", self.manager.var(var1), self.manager.var(var2))
            pointeur = self.manager.apply("and", pointeur, equiv)
        return pointeur


    def show(self, pointeur, limit=20, all=True, title=None):
        if title != None:
            print(title)
        sols = self.manager.pick_iter(pointeur)
        for i, s in enumerate(sols):
            if i < limit:
                print(i, ", ".join(sorted(["'{}':{}".format(v, "1" if k else "0") for v, k in s.items() if all or k])))
            else:
                break
