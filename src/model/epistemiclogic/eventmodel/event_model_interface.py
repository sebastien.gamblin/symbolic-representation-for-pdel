

from abc import ABCMeta, abstractmethod
import random

from .event import Event, EventInterface

class EventModelInterface(metaclass=ABCMeta):

    @abstractmethod
    def __init__(self):
        pass

    @abstractmethod
    def getEvents(self):
        pass

    @abstractmethod
    def getName(self) -> str:
        pass

    def getPointedEvent(self) -> Event:
        pass

    def setPointedEvent(self, event: Event):
        pass

    @abstractmethod
    def getPrecondition(self, event: EventInterface):
        pass

    @abstractmethod
    def getPostcondition(self, event: EventInterface):
        pass


class EventModelPipeline(list):

    id = 0

    def __init__(self, eventmodel_list: [EventModelInterface]=[], name=None):
        super().__init__()
        self.name = name
        for e in eventmodel_list:
            assert isinstance(e, EventModelInterface) or isinstance(e, EventModelList), f"Need to have EventModelInterface or EventModelList, not {type(e).__name__}"
            self.append(e)
        self.id = EventModelPipeline.id
        EventModelPipeline.id += 1

    def getName(self):
        if self.name is None:
            return " - ".join([event.getName() for event in self])
        return self.name

    def __hash__(self):
        return hash("EventModelPipeline" + str(self.id))



class EventModelList(list):

    def __init__(self, events:[EventModelInterface], name, random_choice=True):
        super().__init__()
        for event in events:
            assert isinstance(event, EventModelInterface) or isinstance(event, EventModelPipeline) or \
                   isinstance(event, EventModelList), str(type(event).__name__)
            self.append(event)
        self.random_choice = random_choice
        self.name = name

    def getName(self):
        return self.name


