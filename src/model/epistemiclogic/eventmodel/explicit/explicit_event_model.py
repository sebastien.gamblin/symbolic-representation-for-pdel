


from src.model.datastructure.graph.explicit_graph import GraphInterface, Graph
# from src.model.epistemiclogic.epistemicmodel.epistemicmodel import EpistemicModelInterface, EpistemicModel
from src.model.epistemiclogic.eventmodel.event import Event

from typing import List

from src.model.epistemiclogic.eventmodel.event_model_interface import EventModelInterface


class ExplicitEventModel(EventModelInterface, GraphInterface):

    def __init__(self, name:str , agents: List[str]):
        self.agents = agents
        self.graph = Graph()
        self.precondition = {}
        self.postcondition = {}
        self.pointed = None
        self.name = name

    def __repr__(self):
        string = f"ExplicitEventModel : {self.getName()}\n "
        string += str(self.pointed) + "\n"
        string += str(self.graph) + "\n"
        string += "Preconditions :" + "\n"
        for event, v in self.precondition.items():
            string += f"{event} : {v} \n"
        string += "Postconditions : \n"
        for event, v in self.postcondition.items():
            string += f"{event} : {v} \n"
        return string

    def getPointedEvent(self) -> Event:
        return self.pointed

    def getName(self) -> str:
        return self.name

    def getAgents(self):
        return self.agents

    def setName(self, new_name):
        self.name = new_name

    def setPointedEvent(self, event: Event):
        if isinstance(event, str):
            for e in self.getEvents():
                if e.name == event:
                    self.pointed = e
        else:
            self.pointed = event
        assert isinstance(self.pointed, Event), "pointed event need to be an Event, not a {}".format(type(event).__name__)

    def addArc(self, name_u: Event, name_v:Event, agent):
        self.graph.addArc(name_u, name_v, labels_uv=agent)

    def addEdge(self, name_u: Event, name_v:Event, agent):
        self.graph.addEdge(name_u, name_v, labels_uv=agent)

    def addNode(self, event:Event):
        self.graph.addNode(event)
        self.precondition[event] = event.pre
        self.postcondition[event] = event.post

    def getArcs(self):
        return self.graph.getArcs()

    def hasArc(self, name:Event, name2:Event, agent):
        return self.graph.hasArc(name, name2, labels_uv=agent)

    def getEvents(self):
        return self.getNodes()

    def getNodes(self) -> List[Event]:
        return self.graph.getNodes()

    def getPostcondition(self, event:Event):
        return self.postcondition[event]

    def getPrecondition(self, event:Event):
        return self.precondition[event]

    def getSuccessors(self, event:Event, agent=None):
        return self.graph.getSuccessors(event, label=agent)

    def removeNode(self):
        return NotImplemented

    def toDot(self):

        def label(node):
            return node.name + "\n" +str(node.getPrecondition()) + "\n" +str(node.getPostcondition())

        def node_name(event):
            return '"{}"'.format(event.name)

        def node_description(node, pointed=False):
            return '{}[shape={}, label="{}", color={}];'.format(node_name(node), "rectangle" if not pointed else "component", label(node), "red4" if pointed else "black")

        colors = ["blue", "red", "orange", "purple", "yellow", "green", "dark"]

        list_nodes = [node_description(node) for node in self.getNodes()]

        list_arcs = ["{} -> {}{}; ".format(
            node_name(event),
            node_name(successor),
            '[label="{}", color="{}"]'.format(
                label,
                colors[self.agents.index(label)]))
            for event in self.getEvents() for successor in self.getSuccessors(event)
            for label in self.graph.getLabel(event, successor)]

        list_arcs.sort()
        list_nodes.sort()

        if self.pointed is None:
            print("Caution, no pointed world")
        else:
            list_nodes.append(node_description(self.pointed, pointed=True))

        digraph = """
                        digraph G {{
                            edge[dir = forward]
                            node[shape = plaintext]
                            {0}
                            {1}
                        }}
                        """.format(
            "\n".join(list_nodes),
            "\n\t\t".join(
                list_arcs)
        )

        return digraph


    def print(self):
        self.graph.print()

    def copy(self):

        new_event_model = ExplicitEventModel(self.getName(), self.agents)

        map_nodes = {}
        for node in self.getEvents():
            new_node = node.copy()
            map_nodes[node] = new_node
            new_event_model.addNode(new_node)

        for node1, node2, agent in self.getArcs():
            new_event_model.addArc(map_nodes[node1], map_nodes[node2], agent)

        if self.getPointedEvent() is not None:
            new_event_model.setPointedEvent(map_nodes[self.getPointedEvent()])

        return new_event_model


    def getClique(self, startNode=None):

        new_em = ExplicitEventModel(self.name + " clique", self.agents)

        if startNode==None:
            startNode = self.getPointedEvent()

        seen = []
        toSee = [startNode]
        new_em.addNode(startNode)
        new_em.setPointedEvent(startNode)
        self.browseGraph(seen, toSee, new_em)

        return new_em


    def browseGraph(self, seen: list, toSee: list, new_em):

        if len(toSee) == 0:
            return seen

        node = toSee[0]
        toSee = toSee[1:]
        seen.append(node)

        for succ in self.getSuccessors(node):
            if succ not in seen:
                if succ not in toSee and succ not in seen:
                    toSee.append(succ)

                if not new_em.graph.hasNode(succ):
                    new_em.addNode(succ)

            for l in self.graph.getLabel(node, succ):
                new_em.addArc(node, succ, l)

        self.browseGraph(seen, toSee, new_em)

