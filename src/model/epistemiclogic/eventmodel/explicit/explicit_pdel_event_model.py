# from src.model.epistemiclogic.epistemicmodel.epistemicmodel import EpistemicModelInterface, EpistemicModel
from src.model.epistemiclogic.eventmodel.event import Event
from src.model.datastructure.probability_distribution import DoubleProbabilityDistribution, ConditionalProbabilityDistribution, ProbabilityDistribution

from .explicit_event_model import ExplicitEventModel
from src.model.epistemiclogic.formula.formula import *

class ExplicitPDEL_EventModel(ExplicitEventModel):

    def __init__(self, name, agents: list):
        super().__init__(name, agents)

        self.probabilities = {}
        for a in agents:
            self.probabilities[a] = DoubleProbabilityDistribution()

        self.conditional_formula = {}
        for a in agents:
            self.conditional_formula[a] = None

        self.conditional_probabilities = None

    def __repr__(self):
        string = ">>>\n" + super().__repr__()
        string += " - With probabilities :\n"
        string += str(self.probabilities)
        string += "\n - With conditionnal probabilities :\n"
        string += str(self.conditional_probabilities)
        return string + "<<<"

    def addArc(self, name_u: Event, name_v:Event, agent: str, proba:float=None) -> None:
        self.graph.addArc(name_u, name_v, agent)
        if proba is not None:
            self.probabilities[agent].addProba(name_u, name_v, proba)

    def setProbaArc(self, name_u: Event, name_v:Event, agent: str, proba:float) -> None:
        self.probabilities[agent].setProba(name_u, name_v, proba)

    def getProbaArc(self, name_u: Event, name_v:Event, agent: str) -> float:
        return self.probabilities[agent].getProba(name_u, name_v)

    def setCondProbabilityDistribution(self, cpd: ConditionalProbabilityDistribution):
        self.conditional_probabilities = cpd

    def getCondProbabilityDistribution(self) -> ConditionalProbabilityDistribution:
        return self.conditional_probabilities


    def toDot(self):

        def label(node):
            return node.name + "\n" +str(node.getPrecondition()) + "\n" +str(node.getPostcondition())

        def node_name(event):
            return '"{}"'.format(event.name)

        def precond_name(precond):
            return '"{}"'.format(precond)

        def node_description(node, pointed=False):
            return '{}[shape={}, label="{}", color={}];'.format(node_name(node), "rectangle" if not pointed else "component", label(node), "red4" if pointed else "black")

        colors = ["blue", "red", "orange", "purple", "yellow", "green", "dark"]

        list_nodes = [node_description(node) for node in self.getNodes()]

        list_arcs = ["{} -> {}{}; ".format(
            node_name(event),
            node_name(successor),
            '[label="{}", color="{}"]'.format(
                self.probabilities[label].getProba(event, successor),
                colors[self.agents.index(label)]))
            for event in self.getEvents() for successor in self.getSuccessors(event)
            for label in self.graph.getLabel(event, successor)]

        for precondition, pd in self.getCondProbabilityDistribution().getDatas():
            for event in self.getNodes():
                print(precondition, event, pd.getProba(event))
                list_nodes.append(precond_name(precondition))

        for precondition, pd in self.getCondProbabilityDistribution().getDatas():
            for event in self.getNodes():
                list_arcs.append('{} -> {}[label="{}"]'.format(precond_name(precondition), node_name(event), pd.getProba(event)))

        list_arcs.sort()
        list_nodes.sort()

        list_nodes.append(node_description(self.pointed, pointed=True))

        digraph = """
                        digraph G {{
                            edge[dir = forward]
                            node[shape = plaintext]
                            {0}
                            {1}
                        }}
                        """.format(
            "\n".join(list_nodes),
            "\n\t\t".join(list_arcs)
        )

        return digraph

    @classmethod
    def getRandom(cls, kripke, nbEvents: int, vars, agents, fill_percent: float,
                  symetric=False, reflexive=False,
                  rng=None, seed: int = None,
                  eventname = lambda i: f"e{i}",
                  ):

        if rng is None:
            rng = random.Random(seed)
        if seed is None:
            seed = rng.randrange(100000000)
        rng.seed(seed)

        event_model = ExplicitPDEL_EventModel("event model", agents)

        def get_event():
            postcondition = {}
            for x in vars:
                if rng.getrandbits(1):
                    postcondition[x] = Formula.getRandom(agents, vars, 0, rng=rng, seed=rng.randrange(10000))
            return Event(eventname(i), Top(), postcondition)

        for i in range(0, nbEvents):
            event = get_event()
            event_model.addNode(event)
            if i == 0:
                event_model.setPointedEvent(event)
            if reflexive:
                for a in agents:
                    event_model.addArc(event, event, a)

        # Create arcs
        for a in agents:
            for w in event_model.getEvents():
                for w2 in event_model.getEvents():
                    if w != w2 and not event_model.hasArc(w, w2, a):
                        if rng.random() < fill_percent:
                            if rng.uniform(0, 1) < fill_percent:
                                event_model.addEdge(w, w2, a)
                                if symetric:
                                    event_model.addEdge(w2, w, a)

        for agent in event_model.getAgents():
            for w in event_model.getEvents():
                succ = event_model.getSuccessors(w, agent)
                randoms = [rng.randrange(1, 100) / 100 for _ in succ]
                add = sum(randoms)
                check = 0
                for i, w2 in enumerate(succ):
                    n_proba = randoms[i] / add
                    check += n_proba
                    event_model.setProbaArc(w, w2, agent, n_proba)
                assert check > 0.999999 and check < 1.000001, f"Sum is {check} ??"

        def get_pairwise():
            pairwise = list()
            form_pointed = AndOfVarList([k for k, v in kripke.getPointedWorld().valuation.items() if v])
            pairwise.append(form_pointed)
            #print(kripke.getPointedWorld().getValuation(), pairwise)
            # GENERATE LIST OF PAIRWISE INCONSISTANT FORMULAS
            nb_tries = 0
            while len(pairwise) < nbEvents and nb_tries < 100:
                formula = Formula.getRandom(agents, vars, 0, rng=rng, seed=seed)

                is_ok = True
                for f in pairwise:
                    for w in kripke.getWorlds():
                        if kripke.modelCheck(formula, world=w) and kripke.modelCheck(f, world=w):
                            is_ok = False
                if is_ok:
                    #print("add", formula)
                    pairwise.append(formula)
                else:
                    nb_tries += 1
            return pairwise

        def pairwise_ok(pairwise, kripke):
            somme = 0
            for w in kripke.getWorlds():
                for f in pairwise:
                    if kripke.modelCheck(f, world=w):
                        somme += 1
                        break
            return somme == len(kripke.getWorlds())

        pairwise = get_pairwise()
        # CAUTION  HERE.... INFINITE ??
        nb_tries = 0
        while not pairwise_ok(pairwise, kripke) and nb_tries < 100:
            pairwise = get_pairwise()
            nb_tries += 1

        if nb_tries == 100:
            # contruct an idiot pairwise: one by event:
            pairwise = list()
            for w in kripke.getWorlds():
                pairwise.append(AndOfVarList({k: v for k, v in w.valuation.items()}))


        # GENERATE PROBABILITY DISTRIBUTIONS
        distributions = []
        for _ in pairwise:
            probabilities = {}
            for event in event_model.getEvents():
                probabilities[event] = rng.randrange(1, 100) / 100

            # normalise
            total = sum([v for k,v in probabilities.items()])
            for k, v in probabilities.items():
                probabilities[k] = v / total

            distributions.append(ProbabilityDistribution(dict=probabilities))

        cpd = ConditionalProbabilityDistribution(pairwise, distributions)

        event_model.setCondProbabilityDistribution(cpd)
        return event_model
