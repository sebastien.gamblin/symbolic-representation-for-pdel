# from src.model.epistemiclogic.epistemicmodel.epistemicmodel import EpistemicModelInterface, EpistemicModel
from src.model.epistemiclogic.eventmodel.event import Event
from src.model.datastructure.probability_distribution import DoubleProbabilityDistribution, ConditionalProbabilityDistribution

from .explicit_event_model import ExplicitEventModel
from src.model.epistemiclogic.eventmodel.event_model_interface import EventModelPipeline, EventModelList

class ExplicitDELP_EventModel(ExplicitEventModel):

    def __init__(self, name, agents: list):
        super().__init__(name, agents)

        self.probabilities = {}
        for a in agents:
            self.probabilities[a] = DoubleProbabilityDistribution()

    @staticmethod
    def initWithNonProba(explicitEventModel: ExplicitEventModel):

        def transform(event_model):

            #print("in", type(event_model).__name__)

            if isinstance(event_model, EventModelList):
                return EventModelList([transform(em) for em in event_model], event_model.getName())

            assert isinstance(event_model, ExplicitEventModel), \
                f"need to be ExplicitEventModel, not {type(event_model).__name__}"

            epem = ExplicitDELP_EventModel(event_model.getName(), event_model.getAgents())

            mapping = dict()

            for event in event_model.getEvents():
                # print(event.getName())
                # print("before", event.getPostcondition())
                mapping[event] = Event(event.getName(), event.getPrecondition(), event.getPostcondition())
                # print("after", mapping[event].getPostcondition())
                epem.addNode(mapping[event])

            for event in event_model.getEvents():
                for agent in epem.agents:
                    succ = event_model.getSuccessors(event, agent)
                    for world2 in succ:
                        epem.addArc(mapping[event], mapping[world2], agent, 1 / len(succ))

                    if event_model.getPointedEvent() is not None:
                        epem.setPointedEvent(mapping[event_model.getPointedEvent()])
            return epem

        #print("init", type(explicitEventModel).__name__)

        if isinstance(explicitEventModel, EventModelPipeline):
            return EventModelPipeline([transform(em) for em in explicitEventModel], name=explicitEventModel.getName())
        if isinstance(explicitEventModel, EventModelList):
            return EventModelList([transform(em) for em in explicitEventModel], explicitEventModel.getName(), random_choice=explicitEventModel.random_choice)
        elif isinstance(explicitEventModel, ExplicitEventModel):
            return transform(explicitEventModel)
        else:
            raise TypeError(f"explicitEventModel need to be EventModelPipeline of ExplicitEventModel or ExplicitEventModel")

    def addArc(self, name_u: Event, name_v:Event, agent: str, proba:float) -> None:
        self.graph.addArc(name_u, name_v, agent)
        self.probabilities[agent].addProba(name_u, name_v, proba)

        print(agent, self.probabilities[agent])

    def setProbaArc(self, name_u: Event, name_v:Event, agent: str, proba:float) -> None:
        self.probabilities[agent].setProba(name_u, name_v, proba)

    def getProbaArc(self, name_u: Event, name_v:Event, agent: str) -> float:
        return self.probabilities[agent].getProba(name_u, name_v)

    def toDot(self):

        def label(node):
            return node.name + "\n" +str(node.getPrecondition()) + "\n" +str(node.getPostcondition())

        def node_name(event):
            return '"{}"'.format(event.name)

        def node_description(node, pointed=False):
            return '{}[shape={}, label="{}", color={}];'.format(node_name(node), "rectangle" if not pointed else "component", label(node), "red4" if pointed else "black")

        colors = ["blue", "red", "orange", "purple", "yellow", "green", "dark"]

        list_nodes = [node_description(node) for node in self.getNodes()]

        list_arcs = ["{} -> {}{}; ".format(
            node_name(event),
            node_name(successor),
            '[label="{}", color="{}"]'.format(
                self.probabilities[label].getProba(event, successor),
                colors[self.agents.index(label)]))
            for event in self.getEvents() for successor in self.getSuccessors(event)
            for label in self.graph.getLabel(event, successor)]

        list_arcs.sort()
        list_nodes.sort()

        if isinstance(self.pointed, list):
            for p in self.pointed:
                list_nodes.append(node_description(p, pointed=True))
        elif self.pointed is not None:
            list_nodes.append(node_description(self.pointed, pointed=True))

        digraph = """
                        digraph G {{
                            edge[dir = forward]
                            node[shape = plaintext]
                            {0}
                            {1}
                        }}
                        """.format(
            "\n".join(list_nodes),
            "\n\t\t".join(list_arcs)
        )

        return digraph

    def __repr__(self):
        return f"ExplicitDELP_EventModel:{self.name} {len(self.getEvents())} nodes"




