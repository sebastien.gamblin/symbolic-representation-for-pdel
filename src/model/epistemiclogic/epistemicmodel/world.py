
from abc import ABCMeta, abstractmethod

from src.model.epistemiclogic.eventmodel.event import Event
from src.model.epistemiclogic.formula.formula import Formula


class ValuationInterface(metaclass=ABCMeta):

    @abstractmethod
    def __init__(self):
        pass

    @abstractmethod
    def __getitem__(self, item):
        pass


class Valuation(dict, ValuationInterface):

    def __init__(self):
        super().__init__()

    def __init__(self, dico:dict):
        for key, value in dico.items():
            #if value:
            self[key] = value

    def __getitem__(self, item:str):
        if item not in self.keys():
            return False
        return super().__getitem__(item)

    def hasContradiction(self, other_valuation):
        keys = list(set(list(self.keys()) + list(other_valuation.keys())))
        # print(keys)
        for k in keys:
            if k not in self.keys() or k not in other_valuation.keys():
                continue
            else: # here, must be same value !
                if self[k] != other_valuation[k]:
                    return True
        return False

    def andOther(self, other):
        if self.hasContradiction(other):
            return Valuation({})
        return Valuation({**self, **other})


class World:

    def __init__(self, name, valuation):
        self.name = name
        self.valuation = Valuation(valuation)

    def getName(self):
        return self.name

    def getValuation(self) -> Valuation:
        return self.valuation

    def __repr__(self):
        #return hex(id(self))
        return "({}:{})".format(self.name, [k for k, v in self.valuation.items() if v])

    def getValuationAfterEvent(self, event: Event, epistemicModel) -> Valuation:
        if "pointed" in self.valuation.keys():
            return {"pointed": True}
        if callable(event.getPostcondition()):
            return {**self.valuation, **(event.getPostcondition()())}
        new = {**self.valuation, **event.getPostcondition()}
        for k, v in new.items():
            if isinstance(v, Formula):
                new[k] = epistemicModel.modelCheck(v, world=self)
        return Valuation(new)


if __name__ == '__main__':

    v1 = Valuation({"a": True, "b": False})
    v2 = Valuation({"b": False, "a": True, "c":True})

    print(v1.hasContradiction(v2))

    print(v1.andOther(v2))
