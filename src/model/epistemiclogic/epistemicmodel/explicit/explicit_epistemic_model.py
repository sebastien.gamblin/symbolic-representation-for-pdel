from collections.abc import Iterable
import random
from typing import Callable

from src.model.datastructure.compteur import Compteur
from src.model.datastructure.graph.explicit_graph import Graph, GraphInterface
from src.model.epistemiclogic.epistemicmodel.epistemic_model_interface import EpistemicModelInterface
from src.model.epistemiclogic.epistemicmodel.world import World, Valuation
from src.model.epistemiclogic.eventmodel.event_model_interface import EventModelInterface, EventModelPipeline
from src.model.epistemiclogic.formula import formula as form
from src.utils.timer import timeit
from src.io.write import *

class ExplicitEpistemicModel:
    pass

class ExplicitEpistemicModel(GraphInterface, EpistemicModelInterface):

    def __init__(self, agents: list):
        super().__init__()
        self.agents = agents
        self.graph = Graph()
        self.pointed = None
        self.counters = []

    def getAgents(self):
        return self.agents

    def getPointedWorld(self) -> World:
        return self.pointed

    def setPointedWorld(self, world: World):
        assert isinstance(world, World)
        self.pointed = world

    def getWorldId(self, world):
        return self.graph.getId(world)

    def addNode(self, name: World):
        self.graph.addNode(name)

    def addArc(self, name_u: World, name_v:World, agent: str):
        self.graph.addArc(name_u, name_v, agent)

    def hasArc(self, name_u: World, name_v: World, agent: str):
        return self.graph.hasArc(name_u, name_v, labels_uv=agent)

    def getArcs(self):
        return self.graph.getArcs()

    def addEdge(self, name_u: World, name_v: World, agent:str):
        self.graph.addEdge(name_u, name_v, agent)

    def getSuccessors(self, name: World, agent: str=None):
        if agent==None:
            return self.graph.getSuccessors(name)
        res = [succ for succ in self.graph.getSuccessors(name) if agent in self.graph.getLabel(name, succ)]
        return res

    def show(self, liste, limit=20):

        def print_world(world):
            print(world)
            for cpt in self.counters:
                for var in world.getValuation():
                    if var in cpt.getVars():
                        print("Counter :", cpt.name, cpt.read(world.getValuation()))
                        break

        if isinstance(liste, Iterable):
            for i, l in enumerate(liste):
                print_world(l)
                if i > limit:
                    break
        else: #it's a world
            print_world(liste)


    def getLabel(self, name_u: World, name_v: World):
        return self.graph.getLabel(name_u, name_v)

    def getWorlds(self):
        return self.graph.getNodes()

    def getNodes(self):
        return self.getWorlds()

    def print(self):
        self.graph.print()

    def getNodesWithLabel(self, label):
        return super().getNodesWithLabel(label)

    def removeNode(self):
        return NotImplemented

    def addCounter(self, counter: Compteur):
        self.counters.append(counter)

    def toDot(self, graph_name="G", drawP=False, drawSameValuation=False):

        listSameValuation = list()
        if drawSameValuation:
            for node1 in self.getNodes():
                for node2 in self.getNodes():
                    if node1 != node2 and node1.getValuation() == node2.getValuation():
                        listSameValuation.append((node1, node2))

        graph_name = graph_name.replace(" ", "_")

        def node_format(world, graph):
            return '"{}[{}]"'.format(world.name, graph.getId(world))

        colors = ["blue", "red", "orange", "purple", "yellow", "green", "dark"]

        list_nodes = ["{}[shape=ellipse];".format(node_format(node, self.graph)) for node in self.getNodes()]

        list_nodes.sort()

        if self.getPointedWorld() != None:
            list_nodes += ["{}[shape=doubleoctagon color=black];".format(node_format(self.getPointedWorld(), self.graph))]

        list_arcs = ["{} -> {}{}; ".format(
            node_format(world, self.graph),
            node_format(successor, self.graph),
            '[label="{}", color="{}"]'.format(
                agent,
                colors[self.agents.index(agent)]))
            for agent in self.agents for world in self.getWorlds() for successor in self.getSuccessors(world, agent)]

        for node1, node2 in listSameValuation:
            list_arcs.append("{} -> {}{}; ".format(
                node_format(node1, self.graph),
                node_format(node2, self.graph),
                '[color="{}"]'.format(colors[len(self.agents)])
            ))

        list_arcs.sort()

        #overlap = scalexy;
        # layout = neato;

        digraph = """
                digraph {0} {{
                    compound=true;
                    edge[dir = forward]
                    node[shape = plaintext]
                    {1}
                    {2}
                    labelloc="t";
                    label="{3}";
                }}
                """.format(graph_name,
            "\n".join(list_nodes),
            "\n\t\t".join(
                list_arcs
                ), graph_name
            )

        return digraph

    # @timeit
    def modelCheck(self, formula: form.Formula, world: World=None, assertion=True):
        # print("Model check", assertion)

        # print(type(self.getPointedWorld()).__name__, self.getPointedWorld())

        if world is None:
            world = self.getPointedWorld()

        if assertion:
            assert world in self.getNodes(), "Unknown world {} in EpistemicModel".format(str(world))

        if isinstance(formula, form.Bot): return False
        if isinstance(formula, form.Top): return True
        if isinstance(formula, form.Atom):
            #print("Atom", formula.getFormulas(), [k for k, v in world.getValuation().items() if v])
            return world.getValuation()[formula.getFormulas()]
        if isinstance(formula, form.Box):
            for world2 in self.getSuccessors(world):
                if not self.modelCheck(formula.getFormulas(), world=world2, assertion=assertion):
                    return False
            return True
        if isinstance(formula, form.Box_a) or isinstance(formula, form.K):
            for world2 in self.getSuccessors(world, formula.getAgent()):
                #print(world2, formula.getAgent(), formula.getFormulas(), self.modelCheck(formula.getFormulas(), world=world2, assertion=assertion))
                if not self.modelCheck(formula.getFormulas(), world=world2, assertion=assertion):
                    return False
            return True
        if isinstance(formula, form.nK):
            i = formula.getAgent()
            p = self.inner
            n_form = form.And(form.Not(form.K(i, p)), form.Not(form.K(i, form.Not(p))))
            return self.modelCheck(n_form, world=world, assertion=assertion)
        if isinstance(formula, form.Diamond):
            for world2 in self.getSuccessors(world):
                if not self.modelCheck(formula.getFormulas(), world=world2, assertion=assertion):
                    return False
            return True
        if isinstance(formula, form.Diamond_a):
            for world2 in self.getSuccessors(world, formula.getAgent()):
                if self.modelCheck(formula.getFormulas(), world=world2, assertion=assertion):
                    return True
            return False
        if isinstance(formula, form.K_hat):
            n_form = form.Not(form.K(formula.getAgent(), form.Not(form.Atom(self.inner))))
            return self.modelCheck(n_form, world=world, assertion=assertion)
        if isinstance(formula, form.Implies):
            left, right = formula.getFormulas()
            return self.modelCheck(form.Or(form.Not(left), right), world=world, assertion=assertion)
        if isinstance(formula, form.Equiv):
            left, right = formula.getFormulas()
            return self.modelCheck(form.And(form.Implies(left, right), form.Implies(right, left)), world=world, assertion=assertion)
        if isinstance(formula, form.Not):
            return not self.modelCheck(formula.getFormulas(), assertion=assertion, world=world)
        if isinstance(formula, form.And):
            left, right = formula.getFormulas()
            return self.modelCheck(left, world=world, assertion=assertion) and self.modelCheck(right, world=world, assertion=assertion)
        if isinstance(formula, form.Or):
            left, right = formula.getFormulas()
            return self.modelCheck(left, world=world, assertion=assertion) or self.modelCheck(right, world=world, assertion=assertion)
        if isinstance(formula, form.ite):
            x, left, right = formula.getFormulas()
            return self.modelCheck(form.Or(form.And(x, left), form.And(form.Not(x), right)), world=world, assertion=assertion)
        if isinstance(formula, bool):
            return bool
        elif isinstance(formula, form.Exactly):
            return formula.n == sum([world.getValuation()[var] for var in formula.array])
        elif isinstance(formula, form.After):
            ev, f = formula.getFormulas()
            future = self.apply(ev)
            #print(future.getPointedWorld())
            return future.modelCheck(f, world=future.getPointedWorld(), assertion=assertion)
        else:
            raise Exception(f"{type(formula).__name__} formula is not supported in {type(self).__name__}.")

    def isApplicable(self, event_model: EventModelInterface):

        if isinstance(event_model, EventModelPipeline):
            event_model = event_model[0]
        assert isinstance(event_model, EventModelInterface)

        #print("isApplicable", event_model.getName(), event_model.getPointedEvent())

        if  event_model.getPointedEvent() is None:
            # print(">> try to find", event_model)
            npointed = None
            for eventm in event_model.getEvents():
                res = self.modelCheck(eventm.getPrecondition(), world=self.getPointedWorld())
                if npointed is None and res:
                    npointed = eventm
                elif res and npointed is not None:
                    raise ValueError("Pointed is None and differents events can be pointed one.")
            if npointed is None:
                return False
        else:
            npointed = event_model.getPointedEvent()

        return self.modelCheck(npointed.getPrecondition(), world=self.getPointedWorld())

    @timeit
    def contract(self, nameFunction=None):

        if nameFunction is None:
            nameFunction = ExplicitEpistemicModel.getNameWithValuation

        new_em = ExplicitEpistemicModel(self.agents)

        for world in self.graph.getNodes():
            if not new_em.graphAsAWorldWithThisValuation(world.valuation)[0]:
                new_em.addNode(World(nameFunction(world.valuation), Valuation(world.valuation)))

        for w1, w2, label in self.graph.getArcs():
            w1_assert, w1_2 = new_em.graphAsAWorldWithThisValuation(w1.valuation)
            w2_assert, w2_2 = new_em.graphAsAWorldWithThisValuation(w2.valuation)
            if w1_assert and w2_assert:
                new_em.addArc(w1_2, w2_2, label)

        assert self.getPointedWorld() != None, "Error, the epistemic model must have a pointed world."
        new_pointed = new_em.graphAsAWorldWithThisValuation(self.getPointedWorld().valuation)[1]
        new_em.setPointedWorld(new_pointed)

        return new_em


    def getClique(self, startNode=None):

        new_em = ExplicitEpistemicModel(self.agents)

        if startNode==None:
            startNode = self.getPointedWorld()
            seen = []
            toSee = [startNode]
            new_em.addNode(startNode)
            new_em.setPointedWorld(startNode)
            self.browseGraph(seen, toSee, new_em)

        return new_em

    def browseGraph(self, seen: list, toSee: list, new_em):

        if len(toSee) == 0:
            return seen

        node = toSee[0]
        toSee = toSee[1:]
        seen.append(node)

        for succ in self.getSuccessors(node):
            if succ not in seen:
                if succ not in toSee and succ not in seen:
                    toSee.append(succ)

                if not new_em.graph.hasNode(succ):
                    new_em.addNode(succ)

            for l in self.getLabel(node, succ):
                new_em.addArc(node, succ, l)

        self.browseGraph(seen, toSee, new_em)


    def graphAsAWorldWithThisValuation(self, valuation: Valuation) -> (bool, World):
        for w in self.getNodes():
            if valuation == w.valuation:
                return True, w
        return False, None

    def isVInjective(self):
        valuations = []
        for node in self.getWorlds():
            n_v = Valuation(node.valuation)
            if n_v in valuations:
                return False
            valuations.append(n_v)
        return True

    def countWorldWithValuation(self, valuation):
        count = 0
        for node in self.getWorlds():
            if node.valuation == valuation:
                count += 1
        return count

    def getArcWithValuation(self, valuation1: Valuation, valuation2:Valuation, agent):
        count = []
        for node in self.getWorlds():
            for succ in self.getSuccessors(node, agent):
                if node.valuation == valuation1 and succ.valuation == valuation2:
                    count.append((node, succ, agent))
        return count

    @timeit
    def simple_apply(self, event_model: EventModelInterface, functionName=None):

        if functionName is None:
            functionName = ExplicitEpistemicModel.getNameWithValuation

        new_em = ExplicitEpistemicModel(self.agents)

        parents = {}

        for world in self.getWorlds():
            for event in event_model.getEvents():
                if event.canBeTriggered(self, world):

                    var_i = world.valuation
                    val = world.getValuationAfterEvent(event, self)
                    # print(world, event, var_i, val)

                    new_world = World(functionName(val), val)
                    new_em.addNode(new_world)
                    parents[new_world] = (world, event)
                    #print("->", world, event, new_world)

                    if world == self.getPointedWorld() and event == event_model.getPointedEvent():
                        new_em.setPointedWorld(new_world)

        # if new_em.getPointedWorld() == None:
        #    print(Color.get(" /!\ Warning : product update of {} on {} is impossible.".format(
        #        event_model.getPointedEvent().name,
        #        self.getPointedWorld().name), Color.CRED))

        for agent in self.agents:
            for new_world1 in parents.keys():
                for new_world2 in parents.keys():
                    nw1_world_father, nw1_event_father = parents[new_world1]
                    nw2_world_father, nw2_event_father = parents[new_world2]
                    if self.hasArc(nw1_world_father, nw2_world_father, agent) and \
                            event_model.hasArc(nw1_event_father, nw2_event_father, agent):
                        new_em.addArc(new_world1, new_world2, agent)

        #self.pointed = new_em.pointed
        #self.graph = new_em.graph

        return new_em

    @staticmethod
    def getNameWithValuation(valuation: Valuation) -> str:
        res = ' '.join([key if val else '' for key, val in valuation.items()])
        if res == '':
            return '#'
        if "pointed" in valuation.keys():
            return "S"
        return res


    @classmethod
    def getRandom(cls, nbWorlds: int, nbVars: int, nbAgents, fill_percent: float,
                  symetric=False, reflexive=False,
                  rng=None, seed:int=None,
                  varname:Callable[[str], str]=None,
                  worldname:Callable[[str], str]=None,
                  agentname: Callable[[str], str] = None,
                  vinjective: bool = False) -> ExplicitEpistemicModel:

        assert nbWorlds > 0

        if vinjective:
            assert 2**nbVars >= nbWorlds, f"To be V-injective with {nbVars} variables, you need more than {nbWorlds} worlds (at least {2^nbVars})." \
                                        f"2**{nbVars} >= {nbWorlds}"

        if rng is None:
            rng = random.Random(seed)
        if seed is None:
            seed = rng.randrange(100000000)
        rng.seed(seed)

        varname = varname if not varname is None else lambda x: f"x{x}"
        worldname = worldname if not worldname is None else lambda x: f"W_{x}"
        agentname = agentname if not agentname is None else lambda x: chr(ord('a') + x)

        agents = [agentname(ai) for ai in range(0, nbAgents)]

        eem = ExplicitEpistemicModel(agents)

        existing_valuations = []

        def get_one_val(tries=0):
            bits = [rng.getrandbits(1) for _ in range(0, nbVars)]
            if vinjective:
                if tries > 10:
                    raise Exception(f"10 tries to get v-injective new world. {nbWorlds} worlds, {nbVars} variables, {existing_valuations}")
                if bits in existing_valuations:
                    return get_one_val(tries=tries+1)
                existing_valuations.append(bits)
            dico = {varname(i): b for i, b in enumerate(bits)}
            return Valuation(dico)

        for i in range(0, nbWorlds):
            w = World(worldname(i), get_one_val())
            eem.addNode(w)
            if i == 1:
                eem.setPointedWorld(w)
            if reflexive:
                for a in agents:
                    eem.addArc(w, w, a)

        # Create arcs
        for a in agents:
            for w in eem.getWorlds():
                for w2 in eem.getWorlds():
                    if w != w2 and not eem.hasArc(w, w2, a):
                        if rng.random() < fill_percent:
                            if rng.uniform(0, 1) < fill_percent:
                                eem.addEdge(w, w2, a)
                                if symetric:
                                    eem.addEdge(w2, w, a)

        ## Add at least one arc if zero arc for an agent
        # Test
        find = set()
        for w, w2, a in eem.getArcs():
            find.add(a)
            if len(find) == len(eem.getAgents()):
                break
        # Add
        for a in eem.getAgents():
            if a not in find:
                eem.addArc(eem.getPointedWorld(), eem.getPointedWorld(), a)

        return eem



if __name__ == '__main__':

    eem = ExplicitEpistemicModel.getRandom(4, 3, 2, 0.5, reflexive=True)

    stringToSvg("main", eem.toDot())
