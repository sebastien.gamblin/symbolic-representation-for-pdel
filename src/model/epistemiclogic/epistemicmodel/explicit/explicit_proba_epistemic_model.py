
from typing import Callable
import random

from src.model.epistemiclogic.epistemicmodel.world import World
from src.model.epistemiclogic.eventmodel.explicit.explicit_event_model import EventModelInterface
from src.model.epistemiclogic.formula import formula as form
from src.utils.timer import timeit

from src.model.epistemiclogic.epistemicmodel.explicit.explicit_epistemic_model import ExplicitEpistemicModel
# from src.model.epistemiclogic.eventmodel.explicit_proba_event_model import ExplicitProbaEventModel

from src.model.datastructure.probability_distribution import DoubleProbabilityDistribution




class ExplicitProbaEpistemicModel:
    pass

class ExplicitProbaEpistemicModel(ExplicitEpistemicModel):

    def __init__(self, agents: list):
        super().__init__(agents)

        self.probabilities = {}
        for a in agents:
            self.probabilities[a] = DoubleProbabilityDistribution()

    def __repr__(self):
        string = "===> ExplicitProbaEpistemicModel \n"
        string += "Pointed : " + str(self.pointed) + " \n"
        string += "Agents : " + str(self.agents) +" \n"
        string += "Graph : \n" + str(self.graph)
        for a, probas in self.probabilities.items():
            string += f"> Proba {a} :\n" + repr(probas) + "\n"
        return string

    def addArc(self, name_u: World, name_v:World, agent: str, proba:float) -> None:
        self.graph.addArc(name_u, name_v, agent)
        self.probabilities[agent].addProba(name_u, name_v, proba)

    def setProba(self, name_u: World, name_v: World, agent: str, proba:float) -> None:
        self.probabilities[agent].setProba(name_u, name_v, proba)

    def getProba(self, name_u: World, name_v:World, agent: str) -> float:
        return self.probabilities[agent].getProba(name_u, name_v)

    def checkProba(self):
        res = True
        for agent in self.agents:
            res = True and self.probabilities[agent].check()
        return res

    def print(self):
        self.graph.print()
        for agent in self.getAgents():
            print(agent, self.probabilities[agent])

    @staticmethod
    def initWithNonProba(explicitEpistemicModel: ExplicitEpistemicModel):

        epem = ExplicitProbaEpistemicModel(explicitEpistemicModel.getAgents())

        mapping = dict()

        for world in explicitEpistemicModel.getWorlds():
            mapping[world] = World(world.getName(), world.getValuation())
            epem.addNode(mapping[world])

        for world in explicitEpistemicModel.getWorlds():
            for agent in epem.agents:
                succ = explicitEpistemicModel.getSuccessors(world, agent)
                for world2 in succ:
                    epem.addArc(mapping[world], mapping[world2], agent, 1/len(succ))

                epem.setPointedWorld(mapping[explicitEpistemicModel.getPointedWorld()])
        return epem

    def toDot(self, graph_name="G", drawP=False, drawSameValuation=False, ranks:int=None):

        listSameValuation = list()
        if drawSameValuation:
            for node1 in self.getNodes():
                for node2 in self.getNodes():
                    if node1 != node2 and node1.getValuation() == node2.getValuation():
                        listSameValuation.append((node1, node2))

        graph_name = graph_name.replace(" ", "_") + " "

        def node_name(world):
            return world.getName()
            if len([key for key, value in world.valuation.items() if value]) == 0:
                return "Empty"
            name =  " ".join([key for key, value in world.valuation.items() if value])
            return name

        def node_format(world, graph):
            return '"{}[{}]"'.format(node_name(world), graph.getId(world))

        colors = ["blue", "red", "orange", "purple", "yellow", "green", "dark"]
        graph_name += "_".join([f"{a}_{colors[i]}" for i, a in enumerate(self.getAgents())])

        list_nodes = ["{}[shape=ellipse];".format(node_format(node, self.graph)) for node in self.getNodes()]

        #list_nodes.sort()

        if self.getPointedWorld() != None:
            list_nodes += ["{}[shape=doubleoctagon color=black];".format(node_format(self.getPointedWorld(), self.graph))]

        list_arcs = ["{} -> {}{}; ".format(
            node_format(world, self.graph),
            node_format(successor, self.graph),
            '[label="{}", color="{}"]'.format(
                str(self.probabilities[agent].getProba(world, successor))[0:6],
                colors[self.agents.index(agent)]))
            for agent in self.agents for world in self.getWorlds() for successor in self.getSuccessors(world, agent)]

        for node1, node2 in listSameValuation:
            list_arcs.append("{} -> {}{}; ".format(
                node_format(node1, self.graph),
                node_format(node2, self.graph),
                '[color="{}"]'.format(colors[len(self.agents)])
            ))

        list_arcs.sort()

        rank = ''
        if not ranks is None:
            ranks_dico = {n: [] for n in range(0, ranks+1)}
            for world in self.getNodes():
                ranks_dico[world.rank].append(world)
            for i in range(0, ranks):
                names = " ".join(node_format(world, self.graph) for world in ranks_dico[i])
                rank += "{" + "rank=same; {0}".format(names) + "}\n"

        #overlap = scalexy;
        # layout = neato;

        digraph = """
                digraph {0} {{
                    compound=true;
                    edge[dir = forward]
                    node[shape = plaintext]
                    {1}
                    {2}
                    labelloc="t";
                    label="{3}";
                    {4}
                }}
                """.format('"' + graph_name + '"',
            "\n".join(list_nodes),
            "\n\t\t".join(
                list_arcs
                ), '"' + graph_name + '"',
                rank
            )

        return digraph

    def modelCheck(self, formula: form.Formula, assertion=True, world: World=None, show=False):

        if world is None:
            world = self.getPointedWorld()

        if assertion:
            assert world in self.getNodes(), "Unknown world {} in EpistemicModel".format(str(world))

        if isinstance(formula, form.Top):
            return True
        elif isinstance(formula, form.Bot):
            return False
        elif isinstance(formula, form.Atom):
            #print("Atom", formula, formula.inner, world.getValuation(), world.getValuation()[formula.inner])
            return bool(world.getValuation()[formula.inner])
        elif isinstance(formula, form.Box):
            for world2 in self.getSuccessors(world):
                if not self.modelCheck(formula.getFormulas(), assertion=assertion, world=world2):
                    return False
            return True
        elif isinstance(formula, form.Box_a) or isinstance(formula, form.K):
            for world2 in self.getSuccessors(world, formula.getAgent()):
                if not self.modelCheck(formula.getFormulas(), assertion=assertion, world=world2):
                    #print("K", formula.getFormulas(), world2, "False")
                    return False
            return True
        elif isinstance(formula, form.nK):
            i = formula.getAgent()
            p = formula.inner
            n_form = form.And(form.Not(form.K(i, p)), form.Not(form.K(i, form.Not(p))))
            return self.modelCheck(n_form, assertion=assertion, world=world)
        elif isinstance(formula, form.Diamond):
            for world2 in self.getSuccessors(world):
                if not self.modelCheck(formula.getFormulas(), assertion=assertion, world=world2):
                    return False
            return True
        elif isinstance(formula, form.Diamond_a):
            for world2 in self.getSuccessors(world, formula.getAgent()):
                if self.modelCheck(formula.getFormulas(), assertion=assertion, world=world2):
                    return True
            return False
        elif isinstance(formula, form.K_hat):
            n_form = form.Not(form.K(formula.getAgent(), form.Not(formula.inner)))
            return self.modelCheck(n_form, assertion=assertion, world=world)
        elif isinstance(formula, form.Implies):
            left, right = formula.getFormulas()
            return self.modelCheck(form.Or(form.Not(left), right), assertion=assertion, world=world)
        elif isinstance(formula, form.Equiv):
            left, right = formula.getFormulas()
            return self.modelCheck(form.And(form.Implies(left, right), form.Implies(right, left)), assertion=assertion, world=world)
        elif isinstance(formula, form.Not):
            return not self.modelCheck(formula.getFormulas(), assertion=assertion, world=world)
        elif isinstance(formula, form.And):
            left, right = formula.getFormulas()
            return self.modelCheck(left, assertion=assertion, world=world) and self.modelCheck(right, assertion=assertion, world=world)
        elif isinstance(formula, form.Or):
            left, right = formula.getFormulas()
            return self.modelCheck(left, assertion=assertion, world=world) or self.modelCheck(right, assertion=assertion, world=world)
        elif isinstance(formula, form.ite):
            x, left, right = formula.getFormulas()
            return self.modelCheck(form.Or(form.And(x, left), form.And(form.Not(x), right)), assertion=assertion, world=world)
        elif isinstance(formula, form.Pr):
            somme = 0
            for world2 in self.getSuccessors(world, formula.getAgent()):
                res = self.modelCheck(formula.inner, assertion=assertion, world=world2)
                # print(formula.inner, "?", world, world2, res)
                if res:
                    # print(self.getProba(world, world2, formula.getAgent()))
                    somme += self.getProba(world, world2, formula.getAgent())
            #print(formula, world, somme)
            return eval(f"{somme} {formula.op} {formula.k}")
        elif isinstance(formula, form.Exactly):
            return formula.n == sum([world.getValuation()[var] for var in formula.array])

        elif isinstance(formula, form.After):

            ev, f = formula.getFormulas()
            future = self.apply(ev)
            res = future.modelCheck(f, assertion=assertion, world=future.getPointedWorld())
            if show:
                print("IN MC After ExplPrStuct")
                print(future.getPointedWorld())
                print(future)
                print("MC on", f)
            return res
        else:
            raise Exception(f"{type(formula).__name__} formula is not supported : {formula.__name__}")




    @timeit
    def simple_apply(self, event_model: EventModelInterface, functionName=None, normalisation=True):

        if functionName is None:
            functionName = ExplicitProbaEpistemicModel.getNameWithValuation

        if not type(event_model).__name__ in ["ExplicitPDEL_EventModel", "ExplicitDELP_EventModel"]: # dirty, not import problem...
            raise TypeError("Event model must be an ExplicitPDEL_EventModel or ExplicitDELP_EventModel, not a {}.".format(type(event_model).__name__))

        if type(event_model).__name__ == "ExplicitPDEL_EventModel":
            #print("PDEL OK")
            return self.__apply_pdel(event_model, functionName=functionName, normalisation=normalisation)

        elif type(event_model).__name__ == "ExplicitDELP_EventModel":
            print("APPLY", event_model.getName())
            return self.__apply_delp(event_model, functionName=functionName, normalisation=normalisation)


    def __apply_pdel(self, event_model, functionName=None, normalisation=True):
        new_em = ExplicitProbaEpistemicModel(self.agents)

        parents = {}

        #print("Pointed:", self.getPointedWorld(), event_model.getPointedEvent())

        # CREATE ALL NEW WORLDS
        for world in self.getWorlds():
            for event in event_model.getEvents():

                res = event_model.getCondProbabilityDistribution().getMyProba(self, world, event)
                # print(world, event, res)
                if res > 0 :
                    # if event.canBeTriggered(self, world):

                    # print("ok", world, event)
                    val = world.getValuationAfterEvent(event, self)

                    new_world = World(f"({world.getName()},{event.getName()})", val)
                    new_em.addNode(new_world)
                    parents[new_world] = (world, event)

                    if world == self.getPointedWorld() and event == event_model.getPointedEvent():
                        new_em.setPointedWorld(new_world)
                        # print("new pointed")

        assert new_em.getPointedWorld() is not None

        # CONNECT ALL NEW WORLDS
        for agent in self.agents:
            for new_world1 in parents.keys():
                for new_world2 in parents.keys():
                    nw1_world_father, nw1_event_father = parents[new_world1]
                    nw2_world_father, nw2_event_father = parents[new_world2]
                    if self.hasArc(nw1_world_father, nw2_world_father, agent) and \
                            event_model.hasArc(nw1_event_father, nw2_event_father, agent):

                        proba = self.getProba(nw1_world_father, nw2_world_father, agent) * \
                                event_model.getCondProbabilityDistribution().getMyProba(self, nw2_world_father, nw2_event_father) * \
                                event_model.getProbaArc(nw1_event_father, nw2_event_father, agent)
                        """
                        print((nw1_world_father, nw1_event_father),
                              (nw2_world_father, nw2_event_father),
                              self.getProba(nw1_world_father, nw2_world_father, agent),
                              event_model.getCondProbabilityDistribution().getMyProba(self, nw2_world_father, nw2_event_father),
                              event_model.getProbaArc(nw1_event_father, nw2_event_father, agent))
                        """

                        new_em.addArc(new_world1, new_world2, agent, proba)

        # UPDATE PROBABILITIES    new_proba_arc = proba_arc / sum(outgoing_arcs)
        if normalisation:
            new_em.normalisation()


        return new_em


    def __apply_delp(self, event_model, functionName=None, normalisation=True):
        if functionName is None:
            functionName = ExplicitProbaEpistemicModel.getNameWithValuation

        new_em = ExplicitProbaEpistemicModel(self.agents)

        parents = {}

        for world in self.getWorlds():
            for event in event_model.getEvents():
                if event.canBeTriggered(self, world):

                    var_i = world.valuation
                    #if event.getPostcondition() is not None:
                    val = world.getValuationAfterEvent(event, self)
                    # print(world, event, var_i, val)
                    #else:
                    #    val = world.valuation

                    new_world = World(functionName(val), val)
                    new_em.addNode(new_world)
                    parents[new_world] = (world, event)
                    # print("->", world, event, new_world)

                    if event_model.getPointedEvent() is None:
                        if world == self.getPointedWorld():
                            if new_em.getPointedWorld() is None:
                                new_em.setPointedWorld(new_world)
                            else:
                                print("Caution : multiple pointed worlds")

                    if world == self.getPointedWorld() and event == event_model.getPointedEvent():
                        new_em.setPointedWorld(new_world)

        for agent in self.agents:
            for new_world1 in parents.keys():
                for new_world2 in parents.keys():
                    nw1_world_father, nw1_event_father = parents[new_world1]
                    nw2_world_father, nw2_event_father = parents[new_world2]
                    if self.hasArc(nw1_world_father, nw2_world_father, agent) and \
                            event_model.hasArc(nw1_event_father, nw2_event_father, agent):

                        proba = self.getProba(nw1_world_father, nw2_world_father, agent) * \
                                event_model.getProbaArc(nw1_event_father, nw2_event_father, agent)

                        new_em.addArc(new_world1, new_world2, agent, proba)

        # UPDATE PROBABILITIES    new_proba_arc = proba_arc / sum(outgoing_arcs)
        if normalisation:
            new_em.normalisation()

        return new_em

    def normalisation(self):
        for agent in self.agents:

            for world in self.getWorlds():
                successeurs = self.getSuccessors(world, agent)
                somme = 0
                for succ in successeurs:
                    somme += self.getProba(world, succ, agent)

                for succ in successeurs:
                    new_proba = self.getProba(world, succ, agent) / somme
                    self.setProba(world, succ, agent, new_proba)



    @classmethod
    def getRandom(cls, nbWorlds: int, nbVars: int, nbAgents, fill_percent: float,
                  symetric=False, reflexive=False,
                  rng=None, seed:int=None,
                  varname:Callable[[str], str]=None,
                  worldname:Callable[[str], str]=None,
                  agentname: Callable[[str], str] = None
                  ) -> ExplicitProbaEpistemicModel:


        if rng is None:
            rng = random.Random(seed)
        if seed is None:
            seed = rng.randrange(100000000)
        rng.seed(seed)

        eem = super().getRandom(nbWorlds, nbVars, nbAgents, fill_percent,
                          symetric=symetric, reflexive=reflexive,
                          rng=rng, seed=seed, varname=varname, worldname=worldname, agentname=agentname, vinjective=True)

        epem = cls.initWithNonProba(eem)

        for agent in epem.getAgents():
            for w in epem.getWorlds():
                succ = epem.getSuccessors(w, agent)

                randoms = [rng.randrange(1, 100)/100 for _ in succ]
                add = sum(randoms)

                check = 0
                for i, w2 in enumerate(succ):
                    n_proba = randoms[i]/add
                    check += n_proba
                    epem.setProba(w, w2, agent, n_proba)

                assert check > 0.999999 and check < 1.000001, f"Sum is {check} ??"

        return epem


if __name__ == '__main__':

    epem = ExplicitProbaEpistemicModel(["a", "b"])

    epem.addNode("Node1")
    epem.addNode("Node2")
    epem.addArc("Node1", "Node2", "a", 0.25)

    print(epem.probabilities["a"].sumOf("Node1"))

    ExplicitProbaEpistemicModel.getRandom(4, 4, 2, 0.5).print()
