

from abc import ABCMeta, abstractmethod

from src.model.epistemiclogic.epistemicmodel.world import World
from src.model.epistemiclogic.formula import formula as form
from src.model.epistemiclogic.eventmodel.event_model_interface import EventModelInterface, EventModelPipeline, EventModelList
from src.io.write import stringToSvg

from src.io.write import getDefaultOutputPath

import time

class EpistemicModelInterface:
    pass

class EpistemicModelInterface(metaclass=ABCMeta):

    @abstractmethod
    def __init__(self):
        pass

    @abstractmethod
    def getAgents(self):
        pass

    @abstractmethod
    def getPointedWorld(self) -> World:
        pass

    @abstractmethod
    def setPointedWorld(self, world: World):
        pass

    @abstractmethod
    def show(self, limit=20):
        pass

    @abstractmethod
    def modelCheck(self, formula: form.Formula, world: World=None):
        pass

    @abstractmethod
    def simple_apply(self, event_model: EventModelInterface):
        pass

    #@abstractmethod
    #def apply(self, EventModelInterface):
    def apply(self, eventmodel: EventModelInterface, logtime={}, key="products", normalisation=True):

        def get_random(ev_list):
            import random
            if ev_list.random_choice:

                pick_event = None
                to_checks = [e for e in ev_list]

                while pick_event is None:
                    if len(to_checks) == 0:
                        raise Exception(f"No applicable events in EventModelList {ev}")

                    test_event = random.choice(to_checks)

                    to_checks.remove(test_event)
                    # print("check", test_event, test_event)
                    if self.isApplicable(test_event):
                        pick_event = test_event

            else:
                pick_event = None
                for e in ev_list:
                    if self.isApplicable(e):
                        pick_event = e
                        break
            print(f"pick random={ev_list.random_choice} : {pick_event}. len(ev_list)={len(ev_list)}")
            assert not pick_event is None
            return pick_event

        #print("APPLY", eventmodel)

        if key not in logtime.keys():
            logtime[key] = []

        if isinstance(eventmodel, EventModelList):
            eventmodel = get_random(eventmodel)

        if isinstance(eventmodel, EventModelInterface):
            t1 = time.time()
            res = self.simple_apply(eventmodel, normalisation=normalisation)
            t = time.time() - t1
            logtime[key].append((eventmodel.getName(), t))

            if hasattr(self, 'manager'):
                res.manager = self.manager

            return res

        if isinstance(eventmodel, EventModelPipeline):
            assert len(eventmodel) > 0

            res = self
            all_pipe = time.time()
            for iev, ev in enumerate(eventmodel):
                t = time.time()
                if isinstance(ev, EventModelList):
                    ev = get_random(ev)

                assert isinstance(ev,
                                  EventModelInterface), f"SymbolicProbaEventModel are need in EventModelPipeline, not {type(ev).__name__}"


                res = res.simple_apply(ev, normalisation=normalisation)

                #stringToSvg(ev.getName(), res.toDot())

                if hasattr(self, 'manager'):
                    res.manager = self.manager


                name = f"product_{iev}_{ev.getName()}"

                #print()
                #print("New")
                #res.getPointedWorld().print(trueAtoms=True)
                #for agent in res.getAgents():
                #    if len(list(res.getPointedWorld().support())) == 1:
                #        res.agent_graph[agent].conditionning(res.getPointedWorld()).print(trueAtoms=True, limit=20)
                #    else:
                #        print("NO SUPPORT")
                #print()

                logtime[key].append((name, time.time()-t))
            logtime[key].append(("all_pipe", time.time()-all_pipe))
            return res

        raise Exception(
            f"eventmodel need to be SymbolicProbaEventModel or EventModelPipeline not {type(eventmodel).__name__}")




    @classmethod
    def dumps(cls, emi: EpistemicModelInterface, cache=None) -> None:

        from src.utils.memoize import memoize
        from src.model.datastructure.real_function import PseudoBooleanFunction

        if cache is None:
            cache = {}

        dico = {
            "type": type(emi).__name__,
            "agents": emi.getAgents(),
            "variables": emi.variables,
            "varprimes": emi.varprimes,
            "primetonoprime": emi.primetonoprime,
            "rename_unafter": emi.rename_unafter,
            "manager_type": type(emi.manager).__name__,
            "agent_graph": {
                a: PseudoBooleanFunction.dumps(emi.agent_graph[a])
                for a in emi.getAgents()
            },
            "pointed": PseudoBooleanFunction.dumps(emi.pointed)
        }

        return dico

    @classmethod
    def dump(cls, emi: EpistemicModelInterface, filename:str, cache=None, indent=None, location=getDefaultOutputPath()) -> str:
        """
        Transform DD into string, as json.
        :param pbf: pseudo boolean function as RealFunction
        :return: None
        """
        import json

        with open(f"{location}/{filename}.json", "w") as outfile:
            json.dump(cls.dumps(emi, cache=cache), outfile, indent=indent)



    @classmethod
    def loads(cls, jsonstring: str, cache=None, manager=None) -> EpistemicModelInterface:
        """
        :param manager:
        :param jsonstring:
        :param cache:
        :return:
        """

        if cache is None:
            cache = {}

        import json
        from src.utils.memoize import memoize
        from src.model.datastructure.real_function import PseudoBooleanFunction
        from src.model.datastructure.add.add_real_function import ADDManager
        #from src.model.epistemiclogic.epistemicmodel.symbolic.symbolic_proba_epistemic_model import SymbolicProbaEpistemicModel

        data = json.loads(jsonstring)

        agents = data["agents"]
        variables = data["variables"]

        if manager is None:
            manager = eval(data["manager_type"]).create()
        else:
            assert data["manager_type"] == type(manager).__name__

        proba_tables = []
        for agent in data["agents"]:
            proba_tables.append(PseudoBooleanFunction.loadd(manager, data["agent_graph"][agent]))

        pointed = PseudoBooleanFunction.loadd(manager, data["pointed"])

        return eval(data["type"])(agents, variables, proba_tables, manager=manager, pointed=pointed)


    @classmethod
    def load(cls, filename: str, cache=None, manager=None, location=getDefaultOutputPath()) -> EpistemicModelInterface:

        assert isinstance(filename, str), f"'filename' need to be a string, not a {type(str).__name__}"

        return cls.loads(open(f"{location}/{filename}").read(), cache=cache, manager=manager)
