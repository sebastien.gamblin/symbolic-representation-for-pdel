
from src.model.SMCPDEL.pySMCDEL import *
from src.model.datastructure.real_function import PseudoBooleanFunction


class ConditionalPrecondition(SymbolicPrecondition):

    def __init__(self, conditional_probabilities: Union[Dict[Formula, Tuple[Tuple[List[str], float]]], PseudoBooleanFunction],
                 manager: PseudoBooleanFunctionManager, vocabulary, vocabulary_plus, cache: dict = None):

        super().__init__(manager, vocabulary, cache)

        self.entry = conditional_probabilities

        self.vocabulary_plus = vocabulary_plus

        if isinstance(conditional_probabilities, PseudoBooleanFunction):
            # Nothing to do here, we have the good object
            self.precondition = conditional_probabilities
            return

        is_compilable = True
        if isinstance(conditional_probabilities, dict):
            for pred, distribution in conditional_probabilities.items():
                assert isinstance(pred, Formula), f"precondition must be a Formula, not a {type(pred).__name__}"
                assert isinstance(distribution, tuple), f"distribution must be a tupe, not a {type(distribution).__name__}"
                for pointed, value in distribution:
                    assert isinstance(pointed, list), f"event ids need to be a list, not a {type(pointed).__name__}"
                    assert isinstance(value, float), f"event proba need to be a folat, not a {type(value).__name__}"
                    for p in pointed:
                        assert isinstance(p, str), f"ids event must be a str, not a {type(p).__name__}"

                if not pred.isPropositionnal():
                    is_compilable = False
                    break

        if is_compilable:
            # This pre-compilable
            self.precondition = self.__calculate_precondition(conditional_probabilities)
        else:
            # Impossible to compile, the formula isn't propositionnal
            # By default, this statement isn't uniform !
            self.precondition = None
            self.conditional_probabilities = conditional_probabilities

    def __repr__(self):
        string = "Conditional Probabilities: \n"
        for k, v in self.entry.items():
            string += f"{k} : {v} \n"
        string += str(self.precondition)
        return string

    def get_translation(self, structure: Structure) -> PseudoBooleanFunction:
        if self.precondition is None:
            # we have a dict
            return self.__calculate_precondition(self.conditional_probabilities, structure)
        # we return directly the RealFunction
        return self.precondition

    def __calculate_precondition(self, conditional_probabilities: Dict[Formula, Tuple[Tuple[List[str], float]]], structure: Structure=None):

        if structure is not None:
            assert structure.vocabulary == self.vocabulary

        add_accum = self.manager.getConstant(0.0)
        for formula, distribution in conditional_probabilities.items():
            # print("formula", formula)
            for pointed, value in distribution:
                constant = self.manager.getConstant(value)

                if structure is None:
                    translation = self.manager.from_formula(formula, vars=self.vocabulary + self.vocabulary_plus, cache=self.cache)
                else:
                    assert isinstance(structure, ProbaStructure), f"Structure need to be a ProbaStructure to use conditional" \
                                                                  f" probabilities, not a {type(structure).__name__}"
                    translation = structure.translation(formula, cache=self.cache)

                sqs_pointed = sqsubseteq_formula(pointed, self.vocabulary_plus, self.manager)
                value_times_sqs = sqs_pointed.apply("x", translation.apply("x", constant))
                add_accum = add_accum.apply("+", value_times_sqs, cache=self.cache)

        #add_accum.print()
        scope = add_accum.getVariables()
        struct_voc = [] if structure is None else structure.vocabulary
        extend = [v for v in self.vocabulary + self.vocabulary_plus if v not in scope]
        #print("1", add_accum)
        add_accum.extendScope(extend)
        #print("2", add_accum)
        return add_accum

    def rename(self, rename_dictionary: Dict[str, str]) -> 'ConditionalPrecondition':

        for k in rename_dictionary:
            assert k in self.vocabulary + self.vocabulary_plus, f"{k} isn't in {self.vocabulary}"

        new_vocabulary = [v if k in self.vocabulary else k for k, v in rename_dictionary.items()]
        new_vocabulary_plus = [v if k in self.vocabulary_plus else k for k, v in rename_dictionary.items()]

        if self.precondition is None:
            # Not precalculated
            newtheta = {k.rename(rename_dictionary): v for k, v in self.conditional_probabilities}

            new_cond_pr = {}
            for formula, distribution in self.conditional_probabilities.items():
                # print("formula", formula)
                liste = []
                for pointed, value in distribution:
                    liste.append(([p if p not in rename_dictionary.keys() else rename_dictionary[p] for p in pointed], value))
                new_cond_pr[formula.rename(rename_dictionary)] = tuple(liste)
            return ConditionalPrecondition(newtheta, self.manager, new_vocabulary, new_vocabulary_plus, cache=self.cache)

        return ConditionalPrecondition(self.precondition.rename(rename_dictionary), self.manager, new_vocabulary, new_vocabulary_plus, cache=self.cache)

    def conditionning(self, dico: dict):

        if self.precondition is None:
            new_cond_pr = {}
            for formula, distribution in self.conditional_probabilities.items():
                new_cond_pr[formula.conditionning(dico)] = distribution
            return ConditionalPrecondition(new_cond_pr, self.manager, self.vocabulary, self.vocabulary_plus, cache=self.cache)

        return ConditionalPrecondition(self.precondition.conditionning(dico), self.manager, self.vocabulary, self.vocabulary_plus, cache=self.cache)


class ProbaStructure:
    pass


class ProbaTransformer:
    pass


class ProbaStructure(BeliefStructure):

    @timeit
    def __init__(self, v: List[str], theta: PseudoBooleanFunction, o: Dict[str, Formula], pi: Dict[str, Formula],
                 pointed: List[str], manager, isnormalized=True, pi_in_theta=False, nb_apply=1, cache=None, **kwargs):

        super().__init__(v, theta, o, pointed, manager, nb_apply=nb_apply, cache=cache, **kwargs)

        self.isnormalized = isnormalized

        self.pi = {}
        # PIs are mentionned as a formula
        for k, pia in pi.items():
            self.pi[k] = self.manager.from_formula(pia, vars=self.double_vocabulary) if isinstance(pia, Formula) else pia

        # PIs are mentionned as PBF
        for a, ipi in self.pi.items():
            assert isinstance(ipi, PseudoBooleanFunction), f"Probabilities need to be function, not : {type(ipi).__name__}"
            assert ipi.scopeEquals(self.double_vocabulary), str(set(ipi.getVariables()).symmetric_difference(self.double_vocabulary))

        # Store here the precompilation of Pi x ThetaPrime
        # we do that lazily during Model Checking
        # will be use in all MC (denormalized and normalized)
        self.pithetaprime = None
        # will be use only in denormalized MC
        self.pithetaprime_marginalized = None

        self.compute_pi_theta_prime_time = None
        self.compute_pi_theta_prime_marginalized_time = None

        """
        # DO NOTHING : we doesn't want to precompile PIxThetaPrime anymore.
        """

    def compute_pi_theta_prime(self):
        # If PI not in Theta, we compute pithetaprime, to facilitate Model Checking
        self.pithetaprime = {}
        toprimes = {p: prime(p) for p in self.vocabulary}
        for a in self.pi:
            self.pithetaprime[a] = self.pi[a].apply("x", self.state_law.rename(toprimes))

    def compute_pi_theta_prime_marginalied(self):
        # Do normalization here
        # Caution : pithetaprime need to be computed with compute_pi_theta_prime()
        self.pithetaprime_marginalized = {}
        primes = [prime(p) for p in self.vocabulary]
        for a, pi in self.pithetaprime.items():
            self.pithetaprime_marginalized[a] = pi.marginalization(primes)

    def renameTransformer(self, transformer):

        new_transformer, dico_rename_id, dico_rename_prime = self.gen_renameTransformer(transformer)

        dico = {}
        for a, oi in new_transformer.omega.items():
            dico[a] = oi.rename(dico_rename_id).rename(dico_rename_prime)
        new_transformer.omega = dico

        dico2 = {}
        for a, pi_i in new_transformer.pi.items():
            dico2[a] = pi_i.rename(dico_rename_id).rename(dico_rename_prime)
        new_transformer.pi = dico2

        return new_transformer

    def __repr__(self):
        res1 = "== ProbaStructure ==\n"
        res2 = f"Pi =\n"

        for a, ipi in self.pi.items():
            res2 += f" - {a} :\n"
            print(type(ipi).__name__)
            res2 += ipi.print(string=True, limit=10, trueAtoms=True, notZero=True)

        res2 += f"PiTheta'xTheta =\n"
        for a, ipi in self.pi.items():
            res2 += f" - {a} :\n"
            res2 += self.pithetaprime[a].apply("x", self.state_law).print(string=True, limit=10, trueAtoms=True, notZero=True)

        return res1 + super().__repr__() + res2 + "<< ProbaStructure =="

    @timeit
    def manual_cleaning(self, to_forget, **kwargs):

        # print(self.v, to_forget)

        v = [e for e in self.vocabulary if e not in to_forget]
        theta = self.state_law.EForget(to_forget)
        o = {k: [e for e in l if e not in to_forget] if isinstance(l, list) else l.UForget(to_forget) for k, l in self.omega.items()}
        pointed = [e for e in self.pointed if e not in to_forget]

        pi = {}
        for k, p in self.pi.items():

            num = p.marginalization(to_forget)
            primes = [p for p in num.getVariables() if not is_primed(p)]
            pi[k] = num.apply("/norm", num.marginalization(primes))

        return ProbaStructure(v, theta, o, pi, manager=self.manager, pointed=pointed, cache=self.cache,
                              nb_apply=self.nb_apply, **kwargs)

    def update_law(self, transformer: Transformer) -> PseudoBooleanFunction:
        """
        Rewrote function to deals with Conditional Preconditions.
        - theta^new = [V_/V_°](theta /\ support(||THETA||_F)) /\ /\_{q \in V_} (q <-> [V_/V_°](theta_(q)))
        :param transformer:
        :return:
        """
        # frame : /\_{q \in V_} (q <-> [V_/V_°](theta_(q)))
        cache = self.cache.update(transformer.cache)
        # [V_ -> V_°](theta /\ ||theta+||_F) /\ /\_{q \in V_} (q <-> [V_ -> V_°](theta_(q)))
        frame = self.manager.from_formula(Top())
        for x in transformer.modified_vocabulary:
            eq = equivADD(self.manager.from_formula(Atom(x)),
                          transformer.theta_[x].rename(transformer.historicisation), cache=self.cache)
            frame = frame.apply("and", eq, cache=self.cache)

        # BeliefStructure : (theta /\ ||THETA||_F)

        # print("Translation")
        translation = transformer.event_law.get_translation(self)

        thetanew = self.state_law.apply("and", translation.booleanization(), cache=cache)
        thetanew = thetanew.rename(transformer.historicisation)
        thetanew = thetanew.apply("and", frame, cache=cache)

        return thetanew

    @timeit
    def simple_apply(self, belief_transformer: Transformer, **kwargs):

        assert isinstance(belief_transformer, ProbaTransformer)

        print(belief_transformer.name)

        with Timer(f"simple apply {belief_transformer.name}", show=False) as father:

            with Timer("BeliefStructure simple_apply", father=father):
                res = super().simple_apply(belief_transformer)

            id_event = list(belief_transformer.historicisation.values())
            var_list = [v for v in res.vocabulary + id_event if not is_primed(v)]
            prime_list = [prime(v) for v in res.vocabulary + id_event if not is_primed(v)]
            to_prime = {v: prime(v) for v in res.vocabulary + id_event if not is_primed(v)}

            btthetavariables = belief_transformer.structure_vocabulary + belief_transformer.vocabulary

            if type(belief_transformer.event_law) == ConditionalPrecondition:
                big_theta = belief_transformer.event_law.get_translation(self)

                assert big_theta.scopeEquals(btthetavariables), \
                    f"In left in excess (big_theta):{[v for v in big_theta.getVariables() if v not in btthetavariables]} \n" \
                    f"In right in excess (btthetavariables): {[v for v in btthetavariables if v not in big_theta.getVariables()]}"

            newpi = {}
            for a, pi_i in self.pi.items():

                #print("self.bttheta", res.bttheta.getVariables())
                #print("Pi_i", a)
                #pi_i.print(limit=10)
                #print("Pi+_i", a, bt.pi[a].getVariables())
                #bt.pi[a].print()
                #print("BTTHETA prime", a, to_prime)

                if type(belief_transformer.event_law) == FormulaPrecondition:
                    # no need to multiply with law
                    # ren = ([V_ -> V_°] PI_a x prime(THETA))
                    ren = pi_i.rename(belief_transformer.historicisation).rename(belief_transformer.historicisation_prime)
                else:
                    # multiply with big law
                    big_theta_prime = big_theta.rename({p: prime(p) for p in btthetavariables})
                    # [V_ -> V_°] PI_a x prime(THETA)
                    # ren = ([V_ -> V_°] (PI_a x big_theta) x prime(THETA))
                    ren = pi_i.apply("x", big_theta_prime).rename(belief_transformer.historicisation).rename(
                        belief_transformer.historicisation_prime)

                # ren x PI+_a
                pi_times = ren.apply("x", belief_transformer.pi[a])
                pi_times.extendScope(belief_transformer.modified_vocabulary + [prime(vi) for vi in belief_transformer.modified_vocabulary])

                newpi[a] = pi_times

            if self.isnormalized:

                newpi2 = {}
                for a, pi_i in newpi.items():
                    pithetaprime = newpi[a].apply("x", res.state_law.rename(to_prime))
                    newpi2[a] = pithetaprime.marginalize_and_normalize(prime_list)

                newpi = newpi2

        return ProbaStructure(res.vocabulary, res.state_law, res.omega, newpi, pi_in_theta=self.isnormalized, pointed=res.pointed,
                              manager=res.manager, isnormalized=self.isnormalized, nb_apply=self.nb_apply+1)

    @staticmethod
    def toPDEL(kt, cache=None, normalized=True):
        """
        Transform a PDEL Structure into ProbabilisticStructure
        By default the outgoing probabilities of a node are equal.

        The probabilities could be normalised.

        :param kt: Belief Structure, or Pipeline of BeliefStructure

        :param cache:
        :return:
        """

        if cache is None: cache = {}

        def inner(s):
            assert isinstance(s, BeliefStructure), f"{type(s).__name__}"

            primes = [prime(v) for v in s.vocabulary]

            if normalized:
                pi = {}
                for a, o in s.omega.items():
                    pi[a] = o.marginalize_and_normalize(primes).copy()
            else:
                pi = {a: o.copy() for a, o in s.omega.items()}

            ps = ProbaStructure(s.vocabulary, s.state_law, s.omega.copy(), pi.copy(),
                                manager=s.manager,
                                pointed=s.pointed, isnormalized=normalized)
            return ps

        # for pipeline
        if isinstance(kt, list):
            return [inner(t) for t in kt]

        return inner(kt)

    """
    def modelCheck(self, formula, pointed=None, show=False, cache=None, **kwargs):

        if pointed is None:
            pointed = self.pointed

        if show:
            print(">> in BeliefTransformer.modelCheck()")
            print("of", formula)

        kt = self.translation(formula, cache=self.cache, show=show)

        if show:
            print("Knowledge Translation")
            kt.print(limit=100, trueAtoms=True)

        left = sqsubseteq_formula(pointed, self.vocabulary, self.manager)

        if show:
            print("sqsubseteq_formula on, ", pointed, self.vocabulary)
            left.print(trueAtoms=True)

        res = impliesADD(left, kt, cache=self.cache, **kwargs)

        if show:
            print("(sqsubseteq_formula -> Knowledge Translation MC).isTrue() ?")
            res.print(limit=10, trueAtoms=True)
            print("<< out BeliefTransformer.modelCheck()")
        return res.isTrue()
    """

    @timeit
    def translation(self, phi: Formula, cache=None, show=False, **kwargs) -> PseudoBooleanFunction:

        if cache is None:
            cache = {}

        get_sizes = False

        @memoize(cache)
        def rec_from_formula(form):

            if isinstance(form, Pr):
                """
                ||φ|| is "rec" formula
                'op' is the bool_cut transformation as filter to keep good probabilities

                if Π normalized :
                    MargΣ_{WS'} ( Π x ||φ||' x θ') 'op' β
                    Π x θ' is precompiled in ProbaStructure : pithetaprime
                    
                else:
                   MargΣ_{WS'} ( Π^d x ||φ||' x θ') 'op' β x_norm MargΣ_{Π^d x θ'}

                    Π^d : marg_pi_d is precompiled in ProbaStructure
                    Π^d x θ' : pithetaprime is precompiled in ProbaStructure

                    MargΣ_{Π^d x θ'} need to be seen as denominator of left member, but use in 'x_norm' operator

                """

                if self.pithetaprime is None:
                    with Timer("compute PI THETAPRIME") as t1:
                        self.compute_pi_theta_prime()
                    self.compute_pi_theta_prime_time = t1.t

                # Used only in denormalized MC
                if self.pithetaprime_marginalized is None and not self.isnormalized:
                    with Timer("compute PI THETAPRIME MARG") as t2:
                        self.compute_pi_theta_prime_marginalied()
                    self.compute_pi_theta_prime_marginalized_time = t2.t

                toforget = [prime(x) for x in self.vocabulary]  # prime of v
                to_prime = {x: prime(x) for x in self.vocabulary}

                with Timer(f"rec_from_formula father : Pr_{form.agent}", show=False, incr=2, sort=False) as father:

                    with Timer(f" sub_translation", show=False, incr=1, father=father) as timer_sub_translation:
                        rec = self.translation(form.inner)
                        if get_sizes:
                            infos = rec.get_nodes_informations()
                            timer_sub_translation.add_string(f"noeuds={infos['NoTerminals']}, terminaux={infos['Terminals']}")
                    if show:
                        print("Rec")
                        rec.print(limit=10, trueAtoms=True)

                    if show:
                        print("PITHETA PRIME")
                        self.pithetaprime[form.agent].print(limit=10, trueAtoms=True)
                        print("to_prime", to_prime)
                        print("rec.getVariables()", rec.getVariables())

                    if __debug__:
                        for v in rec.getVariables():
                            assert v in to_prime, f"{v} not in toprime"

                    with Timer(f"to_marg", show=False, incr=1, father=father) as timer_to_marg:
                        to_marg = rec.rename(to_prime).apply("x", self.pithetaprime[form.agent], cache=self.cache)
                        if get_sizes:
                            infos = self.pithetaprime[form.agent].get_nodes_informations()
                            timer_to_marg.add_string(f"PITHETAPRIME : noeuds={infos['NoTerminals']}, terminaux={infos['Terminals']}")
                            infos = to_marg.get_nodes_informations()
                            timer_to_marg.add_string(f"TO_MARG : noeuds={infos['NoTerminals']}, terminaux={infos['Terminals']}")

                    if show:
                        print("To marg")
                        to_marg.print(limit=10)

                    if show:
                        print("rec' x pithetaprime", to_marg.get_size(), to_marg.pointer)

                    with Timer(f"marginalization", show=False, incr=1, father=father) as marginalization_timer:
                        marg = to_marg.marginalization(toforget)
                        if get_sizes:
                            infos = marg.get_nodes_informations()
                            marginalization_timer.add_string(f"noeuds={infos['NoTerminals']}, terminaux={infos['Terminals']}")

                    if show:
                        print("marg")
                        marg.print(trueAtoms=True, limit=10)

                    if self.isnormalized:
                        filtre = marg.bool_cut(form.op, form.k)
                        if show:
                            print("filtre bool cut", form.op, form.k)
                            filtre.print()
                        res = filtre

                    else:
                        if form.op in [">=", "<"]:
                            opp = "xnorm1"
                        elif form.op in ["<=", ">"]:
                            opp = "xnorm2"
                        else:
                            raise Exception(f"Unknown operator for Pr {form.op}")

                        with Timer(f"beta_product_norm", show=False, incr=1, father=father) as beta_product_norm_timer:
                            beta_product_norm = self.manager.getConstant(form.k).apply(opp, self.pithetaprime_marginalized[form.agent])
                            if get_sizes:
                                infos = beta_product_norm.get_nodes_informations()
                                beta_product_norm_timer.add_string(f"noeuds={infos['NoTerminals']}, terminaux={infos['Terminals']}")

                        if show:
                            print("beta_product", opp)
                            beta_product_norm.print(limit=20, trueAtoms=False)

                        with Timer(f"marg.apply(form.op, beta_product_norm)", show=False, incr=1, father=father) as marg_apply_timer:
                            res = marg.apply(form.op, beta_product_norm)
                            if get_sizes:
                                infos = res.get_nodes_informations()
                                marg_apply_timer.add_string(f"noeuds={infos['NoTerminals']}, terminaux={infos['Terminals']}")

                assert res.scopeEquals(self.vocabulary), str(res.getVariables())
                return res

            elif isinstance(form, MPr):
                """
                See documentation in "isinstance(form, Pr)"

                BigΣ_i [ α_i x MargΣ_{WS'} ( Π^d x ||φ_i||' x θ') ] 'op' β x_norm MargΣ_{Π^d x θ'}
                """

                if self.pithetaprime is None:
                    with Timer("compute PI THETAPRIME") as t1:
                        self.compute_pi_theta_prime()
                    self.compute_pi_theta_prime_time = t1.t

                # Used only in denormalized MC
                if self.pithetaprime_marginalized is None and not self.isnormalized:
                    with Timer("compute PI THETAPRIME MARG") as t2:
                        self.compute_pi_theta_prime_marginalied()
                    self.compute_pi_theta_prime_marginalized_time = t2.t

                toforget = [prime(x) for x in self.vocabulary]  # prime of v
                to_prime = {x: prime(x) for x in self.vocabulary}

                with Timer("father", show=False) as father:

                    acc = self.manager.getConstant(0)
                    for a, phi in form.inner_list:

                        rec = self.translation(phi)
                        if __debug__:
                            for v in rec.getVariables():
                                assert v in to_prime, f"{v} not in toprime"

                        pr_i = rec.rename(to_prime).apply("x", self.pithetaprime[form.agent], cache=self.cache)
                        pr_i_marg = pr_i.marginalization(toforget)
                        pr_i_xa = self.manager.getConstant(a).apply("x", pr_i_marg, cache=self.cache)

                        acc = acc.apply("+", pr_i_xa, cache=self.cache)

                    if self.isnormalized:
                        res = acc.bool_cut(form.op, form.beta)
                    else:
                        if form.op in [">=", "<"]:
                            opp = "xnorm1"
                        elif form.op in ["<=", ">"]:
                            opp = "xnorm2"
                        else:
                            raise Exception(f"Unknown operator for Pr {form.op}")

                        beta_product_norm = self.manager.getConstant(form.beta).apply(opp, self.pithetaprime_marginalized[form.agent])
                        res = acc.apply(form.op, beta_product_norm)

                assert res.scopeEquals(self.vocabulary), str(res.getVariables())
                return res

            else:
                return super(ProbaStructure, self).translation(form, cache=cache, **kwargs)
        return rec_from_formula(phi)


class ProbaTransformer(BeliefTransformer):

    #     @timeit
    #     def __init__(self, ksv, v, theta, o, pointed, manager, v_:[str]=[], theta_: Dict[str, Formula]=dict(), name=None,
    #                  precondition=None, cache=None, **kwargs):

    @timeit
    def __init__(self, ksv: List[str], v: List[str], theta_pre,
                 o: Dict[str, PseudoBooleanFunction], pi: Dict[str, PseudoBooleanFunction],
                 pointed,
                 manager: PseudoBooleanFunctionManager,
                 v_: List[str] = [],
                 theta_: Dict[str, Formula] = dict(), name=None, precondition=None, cache=None, **kwargs):

        # theta is Top by default but will bu rewrited.
        super().__init__(ksv, v, theta_pre, o, pointed, manager, v_=v_, theta_=theta_, name=name,
                         precondition=precondition, cache=cache, **kwargs)

        self.pi = {k: self.manager.from_formula(v, vars=self.double_vocabulary_event) if isinstance(v, Formula) else v for k, v in pi.items()}
        for a, ipi in self.pi.items():
            assert isinstance(ipi, PseudoBooleanFunction), f"Probabilities need to be function, not : {type(ipi).__name__}"
            assert ipi.scopeEquals(self.double_vocabulary_event)

    def __repr__(self):
        res1 = "== ProbaTransformer ==\n"
        res2 = f"Pi =\n"
        for a, ipi in self.pi.items():
            res2 += f" - {a} :\n"
            res2 += str(ipi.print(string=True, limit=10, trueAtoms=True, notZero=True))
        return res1 + super().__repr__() + res2

    def copy(self):
        return ProbaTransformer(self.structure_vocabulary[:], self.vocabulary[:], copy(self.event_law),
                                {a: o.copy() for a, o in self.omega.items()},
                                {a: p.copy() for a, p in self.pi.items()},
                            self.pointed[:], self.manager,
                            v_=self.modified_vocabulary[:], theta_=copy(self.theta_), name=self.name[:] if self.name is not None else None, precondition=copy(self.precondition),
                            cache=self.cache)

    @staticmethod
    def toPDEL(kt, cache=None, normalize=False):
        """
        Transform a DEL Transformer into ProbabilisticTransformer
        By default the outgoing probabilities of a node are equal.
        The probabilities could be normalised, but this is not required
        as it will be normalised during product update.

        :param kt: Belief Transformer, or Pipeline of BeliefTransformer

        :param cache:
        :param normalize: if True, the probabilities are normalized but it's useless except for pretty printing.
        :return:
        """

        if cache is None: cache = {}

        def inner(t):
            assert isinstance(t, BeliefTransformer), f"{type(t).__name__}"

            pi = t.omega #{a: o.copy() for a, o in t.omega.items()}

            if normalize:
                npi = {}
                for a, ipi in pi.items():
                    npi[a] = ipi.marginalize_and_normalize([prime(p) for p in t.vocabulary])
                pi = npi

            bt = ProbaTransformer(t.structure_vocabulary, t.vocabulary, t.event_law, t.omega, pi,
                                  t.pointed,
                                  t.manager,
                                  v_=t.modified_vocabulary,
                                  theta_=t.theta_, #{key: value.copy() for key, value in t.theta_.items()},
                                  name=t.name, precondition=t.precondition)

            if hasattr(t, "all_preds"):
                bt.all_preds = t.all_preds

            return bt

        # for pipeline
        if isinstance(kt, list):
            return [inner(t) for t in kt]

        return inner(kt)
