
from src.model.datastructure.add.add_real_function import PseudoBooleanFunctionManager, PseudoBooleanFunction
from src.model.epistemiclogic.examples.hanabi_example import *
from src.utils.timer import timeit, Timer

from typing import Union, Optional, Dict, Any

from copy import copy, deepcopy
from src.utils.memoize import memoize
from src.model.epistemiclogic.epistemicmodel.world import Valuation

from src.io.colors import *

"""
Progam based on Malvin Gattinger Thesis :

New Directions in Model CheckingDynamic Epistemic Logic
https://malv.in/phdthesis/gattinger-thesis.pdf

Symbolic Model Checking for Dynamic Epistemic Logic — S5 and Beyond
https://research-repository.griffith.edu.au/bitstream/handle/10072/400965/Su445471-Accepted.pdf?sequence=2

Towards Symbolic Factual Change in DEL
https://arxiv.org/pdf/1912.10717.pdf
"""

def get_variants(var, prof, symb, prime=False):
    all_V_order = []

    subvars = []

    for i in range(0, prof):
        circle_symb = id_step(var, i, symb)
        subvars.append(circle_symb)
        all_V_order.append(circle_symb)
    subvars.append(var)

    #for svar in subvars[::-1]:
    #    all_V_order.insert(0, prime(svar))
    if prime:
        for svar in subvars:
            all_V_order.append(prime(svar))

    return all_V_order

def generate_order(V, prof, symb, getprime=True):

    for sv in V:
        assert isinstance(sv, str), f"Vocabulary need to be a string, not a {type(sv).__name__}"

    all_V_order = []
    for var in V:

        subvars = []

        variants = get_variants(var, prof, symb)
        #variants.reverse()
        for v in variants:
            all_V_order.append(v)
            subvars.append(v)
        subvars.append(var)
        all_V_order.append(var)

        if getprime:
            for subvar in subvars:
                all_V_order.append(prime(subvar))

    return all_V_order

def get_modified_variable(var, modifier):
    if isinstance(var, list) or isinstance(var, set):
        return [modifier(p) for p in var]
    elif isinstance(var, str):
        return modifier(var)
    else:
        raise ValueError(f"var need to be string or list of string, not {type(var).__name__}")

def prime(p):
    return get_modified_variable(p, getPrimedVariable)

def is_primed(p):
    return p[-len(getPrimeSymbol()):] == getPrimeSymbol()

def circle(p):
    return get_modified_variable(p, getAfterVariable)

def get_id_symbol():
    return "⊗"

def circle_step(list, i):
    return [id_step(l, i, getAfterSymbol()) for l in list]

def to_circle(V, nb):
    return {v: id_step(circle(v), nb) for v in V}

def id_step(x, i, op):
    assert len(op)==1, "op is only one symbol !"
    # print(x, i, op)
    if op not in x:
        return f"{x}{op}{i}"
    return f"{x[:x.index(op)]}{op}{i}"


def sqsubseteq(set_a, set_b, cache=None):
    # A ⊑ B
    # ∧ A ∧ {¬p | p ∈ B \A }
    if cache is None: cache = {}
    @memoize(cache)
    def inner(set_a, set_b):
        return And(
            MAnd(*[Atom(a) for a in set_a]),
            MAnd(*[Not(Atom(b)) for b in set_b if b not in set_a])
        )
    return inner(tuple(set_a), tuple(set_b))

def sqsubseteq_phi(set_a, set_b, phi, manager):
    # [A ⊑ B]phi p.12
    return manager.from_formula(sqsubseteq(set_a, set_b)).apply("and", phi)

def sqsubseteq_formula(set_a, set_b, manager, cache=None):
    if cache is None: cache = {}
    #@memoize(cache)
    def inner(set_a, set_b):
        sq = sqsubseteq(set_a, set_b, cache=cache)
        return manager.from_formula(sq)
    return inner(tuple(set_a), tuple(set_b))


def obs_to_equiv(obs, manager, vars, cache=None):
    if cache is None: cache = {}

    with Timer("equiv"):
        newo = {}
        for a, lo in obs.items():
            f = Top()
            for o in lo:
                #print(o, "<->", getPrimedVariable(o))
                f = And(f, Equiv(
                    Atom(o),
                    Atom(getPrimedVariable(o))))
            newo[a] = manager.from_formula(f, vars=vars, cache=cache)

    return newo

@timeit
def impliesADD(left, right, cache=None, **kwargs):
    if cache is None: cache = {}
    not_left = left.apply("not", cache=cache)
    return not_left.apply("or", right, cache=cache)

def equivADD(left, right, cache=None):
    if cache is None: cache = {}
    left2 = impliesADD(left, right, cache=cache)
    right2 = impliesADD(right, left, cache=cache)
    return left2.apply("and", right2, cache=cache)


class SymbolicPrecondition:
    pass

class Structure:
    pass

class SymbolicPrecondition(metaclass=ABCMeta):

    def __init__(self, manager: PseudoBooleanFunctionManager, vocabulary, cache: Optional[dict]=None):

        assert isinstance(manager, PseudoBooleanFunctionManager)
        assert isinstance(vocabulary, list)

        self.manager = manager
        self.cache = {} if cache is None else cache
        self.vocabulary = vocabulary

    @abstractmethod
    def rename(self, rename_dictionary: Dict[str, str]) -> SymbolicPrecondition:
        raise NotImplementedError

    @abstractmethod
    def get_translation(self, structure: Optional[Structure]=None):
        raise NotImplementedError

    @abstractmethod
    def conditionning(self, x: List[str], vocabulary: List[str]):
        raise NotImplementedError


class FormulaPrecondition(SymbolicPrecondition):

    def __init__(self, theta: Union[Formula, PseudoBooleanFunction], manager:PseudoBooleanFunctionManager, vocabulary: List[str], cache: Optional[dict]=None):

        assert isinstance(theta, Formula) or isinstance(theta, PseudoBooleanFunction)

        super().__init__(manager, vocabulary, cache=cache)

        if isinstance(theta, Formula) and theta.isPropositionnal():
            # This pre-compilable
            self.precondition = self.manager.from_formula(theta, vars=vocabulary, cache=cache)
        elif isinstance(theta, PseudoBooleanFunction):
            # Nothing to do here, we have the good object
            self.precondition = theta
        else:
            # Impossible to compile, the formula isn't propositionnal
            assert isinstance(theta, Formula), f"Theta need to be a epistemic formula, not {type(theta).__name__}"
            self.precondition = None
            self.theta = theta

    def get_translation(self, structure: Optional[Structure] = None, cache:dict=None):
        if self.precondition is None:
            translation = structure.translation(self._precondition, cache=cache)
            translation_voc = translation.getVariables()
            translation.extendScope([v for v in self.vocabulary if v not in translation_voc])
            return translation
        return self.precondition

    def rename(self, rename_dictionary: Dict[str, str]) -> SymbolicPrecondition:
        new_vocabulary = [v if k in self.vocabulary else k for k, v in rename_dictionary.items()]
        if self.precondition is None:
            return FormulaPrecondition(self.theta.rename(rename_dictionary), self.manager, new_vocabulary, cache=self.cache)
        return FormulaPrecondition(self.precondition.rename(rename_dictionary), self.manager, new_vocabulary, cache=self.cache)

    def conditionning(self, dico: dict):
        if self.precondition is None:
            return FormulaPrecondition(self.precondition.conditionning(dico), self.manager, self.vocabulary, cache=self.cache)
        return FormulaPrecondition(self.precondition.conditionning(dico), self.manager, self.vocabulary, cache=self.cache)




##################################################################
#### Some factorised things in Structure and Transformer
# Structure
# -> KnowledgeStructureKnowledge
# -> BeliefStructure
# Transformer
# -> KnowledgeTransformer
# -> BeliefTransformer

class Transformer:
    pass

class Structure(metaclass=ABCMeta):

    filter_transformer = False

    @timeit
    def __init__(self, v: [str], theta: PseudoBooleanFunction, pointed: List[str], manager: PseudoBooleanFunctionManager, nb_apply=1, cache=None, **kwargs):
        """ .p65
        Common features between Knowledge Struture and Belief Structure for V, Theta, manager and pointed

        A tuple F = (V, theta, [O_i or Omega_i])
        :param v: Finite set of atomic propositions called 'Vocabulary'
        :param theta: theta \in L_B(V) is the 'state law'
        :param pointed: a world of theta, as valuation ⊆ V
        :param manager: manager to manipulate formulas as BDD. If None, new Manager is created.
        """

        assert isinstance(v, list), f"vocabulary need to be a list of str, not {type(v).__name__}"
        assert isinstance(theta, Formula) or isinstance(theta, PseudoBooleanFunction), f"state_law need to be a Formula or a PseudoBooleanFunction, not {type(pointed).__name__}"
        assert isinstance(pointed, list), f"Pointed need to be a list of str, not {type(pointed).__name__}"
        assert isinstance(manager, PseudoBooleanFunctionManager), f"Manager need to be a Manager, not {type(pointed).__name__}"
        assert isinstance(nb_apply, int), f"nb_apply need to be an int, not {type(pointed).__name__}"
        self.nb_apply = nb_apply

        self.manager = manager

        self.cache = {} if cache is None else cache

        self.vocabulary = v
        self.double_vocabulary = self.vocabulary + [prime(vi) for vi in self.vocabulary]
        if isinstance(theta, Formula):
            self.state_law = self.manager.from_formula(theta, vars=v, cache=self.cache) # Boolean Formula into BDD
        else:
            self.state_law = theta

        self.pointed = pointed
        assert isinstance(self.pointed, list)

    def __repr__(self):
        res  = f"s     = {str(self.pointed)}\n"
        res += f"V     = {self.vocabulary}\n"
        res += f"Theta =\n{str(self.state_law.print(string=True, limit=10, trueAtoms=True, notZero=True))}"
        return res



    def getPointedWorld(self):
        return self.pointed

    @timeit
    @abstractmethod
    def simple_apply(self, t, **kwargs):
        """
        apply for Product Update with One Transformer
        :param t: Transformer
        :param kwargs:
        :return: Structure
        """
        raise NotImplementedError

    def get_worlds(self):
        """
        USE ONLY ON SMALL EXAMPLES
        :return: list of valuation, describing worlds in state law.
        """
        liste = []
        vars = self.state_law.getVariables()
        for valuation in [list(i) for i in itertools.product([False, True], repeat=len(vars))]:
            val = {x: valuation[i] for i, x in enumerate(vars)}
            res = self.state_law.getValue(Valuation(val))
            #print(val, res)
            if res != 0.0:
                liste.append(val)
        return liste

    @timeit
    def modelCheck(self, formula, pointed=None, show=False, cache=None, **kwargs):

        if pointed is None:
            pointed = self.pointed

        if show:
            print(">> in modelCheck()")
            print("of", formula)

        kt = self.translation(formula, cache=self.cache, show=show, **kwargs)

        if show:
            print("Knowledge Translation")
            kt.print(limit=100, trueAtoms=True)


        left = sqsubseteq_formula(pointed, self.vocabulary, self.manager)

        if show:
            print("sqsubseteq_formula on, ", pointed, self.vocabulary)
            left.print(trueAtoms=True)

        res = impliesADD(left, kt, cache=self.cache, **kwargs)

        if show:
            print("(sqsubseteq_formula -> Knowledge Translation MC).isTrue() ?")
            res.print(limit=10, trueAtoms=True)
            print("<< out BeliefTransformer.modelCheck()")

        return res.isTrue()

    @timeit
    def apply(self, t, cleaning=[], with_event=False, cache=None, **kwargs):
        """
        An apply permit to use apply on simple event, or pipeline of events
        :param: Transformer
        :return: Structure
        """

        if isinstance(t, Transformer):
            pu_key = hash(frozenset((t, tuple(cleaning), with_event)))
        elif isinstance(t, list):
            pu_key = hash(frozenset((frozenset(t), tuple(cleaning), with_event)))
        else:
            raise Exception(f"We need a Transformer or a list of Transformers, not a {type(t).__name__}")

        cache = {} if cache is None else cache
        if pu_key in cache:
            return cache[pu_key]

        if isinstance(t, Transformer):

            with Timer(name="Rename Transformer", show=False):
                #print(">> SMCDEL apply before rename", t.vocabulary, t.event_law, t.omega)
                nt = self.renameTransformer(copy(t))
                #print("<< SMCDEL apply after rename", nt.vocabulary, t.event_law, nt.omega)

            # p65, last paragraph : applying only possible events
            if Structure.filter_transformer and hasattr(nt, "all_preds"):

                for q, pred in nt.all_preds.items():

                    if self.manager.from_formula(pred).apply("x", self.state_law).isFalse():

                        if nt.theta_minus_compiled:
                            # before = nt.state_law.get_size()
                            nt.state_law = nt.event_law.get_translation(self).apply("and", self.manager.from_formula(Not(Atom(q))))
                            #print("Clean", q, before, "->", nt.theta.get_size())
                        else:
                            nt.state_law = And(Not(Atom(q)), nt.state_law)

                        for at, post in nt.theta_.items():
                            if q in post.getVariables():
                                if nt.theta_minus_compiled:
                                    nt.theta_[at] = nt.theta_[at].apply("and", self.manager.from_formula(Not(Atom(q))))
                                else:
                                    nt.theta_[at] = And(Not(Atom(q)), post)

                        if isinstance(nt, KnowledgeTransformer):
                            for a in nt.omega.keys():
                                nt.omega[a].remove(q)
                        elif isinstance(nt, BeliefStructure):
                            for a in nt.omega.keys():
                                nt.omega[a] = nt.omega[a].apply("and", Not(Atom(q)))

                        from src.model.SMCPDEL.pySMCPDEL import ProbaStructure
                        if isinstance(nt, ProbaStructure):
                            for a in nt.pi.keys():
                                nt.omega[a] = nt.omega[a].apply("x", Not(Atom(q)))


            #print(Color.get(f"-> Simple apply : {nt.name}", Color.CBLUE))
            #print(Color.get(f"on : {nt.pointed}", Color.CBLUE))
            tmp = self.simple_apply(nt, **kwargs)
            if cleaning != []:
                tmp = tmp.manual_cleaning(cleaning, **kwargs)
            if with_event:
                cache[pu_key] = tmp, nt
                return tmp, nt
            cache[pu_key] = tmp
            return tmp
        elif isinstance(t, list):
            tmp = self
            for ti in t:
                tmp = tmp.apply(ti, **kwargs)
                if cleaning != []:
                    tmp = tmp.manual_cleaning(cleaning, **kwargs)
            cache[pu_key] = tmp
            return tmp
        else:
            raise TypeError(f"Transformer need to be list or Transformer, not : {type(t).__name__}")

    def get_dict_rename_x_circle(self, liste):
        """
        Rename a list of variable into next step.
        Exemple : x -> x°1 , x° -> x°2
        :param list: list of str variables
        :return: list of modified str variables
        """
        assert isinstance(liste, list) or  isinstance(liste, set)
        dico = {}
        for x in liste:
            dico[x] = id_step(x, self.nb_apply, getAfterSymbol())
        return dico

    def get_list_rename_event_id(self, liste):
        """
        Rename a list of variable as event id into next apply step.
        Exemple : x -> x⊗1 , x° -> x⊗2
        :param list: list of str variables
        :return: list of modified str variables
        """
        assert isinstance(liste, list)
        list2 = []
        for x in liste:
            res = id_step(x, self.nb_apply, get_id_symbol())
            list2.append(res)
        return list2

    def get_dict_rename_event_id(self, list):
        """
        Use Structure.rename_event_id to create a dictionnary to rename formulas.
        :param list: list of str variables
        :return: dict of variable into modified str variables
        """
        res = self.get_list_rename_event_id(list)
        return {list[i]: res[i] for i in range(0, len(res))}

    def gen_renameTransformer(self, transformer: Transformer):
        """
        Renaming the transformer to permit product update.
        We want to :
        - create [V_ -> V_°]
        - create new ids in V+ (transformer.vocabulary), to permit to use it multiple times.
            - in all formulas.
        :return: new Transformer
        """
        """
        Transformer attributes(
                name                 = {self.name},
                precondition         = {self.precondition},
                self.manager         = {self.manager},
                X - vocabulary           = {self.vocabulary},
                structure_vocabulary = {self.structure_vocabulary},
                X - voc_cup_voc_plus     = {self.voc_cup_voc_plus},
                double_vocabulary    = {self.double_vocabulary},
                X - double_vocabulary_event = {self.double_vocabulary_event},
                X - event_law            = {self.event_law},
                X - pointed              = {self.pointed},
                modified_vocabulary  = {self.modified_vocabulary},
                theta_minus_compiled = {self.theta_minus_compiled},
                X - theta_               = {self.theta_}.
        )
        """

        new_transformer = transformer.copy()
        rename_circle = self.get_dict_rename_x_circle(new_transformer.modified_vocabulary)

        print(rename_circle)

        # dico_id_event = Structure.rename_event_id_dico(list(set([v for v in transformer.vocabulary])))
        dico_rename_id_event = self.get_dict_rename_event_id(new_transformer.vocabulary)
        dico_rename_id_event_prime = {prime(k): prime(v) for k, v in dico_rename_id_event.items()}

        #print(">> gen_rename Transformer", dico_rename_id_event, new_transformer.pointed)

        new_transformer.pointed = [dico_rename_id_event[v] for v in new_transformer.pointed]
        #print(">> gen_rename Transformer", dico_rename_id_event, new_transformer.pointed)
        new_transformer.vocabulary = [dico_rename_id_event[v] for v in new_transformer.vocabulary]

        new_transformer.event_law = new_transformer.event_law.rename(dico_rename_id_event)

        new_transformer.voc_cup_voc_plus = [dico_rename_id_event[v] for v in new_transformer.voc_cup_voc_plus if v in dico_rename_id_event]

        new_transformer.double_vocabulary_event = [dico_rename_id_event[v] for v in new_transformer.double_vocabulary_event if v in dico_rename_id_event]
        new_transformer.double_vocabulary_event = [dico_rename_id_event_prime[v] for v in new_transformer.double_vocabulary_event if v in dico_rename_id_event_prime]

        tmp = {}
        for v, f in new_transformer.theta_.items():
            tmp[v] = f.rename(dico_rename_id_event)
        new_transformer.theta_ = tmp

        # Dictionary to rename a variable 'p' into 'p°', or 'p°2', etc...
        #print("Change histo : ", rename_circle)
        new_transformer.historicisation = rename_circle
        new_transformer.historicisation_prime = {prime(var): prime(varc) for var, varc in rename_circle.items()}

        # use only in After in Model checking
        new_transformer.unhistoricisation = {v:k for k, v in new_transformer.historicisation.items()}
        new_transformer.unhistoricisation_prime = {v:k for k, v in new_transformer.historicisation_prime.items()}

        return new_transformer, dico_rename_id_event, dico_rename_id_event_prime

    def update_law(self, transformer: Transformer) -> PseudoBooleanFunction:
        """
        - theta^new = [V_/V_°](theta /\ ||theta+||_F) /\ /\_{q \in V_} (q <-> [V_/V_°](theta_(q)))
        """

        # frame : /\_{q \in V_} (q <-> [V_/V_°](theta_(q)))
        cache = self.cache.update(transformer.cache)
        # [V_ -> V_°](theta /\ ||theta+||_F) /\ /\_{q \in V_} (q <-> [V_ -> V_°](theta_(q)))
        frame = self.manager.from_formula(Top())
        for x in transformer.modified_vocabulary:
            eq = equivADD(self.manager.from_formula(Atom(x)),
                          transformer.theta_[x].rename(transformer.historicisation), cache=self.cache)
            frame = frame.apply("and", eq, cache=self.cache)

        # BeliefStructure : (theta /\ ||theta+||_F)

        thetanew = self.state_law.apply("and", transformer.event_law.get_translation(self, cache=cache).booleanization(), cache=cache)

        thetanew = thetanew.rename(transformer.historicisation)
        thetanew = thetanew.apply("and", frame, cache=cache)
        return thetanew

    def update_vocabulary(self, transformer):
        """
        Update vocabulary
        V^new = V + V+ + V_
        :param transformer:
        :return:
        """
        return self.vocabulary + transformer.vocabulary + [transformer.historicisation[v] for v in transformer.modified_vocabulary]

    def update_pointed(self, transformer: Transformer) -> List[str]:
        """
        Create the pointed as list of variables.
        s^new = (s \V_) \cup (s \cap V_)° \cup x \cup {p \in V_ | s \cup x \models theta_(p)}
        """

        s_new = [p for p in self.pointed if p not in transformer.modified_vocabulary]
        for p in self.pointed:
            if p in transformer.modified_vocabulary:
                s_new += [transformer.historicisation[p]]

        s_new += transformer.pointed
        sbs = sqsubseteq_formula(self.pointed, self.vocabulary, self.manager)
        sbx = sqsubseteq_formula(transformer.pointed, transformer.vocabulary, self.manager)
        left = sbs.apply("and", sbx, cache=self.cache)
        for p in transformer.modified_vocabulary:
            res = impliesADD(left, transformer.theta_[p], cache=self.cache)
            if res.isTrue():
                s_new += [p]

        return s_new

    @timeit
    def gen_apply(self, transformer, **kwargs):
        """p.65
        Common features between "apply" on Knowledge Struture and Belief Structure : V^new, Theta^new and pointed
        Missing : O^new or Omega^new
        - V^new = V \cup V+ \cup V°
        - theta^new = [V_/V_°](theta /\ ||theta+||_F) /\ /\_{q \in V_} (q <-> [V_/V_°](theta_(q)))
        - s = s^new = (s \V_) \cup (s \cap V_)° \cup x \cup {p \in V_ | s \cup x \models theta_(p)}
        :param transformer: Transformer
        :return: Structure
        """

        with Timer(name="Update Vocabulary", show=False):
            Vnew = self.update_vocabulary(transformer)
        with Timer(name="Update Theta", show=False):
            thetanew = self.update_law(transformer)
        with Timer(name="Update Pointed", show=False):
            pointed = self.update_pointed(transformer)

        return Vnew, thetanew, pointed

    ##################################################################
    #### definition of Translation : S5 or KD45
    @timeit
    def translation(self, phi: Formula, cache=None, show=False, **kwargs) -> PseudoBooleanFunction:
        """p.41
        ||T||_F = T
        ||p||_F = p
        ||¬phi||_F = ¬||phi||_F
        ||phi \/ psi ||_F = ||phi||_F or ||psi||_F

        p.40
        ||[phi]psi||_F = ||phi||_F -> ||psi||_{F^psi}

        p.56
        ||[X, x]phi||_F = ||[x ⊑ V+] theta+]||_F -> [x ⊑ V+]||phi||_FxX

        p.7 Beyond
        ‖[phi]_∆ξ‖_F := ‖phi‖_F →(‖ξ‖_{F^∆_phi} )(p_phi=T).
        p.30 Beyond
        [[Box_i phi||_F = Forall V' (theta' -> (Omega_i ->(||phi||_F)'))

        :param ks: Structure
        :param phi: Formula
        :return: PseudoBooleanFunction
        """

        if cache is None: cache = {}

        @memoize(cache)
        def rec_from_formula(form):

            assert not isinstance(form, str), f"Formula {form} need to be a formula. Not a string (Caution ? Atom(str))."

            if isinstance(form, PseudoBooleanFunction):
                #assert form.scopeEquals(self.vocabulary), \
                #    f"In left in excess (precompiled_add):{[v for v in form.getVariables() if v not in self.vocabulary]} \n" \
                #    f"In right in excess (self.vocabulary): {[v for v in self.vocabulary if v not in form.getVariables() ]}"
                # It has been pre-compiled !
                return form

            elif form.isPropositionnal():
                return self.manager.from_formula(form, vars=self.vocabulary)

            elif isinstance(form, Not):
                return self.translation(form.inner, cache=cache).apply("not", cache=cache)

            elif isinstance(form, And):
                return self.translation(form.left, cache=cache).apply("and", self.translation(form.right, cache=cache), cache=cache)

            elif isinstance(form, Or):
                return self.translation(form.left, cache=cache).apply("or", self.translation(form.right, cache=cache), cache=cache)

            elif isinstance(form, Implies):
                return self.translation(Or(Not(form.left), form.right), cache=cache)

            elif isinstance(form, Equiv):
                return self.translation(And(Implies(form.left, form.right), Implies(form.right, form.right)), cache=cache)

            elif isinstance(form, Announcement):

                phi, formula, group = form.getFormulas()
                if group is None:
                    sks_phi = self.annoucement(self, phi)
                    return impliesADD(self.translation(phi, cache=cache),
                                      sks_phi.translation(formula, cache=cache)
                                      )
                else:
                    raise NotImplementedError("need to be tested")
                    if isinstance(self, KnowledgeStructure):  # PRIVATE ANNOUCEMENT
                        sks_phi, p_phi = group_annoucement(self, phi)
                        return impliesADD(self.translation(phi, cache=cache),
                                          sks_phi.translation(formula, cache=cache).conditionning({p_phi, True})
                                          )
                    elif isinstance(self, BeliefStructure):  # FULLY PRIVATE ANNOUCEMENT (SECRET)
                        sks_phi, p_phi = private_group_annoucement(self, phi)
                        # no translation find
                        raise Exception
                    else:
                        raise Exception

            elif isinstance(form, After):
                # p.74 Thesis
                # F : Structure
                # X, x : Transformer, pointed event
                # ||[x ⊑ V+] theta+]||_F -> [V°_->V_][x ⊑ V+][V_->theta_(V_)]||phi||_FxX

                event, phi = form.getFormulas()
                FxX, transformer = self.apply(event, with_event=True, cache=cache)
                dico_x = {p: p in transformer.pointed for p in transformer.vocabulary}

                ### Left of implies
                # ||[x ⊑ V+] theta+]||_F
                sqs_event_law = transformer.event_law.conditionning(dico_x).get_translation(self)

                ### Right of implies
                # ||phi||_FxX
                phi_FxX = FxX.translation(phi)

                TRY = True
                if TRY:
                    # p.74 Thesis (5)
                    post = {}
                    for p, formula in transformer.theta_.items():
                        post[p] = formula.rename(transformer.historicisation)
                else:
                    # p.74 Thesis (6)
                    post = transformer.theta_

                # [V_->theta_(V_)] || phi || _FxX
                phi_FxX_composed = phi_FxX.compose(post)

                #print("After compose", phi_FxX_composed._get_mentioned_variables_with_order())

                #print(phi_FxX_composed.get_nodes_informations())

                # [x ⊑ V+][V_->theta_(V_)]||phi||_FxX
                #print("dico_x", dico_x)
                x_sqsub = phi_FxX_composed.conditionning(dico_x)
                #print("After conditionning", x_sqsub._get_mentioned_variables_with_order())
                #print(x_sqsub)

                if TRY:
                    # [V°_->V_][x ⊑ V+][V_->theta_(V_)]||phi||_FxX
                    #print("unhisto", transformer.unhistoricisation)
                    unhisto = x_sqsub.rename(transformer.unhistoricisation)
                else:
                    unhisto_realfunction = {}
                    for v1, v2 in transformer.unhistoricisation.items():
                        unhisto_realfunction[v1] = self.manager.from_formula(Atom(v2), vars=[v2])
                    unhisto = x_sqsub.compose(unhisto_realfunction)

                return impliesADD(sqs_event_law, unhisto)
            else:
                raise ValueError("Formula not implemented: {} of type {}".format(form, type(form).__name__))

            return res

        return rec_from_formula(phi)

    @classmethod
    def announcement(cls, ks, phi):
        """
        def 2.2.3 p40
        def 5 page 6 Beyond
        :param ks:
        :param phi:
        :return:
        """
        assert isinstance(ks, Structure)
        translation = ks.translation(phi)
        new_law = translation.apply("x", ks.state_law)
        res = cls(
            ks.vocabulary,
            new_law,
            ks.omega, ks.pointed, ks.manager
        )
        return res

    @classmethod
    def group_annoucement(cls, ks: Structure, phi: Formula, group: [str], id_announce):
        """
        def 5 page 6 Beyond
        """
        assert isinstance(ks, Structure)
        assert isinstance(phi, Formula)
        assert isinstance(group, list)
        assert isinstance(id_announce, str)

        for a in group:
            assert a in ks.omega.keys()

        # refactor that
        ox = {}
        for a, o in ks.omega.items():
            ox[a] = o
            if a in group:
                if isinstance(ks, KnowledgeStructure):
                    ox[a].append(id_announce)
                if isinstance(ks, BeliefStructure):
                    equiv = Equiv(Atom(id_announce), Atom(prime(id_announce)))
                    equiv = ks.manager.from_formula(equiv)
                    ox[a] = ox[a].apply("and", equiv)

        atom = ks.manager.from_formula(Atom(id_announce))

        new_law = ks.state_law.apply("and", equivADD(atom, ks.translation(phi)))

        return cls(
            ks.vocabulary + [id_announce],
            new_law,
            ox, ks.pointed + [id_announce], ks.manager
        )


    def where(self, formula: Formula):

        translation = self.translation(formula)

        bdd = translation.apply("and", self.state_law)

        liste = []
        vars = bdd.getVariables()
        for valuation in [list(i) for i in itertools.product([False, True], repeat=len(vars))]:
            val = {x: valuation[i] for i, x in enumerate(vars)}
            res = bdd.getValue(Valuation(val))
            # print(val, res)
            if res != 0.0:
                liste.append([k for k, v in val.items() if v])

        return liste




class Transformer(metaclass=ABCMeta):

    @timeit
    def __init__(self, ksv: List[str], v: List[str], theta: SymbolicPrecondition,
                 pointed: List[str], manager: PseudoBooleanFunctionManager, v_: [str]=[], theta_: Dict[str, Formula]=dict(), name: str=None,
                 precondition=None, cache=None, **kwargs):

        """

        Common features between Knowledge Transformer and Knowledge Structure for V+, Theta+,
        manager, pointed, V_ and theta_

        For KnowledgeTransformer without Factual change : A tuple X = (V+, theta+, [O+ or Omega+])
        For KnowledgeTransformer with Factual change :  A tuple X = (V+, theta+, V_, theta_, [O+ or Omega+])

        V_ and theta_ can be omitted to KnowledgeTransformer without Factual change.

        :param v: V+ is a set of fresh atomic propositions such that V \cap V+ = \varnothing
        :param theta: \theta+ is a possibly epistemic formula from L_(V \cup V+)
                      If is propositional, compilation in BDD, else, keep Formula

        :param pointed: a world of theta+, as valuation, as BDD. \in L_(V+)

        :param v_: v_ \in V is the modified subset of the original vocabulary
        :param theta_: V_ -> L_B(V \cup V+) maps modified propositions to boolean formulas

        :param manager: manager to manipulate formulas as BDD. If None, new Manager is created.
        Is Needed because we want the same as the Belief structure
        """

        self.name = name
        self.precondition = precondition

        self.manager = manager

        for sv in ksv:
            assert isinstance(sv, str), f"Vocabulary of Structure need to be string, not {type(sv).__name__}"
        for sv in v:
            assert isinstance(sv, str), f"Vocabulary of Transformer need to be string, not {type(sv).__name__}"
        for sv in pointed:
            assert isinstance(sv, str), f"Vocabulary of pointed world need to be string, not {type(sv).__name__}"

        self.vocabulary = v
        self.structure_vocabulary = ksv
        self.voc_cup_voc_plus = ksv + v

        self.double_vocabulary = ksv + [prime(vi) for vi in ksv]
        self.double_vocabulary_event = v + [prime(vi) for vi in  v]

        self.cache = {} if cache is None else cache

        assert isinstance(theta, SymbolicPrecondition), f"event_law need to be a SymbolicPrecondition, not {type(theta).__name__}"
        self.event_law = theta

        #print("CREATE TRANSFORMER", self.pointed)
        self.pointed = pointed
        assert isinstance(self.pointed, list) or isinstance(pointed, dict), f"Pointed must be a list, not a {type(pointed).__name__}"

        self.modified_vocabulary = v_

        with Timer("V_", show=False) as tvm:
            self.theta_minus_compiled = True
            self.theta_ = {}
            for k, p in theta_.items():
                if isinstance(p, Formula):
                    theta_[k] =  self.manager.from_formula(p, vars=self.voc_cup_voc_plus, cache=self.cache)
                else:
                    theta_[k] = p
                assert theta_[k].scopeEquals(self.voc_cup_voc_plus)
                assert isinstance(theta_[k], PseudoBooleanFunction), f"Need a ADDRealfunction or a Formula, not a {type(self.theta_[k]).__name__} with {self.theta_[k]}"

        self.theta_ = theta_

    def print(self):
        print(f"""
                name                 = {self.name},
                precondition         = {self.precondition},
                self.manager         = {self.manager},
                vocabulary           = {self.vocabulary},
                structure_vocabulary = {self.structure_vocabulary},
                voc_cup_voc_plus     = {self.voc_cup_voc_plus},
                double_vocabulary    = {self.double_vocabulary},
                double_vocabulary_event = {self.double_vocabulary_event},
                event_law            = {self.event_law},
                pointed              = {self.pointed},
                modified_vocabulary  = {self.modified_vocabulary},
                theta_minus_compiled = {self.theta_minus_compiled},
                theta_               = {self.theta_}.

                """)

    def __repr__(self):
        res = f"s     = {str(self.pointed)}\n"
        res += f"V+     = {self.vocabulary}\n"
        res += f"Theta+ =\n{str(self.event_law)}\n"
        res += f"Theta_ =\n"
        for v in self.theta_.keys():
            res += f" - {v} : \n{self.theta_[v].print(string=True, limit=10, trueAtoms=True, notZero=True)}"
        return res

    def __str__(self):
        return f"[{self.name}]"

    def has_facutal_change(self):
        return self.modified_vocabulary != []

    def is_announcement(self):
        #print("factual", self.has_facutal_change(), self.v)
        return not self.has_facutal_change() and self.vocabulary == []

    @staticmethod
    def to_KD45(kt, cache=None):

        if cache is None: cache = {}

        def inner(t):
            assert isinstance(t, KnowledgeTransformer), f"Need to be KnowledgeTransformer, not {type(t).__name__}"

            bt = BeliefTransformer(t.structure_vocabulary, t.vocabulary, t.event_law,
                                   obs_to_equiv(t.omega, t.manager, t.double_vocabulary_event, cache=cache),
                                   t.pointed, t.manager, v_=t.modified_vocabulary, name=t.name,
                                   theta_= t.theta_, #{key: value.copy() for key, value in t.theta_.items()},
                                   precondition=t.precondition)

            if hasattr(t, "all_preds"): bt.all_preds = t.all_preds
            return bt

        # for pipeline
        if isinstance(kt, list):
            return [inner(t) for t in kt]

        return inner(kt)



##################################################################
#### for S5 with omega variables

class KnowledgeStructure:
    pass

class KnowledgeTransformer:
    pass

class KnowledgeStructure(Structure):

    @timeit
    def __init__(self, v: [str], theta: PseudoBooleanFunction, o: Dict[str, List[str]], pointed, manager, nb_apply=1, **kwargs):
        """ p.39
        A tuple F = (V, theta, O)
        :param v: Finite set of atomic propositions called 'Vocabulary'
        :param theta: theta \in L_B(V) is the 'state law'
        :param o: \O_i ⊆ V are called 'omega'
        :param pointed: a world of theta, as valuation ⊆ V

        :param manager: manager to manipulate formulas as BDD. If None, new Manager is created.
        """
        #print("New KnowledgeStructure")
        super().__init__(v, theta, pointed, manager, nb_apply=nb_apply)
        self.omega = o
        for a, oi in self.omega.items():
            assert isinstance(oi, list), f"Observations (S5) need to be list, not : {type(oi).__name__}"

    def __repr__(self):
        res1 = Color.get(">> KnowledgeStructure ==\n", Color.CGREEN2, Color.CBOLD)
        res2 = f"O =\n"
        for a, o in self.omega.items():
            res2 += f" - {a} :" + str(o) + "\n"
        return res1 + super().__repr__() + res2 + Color.get("<< KnowledgeStructure.", Color.CGREEN2, Color.CBOLD)



    @staticmethod
    def to_KD45(ks):
        assert isinstance(ks, KnowledgeStructure)
        bs = BeliefStructure(ks.vocabulary, ks.state_law.copy(), obs_to_equiv(ks.omega, ks.manager, ks.double_vocabulary), ks.pointed, ks.manager, cache=ks.cache)
        return bs

    def renameTransformer(self, transformer):

        new_transformer, dico_rename_id, dico_rename_prime = self.gen_renameTransformer(transformer)

        for a, oi in new_transformer.omega.items():
            new_transformer.omega[a] = [dico_rename_id[o] for o in oi]

        return new_transformer

    @timeit
    def simple_apply(self, kt: Transformer, **kwargs):
        """ p.65 / 66
        - V^new = V \cup V+ \cup V°
        - theta^new = [V_/V_°](theta /\ ||theta+||_F) /\ /\_{q \in V_} (q <-> [V_/V_°](theta_(q)))
        - Omega_i = [[V_ -> V_°] O_i) \cup O_i+
        - s = s^new = (s \V_) \cup (s \cap V_)° \cup x \cup {p \in V_ | s \cup x \models theta_(p)}
        :param t: Transformer
        :return: Structure
        """

        assert isinstance(kt, KnowledgeTransformer), f"kt need to be a Transformer not a {type(kt).__name__}"

        for p in kt.modified_vocabulary:
            assert p in self.vocabulary, f"{p} in Transformer.v_ is unknowed in Structure.v : {self.vocabulary}"

        assert self.manager == kt.manager

        if 'logtime' in kwargs and kwargs["logtime"] is not None:
            if "events_applyed" not in kwargs["logtime"].keys():
                kwargs["logtime"]["events_applyed"] = []
            kwargs["logtime"]["events_applyed"].append(kt.name)

        Vnew, thetanew, pointed = self.gen_apply(kt, **kwargs)

        omeganew = {}
        # [V_ -> V_°] O_i) \cup O_i+
        for a in self.omega.keys():
            omeganew[a] = []
            # kt.o
            #print("AGENT", a)
            for o in self.omega[a]:
                #print("> self.o", o)
                if o in kt.modified_vocabulary:
                    omeganew[a].append(kt.historicisation[o])
                else:
                    omeganew[a].append(o)
        return KnowledgeStructure(Vnew, thetanew, omeganew, pointed, self.manager, nb_apply=self.nb_apply+1) #, cache=self.cache)

    @timeit
    def manual_cleaning(self, to_forget, **kwargs):

        # print(self.v, to_forget)

        v = [e for e in self.vocabulary if e not in to_forget]
        theta = self.state_law.EForget(to_forget)
        o = {k: [e for e in l if e not in to_forget] if isinstance(l, list) else l.UForget(to_forget) for k, l in self.omega.items()}
        pointed = [e for e in self.pointed if e not in to_forget]

        return KnowledgeStructure(v, theta, o, self.manager, pointed, cache=self.cache, nb_apply=self.nb_apply)

    ##################################################################
    #### definition of Translation : S5 or KD45
    @timeit
    def translation(self, phi: Formula, cache=None, show=False, **kwargs) -> PseudoBooleanFunction:
        """p.41
        ||Ki phi||_F = Forall(V\Oi)(theta -> ||phi||_F)
        :param ks: Structure
        :param phi: Formula
        :return: PseudoBooleanFunction
        """

        # print("Formula :", phi)

        #print("In Translation Knowledge")

        if cache is None: cache = {}

        @memoize(cache)
        def rec_from_formula(form):

            if isinstance(form, K):

                if show:
                    print("Theta")
                    self.state_law.print(trueAtoms=True, limit=10)

                toforget = [x for x in self.vocabulary if x not in self.omega[form.agent]]

                if show:
                    print("To forget :", toforget)
                    print("Enleve", self.omega[form.agent])
                    print("Restant:", [x for x in self.vocabulary if x in self.omega[form.agent]])

                rec = self.translation(form.inner, cache=cache)
                if show:
                    print("Translation")
                    rec.print(trueAtoms=True)

                res = impliesADD(self.state_law, rec, cache=cache)
                if show:
                    print("Implies")
                    res.print(trueAtoms=True, limit=10)

                    # ks.manager.from_formula(Atom("a0-R22")).apply("and", ks.theta).print(trueAtoms=True, limit=10)

                res = res.UForget(toforget, cache=cache)
                if show:
                    print("res")
                    res.print(limit=10)
                return res
            else:
                return super(KnowledgeStructure, self).translation(phi, cache=cache, show=show, **kwargs)
        return rec_from_formula(phi)



class KnowledgeTransformer(Transformer):

    @timeit
    def __init__(self, ksv, v, theta, o, pointed, manager,  v_:[str]=[], theta_: Dict[str, Formula]=dict(),
                 name=None, precondition=None, cache=None, **kwargs):

        """ p.52 p.66
        For KnowledgeTransformer without Factual change : A tuple X = (V+, theta+, O+)
        For KnowledgeTransformer with Factual change :  A tuple X = (V+, theta+, V_, theta_, O+)

        V_ and theta_ can be omitted to KnowledgeTransformer without Factual change.

        :param v: V+ is a set of fresh atomic propositions such that V \cap V' = \varnothing
        :param theta: \theta+ is a possibly epistemic formula from L_(V \cup V')
                      If is propositional, compilation in BDD, else, keep Formula
        :param o: \O+ \in L_B(V+ \cup V+') are boolean formulas for each agent
        :param pointed: a world of theta+, as valuation, as BDD. \in L_(V+)

        :param v_: v_ \in V is the modified subset of the original vocabulary
        :param theta_: V_ -> L_B(V \cup V+) maps modified propositions to boolean formulas

        :param manager: manager to manipulate formulas as BDD. If None, new Manager is created.
        Is Needed because we want the same as the Belief structure
        """

        super().__init__(ksv, v, theta, pointed, manager, v_=v_, theta_=theta_, name=name, precondition=precondition, cache=cache)

        self.omega = o
        if __debug__:
            for a, oi in self.omega.items():
                for oii in oi:
                    assert oii in v, f"Error in observations : {oii} unknown in {v}."
                assert isinstance(oi, list), f"Observations (S5) need to be list, not : {type(oi).__name__}"

    def print(self):
        super().print()
        print(f"""
                omega = {self.omega}
        """)


    def __repr__(self):
        res1 = Color.get("== KnowledggeTransformer ==\n", Color.CBLUE2, Color.CBOLD)
        res2 = f"O =\n"
        for a, o in self.omega.items():
            res2 += f" - {a} : {str(o)}" + "\n"
        return res1 + super().__repr__() + res2

    def copy(self):
        """
        (self, ksv, v, theta, o, manager, pointed, v_:[str]=[], theta_: Dict[str, Formula]=dict(),
                 name=None, precondition=None, cache=None
        """
        return KnowledgeTransformer(self.structure_vocabulary[:], self.vocabulary[:], copy(self.event_law),
            {a:o for a,o in self.omega.items()}, self.pointed[:], self.manager, v_=self.modified_vocabulary[:],
            theta_=self.theta_, name=self.name if self.name is not None else None, precondition=self.precondition,
            cache=self.cache)







##################################################################
#### for KD45 with Observation Law : Omega

class BeliefStructure:
    pass

class BeliefTransformer:
    pass

class BeliefStructure(Structure):

    @timeit
    def __init__(self, v: [str], theta: PseudoBooleanFunction, o:  Dict[str, Formula], pointed: Formula, manager: PseudoBooleanFunctionManager,
                 nb_apply=1, cache=None, **kwargs):
        """ p.56
        A tuple F = (V, theta, Omega)
        :param v: Finite set of atomic propositions called 'Vocabulary'
        :param theta: theta \in L_B(V) is the 'state law'
        :param o: \Omega_i \in L_B(V \cup V') are called 'omega'
        :param pointed: a world of theta, as valuation, as BDD. \in L_B(V)

        :param manager: manager to manipulate formulas as BDD. If None, new Manager is created.
        """

        super().__init__(v, theta, pointed, manager, nb_apply=nb_apply, cache=cache)

        self.omega = {k: self.manager.from_formula(v, cache=self.cache) if isinstance(v, Formula) else v for k, v in o.items()}
        for a, oi in self.omega.items():
            assert isinstance(oi, PseudoBooleanFunction), f"Observations (KD45) need to be function, not : {type(oi).__name__}"

    def __repr__(self):
        res1 = Color.get(">> BeliefStructure ==\n", Color.CBOLD, Color.CGREEN2)
        res2 = f"Omega =\n"
        for a, o in self.omega.items():
            res2 += f" - {a} :\n"
            res2 += str(o.print(string=True, limit=10, trueAtoms=True, notZero=True))
        return res1 + super().__repr__() + res2 + Color.get("<< BeliefStructure ==\n", Color.CBOLD, Color.CGREEN2)

    def copy(self):
        raise NotImplementedError

    def renameTransformer(self, transformer):
        new_transformer, dico_rename_id, dico_rename_prime = self.gen_renameTransformer(transformer)

        #dico = {}
        for a, oi in new_transformer.omega.items():
            new_transformer.omega[a] = oi.rename(dico_rename_id).rename(dico_rename_prime)
        #new_transformer.omega = dico
        return new_transformer

    @timeit
    def simple_apply(self, belief_transformer: Transformer, **kwargs):
        """ p.63
        - V^new = V \cup V+ \cup V°
        - theta^new = [V_/V_°](theta /\ ||theta+||_F) /\ /\_{q \in V_} (q <-> [V_/V_°](theta_(q)))
        - Omega_i_new = ([V_/V_°][V_'/V_°']) Omega_i) /\ Omega_i+
        - s = s^new = (s \V_) \cup (s \cap V_)° \cup x \cup {p \in V_ | s \cup x \models theta_(p)}
        :param t: Transformer
        :return: Structure
        """
        assert isinstance(belief_transformer, BeliefTransformer)

        for p in belief_transformer.modified_vocabulary:
            assert p in self.vocabulary, f"{p} in bt.v_ is not in KT.v : {self.vocabulary}"

        assert self.manager == belief_transformer.manager

        Vnew, thetanew, pointed = self.gen_apply(belief_transformer)

        # Omega_i_new = ([V_ -> V_°][V_' -> V_°']) Omega_i) /\ Omega_i+
        omeganew = {}
        for a, omega_i in self.omega.items():
            rename_function = omega_i.rename(belief_transformer.historicisation).rename(belief_transformer.historicisation_prime)
            times = rename_function.apply("and", belief_transformer.omega[a], cache=self.cache)
            times.extendScope(belief_transformer.modified_vocabulary + [prime(vi) for vi in belief_transformer.modified_vocabulary])
            omeganew[a] = times

            #print("NEW V 2", Vnew)

        res = BeliefStructure(Vnew, thetanew, omeganew, pointed, self.manager, nb_apply=self.nb_apply+1)
        return res

    @timeit
    def manual_cleaning(self, to_forget, **kwargs):

        v = [e for e in self.vocabulary if e not in to_forget]
        theta = self.state_law.EForget(to_forget)
        o = {k: [e for e in l if e not in to_forget] if isinstance(l, list) else l.UForget(to_forget) for k, l in self.omega.items()}
        pointed = [e for e in self.pointed if e not in to_forget]

        return BeliefStructure(v, theta, o, self.manager, pointed)

    def translation(self, phi: Formula, cache=None, show=False, **kwargs) -> PseudoBooleanFunction:
        """p.30 Beyond
        [[Box_i phi||_F = Forall V' (theta' -> (Omega_i ->(||phi||_F)'))

        :param ks: Structure
        :param phi: Formula
        :return: PseudoBooleanFunction
        """
        assert isinstance(phi, Formula), f"Phi need to be a Formula, not a {type(phi).__name__}."

        #print("In Translation Belief", phi)

        if cache is None: cache = {}

        @memoize(cache)
        def rec_from_formula(form):

            if isinstance(form, Box_a):

                #print("Box_a", show)

                toforget = [prime(x) for x in self.vocabulary]  # prime of v
                to_prime = {x: prime(x) for x in self.vocabulary}

                traduction_prime = self.translation(form.inner).rename(to_prime)

                lawprime = self.state_law.rename(to_prime)

                lawprime_and_omega = self.omega[form.agent].apply("and", lawprime, cache=cache)

                implies = impliesADD(lawprime_and_omega, traduction_prime, cache=cache)

                forget = implies.UForget(toforget)
                return forget

                return impliesADD(self.state_law.rename(to_prime), implies, cache=cache).UForget(toforget)
            else:
                return super(BeliefStructure, self).translation(phi, cache=cache, show=show, **kwargs)

        return rec_from_formula(phi)

    @classmethod
    def private_group_annoucement(cls, bs: BeliefStructure, phi: Formula, group: [str], id_announce: str):
        """
        def 14 p 27 Beyond
        Annouce is fully private. Agent not in group doesn't know an annoucement occured
        :return:
        """
        assert isinstance(bs, BeliefStructure)

        ox = {}
        for a, o in bs.omega.items():
            if a in group:
                ox[a] = o.apply("and", bs.manager.from_formula(
                    Equiv(Atom(id_announce),
                          Atom(prime(id_announce)))))
            else:
                ox[a] = o.apply("and", bs.manager.from_formula(Not(Atom(prime(id_announce)))))

        return BeliefStructure(
            bs.vocabulary + [id_announce],
            bs.state_law.apply("and",
                               impliesADD(bs.manager.from_formula(Atom(id_announce)),
                                          bs.translation(phi))),
            ox, bs.pointed + [id_announce], bs.manager
        )


class BeliefTransformer(Transformer):

    @timeit
    def __init__(self, ksv, v, theta, o, pointed, manager, v_:[str]=[], theta_: Dict[str, Formula]=dict(), name=None,
                 precondition=None, cache=None, **kwargs):
        """ .p63 / 66
        For BeliefTransformer without Factual change : A tuple X = (V+, theta+, Omega+)
        For BeliefTransformer with Factual change :  A tuple X = (V+, theta+, V_, theta_, Omega+)

        V_ and theta_ can be omitted to BeliefTransformer without Factual change.

        :param v: V+ is a set of fresh atomic propositions such that V \cap V' = \varnothing
        :param theta: \theta+ is a possibly epistemic formula from L_(V \cup V')
                      If is propositional, compilation in BDD, else, keep Formula
        :param o: \Omega+ \in L_B(V+ \cup V+') are boolean formulas for each agent
        :param pointed: a world of theta+, as valuation, as BDD. \in L_(V+)

        :param v_: v_ \in V is the modified subset of the original vocabulary
        :param theta_: V_ -> L_B(V \cup V+) maps modified propositions to boolean formulas

        :param manager: manager to manipulate formulas as BDD. If None, new Manager is created.
        Is Needed because we want the same as the Belief structure
        """

        super().__init__(ksv, v, theta, pointed, manager, v_=v_, theta_=theta_, name=name, precondition=precondition, cache=cache)
        self.omega = {k: self.manager.from_formula(v, vars=self.double_vocabulary_event) if isinstance(v, Formula) else v
                  for k, v in o.items()}

        if __debug__:
            for a, oi in self.omega.items():
                assert isinstance(oi, PseudoBooleanFunction), f"Observations (KD45) need to be function, not : {type(oi).__name__}"
                assert oi.scopeEquals(self.double_vocabulary_event), f"Omega scope need to be on " \
                                                                     f"{self.double_vocabulary_event} not {oi.getVariables()}."



    def print(self):
        super().print()
        print(f"""
                        omega = {self.omega}
                """)

    def __repr__(self):
        res1 = "== BeliefTransformer ==\n"
        res2 = f"Omega+ = \n"
        for a, o in self.omega.items():
            res2 += f" - {a} :\n"
            res2 += str(o)
        res2 += f"V_ = {self.modified_vocabulary}\n"
        res2 += f"Theta_ =\n"
        for a, t in self.theta_.items():
            res2 += f" - {a} :\n"
            res2 += str(t.print(string=True, limit=10, trueAtoms=True, notZero=True))
        return res1 + super().__repr__() + res2

    def copy(self):
        #ksv, v, theta, o, manager, pointed, v_: [str] = [], theta_: Dict[str, Formula] = dict(), name = None,
        #precondition = None, cache = None, ** kwargs):
        return BeliefTransformer(self.structure_vocabulary[:], self.vocabulary[:], copy(self.event_law), {a:o.copy() for a,o in self.omega.items()},
                            self.pointed[:], self.manager,
                            v_=self.modified_vocabulary[:], theta_=copy(self.theta_), name=self.name[:] if self.name is not None else None, precondition=copy(self.precondition),
                            cache=self.cache)


def Trf(V, A, pre, post, R, pointed, label, manager):
    """
    p.67 def 2.9.2
    :return:
    """
    n = math.ceil(math.log(len(A)))
    qi = [f"{label}{i}" for i in range(0, n+1)]
    print(len(A), len(qi), len(qi)**2, qi)
    #print(qi)
    cpt = Compteur(label, qi)
    #print(cpt.max)

    cache = {}
    @memoize(cache)
    def l(a):
        return cpt.get(A.index(a))

    Vp = qi

    #print("Theta+")
    thetap = Bot()
    for a in A:
        inner = And(pre[a], AndOfVarList(l(a)))
        #print(inner)
        thetap = Or(thetap, inner)

    #print()

    V_ = []
    for action, atom_dict in post.items():

        for p in V:
            # fill post with frame...
            if not p in atom_dict.keys():
                post[action][p] = Atom(p)
            if post[action][p] != p:
                V_.append(p)

    V_ = set(V_)

    theta_ = {}
    for p in V:
        orf = Bot()
        for a in A:
            orf = Or(orf, And(AndOfVarList(l(a)), post[a][p]))
        theta_[p] = orf

    rename_list = {q :prime(q) for q in qi}
    #print(rename_list)
    Omegap = {}
    for a in R.keys():
        Omegapi = Bot()
        for (r1, r2) in R[a]:
            inner = And(AndOfVarList(l(r1)), AndOfVarList(l(r2)).rename(rename_list))
            #print(inner)
            Omegapi = Or(Omegapi, inner)
        Omegap[a] = Omegapi

    if pointed is not None:
        pointed = [k for k, v in l(pointed).items() if v]

    # NO POINTED ?
    # We suppose there is one possible pointed event
    pointed = dict() # Formula to get pointed, pointed
    for a in A:
        pointed[pre[a]] = [k for k, v in l(a).items() if v]

    return BeliefTransformer(Vp, thetap, Omegap, pointed, manager, v_=V_, theta_=theta_)
