
import sys
from functools import wraps

class Lazy:
    def __init__(self, func):
        self.func = func
    def __str__(self):
        return self.func()


def mydebug(incr, *msg):
    debug(Lazy(lambda: f"{'|   ' * incr}"), *msg)


def debug(*args, **kwargs):
    if DEBUG:
        print(*args, **kwargs)


DEBUG = False

DEBUG_LIST = ["SLDDManager.from_formula", "Grove.multiply", "ADDRealFunction.marginalization", "ADD.marginalize"]
DEBUG_LIST = ["ADD.__marginalize_step", "ADDRealFunction.marginalization"]
DEBUG_LIST = []

def debug_me(f):
    """
    decorator to debug specific methods or functions.
    DEBUG has to be False to perform a filter, otherwise everything will be displayed anyway.
    :param f: function or method to debug
    :return:
    """

    @wraps(f)
    def wrap(*args, **kwargs):
        global DEBUG_LIST, DEBUG
        #print("here:", f.__qualname__)
        DEBUG = False
        for method in DEBUG_LIST:
            #print(">", f.__qualname__, method, method in f.__qualname__)
            if method in f.__qualname__:
                DEBUG = True
                break
        res = f(*args, **kwargs)
        #DEBUG = False
        return res

    return wrap







if __name__ == '__main__':

    if __debug__:
        print('Debug ON')
    else:
        print('Debug OFF')

    DEBUG = False

    DEBUG_LIST = [] #["Test.f1", "Test.f2"]


    @for_all_methods(debug_me)
    class Test():

        def f1(self):
            mydebug(1, "f1")

            @debug_me
            def subf1():
                mydebug(2, "subf1")

            subf1()

        def f2(self):
            mydebug(1, "f2")

        def f3(self):
            mydebug(1, "f3")

    t = Test()

    t.f1()
    t.f2()
    t.f3()
