
import datetime
import inspect
import time
import os.path

from prettytable import PrettyTable

import src.io.write as write
from src.io.write import getDefaultOutputPath
from src.utils.singleton import Singleton

"""
Add @timeit behind the function you want to record
logtime_data ={}
Add 'log_time = logtime_data' in parameters of the called function
Datas are stored in logtime_data
"""


class TimerData(metaclass=Singleton):

    def __init__(self, disable=True, asyougo:str=None, toDraw=None):
        self.storage = {}
        self.disable = disable
        self.asyougo = asyougo
        self.toDraw = toDraw

        if not asyougo is None:
            path = getDefaultOutputPath() + "timer_folder/"
            name = "{}{}_{}.txt".format(path, asyougo, datetime.datetime.now())
            self.file = open(name, "w")
            self.writeLine("TimerData at {}:\n".format(datetime.datetime.now()))

    def get(self):
        return self.storage

    def isDisable(self):
        return self.disable

    def store(self, classe, fonction, args, time):

        if self.isDisable():
            return

        if classe not in self.storage.keys():
            self.storage[classe] = {}
        if fonction not in self.storage[classe].keys():
            self.storage[classe][fonction] = []
        arg = [self.__transform(arg) for arg in args]
        self.storage[classe][fonction].append((arg, time))
        # print(classe, fonction, arg, time)
        if not self.asyougo is None:
            self.writeLine("{}\t{}\t{}\t{}".format(classe, fonction, arg, time))

    def __transform(self, arg):
        if "object at 0x" in str(arg):
            return str(type(arg).__name__)
        return str((type(arg).__name__, arg if len(str(arg)) <= 40 else str(arg)[0:40]+"[{}]".format(len(str(arg)))))

    def print(self, limit=None):
        if self.isDisable():
            print("TimerData disable.")
            return "TimerData disable."
        res = "TimerData at {}:\n".format(datetime.datetime.now())
        for classe in sorted(self.storage.keys()):
            res += "{}\n".format(classe)
            for fonction in sorted(self.storage[classe].keys()):
                res += "\t{}\n".format(fonction)
                for i, (arg, t) in enumerate(self.storage[classe][fonction]):
                    res += "\t\t{} : {}\n".format(arg, t)
                    if limit != None and i > limit:
                        res += "\t\t...\n"
                        break
            res += "\n"
        return res

    def save(self, name="TimeOutput", location="timer_folder/", limit=None):
        if not self.isDisable():
            if self.asyougo is not None:
                file = open(self.file.name.replace(".txt", "_end.txt"), "w")
                file.write(self.print(limit=limit))
                file.close()
                self.file.close()
            else:
                write.stringToFile("{}_{}".format(name, datetime.datetime.now()), self.print(limit=limit), extension="txt", location=location)
        else:
            print("TimerData disable.")

    def writeLine(self, line):
        self.file.write(line + "\n")

    def toCSV(self, name="TimeComparator", location="timer_folder/", begin=0, toPNG=False):

        if self.isDisable():
            print("TimerData disable.")
            return "TimerData disable."

        lines = []
        for classe in sorted(self.storage.keys()):
            for fonction in sorted(self.storage[classe].keys()):

                if self.toDraw != None and fonction in self.toDraw:
                    line = []
                    title = classe + " " + fonction
                    line.append(title.replace("_", " "))
                    # line.append(fonction)
                    for i, (arg, t) in enumerate(self.storage[classe][fonction]):
                        line.append(t)
                    lines.append(line)

        #M = np.array(lines)
        #lines = M.T

        nbCols = len(lines)
        nbLines = max([len(l) for l in lines])

        matrix = [["" for _ in range(nbCols)] for _ in range(nbLines)]

        #for j in range(0, nbLines):
        #    matrix[j][0] = begin + j

        for i in range(0, nbCols):
            for j in range(0, nbLines):
                #print(i, j, nbCols, nbLines)
                if j < len(lines[i]):
                    #print("   >", lines[i][j])
                    matrix[j][i] = lines[i][j]
                else:
                    matrix[j][i] = " "

        print(matrix)

        csv = ""
        for i, line in enumerate(matrix):
            l = str(i+begin) + ";"
            for case in line:
                l += str(case) + ";"
            print("->", l)
            csv += "{}\n".format(l)

        write.stringToFile("{}".format(name), csv, extension="csv", location=location)

        if toPNG:
            write.plotCSV(name, location=location)


def get_class_that_defined_method(meth):
    if inspect.ismethod(meth):
        for cls in inspect.getmro(meth.__self__.__class__):
           if cls.__dict__.get(meth.__name__) is meth:
                return cls
        meth = meth.__func__  # fallback to __qualname__ parsing
    if inspect.isfunction(meth):
        cls = getattr(inspect.getmodule(meth),
                      meth.__qualname__.split('.<locals>', 1)[0].rsplit('.', 1)[0])
        if isinstance(cls, type):
            return cls
    return getattr(meth, '__objclass__', None)


def timeit(method):

    @wraps(method)
    def timed(*args, **kw):

        #print(method.__name__)
        ts = time.time()
        result = method(*args, **kw)
        te = time.time()

        if 'logtime' in kw and kw["logtime"] is not None:

            try:
                is_method = inspect.getargspec(method)[0][0] == 'self'
                if is_method:
                    name = '{}.{}.{}'.format(method.__module__, args[0].__class__.__name__, method.__name__)
                else:
                    name = '{}.{}'.format(method.__module__, method.__name__)
            except:
                if "__init__" == method.__name__:
                    name = '{}.{}.{}'.format(method.__module__, args[0].__class__.__name__, method.__name__)
                else:
                    name = '{}.{}'.format(method.__module__, method.__name__)
                    #print("FAIL", method.__name__, args[0].__class__.__name__)

            if name not in kw['logtime'].keys():
                kw['logtime'][name] = []
            kw['logtime'][name].append(te - ts)
        return result
    return timed


class MemoryTimer:

    def __init__(self, begin):
        self.begin = begin
        self.memory = {}

    def add(self, name, value):
        if name not in self.memory:
            self.memory[name] = []
        self.memory[name].append(value)

    def print(self, threshold=0):
        total = time.time() - self.begin
        table = PrettyTable(["Timer", "Percent", "Time"])
        table.title = 'MemoryTimer'
        table.align["Timer"] = "l"
        for name in self.memory.keys():
            subtime = sum(self.memory[name])
            percent = subtime/total*100
            if percent > threshold:
                table.add_row([name, '{:.4f}'.format(percent), '{:.5f}s'.format(subtime)])

        res = table.get_string(sortby="Percent", reversesort=True)
        print(res)


class Timer:

    memory = None

    def __init__(self, name="", show=False, incr=0, prec=5, father=None, sort=True):

        if Timer.memory is None:
            Timer.memory = MemoryTimer(time.time())

        self.show = show
        self.name = name
        self.incr = incr
        self.string = ""
        self.sort = sort

        self.childrens = []
        if father is not None:
            father.childrens.append(self)


    def __enter__(self):
        self.start = time.time()
        #if self.show:
        #    print("\t"*self.incr + f'>> Timer block {self.name} start.')
        return self

    def __exit__(self, *args):
        self.end = time.time()
        self.t = self.end - self.start
        Timer.memory.add(self.name, self.t)
        if self.show:
            print("\t"*self.incr + f'<< Timer block {self.name} took {self.t:.5f} sec.' + " " + self.string)
            total = 0
            order = []
            for child in self.childrens:
                string = "\t" * self.incr + '  <|| {:.1%} - {:.5f} - {}'.format(child.t/self.t, child.t, child.name) + " " + child.string
                order.append((child.t, string))
                total += child.t/self.t

            if self.sort:
                order = sorted(order, key=lambda x: x[0], reverse=True)
            for ord, order_string in order:
                print(order_string)
            if total != 0:
                print("\t" * self.incr + "  <<<< Total : {:.1%} - {:.5f}".format(total, self.t))

    def add_string(self, string):
        self.string += string


def for_all_methods(decorator):
    def decorate(cls):
        for attr, v in cls.__dict__.items(): # there's propably a better way to do this
            if callable(getattr(cls, attr)):
                #print(cls, attr, type(attr), v)
                setattr(cls, attr, decorator(getattr(cls, attr)))
                #print(dir(v)) #.__dict__)
                #for subattr, v2 in v.__dict__.items():
                #    print(subattr, v2)

        return cls
    return decorate


def timed(method):

    def timed(*args, **kw):

        ts = time.time()
        result = method(*args, **kw)
        te = time.time()
        t = (te - ts)
        t = "{0:.5f}".format(t)
        print(t)

        return result

    return timed


class MyWriter():

    def __init__(self, name):
        self.name = name
        self.path = getDefaultOutputPath() + "timeCSV_folder/" + self.name

    def writeLine(self, liste: [str]):
        file = open(self.path, "a+")
        file.write(";".join([str(e).replace(".", ",") if isinstance(e, float) else str(e) for e in liste]) + "\n")
        file.close()


from functools import wraps


def timeCSV(name_file):
    """
    decorator with a string as argument to print data in CSV file named "name_file"
    :param name_file: name of output file
    :return: result of decored function
    """

    def inner_function(function):
        @wraps(function)
        def timed(*args, **kwargs):
            ts = time.time()
            result = function(*args, **kwargs)
            te = time.time()
            t = (te - ts)
            t = "{0:.5f}".format(t)
            classe = get_class_that_defined_method(function).__name__
            fonction = function.__name__

            MyWriter(name_file).writeLine([classe, fonction, classe, t, ] + [str(a) for a in args])
            return result

        return timed

    return inner_function


