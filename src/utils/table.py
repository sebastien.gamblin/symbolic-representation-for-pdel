

from typing import List, Tuple, Any, Callable
from prettytable import PrettyTable

class Table:

    def __init__(self, tuples: List[Tuple[Any,...]], names: List[str], title=None):
        self.names = names
        self.tuples = tuples
        self.title = title

    def __get_table(self, filter: Callable):

        table = PrettyTable(self.names)
        if not self.title is None:
            table.title = self.title

        for tupl in self.tuples:
            if filter(tupl):
                table.add_row(list(tupl))

        return table

    def print(self, filter, sortby, reversesort=True):

        table = self.__get_table(filter)
        assert sortby in self.names
        res = table.get_string(sortby=sortby, reversesort=reversesort)
        print(res)

