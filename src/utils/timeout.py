
import multiprocessing as mp
import traceback
import sys
import time


class Process(mp.Process):
    def __init__(self, *args, **kwargs):
        mp.Process.__init__(self, *args, **kwargs)
        self._pconn, self._cconn = mp.Pipe()
        self._exception = None

    def run(self):
        try:
            mp.Process.run(self)
            self._cconn.send(None)
        except Exception as e:
            tb = traceback.format_exc()
            self._cconn.send((e, tb))
            raise e  # You can still rise this exception if you need to

    @property
    def exception(self):
        if self._pconn.poll():
            self._exception = self._pconn.recv()
        return self._exception


import time
from functools import partial, wraps

#############


import threading

try:
    import thread
except ImportError:
    import _thread as thread

def quit_function(fn_name):
    # print to stderr, unbuffered in Python 2.
    #print('{0} took too long'.format(fn_name), file=sys.stderr)
    sys.stderr.flush() # Python 3 stderr is likely buffered.
    thread.interrupt_main() # raises KeyboardInterrupt
    #raise TimeoutError('{0} took too long'.format(fn_name))


def exit_after(s):
    '''
    use as decorator to exit process if
    function takes longer than s seconds
    '''
    def outer(fn):
        def inner(*args, **kwargs):
            timer = threading.Timer(s, quit_function, args=[fn.__name__ + f" with {s}sec"])
            timer.start()
            try:
                result = fn(*args, **kwargs)
            finally:
                timer.cancel()
            return result
        return inner
    return outer


def dec_exit_after(fun, argument):
    # magic sauce to lift the name and doc of the function
    @exit_after(argument)
    def dec_exit_after_ret_fun(*args, **kwargs):
        #do stuff here, for eg.
        print (f">>decorator arg for dec_exit_after on {fun.__name__} is {argument}.")
        try:
            res = fun(*args, **kwargs)
        except TimeoutError:
            print(f"Timeout error {fn.__name__}")
        return res

    return dec_exit_after_ret_fun



# use
"""
my_decorator = partial(dec_exit_after, argument=30)

@my_decorator
def my_function():
    pass

"""

my_decorator = partial(dec_exit_after, argument=5)

@my_decorator
def countdown(n):
    print('countdown started', flush=True)
    for i in range(n, -1, -1):
        print(i, end=', ', flush=True)
        time.sleep(1)
    print('countdown finished')


if __name__ == '__main__':

    print(countdown(3))