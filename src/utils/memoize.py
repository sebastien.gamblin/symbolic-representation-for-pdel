
import functools
import collections
from src.io.colors import Color
import itertools


def hashable(v):
    """Determine whether `v` can be hashed."""
    try:
        hash(v)
    except TypeError:
        return False
    return True



def memoize(cache=None, useSetForKey=False, debug=False):
    """
    Cache function as decorator.
    use :

    def main_method(cache=None)
        @memoize() | @memoize(cache)
        def method():
            ...
    return method()

    :param cache: dictionary
    :param useSetForKey: Memoize use args as key. Allow this permit to use Commutative property
    :return:
    """
    if cache is None:
        if debug:
            print(Color.get(f"init cache memoize", Color.CRED))
        cache = dict()

    def decorator(f):

        #if f.__name__ == "rec_nodes__multiply":
            #print(f, f.__name__, f.__name__ == "rec_nodes__multiply")
            #print(cache)
        @functools.wraps(f)
        def memoized(*args, **kwargs):
            #print(Color.get(f"in memoized({f.__qualname__})({args}, {kwargs})", Color.CBLACK))
            #print(args)

            if not hashable(args) and useSetForKey:
                raise Exception("useSetForKey need to use hashable arguments for the function.")

            subkey = frozenset(args) if useSetForKey else args
            if not hashable(subkey):
                types = [type(e).__name__ for e in subkey]
                types = ", ".join([Color.get(types[i], Color.CGREEN2 if hashable(t) else Color.CRED) for i, t in enumerate(subkey)])
                subkey = str(subkey)
                print(Color.get(f"Caution, memoize with unhashable key. By default : str(key). Are you sure this is unique ? ", Color.CRED, Color.CBOLD) +
                      f"Call on '{f.__qualname__}'. Unhashable : {types}")
            key = (f.__qualname__, subkey)
            if debug:
                print("New key:", key)
            #print(Color.get(f"\tCache beg ='{cache}'", Color.CGREEN))
            # print(key, cache)
            if key in cache:
                if debug:
                    print(Color.get(f"\tFIND ='key={key}'", Color.CGREEN))
                return cache[key]
            #if debug:
            #print(Color.get(f"\tnew key={key}'", Color.CBLUE))
            res = f(*args, **kwargs)
            cache[key] = res
            if debug:
                print(Color.get(f"\tCache end", Color.CGREEN))
            #print(Color.get(f"\treturn {res}", Color.CBLUE))
            return res
        return memoized
    return decorator



def memoize(cache=None, useSetForKey=False, debug=False):
    """
    Cache function as decorator.
    use :

    def main_method(cache=None)
        @memoize() | @memoize(cache)
        def method():
            ...
    return method()

    :param cache: dictionary
    :param useSetForKey: Memoize use args as key. Allow this permit to use Commutative property
    :return:
    """
    if cache is None:
        if debug:
            print(Color.get(f"init cache memoize", Color.CRED))
        cache = dict()
        
    def decorator(f):

        @functools.wraps(f)
        def memoized(*args, **kwargs):

            if not hashable(args) and useSetForKey:
                raise Exception("useSetForKey need to use hashable arguments for the function.")

            subkey = frozenset(args) if useSetForKey else args
            if not hashable(subkey):
                types = [type(e).__name__ for e in subkey]
                types = ", ".join([Color.get(types[i], Color.CGREEN2 if hashable(t) else Color.CRED) for i, t in enumerate(subkey)])
                subkey = str(subkey)
                print(Color.get(f"Caution, memoize with unhashable key. By default : str(key). Are you sure this is unique ? ", Color.CRED, Color.CBOLD) +
                      f"Call on '{f.__qualname__}'. Unhashable : {types}")
            key = (f.__qualname__, subkey)
            if key in cache:
                if debug:
                    print(Color.get(f"\tFIND ='key={key}'", Color.CGREEN))
                return cache[key]
            res = f(*args, **kwargs)
            cache[key] = res
            return res
        return memoized
    return decorator

def memoize_yield(func):
    def inner(arg):
        if isinstance(arg, list):
            # Make arg immutable
            arg = tuple(arg)
        if arg in inner.cache:
            print("Using cache for %s" % repr(arg))
            for i in inner.cache[arg]:
                yield i
        else:
            print("Building new for %s" % repr(arg))
            temp = []
            for i in func(arg):
                temp.append(i)
                yield i
            inner.cache[arg] = temp
    inner.cache = {}
    return inner

def memoized_generator(f):
    cache = {}
    @functools.wraps(f)
    def wrapper(*args, **kwargs):
        k = args, frozenset(kwargs.items())
        it = cache[k] if k in cache else f(*args, **kwargs)
        cache[k], result = itertools.tee(it)
        return result
    return wrapper
