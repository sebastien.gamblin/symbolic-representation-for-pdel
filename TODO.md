
- Rewrite formulas package, with Single and Binary Op.
    "simplified=True or False" to rewrite it as Not, Or, And?


- Variable ordering in DD
- implement add.compose() smarter
- test and unit test on add.restrict_law()
- add.getIte() without cache ?
- Add hasModel()/Satisfy_one(val) in RealFunctions

- Split BDD et ADD cleaner
  - ADD.toBDD() et BDD.toADD() ?

- Some clean-up

- externalise O or Omega of a Structure
  - UML : SMCPDEL possède SMCDEL

- Allow Product Update to dynamically add the 'round' variables
variables in the DDs, so that they are automatically placed in the right place if they are
not yet declared /or used.

- Add a tutorial on how to use BDDs and ADDs.
