
#--->
# It's a dirty trick to deal with 'scripts' folder
# Otherwise, this file can be copy/paste near to 'src' folder
# and launched with 'python3 -O XXX.py'
# Caution : 'scripts/' need to be remove to call 'hanabi_launcher.py' below.

import os, sys

p = os.path.abspath('scripts')
sys.path.insert(1, p)

#<-- End of the trick

import subprocess
import argparse
from my_args import arg_parser
import sys
import time
import random

def run(string):
    out = subprocess.Popen(string.split(" "),
                           stdout=None,
                           stderr=None)
    return_string = out.communicate()[0]
    #print(return_string)
    out.wait()
    #print(out)
    return out.returncode

def run2(string):
    out = subprocess.Popen(string.split(" "),
                           stdout=subprocess.PIPE,
                           stderr=subprocess.PIPE)

    stdout, stderr = out.communicate()

    #out.wait()
    return out.returncode, stdout.decode("utf-8") , stderr.decode("utf-8")


if __name__ == '__main__':

    #  ./all_comparaisons.sh 120 2 2>&1 | tee comparaisons_symbproba_h2_p0.csv

    # python3.6 hanabi_loop.py -b 6 -e 50 -s 1 -m 3 -x 120 2>&1 | tee output.csv

    # python3.6 hanabi_loop.py -a 2 -ch 2 -b 6 -e 20 -step 2 -s -p -x 300 -o cards prime position agents after  2>&1 | tee output14_12.csv

    args = arg_parser(boucle=True)

    o = " -o " + ' '.join(args.order) if args.order != [] else ""

    from datetime import datetime

    # datetime object containing current date and time
    now = datetime.now()
    # dd/mm/YY H:M:S
    dt_string = now.strftime("%d/%m/%Y %H:%M:%S")
    start_time = time.time()

    print("‣‣‣ A LOOP FOR HANABI")
    print("‣ Begin", dt_string)

    print(f"‣ Symb={args.symbolic}, S5={args.s5}, Proba={args.probability}, MC={args.modelcheck}, PU={args.productupdate}.")
    print(f"‣ nbA={args.agents}, nbC=range({args.begin}, {args.end}, {args.step}), nbCH={args.chands}.")
    print()

    formula_to_forget = []

    for nb_cards in range(args.begin, args.end, args.step):
        #print("asserts", args.asserts)
        string = f"python3 {'-O' if args.asserts else ''} hanabi_launcher.py {nb_cards}" \
                 f" -a {args.agents} " \
                 f" -ch {args.chands} " \
                 f" -t {args.type}" \
                 f" -xc {args.timeoutcreation} " \
                 f" -lr {args.recursion} " \
                 f" -mc {args.modelcheck}"\
                 f" -pu {args.productupdate}" \
                 f"{' -v' if args.verbosity else ''}" \
                 f"{' -p' if args.probability else ''}" \
                 f"{' -s' if args.symbolic else ''}" \
                 f"{' -mal' if args.malvin else ''}" \
                 f"{' -s5' if args.s5 else ''}" \
                 f"{' -n' if args.norm else ''}" \
                 f" -prof {args.prof}" \
                 f"{o}"\

        if len(formula_to_forget) != 0:

            string += f" -f " + " ".join(formula_to_forget)

        string = string.replace("  ", " ").strip()

        print(string)

        retn, stdout, stderr = run2(string)

        print(stdout.replace("\n", ""))
        print(stderr.replace("\n", ""))

        #liste = stderr.split(";")

        if not "[OK]" in stderr:
            break

        #if args.modelcheck != -1:
        #    formula_to_forget = [e for e in liste[2:liste.index("Apply")] if e != '']

    #print("")

    # datetime object containing current date and time
    now = datetime.now()
    # dd/mm/YY H:M:S
    dt_string = now.strftime("%d/%m/%Y %H:%M:%S")
    print("--- %s seconds ---" % (time.time() - start_time))
    print("End", dt_string)
