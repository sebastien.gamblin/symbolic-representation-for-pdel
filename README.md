

This is the code used to run the experiments of the Thesis of Sébastien Gamblin :

*Symbolic model checking for probabilistic dynamic epistemic logic*

This work consists in representing probabilistic Kripke structures (from Probabilistic Dynamic Epistemic Logic) by a
 symbolic representation based on Malvin Gattinger thesis[^fn1] for DEL (Dynamic Epistemic Logic),
  implemented via an adapted data structure that is Albebraic Decision Diagrams (ADDs [^fn2]),
  a generalization of Binary Decision Diagrams (BDDs [^fn3]).

This code was also used for the AAMAS 2022 paper **A Symbolic Representation for
 *Probabilistic Dynamic Epistemic Logic* (S. Gamblin, A. Niveau, M. Bouzid).

 The code was developed and run only on Linux. Since the main code
 is in Python[^fn4], it should be possible to run it on other platforms,
 but this was not tested.

 This README explains how to run several tools: some run model
 checking on classical toy problems from the DEL literature, some
 run unit tests, some run experiments, how to compile it in a wheel.


# Table of Contents
 1. [Installation](#installation)
 2. [Tools](#tools)
 3. [Reproduction of results in the AAMAS paper](#reproduction-of-results-in-the-AAMAS-paper)
 4. [Note](#note)
 5. [Contact](#contact)


# Installation

Install Python (the version used for development is 3.8)

Create a python virtual environment

```bash
python3 -m venv venv
source venv/bin/activate
```
Then install the required libraries
```bash
python3 -m pip install -r script_dist/requirements.txt
```


# Tools

 NB: some parameters are used to run python3.

  - **-O**     Remove assert statements and any code conditional on the value of \_\_debug\_\_, which gives them an important speed-up.
  - **-B**     Don't write .pyc files on import.


### toy_examples.py

Runs MuddyChildren, CherrylsBirthday, DrinkingLogicians,
SallyAndAnne and Flip Coin with tests on multiple formulas.

```bash
python3 -B scripts/toy_examples.py
```

### pySMCPDEL_tests.py
  - n : number of random tests

Random units tests for model checking and product update :
creates random explicit structures and transforms them
into symbolic ones. Checks that explicit and symbolic
model checking give the same result on random PDEL formulas.


```bash
python3 -B scripts/pySMCPDEL_tests.py 5
```

### hanabi_smc.py

 Runs some tests on the game Hanabi[^fn5], printing information about them
 such as timings.

The first argument must be the total number of cards; the remaining parameters
 are the following (X is an integer):

  - -0 : remove asserts
  - -a X : number of agents
  - -ch X : number of cards in hands
  - -xc X : timeout (seconds) for creation time
  - -lr X: limit recursion for python
  - -p : flag for "probabilistic" (by default, a pure DEL implementation of Hanabi is used)
  - -s : flag for "symbolic" (by default, explicit structures are used)
  - -pu X : enables product update calculations. X is the timeout of product update
  - -mc X : enables model checking calculations. X is the timeout of model checking

```bash
python3 -OB scripts/hanabi_smc.py 10 -a 2 -ch 2 -p -s -pu 100 -mc 100
```

### hanabi_smc_tests.py

 Runs some unit tests on Hanabi

```bash
python3 -B scripts/hanabi_smc_tests.py
```

### hanabi_launcher.py

 Runs experiments on Hanabi for a given number of cards, and prints
 the results in an easy-to-parse format.

 Parameters are exactly the same as hanabi_smc.py
```bash
python3 -OB scripts/hanabi_launcher.py 8 -a 2 -ch 2 -s -p -pu 10 -mc 10
```

### hanabi_loop.py

 Runs experiments on Hanabi with a loop on the total number of cards.
 Parameters are the same as hanabi_smc.py, except that instead
 of giving the total number of cards as first argument, you have
 to specify the range using those three parameters:
- -b X : number of cards at the beginning the loop
- -e X : number of cards at the end the loop (exclusive)
- -step X : step of range (default 2)
```bash
python3 -OB scripts/hanabi_loop.py -b 4 -e 6 -step 1 -a 2 -ch 1 -s -p -pu 10 -mc 10

python3 -OB scripts/hanabi_loop.py -b 24 -e 26 -step 2 -a 2 -ch 2 -s -p -pu 300 -mc 300 -lr 5 -O
```


### pytests_main.py

Launching unit tests for ADDManager

```bash
python3 -B scripts/pytests_main.py
```

# Reproduction of results in the AAMAS paper

 The following shell script runs exactly the experiments that
 we report on in the paper (using the appropriate calls of the
 __hanabi_loop__ tool presented above).

```bash
bash scripts/launch_xp.sh Experiments 20 2400
```

__*CAUTION:*__ it runs 40 processes in parallel, each one CPU-intensive,
 and some very memory-hungry. It is not meant to be run on a standard
 desktop machine. To reproduce the experiments on such a machine, the
 simplest way is to run everything sequentially, by removing the “&”
 at the end of line 51 of launch_xp.sh



## Cite article

- Link on DBLP ([here](https://dblp.org/rec/conf/atal/GamblinNB22.html?view=bibtex)) and bibtex :

 ```bibtex
@inproceedings{DBLP:conf/atal/GamblinNB22,
  author    = {S{\'{e}}bastien Gamblin and
               Alexandre Niveau and
               Maroua Bouzid},
  editor    = {Piotr Faliszewski and
               Viviana Mascardi and
               Catherine Pelachaud and
               Matthew E. Taylor},
  title     = {A Symbolic Representation for Probabilistic Dynamic Epistemic Logic},
  booktitle = {21st International Conference on Autonomous Agents and Multiagent
               Systems, {AAMAS} 2022, Auckland, New Zealand, May 9-13, 2022},
  pages     = {445--453},
  publisher = {International Foundation for Autonomous Agents and Multiagent Systems
               {(IFAAMAS)}},
  year      = {2022},
  url       = {https://www.ifaamas.org/Proceedings/aamas2022/pdfs/p445.pdf},
  doi       = {10.5555/3535850.3535901},
  timestamp = {Mon, 18 Jul 2022 17:13:00 +0200},
  biburl    = {https://dblp.org/rec/conf/atal/GamblinNB22.bib},
  bibsource = {dblp computer science bibliography, https://dblp.org}
}
```

# Note

Changes may be made to make the program more readable and easier to use.

Furthermore, the tools for using the BDDs and ADDs underlying the program will be made explicit and externalized.


# Contact

Team MAD at [GREYC](https://www.greyc.fr/), University of Caen Normandy, France

- sebastien.gamblin@unicaen.fr
- alexandre.niveau@unicaen.fr
- maroua.bouzid-mouaddib@unicaen.fr


[^fn1]: [Malvin Gattinger siteweb : https://malv.in/phdthesis/](https://malv.in/phdthesis/)
[^fn2]: R. I. Bahar, E. A. Frohm, C. M. Gaona, G. D. Hachtel, E. Macii, A. Pardo, and F. Somenzi. *Algebraic decision diagrams and their applications*. In Proceedings of the International Conference on Computer-Aided Design, pages 188-191, Santa Clara, CA, November 1993.
[^fn3]: R.E. Bryant. *Graph-based algorithms for Boolean function manipulation*. IEEE Transactions on Computers, C-35(8):677-691, August 1986.
[^fn4]: [https://www.python.org/](https://www.python.org/)
[^fn5]: [https://en.wikipedia.org/wiki/Hanabi_(card_game)](https://en.wikipedia.org/wiki/Hanabi_(card_game))
