
from __future__ import print_function

#--->
# It's a dirty trick to deal with 'scripts' folder
# Otherwise, this file can be copy/paste near to 'src' folder
# and launched with 'python3 -O XXX.py'

import os, sys

p = os.path.abspath('scripts')
sys.path.insert(1, p)

#<-- End of the trick

from src.model.datastructure.real_function_tests import *
from src.model.epistemiclogic.examples.hanabi_example import HanabiExampleExplicit, HanabiExampleExplicitProba

from src.utils.timeout import partial, dec_exit_after
from src.model.datastructure.graph.symbolic_graph import BDDManager
#from src.model.datastructure.sldd import SLDDManager

from my_args import arg_parser

import linecache

if __name__ == '__main__':
    args = arg_parser()

def blank_decorator(func):
    def wrapper():
        func()
    return wrapper


def display_top(snapshot, key_type='lineno', limit=3):
    snapshot = snapshot.filter_traces((
        tracemalloc.Filter(False, "<frozen importlib._bootstrap>"),
        tracemalloc.Filter(False, "<unknown>"),
    ))
    top_stats = snapshot.statistics(key_type)

    print("="*10, ">> Display RAM")
    print("Top %s lines" % limit)
    for index, stat in enumerate(top_stats[:limit], 1):
        frame = stat.traceback[0]
        # replace "/path/to/module/file.py" with "module/file.py"
        filename = os.sep.join(frame.filename.split(os.sep)[-2:])
        print("#%s: %s:%s: %.1f KiB"
              % (index, filename, frame.lineno, stat.size / 1024))
        line = linecache.getline(frame.filename, frame.lineno).strip()
        if line:
            print('    %s' % line)

    other = top_stats[limit:]
    if other:
        size = sum(stat.size for stat in other)
        print("%s other: %.1f KiB" % (len(other), size / 1024))
    total = sum(stat.size for stat in top_stats)
    print("Total allocated size: %.1f KiB" % (total / 1024))
    print("=" * 10, "<< Display RAM")
    return total / 1024

def print_manager_data(manager, title="> Manager"):
    print(f"{Color.get(title, Color.CUNDERLINE)}\n"
          f"Nb vars = {len(manager.variables())}\n"
          f"Size = {manager.size()}\n"
          f"Nb Obs = {len(manager._observers)}\n")


if __name__ == '__main__':
    product_decorator = partial(dec_exit_after, argument=args.productupdate)
else:
    product_decorator = partial(blank_decorator)

#@product_decorator
def run_product(ex, logtime, select=False, display=False):

    current_em = ex.getEpistemicModel()
    t1 = time.time()

    """
    playables_actions = []
    for action in ex.getEventModels():
        print(action)
        if hasattr(action, "pred"):
            if current_em.modelCheck(action.pred):
                playables_actions.append(action)
    """

    #names = [action.name for action in playables_actions]

    select = False

    if args.symbolic:
        names = ["a-plays-pos0-R11:T", "Announce for b : 1 at [0]"]
        actions = ex.get_actions_to_apply(names)
    else:  # old system
        names = ["a_plays_p0", "Announce for b : 1 at [0]"]
        actions = ex.get_actions_to_apply(names)

    tmp = time.time()

    logtime["tmp time"] = time.time() - tmp

    #logtime["trace"] = ""

    for i, action in enumerate(actions):
        #print(Color.get(f"ACTION {i} {action}", Color.CRED))
        if hasattr(current_em, 'resetApply'):
            current_em.resetApply()
        tf1 = time.time()
        res = current_em.apply(action, logtime=logtime)
        # print(res.getPointedWorld())
        logtime["PUi" + names[i]] = (time.time() - tf1)
        #print(Color.get(f"PUi {names[i]}", Color.CRED))
        #logtime["trace"] += "PUi" + names[i] + " " + str(logtime["PUi" + names[i]])

    logtime["Apply time"] = time.time() - t1

    return current_em


mc_timeout = []


def model_checking(ex, logtime):

    global args, mc_timeout

    modelchecking_decorator = partial(dec_exit_after, argument=args.modelcheck)

    @modelchecking_decorator
    def sub_mc(i, fname, f):
        tf1 = time.time()
        res = ex.getEpistemicModel().modelCheck(f)  # , cache=cache)
        print(fname, f, res)
        logtime["FMC" + fname] = (time.time() - tf1, res)

    time_f = time.time()

    for i, (fname, f) in enumerate(ex.get_formulas_to_check(proba=args.probability)):
        if i not in args.formulas:
            try:
                sub_mc(i, fname, f)
            except KeyboardInterrupt:
                print("ERROR")
                mc_timeout.append(i)
                logtime["FMC" + fname] = (f"Timeout of {args.modelcheck}s", f"Timeout of {args.modelcheck}s")
        else:
            mc_timeout.append(i)
            logtime[fname] = (f"Previous timeout of {args.modelcheck}s", f"Previous timeout of {args.modelcheck}s")

    logtime["total time MC"] = time.time() - time_f
    if type(ex.getEpistemicModel()).__name__ == "ProbaStructure":
        logtime["Precompile PiTheta"] = ex.getEpistemicModel().compute_pi_theta_prime_time
        logtime["Precompile Marg(PiTheta)"] = ex.getEpistemicModel().compute_pi_theta_prime_marginalized_time

    return logtime


def model_checking_with_dec(ex, logtime):
    #print("BEGIN MC")

    res = model_checking(ex, logtime)

    #print("END MC")
    return res


if __name__ == '__main__':
    if args.timeoutcreation == 0:
        # by default, creation time < 1h = 3600s.
        args.timeoutcreation = 3600
    # print("timeout_creation =", args.timeoutcreation)
    creation_decorator = partial(dec_exit_after, argument=args.timeoutcreation)
else:
    creation_decorator = partial(blank_decorator)


@creation_decorator
def creation(args, logtime, random=False):

    begin_time = time.time()

    logtime_create = {}

    print("BEGIN CREATION", args.agents)

    if not args.symbolic:

        t1 = time.time()
        if not args.probability:
            ex = HanabiExampleExplicit(args.agents, args.nbCards, nbCardsInHand=args.chands, random_pointed=random)
        else:
            ex = HanabiExampleExplicitProba(args.agents, args.nbCards, nbCardsInHand=args.chands, random_pointed=random)

        ex.getEpistemicModel(log_time=logtime_create)
        logtime["create_epistemic_model"] = time.time() - t1

        t2 = time.time()
        events = ex.getEventModels(log_time=logtime_create)

        for event in events:
            print(event.getName())

        logtime["create_event_models"] = time.time() - t2

        logtime_create["total time create"] = time.time() - begin_time
        logtime_create["nbArcs"] = len(ex.getEpistemicModel().getArcs())
        logtime_create["nbWorlds"] = len(ex.getEpistemicModel().getNodes())

    else:
        # ELSE : symbolic
        types = {"add": ADDManager, "cudd": BDDManager} #, "sldd": SLDDManager}
        logtime_create["1_type"] = types[args.type.lower()].__name__

        from hanabi_smc import HanabiSMC

        ex = HanabiSMC(args.s5, args.agents, args.nbCards, args.chands,
                       random=args.random, logtime=logtime_create, order=args.order,
                       prof=args.prof, proba=args.probability, cleaning=False, manager=types[args.type.lower()],
                       normalize=args.norm)

        t1 = time.time()
        ex.getEpistemicModel()
        logtime["create_epistemic_model"] = time.time() - t1

        logtime["NbVars"] = len(ex.getEpistemicModel().manager.variables())
        logtime["ThetaSize"] = ex.getEpistemicModel().state_law.get_size()
        if not args.s5:
            for a in ex.getAgents():
                logtime["OmegaSize-"+a] = ex.getEpistemicModel().omega[a].get_size()
                if args.probability:
                    logtime["PiSize-"+a] = ex.getEpistemicModel().pi[a].get_size()

        t2 = time.time()
        ex.getEventModels()
        logtime["create_event_models"] = time.time() - t2



    logtime["create_logtime"] = logtime_create

    #print(ex)
    print("END CREATION", type(ex).__name__)

    return ex, logtime


#print("timeout_modelchecking =", args.modelcheck)




#print("timeout product_update =", args.modelcheck)

apply_log = "OK"

def run_product_with_dec(ex, logtime):

    global apply_log

    def sub_run_product(ex, logtime):
        return run_product(ex, logtime)

    try:
        sub_run_product(ex, logtime)
    except KeyboardInterrupt:
        print("ERROR")
        logtime["Apply time"] = logtime["total time product"] = f"Timeout of {args.productupdate}"
        apply_log = "KO"



if __name__ == '__main__':

    #python3.6 hanabi_launcher.py -h

    """
    ARGS PRECALCULATE TOPPER
    #args = arg_parser()
    """

    def wprint(ex):

        w = ex.getEpistemicModel().getPointedWorld()

        def print_dico(x):
            return ", ".join([f"{k}" for k, v in x.items() if v])

        if isinstance(w, list):
            return " ".join(w)

        res = ""
        for x, y in w.support():
            res += f"-> {print_dico(x)} = {y}"

        return res

    if args.memory:
        import tracemalloc
        tracemalloc.start()

    logtime = {}

    begin_time = time.time()

    sys.setrecursionlimit(10 ** args.recursion)

    result = f"[RES];SMCDEL; {args.malvin}; S5; {args.s5}; Symb; {args.symbolic};type;{args.type if args.symbolic else 'None'}; proba; {args.probability}; " \
             f"nbCards; {args.nbCards}; cardsHands; {args.chands}; nbAgents; {args.agents};order;{args.order}; normalize;{args.norm}; asserts; {__debug__};"

    #print(result)
    #time.sleep(0.1)

    #sys.exit("[OK]; MC errors;" + f";Apply; {0.0};")

    if not args.verbosity:
        blockPrint()

    try:
        ex, logtime = creation(args, logtime, random=args.random)
        result += f"random;{args.random};pointed; order; {ex.manager.variables() if hasattr(ex, 'manager') else '[expl]'};" #wprint(ex)
    except KeyboardInterrupt:
        enablePrint()
        print(result + f"Timeout creation on {args.timeoutcreation};")
        sys.exit(f"[KO]: Timeout_creation; {args.timeoutcreation};")

    if args.modelcheck != -1:
        model_checking_with_dec(ex, logtime)

    if args.productupdate != -1:
        run_product_with_dec(ex, logtime)

    #if args.kbp != -1:
    #    run_kbp(ex, logtime)

    if args.memory:
        tm1 = time.time()
        snapshot = tracemalloc.take_snapshot()
        logtime["memory"] = display_top(snapshot)
        logtime["memory time"] = time.time() - tm1;

    logtime["total time"] = time.time() - begin_time

    if not args.verbosity:
        enablePrint()

    ### PRINT RESULT

    sort = list(logtime.keys())
    sort.sort()

    #result += f"random;{args.random};pointed;{wprint(ex)};"

    for k in sort:
        if isinstance(logtime[k], dict):
            result += f"{k};"
            sort2 = list(logtime[k].keys())
            for k2 in sort2:
                if isinstance(logtime[k][k2], tuple) or isinstance(logtime[k][k2], list):
                    result += f"{k2};"
                    for item in logtime[k][k2]:
                        result += str(item) + ";"
                else:
                    result += f"{k2}; {logtime[k][k2]};"
        elif isinstance(logtime[k], tuple) or isinstance(logtime[k], list):
            result += f"{k};"
            for item in logtime[k]:
                result += str(item) + ";"
        else:
            result += f"{k}; {logtime[k]};"

    print(result) #.replace(".", ",")

    if args.modelcheck == 1:
        mc = "MC errors;" + ";".join([str(e) for e in mc_timeout])
    else:
        mc = ""

    if args.productupdate == 1:
        pu = f";Apply; {apply_log};"
    else:
        pu = ""

    #order = ex.manager.getOrder()

    sys.exit("[OK];" + mc + pu ) #+ "\n" + str(order))
