
#--->
# It's a dirty trick to deal with 'scripts' folder
# Otherwise, this file can be copy/paste near to 'src' folder
# and launched with 'python3 -O XXX.py'

import os, sys

p = os.path.abspath('scripts')
sys.path.insert(1, p)

#<-- End of the trick

from src.model.SMCPDEL.pySMCDEL import *
from src.model.SMCPDEL.pySMCPDEL import ProbaStructure, ProbaTransformer, ConditionalPrecondition

from src.model.epistemiclogic.formula.formula import *

from src.model.datastructure.add.add_real_function import ADDManager

from hanabi_smc import generate_order

from src.io.write import *

from functools import wraps


def timing(f):
    @wraps(f)
    def wrap(*args, **kw):
        ts = time.time()
        result = f(*args, **kw)
        te = time.time()

        string = '%r args:[%r] took: %2.4f sec'% (f.__name__, args, te-ts)
        validate_list.append(string)
        return result
    return wrap

def assert_and_show(structure, formula, string=None, add="", cache=None):
    if cache is None: cache = {}
    res = structure.modelCheck(formula, cache=cache)
    if res:
        if string is not None:
            print(Color.get(string, Color.CGREEN))
        else:
            print(Color.get(f"(F, {structure.pointed}) ⊨ {add}{formula}", Color.CGREEN))
    assert res

def assert_and_show_announces(announce_list, structure, formula, string):
    current = structure
    for a in announce_list:
        current = announce(current, a)
    res = current.modelCheck(formula)
    if res:
        print(Color.get(f"(F, {structure.pointed} ⊨ {string} ", Color.CGREEN))
    else:
        print(Color.get(f"(F, {structure.pointed} ⊨ {string} ", Color.CRED))
    assert res

def give_op(S5, a, phi):
    if S5:
        return K(a, phi)
    else:
        return Box_a(a, phi)

def give_op_w(S5, a, phi):
    if S5:
        return Kw(a, phi)
    else:
        return Boxw(a, phi)

def check(structure, formulas, results_pr, results_box, normalize, add=""):

    nb_formulas = 0
    for pointed in structure.get_worlds():
        pointed_atoms = [p for p, v in pointed.items() if v]

        #print(pointed_atoms)

        tuple_key = tuple(sorted(list(pointed_atoms)))

        #print("\nIN POINTED :", " ".join([k for k, v in pointed.items() if v]))

        for f in formulas:

            result = structure.modelCheck(f, pointed=pointed_atoms, show=False)

            if isinstance(f, Pr):
                compare = eval(f"{results_pr[f.inner.inner][tuple_key][f.agent]} {f.op} {f.k}")
            elif isinstance(f, MPr):
                compare = f.beta == 1
            elif isinstance(f, Box_a):
                compare = results_box[f.inner.inner][tuple_key][f.agent]
            elif isinstance(f, Not) and isinstance(f.inner, Box_a):
                compare = not results_box[f.inner.inner.inner][tuple_key][f.inner.agent]
            else:
                raise Exception(f"Formula {type(f).__name__} ({f}) not supported.")

            #print(">", f, Color.get(str(result), Color.CGREEN if result == compare else Color.CRED))

            print(Color.get(f"{add}(F, {pointed_atoms}) ⊨ {f}",Color.CGREEN if result == compare else Color.CRED))

            nb_formulas += 1

            if result != compare:
                #print(Color.get("\n>>>PRINT ERROR", Color.CRED, Color.CBOLD))
                structure.modelCheck(f, pointed=pointed_atoms, show=True)
                assert False, f"Error on {f} in world {pointed_atoms}: expected={compare}, got={result}. Normalize={normalize}."

        print()
    return nb_formulas


def announce(struct, phi):
    if isinstance(struct, KnowledgeStructure):
        sks_phi = KnowledgeStructure.announcement(struct, phi)
    else:
        sks_phi = BeliefStructure.announcement(struct, phi)
    return sks_phi


def get_muddy_structure(n: int, S5: bool):

    # p1 ... p(n+1) : proposition to get childrens muddy or not
    V = [f"p{i}" for i in range(1, n + 1)]

    order = []
    for v in V:
        order.append(v)
        order.append(prime(v))

    manager = ADDManager.create(order=order)

    theta = Top() # All worlds are possibles.
    agents = [str(i) for i in range(1, n+1)]
    # Agents sees all others foreheads, but not theirs
    O = {str(a): [f"p{a2}" for a2 in range(1, n+1) if str(a2) != a] for a in agents}

    ks = KnowledgeStructure(V, theta, O, V, manager)
    if not S5:
        ks = KnowledgeStructure.to_KD45(ks)

    return ks, agents, V

@timing
def MuddyChildrens(n: int, S5: bool):
    """
    Test S5 Muddy Children
    All Section 4 Symbolic MC for DEL - S5 and Beyond
    :return:
    """

    ks, agents, atoms = get_muddy_structure(n, S5)

    alice = agents[0]

    # TRUE? Alice knows 2 and 3, and Not Know whether 1
    formula = And(give_op(S5,alice, MAnd(*map(Atom, atoms[1:]))), Not(give_op_w(S5,alice, Atom(atoms[0]))))
    #assert ks.modelCheck(formula) # True
    assert_and_show(ks, formula, "TRUE? Alice knows 2 and 3, and Not Know whether 1")

    # VALID? Bigand_a Not Know_whether_a 'a'
    formula2 = MAnd(*[Not(give_op_w(S5,a, Atom(f"p{a}"))) for a in agents])
    #assert ks.modelCheck(formula2)
    assert_and_show(ks, formula2, "VALID? Bigand_a Not Know_whether_a 'a'")

    # WHERE? <! (BigOr_{a in agents} a) > ( BigOr_{a in agents} Know_whether a)
    at_least_one = MOr(*map(Atom, atoms))
    ann_form = MOr(*[give_op_w(S5,a, Atom(atoms[i])) for i, a in enumerate(agents)])
    ann = announce(ks, at_least_one)
    assert sorted(ann.where(ann_form)) == sorted([[k] for k in ks.vocabulary])
    print(Color.get("WHERE? <! (BigOr_{a in agents} a) > ( BigOr_{a in agents} Know_whether a)", Color.CGREEN))

    # no_one_know_wheter_he_is_muddy nokwhim
    nokwhim = MAnd(*[give_op_w(S5,a, Atom(atoms[i])) for i, a in enumerate(agents)])
    all_are_muddy = MAnd(*map(Atom, atoms))
    # VALID? [! at_least_one][! no_one_know_wheter_he_is_muddy][! no_one_know_wheter_he_is_muddy] (all_are_muddy)
    assert announce(announce(announce(ks, at_least_one), nokwhim), nokwhim).modelCheck(all_are_muddy)
    assert_and_show_announces([at_least_one, nokwhim, nokwhim], ks, all_are_muddy,
                              "[! at_least_one][! no_one_know_wheter_he_is_muddy][! no_one_know_wheter_he_is_muddy] (all_are_muddy)")

    print()

@timing
def semi_private_announcement(S5: bool):
    """
    Test S5 : Announce to 1 of p
    Section 7 Fig 10 Symbolic MC for DEL - S5 and Beyond
    :return:
    """

    order = generate_order(["p", "p_id"], 2, getAfterSymbol())

    manager = ADDManager.create(order=order)

    # Two agents : '1' and '2'
    # Two worlds ('p' and not('p')), clique for two agent
    ks = KnowledgeStructure(["p"], Top(), {"1": [], "2": []}, ["p"], manager)

    # Announcement only for agent '2' of value 'p', but '1' see the announcement
    if S5:
        ann = KnowledgeStructure.group_annoucement(ks, Atom("p"), ["2"], "p_id")
    else:
        ks = KnowledgeStructure.to_KD45(ks)
        ann = BeliefStructure.group_annoucement(ks, Atom("p"), ["2"], "p_id")

    # Before announcement
    assert_and_show(ks, Not(give_op_w(S5,"1", Atom("p"))))
    assert_and_show(ks, Not(give_op_w(S5,"2", Atom("p"))))

    # After announcement
    add = "[! p]_{2} "
    assert_and_show(ann, give_op_w(S5, "2", Atom("p")), add=add)
    assert_and_show(ann, Not(give_op_w(S5, "1", Atom("p"))), add=add)

@timing
def fully_private_annoucement(s5):
    """
    Test exemple Alice et Bob. KD45 Private annoucement to Alice
    Section 7 Fig 10 Symbolic MC for DEL - S5 and Beyond
    :return:
    """

    order = generate_order(["p", "p_id"], 2, getAfterSymbol())

    manager = ADDManager.create(order=order)

    # Two agents : '1' and '2'
    # Two worlds ('p' and not('p')), clique for two agent
    ks = KnowledgeStructure(["p"], Top(), {"1": [], "2": []}, ["p"], manager)
    ks = KnowledgeStructure.to_KD45(ks)

    # Announcement only for agent '2' of value 'p', but '1' see nothing.
    ann = BeliefStructure.private_group_annoucement(ks, Atom("p"), ["2"], "p_id")

    # Before
    assert_and_show(ks, Not(give_op_w(s5,"2", Atom("p"))))
    assert_and_show(ks, Not(give_op_w(s5,"1", Not(Atom("p")))))
    #assert ks.modelCheck(Not(give_op_w(s5,"2", Atom("p"))))
    #assert ks.modelCheck(Not(give_op_w(s5,"1", Not(Atom("p")))))
    # After
    add = "[! p]_{2} "
    assert_and_show(ann, give_op_w(s5,"2", Atom("p")), add=add)
    assert_and_show(ann, Not(give_op(s5,"1", Atom("p"))), add=add)
    assert_and_show(ann, Not(give_op(s5,"1", give_op_w(s5,"2", Atom("p")))), add=add)
    #assert ann.modelCheck(give_op_w(S5,"2", Atom("p")))
    #assert ann.modelCheck(Not(give_op(S5,"1", Atom("p"))))
    #assert ann.modelCheck(Not(give_op(S5,"1", give_op_w(S5,"2", Atom("p")))))


@timing
def SallyAndAnne():
    """
    The Sally–Anne test is a psychological test to measure the ability of a
    person to attribute false beliefs to others. Here is how this test works.
    There is a ball in a basket. Sally goes out for a walk, and in the meantime,
    Anne hides the ball. When Sally comes back, what does Anne think of the position of the ball?
    """

    A, S = "Anne", "Sally"

    V = ["p", "t"]

    p, t = map(Atom, V)

    strq = "q"
    q = Atom(strq)
    qp = Atom(prime(strq))

    order = generate_order(V, 5, getAfterSymbol())
    order += generate_order(["q"], 5, get_id_symbol())

    manager = ADDManager.create(order=order)

    obs = {A: Top(), S: Top()}

    init = BeliefStructure(V, And(p, Not(t)), {a: manager.from_formula(Top(), vars=V+prime(V)) for a in [A, S]}, ["p"], manager)

    top = FormulaPrecondition(Top(), manager, V)

    put_marble_basket = BeliefTransformer(V, [], top, obs, [], manager, v_=["t"], theta_={"t": Top()}, name="Put in basket")

    sally_leaves = BeliefTransformer(V, [], top, obs, [], manager, v_=["p"], theta_={"p": Bot()}, name="Sally leaves")

    #anne_put_the marble in the box but not_observed_by_sally
    put_marble_box = BeliefTransformer(V, [strq], FormulaPrecondition(Top(), manager, V+[strq]),
                                       {S: Not(qp), A: Equiv(q, qp)}, [strq], manager,
                                   v_=["t"], theta_={"t":And(Implies(Not(q), t), Implies(q, Bot()))}, name="Put in box")


    sally_comes_back = BeliefTransformer(V, [], top, obs, [], manager, v_=["p"], theta_={"p":Top()}, name="Sally comes back")

    res = init.apply(put_marble_basket)
    res = res.apply(sally_leaves)
    res = res.apply(put_marble_box)
    res = res.apply(sally_comes_back)

    for f in [
        Not(Box_a(S, Not(Atom("t")))),
        Box_a(A, Not(Atom("t"))),
        Or(Box_a(S, Atom("t")), Box_a(S, Not(Atom("t")))),
        Or(Box_a(A, Atom("t")), Box_a(A, Not(Atom("t")))),
        Box_a(A, Box_a(S, Atom("t")))
    ]:
        assert_and_show(res, f)
        # assert res.modelCheck(f)






@timing
def CherrylsBirthday(s5):

    days = '15m', '16m', '19m', '17j', '18j', '14jl', '16jl', '14a', '15a', '17a'

    numbers = ['14', '15', '16', '17', '18', '19']
    months = ['m', 'j', 'jl', 'a']

    n14, n15, n16, n17, n18, n19 = map(Atom, numbers)
    m, j, jl, a = map(Atom, months)

    days = [And(n15, m), And(n16, m), And(n19, m), And(n17, j), And(n18, j),
            And(n14, jl), And(n16, jl), And(n14, a), And(n15, a), And(n17, a)]

    order = generate_order(numbers + months, 1, getAfterSymbol())
    manager = ADDManager.create(order)

    def a_knows(a):
        """
        return: BigOr_{day in days} ( Know('a', day) )
        """
        return MOr(*[give_op(s5,a, day) for day in days])

    # State law : all the days (but with only on month and number at any time)
    law = MAnd(MOr(*days), Exactly(1, numbers), Exactly(1, months))

    # Bernard knows_numbers, Albert knows_months
    bernard, albert = "Bernard", "Albert"
    obs = {bernard: numbers, albert: months}

    ks = KnowledgeStructure(numbers + months, law, obs, [n16, jl], manager)
    if not s5:
        ks = KnowledgeStructure.to_KD45(ks)

    # Albert: I don't know when Cheryl's birthday is...
    albert_doesnt_know = announce(ks, Not(a_knows(albert)))

    # ... but I know that Bernard doesn't know too.
    but = announce(albert_doesnt_know, give_op(s5,albert, Not(a_knows(bernard))))

    # Bernard: First I did not know when Cheryl's birthday is, but now I know.
    bernard_knows = announce(but, a_knows(bernard))

    # Albert: Then I also know when Cheryl's birthday is
    albert_knows = announce(bernard_knows, a_knows(albert))

    # Only one world left
    assert sorted(albert_knows.where(Top())[0]) == ['16', 'jl']
    print(Color.get("WHERE?"
                    "[Albert: I don't know when Cheryl's birthday is...]\n"
                    "  [... but I know that Bernard doesn't know too.]\n"
                    "    [Bernard: First I did not know when Cheryl's birthday is, but now I know.]\n"
                    "      [Albert: Then I also know when Cheryl's birthday is]"
                    " == ['16', 'jl']", Color.CGREEN))


"""
@timing
def DrinkingLogicians(s5):

    # Want a drink ?
    agents = ["a", "b", "c"]
    V = ["1", "2", "3"]
    Law = Top()
    Obs = {agents[i]: [v] for i, v in enumerate(V)}

    bigand = MAnd(*map(Atom, V))

    order = generate_order(V, 1, getAfterSymbol())
    manager = ADDManager.create(order)

    ks = KnowledgeStructure(V, Law, Obs, V, manager)
    if not s5:
        ks = KnowledgeStructure.to_KD45(ks)

    print(ks)
    # assert ks.modelCheck(Not(give_op(s5, "a", Not(bigand))))

    #big_and = '1' & '2' & '3'
    #TRUE? [! Not(give_op_w(S5,'a', bigand))][! Not(give_op_w(S5,'b', bigand))] give_op_w(S5,'c', bigand)
    f_1 = Not(give_op_w(s5, "a", bigand))
    f_2 = Not(give_op_w(s5, "b", bigand))
    f_3 = give_op_w(s5, "c", bigand)
    print(f_1)
    print(f_2)
    print(f_3)
    res = announce(announce(ks, f_1), f_2).modelCheck(f_3)
    print("res", res)
    assert_and_show_announces([f_1, f_2], ks, f_3,
                              f"[! Not(give_op_w(S5,'a', {bigand}))][! Not(give_op_w(S5,'b', {bigand}))] give_op_w(S5,'c', {bigand})")
"""

@timing
def DrinkingLogicians(s5):

    # Want a drink ?
    agents = ["a", "b", "c"]
    V = ["1", "2", "3"]
    Law = Top()
    Obs = {agents[i]: [v] for i, v in enumerate(V)}

    bigand = MAnd(*map(Atom, V))

    order = generate_order(V, 1, getAfterSymbol())
    manager = ADDManager.create(order)

    ks = KnowledgeStructure(V, Law, Obs, V, manager)
    if not s5:
        ks = KnowledgeStructure.to_KD45(ks)

    assert ks.modelCheck(Not(give_op_w(s5, 'a', bigand)))

    #big_and = '1' & '2' & '3'
    #TRUE? [! Not(Kw('a', bigand))][! Not(Kw('b', bigand))] Kw('c', bigand)
    assert announce(announce(ks, Not(give_op_w(s5,'a', bigand))), Not(give_op_w(s5,'b', bigand))).modelCheck(give_op_w(s5,'c', bigand))


@timing
def DiningGryptographers(s5):

    #TODO: Add common knowledge
    raise NotImplementedError("Common knowledge is needed to play the DiningGryptographers Example.")

    n = 3

    nasa_paid = ["nasa_paid"]
    i_paid = [f"i{i}" for i in range(1, n+1)]
    i_coin = [f"c{i}" for i in range(1, n + 1)]

    V = nasa_paid + i_paid + i_coin

    Law = Exactly(1, nasa_paid + i_paid)

    Obs = {
        "1": i_paid[0:1] + i_coin[0:2],
        "2": i_paid[1:2] + i_coin[0:1] + i_coin[2:],
        "3": i_paid[2:3] + i_coin[1:3]}

    order = generate_order(V, 1, getAfterSymbol())
    manager = ADDManager.create(order)

    ks = KnowledgeStructure(V, Law, Obs, nasa_paid, manager)
    if not s5:
        ks = KnowledgeStructure.to_KD45(ks)

    assert ks.modelCheck(give_op_w(s5,"1", Atom("i1")))

    for k, o in Obs.items():
        ks = announce(ks, MXor(*map(Atom, o)))

    formula = MAnd(
        *[Implies(Atom(i), MAnd(*[Not(give_op(s5,str(a), Atom(i_paid[a-1]))) for a in range(1, n+1) if str(a) not in i])) for i in i_paid]
    )

    print(formula)
    assert ks.modelCheck(formula)


@timing
def flip_coin(normalize, probaTable=True):
    """
    Test example Flip Coin
    Section 1 Towards Symbolic Factual Change in DEL
    :return:
    """

    # h : coin is head
    # q : variable of event coin flip with result "head"
    # p : coin is pipped (no fair)

    h, q, p = map(Atom, ["h", "q", "p"])
    hp, qp, pp = map(Atom, map(prime, ["h", "q", "p"]))

    # Initialise Manager
    if probaTable:
        from src.model.datastructure.proba_table import ProbaTableManager
        manager = ProbaTableManager.create()
    else:
        order = generate_order(["h"], 2, getAfterSymbol())
        order += generate_order(["p"], 2, getAfterSymbol())
        order += generate_order(["q"], 2, get_id_symbol())
        manager = ADDManager.create(order=order, tmp=True)


    ks = KnowledgeStructure(["h", "p"],
                            And(h, Or(p, Not(p))),               # 2 Worlds : 'hp' and 'h not(p)'
                            {"blue": ["h", "p"], "red": ["h"]},  # blue knows all, red doens't know it's pipped
                            manager=manager, pointed=["h"])

    bs = KnowledgeStructure.to_KD45(ks)
    ps = ProbaStructure.toPDEL(bs, normalized=normalize)

    Thetap = ConditionalPrecondition({
        p: ((["q"], 1.0), ([], 0.0)),
        Not(p): ((["q"], 0.5), ([], 0.5))
    }, manager, ["h", "p"], ["q"])

    # Toss the coin.
    # Blue sees nothing
    # Red sees the result : 'q'
    init_pt = ProbaTransformer(
        ["h", "p"],    # Vocabulary of ProbaStructure
        ["q"],         # Vocabulary of ProbaTransformer
        Thetap,  # Preconditions as {Formula : float}
        {"blue": ks.manager.from_formula(Top(), vars=["q", "q_p"]),          # Observations as Omega
         "red": ks.manager.from_formula(Equiv(q, qp), vars=["q", "q_p"])},
        {"blue": ks.manager.from_formula(Top(), vars=["q", "q_p"]).apply("x", ks.manager.getConstant(0.5)) if normalize else ks.manager.from_formula(Top(), vars=["q", "q_p"]),
         "red": ks.manager.from_formula(Equiv(q, qp), vars=["q", "q_p"])},
        ["q"], # Pointed event
        ks.manager,
        v_=["h"],      # Modified vocabulary in postconditions
        theta_={"h": q},  # Postconditions
    )

    init_pt.name = "flip_coin"

    print("Normalized ?", ps.isnormalized)

    op = ">="

    formulas = []
    for a in ks.omega.keys():

        f, f2 = Box_a(a, h), Box_a(a, p)
        formulas.append(f)
        formulas.append(Not(f))
        formulas.append(f2)
        formulas.append(Not(f2))

        for s in [-1, 0., 0.33, 0.34, 0.49, 0.51, 0.66, 0.67, 1., 2.]:
            formulas.append(Pr(a, p, op, s))

        formulas.append(MPr(a, [(1, p), (1, Not(p))], "<=", 0.99))
        formulas.append(MPr(a, [(1, p), (1, Not(p))], ">=", 1))

        formulas.append(MPr(a, [(1, h), (1, Not(h))], "<=", 0.99))
        formulas.append(MPr(a, [(1, h), (1, Not(h))], ">=", 1))

    seuils = {"p": {
            ("h", "p"): {"red": 0.5, "blue": 1},
            ("h",): {"red": 0.5, "blue": 0}
        }}

    box_res = {
        "p": {
            ("h", "p"): {"red": False, "blue": True},
            ("h",): {"red": False, "blue": False}
        },
        "h": {
            ("h", "p"): {"red": True, "blue": True},
            ("h",): {"red": True, "blue": True}
        }}

    nb_formulas = 0
    nb_formulas += check(ps, formulas, seuils, box_res, normalize)

    pt = init_pt
    res = ps.apply(pt)

    seuils = {
        "p": {
            ('h°1',): {"red": 0, "blue": 0},
            ('h', 'h°1', 'q⊗1'): {"red": 0.66, "blue": 0},
            ('h', 'h°1', 'p', 'q⊗1'): {"red": 0.66, "blue": 1},
        }}

    box_res = {
        "p": {
            ('h°1',): {"red": False, "blue": False},
            ('h', 'h°1', 'q⊗1'): {"red": False, "blue": False},
            ('h', 'h°1', 'p', 'q⊗1'): {"red": False, "blue": True},
        },
        "h": {
            ('h°1',): {"red": False, "blue": False},
            ('h', 'h°1', 'q⊗1'): {"red": True, "blue": False},
            ('h', 'h°1', 'p', 'q⊗1'): {"red": True, "blue": True},
        }}

    nb_formulas += check(res, formulas, seuils, box_res, normalize, add="?After[flip_coin] : ")

    cache = {}
    # ProbaTable need composition method to permit After.
    if isinstance(manager, ADDManager):
        for f in [After(init_pt, Atom("h")),
                  Not(After(init_pt, Not(Atom("h")))),
                  Not(After(init_pt, MPr(a, [(1, h), (1, Not(h))], "<=", 0.99))),
                  After(init_pt, MPr(a, [(1, h), (1, Not(h))], ">=", 1.0))]:
            assert_and_show(ps, f, cache=cache)
            # assert ps.modelCheck(f, cache=cache)

BLOCK_PRINT = False
validate_list = []

def run():

    V = ["p"]

    order = generate_order(V, 1, getAfterSymbol())
    manager = ADDManager.create(order)

    Law = Atom("p")
    Law = Top()

    rel = Equiv(Atom("p"), Atom(prime("p")))

    rel = manager.from_formula(Top(), vars=["p", prime("p")])

    Obs = {"1": rel}
    Pi = {"1": rel}
    # Pi = {"1": manager.from_formula(And(rel, Law))}

    ks = ProbaStructure(V, Law, Obs, Pi, ["p"], manager=manager, isnormalized=False, pi_in_theta=False)

    formulas = []
    formulas.append(Box_a("1", Atom("p")))
    formulas.append(Box_a("1", Not(Atom("p"))))
    formulas.append(Pr("1", Atom("p"), ">=", 0.49))
    formulas.append(Pr("1", Atom("p"), ">=", 0.51))
    resultats = [False, False, True, False]
    for i, f in enumerate(formulas):
        assert ks.modelCheck(f) == resultats[i]


    with BlockPrint(BLOCK_PRINT, showtime=True) as blocktime:

        ### This is S5 (True), but also tested on KD45 implementation (False)
        for nb_child in range(10, 21, 10):
            for S5 in [True, False]:
                print(Color.get(f">> Muddy - nbChilds={nb_child}, s5={S5} :", Color.CBOLD))
                MuddyChildrens(nb_child, S5)
        for S5 in [True, False]:
            print(Color.get(f">> s5={S5} :", Color.CBOLD))
            print(Color.get(f">CherrylsBirthday", Color.CBOLD))
            CherrylsBirthday(S5)
            print(Color.get(f">DrinkingLogicians", Color.CBOLD))
            DrinkingLogicians(S5)
            print(Color.get(f">semi_private_announcement", Color.CBOLD))
            semi_private_announcement(S5)

        ### This is purely KD45
        print(Color.get(f">fully_private_annoucement", Color.CBOLD))
        fully_private_annoucement(False)
        print(Color.get(f">SallyAndAnne", Color.CBOLD))
        SallyAndAnne()

        ### This is ProbaStructure tests, for PDEL
        print(Color.get(f">FlipCoin", Color.CBOLD))
        for normalized in [False, True]:
            # Test with different type of PseudoBooleanFunction
            for with_probaTable in [False, True]:
                # if with_probaTable==False, ADDPseudoBooleanFunction
                # if with_probaTable==True,  ProbaTable
                flip_coin(normalized, with_probaTable)

    print(Color.get("All tests passed successfully:", Color.CBOLD, Color.CGREEN))
    for valide in validate_list:
        print(Color.get(f"  - {valide}", Color.CBOLD, Color.CGREEN))
if __name__ == "__main__":

    # python3 toy_examples.py
    # or python3 -O toy_examples.py to gain large speedup (but no assertions).
    # on i7, 8eme Gen : Total time : 18.8608 sec versus 0.5361 sec

    run()
