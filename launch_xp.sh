#!/bin/bash

# Simple use:
# ./launch.sh folder nb_loop timeout
# ex: ./launch.sh NEWEXP 2 2400
# create :
# ./NEWEXP/
#  - all files *.txt with results
# ./NEWEXP_graphes/
#  - differents graphes *.jpg

# Use with timers and datas:
# ex: /usr/bin/time --verbose -o NEWEXP_report.txt ./launch_xp.sh NEWEXP 2 2400
# create :
# NEWEXP_report.txt # with time, CPU and RAM informations for all script.
# ./NEWEXP/
#  - all files *.txt with results
#  - all files *_time.txt with time, CPU and RAM informations
# ./NEWEXP_graphes/
#  - differents graphes *.jpg

if [ "$#" -ne 4 ]; then
    echo "Illegal number of parameters"
    exit 1;
fi

if [ -d "$1" ]; then
  echo "'$1' already exists"
  exit 1;
fi

Folder="$1"
mkdir "$Folder";

Nb_loops="$2"
Timeout="$3"
TimeoutMCPU="$4"

echo "$0 Folder: '$Folder', timeout: '$Timeout', timeout_mcpu: '$TimeoutMCPU' nb_loops: '$Nb_loops'"
date

run_process_loop () {
  begin="$1"
  shift 1
  for end; do

    filename=./"$Folder"/"$Prefix:"a"$agent"_ch"$Cards_in_hand"_"$begin"-"$end"${proba}"$Norm_flag"_x${i}

    echo $filename

    # shellcheck disable=SC2086
    if true; then
      /usr/bin/time -f "; %C; time; %es; memory; %MKB; cpu; %P;ch;${Cards_in_hand};end;${end}" -o "$filename"_time.txt \
        python3 hanabi_loop.py \
        -O -b "$begin" -e "$end" -step "$Nb_steps" -a "$agent" -ch "$Cards_in_hand" -xc "$Timeout" -lr 5 $Symbolic_flag -mc "$TimeoutMCPU" -pu "$TimeoutMCPU" $proba $Norm_flag \
          >"$filename".txt &
       ##REMOVE THE & HERE ^  TO RUN EVERYTHING SEQUENTIALLY (useful to reproduce the experiments on a standard desktop machine)
      begin="$end"
    fi

  done
}

for i in $(seq 1 "$Nb_loops");
do
  for proba in "-p" # "" "-p"
  do

    ######## SMCPDEL
    Prefix=smcpdel
    Symbolic_flag='-s'
    Nb_steps=2


    if [[ "$TimeoutMCPU" -eq -1 ]]; then
      echo "Pas de MC ou PU en dénormalisé."

      Norm_flag=''

      agent=2
      Cards_in_hand=2
      run_process_loop 6 26 34 42 46 51
      Cards_in_hand=3
      run_process_loop 7 23 33 39 43 47 49 51
      Cards_in_hand=4
      run_process_loop 9 23 33 39 43 47 49 51
      Cards_in_hand=5
      run_process_loop 11 23 33 39 43 47 49 51

      agent=3
      Cards_in_hand=2
      run_process_loop 7 26 34 42 46 51
      Cards_in_hand=3
      run_process_loop 10 23 33 39 43 47 49 51
      Cards_in_hand=4
      run_process_loop 13 23 33

      agents=4
      Cards_in_hand=2
      run_process_loop 9 26 34 42 46 51
      Cards_in_hand=3
      run_process_loop 13 25 33 39 43 47 49 51


    else
      echo "MC et PU en normalisé et dénormalisé"

      for Norm_flag in "" "-n"
      do
        agent=2
        Cards_in_hand=2
        run_process_loop 6 26 34 42 46 51
        Cards_in_hand=3
        run_process_loop 7 23 33 39 43 47 49 51

        agent=3
        Cards_in_hand=2
        run_process_loop 7 26 34 42 46 51
        Cards_in_hand=3
        run_process_loop 10 23 33 39 43 47 49 51
      done

    fi

    #run_process_loop 50 51

    ######## explicit
    #Prefix=expl
    #Symbolic_flag=''
    #Nb_steps=1
    #Cards_in_hand=2
    #run_process_loop 6 14 18

    #Cards_in_hand=3
    #run_process_loop 7 12 16

  done
  wait # does not overload the computing capacities with the biggest calculations: reproduces the run identically in CPU demand
done

echo "All processes done!"

# Finding total time for all 'atomic run' and CPU
#grep --include=*time.txt -rnw ./"$Folder"/ -e "User time (seconds)" -e "Maximum resident set size (kbytes)" >> "$Folder"_report.txt
grep --include=*time.txt -rnw ./"$Folder"/ -e "time;" >> "$Folder"_all_report_tmp.csv
sort --field-separator=',' -r -k4 -k6 "$Folder"_all_report_tmp.csv > "$Folder"_all_report.csv
echo "Print ${Folder}_all_report.csv"
rm -f "$Folder"_all_report_tmp.csv

# Generation of graphes to show results
# python3 scripts/rendering_of_calculations.py ./"$Folder"/ ./"$Folder"_graphes
date
