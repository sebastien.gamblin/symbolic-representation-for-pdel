
#--->
# It's a dirty trick to deal with 'scripts' folder
# Otherwise, this file can be copy/paste near to 'src' folder
# and launched with 'python3 -O XXX.py'

import os, sys

p = os.path.abspath('scripts')
sys.path.insert(1, p)

#<-- End of the trick

from src.utils.timer import Timer

from src.model.datastructure.compteur import Compteur
from src.model.datastructure.real_function_tests import *
from src.model.datastructure.add.add_real_function import ADDManager

from src.model.epistemiclogic.epistemicmodel.explicit.explicit_proba_epistemic_model import ExplicitProbaEpistemicModel
from src.model.epistemiclogic.eventmodel.explicit.explicit_pdel_event_model import *

from src.model.SMCPDEL.pySMCPDEL import ProbaStructure, ProbaTransformer, ConditionalPrecondition, sqsubseteq, \
    sqsubseteq_formula, generate_order, prime, get_id_symbol

"""
Module to model check on random Kripke structure
and their symbolic representation and compare
results.

Caution:
- Create random formulas with big depth can be long.
- Create random worlds with valuation, by default valuation-injective need
multiple values tries. But with multiple random tries, it would be fine.
- Create random event model can be dangerous, there is a while until get a
good inconsistant set of formulas.
"""


def get_omega(arcs, agents, double_vocabulary, manager, get_s):
    omega = {a: manager.getConstant(0.0, vars=double_vocabulary) for a in agents}
    for world1, world2, agent in arcs:
        s1, s2 = get_s(world1), prime(get_s(world2))
        omega[agent] = omega[agent].apply("+", sqsubseteq_formula(s1 + s2, double_vocabulary, manager))
    return omega


def get_pi(probabilities, double_vocabulary, manager, get_s):
    pi = {}
    for agent, probabilities_agent in probabilities.items():
        accumulator = manager.getConstant(0.0, vars=double_vocabulary)
        for world1 in probabilities_agent.dict.keys():
            s1 = get_s(world1)
            for world2, proba in probabilities_agent.dict[world1].items():
                sqs = sqsubseteq_formula(s1 + prime(get_s(world2)), double_vocabulary, manager)
                accumulator = accumulator.apply("+", sqs.apply("x", manager.getConstant(proba, vars=double_vocabulary)))
        pi[agent] = accumulator
    return pi

def explicite_to_symbolic_structure(expl, vocabulary):

    vocabulary_prime = [prime(v) for v in vocabulary]
    double_vocabulary = vocabulary + vocabulary_prime

    agents = expl.getAgents()

    order = generate_order(vocabulary, 2, getAfterSymbol())
    manager = ADDManager.create(order=order, tmp=True)

    def get_s(world):
        return [p for p, v in world.getValuation().items() if v]

    def get_law(worlds, vocabulary):
        bot = manager.getConstant(0.0, vars=vocabulary)
        for world in worlds:
            bot = bot.apply("+", sqsubseteq_formula(get_s(world), vocabulary, manager))
        return bot

    # v: List[str], theta: PseudoBooleanFunction, o: Dict[str, Formula], pi: Dict[str, Formula],
    #                  pointed: List[str], manager, isnormalized=True, pi_in_theta=False, nb_apply=1, cache=None, **kwargs):
    proba_structure = ProbaStructure(
        vocabulary,
        get_law(expl.getNodes(), vocabulary),
        get_omega(expl.getArcs(), agents, double_vocabulary, manager, get_s),
        get_pi(expl.probabilities, double_vocabulary, manager, get_s),
        get_s(expl.getPointedWorld()),
        manager)

    return proba_structure


def explicite_to_symbolic_transformer(event_model, vocabulary, manager):
    """
    p.67 def 2.9.2
    :return:
    """

    # PRE-FORMATE THINGS
    # Format of Event Model into simple list of dict
    label = "ie"

    A = [e.getName() for e in event_model.getEvents()]

    modified_vocabulary = set() # modified subset of formula
    post = {}

    known_names = set()
    for e in event_model.getEvents():
        e_name = e.getName()
        assert e_name not in known_names
        known_names.add(e_name)
        post[e_name] = {}
        for atom, formula in e.getPostcondition().items():
            assert atom in vocabulary
            post[e_name][atom] = formula
            modified_vocabulary.add(atom)

    modified_vocabulary = list(modified_vocabulary)

    relations = []
    for e1, e2, agent in event_model.getArcs():
        relations.append((e1.getName(), e2.getName(), agent))

    ### Begin the transformation

    vocabulary_plus = [f"{label}{i}" for i in range(0, math.ceil(math.log(len(A))))]
    vocabulary_plus_prime = [prime(p) for p in vocabulary_plus]
    double_vocabulary_plus = vocabulary_plus + vocabulary_plus_prime

    cpt = Compteur(label, vocabulary_plus)
    cache = {}
    @memoize(cache)
    def labelling(a):
        """
        Labelling function
        """
        return [k for k, v in  cpt.get(A.index(a)).items() if v]

    for p in generate_order(vocabulary_plus, 2, get_id_symbol()):
        manager.add_var(p)

    pointed = labelling(event_model.getPointedEvent().getName())

    # this is on events with dict
    probability_distribution = event_model.getCondProbabilityDistribution().get_dict()
    # this is on label on event name's with tuples
    probability_distribution = {f: tuple((labelling(e.getName()), proba) for e, proba in distri.items()) for f, distri in probability_distribution.items()}

    conditonnal_preconditions = ConditionalPrecondition(probability_distribution, manager, vocabulary, vocabulary_plus)

    theta_ = {}
    for p in modified_vocabulary:
        or_formula = Bot()
        for a in A:
            # the postcondition of 'p' in atomic event 'a' is mentionned
            fill_postcondition = post[a][p] if p in post[a] else Atom(p)
            # BigOr_{ a \in events } l(a) and post(a)
            or_formula = Or(or_formula, And(sqsubseteq(labelling(a), vocabulary_plus), fill_postcondition))

        theta_[p] = or_formula

    def get_e(event):
        return labelling(event.getName())

    # arcs, agents, double_vocabulary, manager, get_s):
    omega = get_omega(event_model.getArcs(), event_model.getAgents(), double_vocabulary_plus,  manager, get_e)

    # (probabilities, double_vocabulary, manager, get_s):
    pi = get_pi(event_model.probabilities, double_vocabulary_plus, manager, get_e)

    # ksv: List[str], v: List[str], theta_pre,
    #                  o: Dict[str, PseudoBooleanFunction], pi: Dict[str, PseudoBooleanFunction],
    #                  pointed,
    #                  manager: PseudoBooleanFunctionManager,
    #                  v_: List[str] = [],
    #                  theta_: Dict[str, Formula] = dict(), name=None
    return ProbaTransformer(vocabulary, vocabulary_plus, conditonnal_preconditions, omega, pi, pointed, manager,
                            v_=modified_vocabulary, theta_=theta_)




def explicite_versus_symbolic(nb_worlds, nb_events, nb_vars, nb_agents, nb_formulas, fill_percent, seed=None):

    global FORMULAS_CHECKED, reflexive, symetric

    import random

    if seed is None:
        rng = random.Random()
        seed = rng.randrange(100000000)
        rng.seed(seed)
        print("SEED =", seed)
    else:
        print("SEED (fixed)=", seed)
        rng = random.Random(seed)

    varnames, varagents = lambda i: f"x{i}", lambda i: f"a{i}"
    variables = [varnames(i) for i in range(0, nb_vars)]
    agents = [varagents(i) for i in range(0, nb_agents)]

    #### CREATE A RANDOM PROBABILISTIC KRIPKE STRUCTURE
    kripke_structure = ExplicitProbaEpistemicModel.getRandom(nb_worlds, nb_vars, nb_agents, fill_percent, varname=varnames,
                                                 agentname=varagents, reflexive=reflexive, symetric=symetric, rng=rng, seed=seed)

    # print(kripke_structure.getWorlds())
    #### AND ITS SYMBOLIC REPRESENTATION
    proba_structure = explicite_to_symbolic_structure(kripke_structure, variables)
    print(f"nbWorlds={len(kripke_structure.getWorlds())}, nbArcs={len(kripke_structure.getArcs())}")

    ### GENERATE SOME RANDOM FORMULAS AND MODEL CHECK
    for i in range(0, nb_formulas):

        new_seed = rng.randrange(100000000)
        formula = Formula.getRandom(agents, variables, 2, proba=True, seed=new_seed, rng=rng, S5=False)

        res1, res2 = kripke_structure.modelCheck(formula), proba_structure.modelCheck(formula)
        FORMULAS_CHECKED += 1
        compare = res1 == res2

        if not compare:
            print(kripke_structure)
            print(proba_structure)
            print(formula)
        assert compare, f"Differents results : {res1} and {res2} on {formula}"
        if SHOW:
            print(Color.get(f"(F, {proba_structure.pointed}) {'⊨' if res1 else '⊭'} {formula}", Color.CGREEN))

    #### CREATE A RANDOM PROBABILISTIC EVENT MODEL
    # With Kripke on parameter because it needs to be applyed on.
    event_name = lambda i: f"e{i}"
    event_model_expl = ExplicitPDEL_EventModel.getRandom(kripke_structure, nb_events, variables, agents, fill_percent, eventname=event_name,
                                                    reflexive=reflexive, symetric=symetric, seed=seed, rng=rng)
    print(f"nbEvents={len(event_model_expl.getEvents())}, nbArcs={len(event_model_expl.getArcs())}")
    ### AND ITS SYMBOLIC REPRESENTATION
    proba_transformer = explicite_to_symbolic_transformer(event_model_expl, variables, proba_structure.manager)

    ## GENERATE SOME RANDOM FORMULAS AND MODEL CHECK
    cache = {}
    for i in range(0, nb_formulas):

        new_seed = rng.randrange(100000000)
        formula = Formula.getRandom(agents, variables, 2, proba=True, seed=new_seed, rng=rng, S5=False)

        formula_expl = After(event_model_expl, formula)
        formula_symb = After(proba_transformer, formula)

        res1 = kripke_structure.modelCheck(formula_expl)
        res2 = proba_structure.modelCheck(formula_symb, cache=cache)

        compare = res1 == res2
        if not compare:

            print(event_model_expl)

            print(proba_transformer)

            res1 = kripke_structure.modelCheck(formula_expl, show=False)
            res2 = proba_structure.modelCheck(formula_symb, show=False)
            print(Color.get(f"{res1},{res2} : After(event, {formula})", Color.CRED))
        assert compare, f"Differents results : {res1} and {res2} on {formula} (After)"

        if SHOW:
            #print(Color.get(f"{res1} : After(random_event, {formula})", Color.CGREEN))
            print(Color.get(f"(F, {proba_structure.pointed}) {'⊨' if res1 else '⊭'} [!random_event]{formula}", Color.CGREEN))

        FORMULAS_CHECKED += 1





if __name__ == '__main__':

    SHOW = True

    FORMULAS_CHECKED = 0

    nb_worlds, nb_events, nb_vars, nb_agents, nb_formulas = 16, 4, 8, 3, 10

    # parameters to create structures
    reflexive = False
    symetric = False
    fill_percent = 0.7

    if len(sys.argv) == 1:
        nb_structures = 2
    else:
        assert len(sys.argv) == 2, f"Need one argument : number of random generations."
        nb_structures = int(sys.argv[1])

    print("Number of tests                          =", nb_structures)

    print("Number of agents                         =", nb_agents)
    print("Number of worlds in Kripke Structures    =", nb_worlds)
    print("Number of events in Event models         =", nb_events)
    print("Number of propositional variables        =", nb_vars)
    print("Number of random structures to construct =", nb_structures)
    print("Number of random formulas to check (x2 with After) =", nb_formulas)
    print(f"Fill percent of structures : {fill_percent}, reflexive={reflexive}, symetric={symetric}")
    print()

    seed = None

    if (seed is not None and nb_structures != 1):
        print(Color.get("Caution : if seed is not None, you need only one structure to check (nb_structures=1). "
              f"Do you really want to launch {nb_structures} times the same execution ?", Color.CRED))
        sys.exit()

    with Timer(show=True):
        for i in range(0, nb_structures):
            # generate differents structures
            explicite_versus_symbolic(nb_worlds, nb_events, nb_vars, nb_agents, nb_formulas, fill_percent, seed=seed)

        print("NB STRUCTURES =", nb_structures)
        print("NB FORMULAS_CHECKED =", FORMULAS_CHECKED)
