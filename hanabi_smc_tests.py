
#--->
# It's a dirty trick to deal with 'scripts' folder
# Otherwise, this file can be copy/paste near to 'src' folder
# and launched with 'python3 -O XXX.py'

import os, sys

p = os.path.abspath('scripts')
sys.path.insert(1, p)

#<-- End of the trick

from hanabi_smc import HanabiSMC, read_atome, TABLE_NAME, DISCARD_NAME, DRAW_NAME
from my_args import arg_parser
from src.io.colors import *
from src.model.SMCPDEL.pySMCDEL import get_id_symbol
from src.model.datastructure.add.add_real_function import ADDManager
from src.model.epistemiclogic.formula.formula import *


def get_structure_and_transformers(nbCards, nbCH, normalize):

    hanabi_example = HanabiSMC(False, 2, nbCards, nbCH, False, inter=False, manager=ADDManager, prof=5,
                       order=['cards', 'prime', 'position', 'agents', 'after'], cleaning=False,
                       proba=True, tokens=True, normalize=normalize)

    ks = hanabi_example.getEpistemicModel()
    ts = hanabi_example.getEventModels()
    return hanabi_example, ks, ts


def check(ks, formulas_and_results):

    print(Color.get("\n>> MODEL CHECKING", Color.CVIOLET, Color.CBOLD))
    print("Pointed (filter):", sorted(list(set([p for p in ks.pointed if not getAfterSymbol() in p and not get_id_symbol() in p]))))

    cache = {}

    for formula, result in formulas_and_results:

        #print(formula, result)

        res = ks.modelCheck(formula, cache=cache)
        print(">>MC", formula, Color.get(str((res, result)), Color.CGREEN if result == res else Color.CRED))

        if result != res:
            ks.modelCheck(formula, show=False)
            assert False


def get_formulas(ks, seuil):

    n_pointed = ks.pointed

    formulas = []

    for agent in ks.omega.keys():
        for p in ks.pointed:
            if get_id_symbol() not in p and getAfterSymbol() not in p:

                try:
                    res = read_atome(p)
                except Exception:
                    continue

                if len(res) == 4:
                    # Variable of table, discard, of deck
                    X, c, v, i = res
                    if X in [TABLE_NAME, DISCARD_NAME]:
                        formulas.append((Box_a(agent, Atom(p)), p in ks.pointed))
                        for agent2 in ks.observations.keys():
                            formulas.append((Box_a(agent, Box_a(agent2, Atom(p))), True))
                    elif X == DRAW_NAME:
                        formulas.append((Box_a(agent, Atom(p)), False))
                        formulas.append((Pr(agent, Atom(p), "<=", 1), True))
                        for agent2 in ks.omega.keys():
                            formulas.append((Box_a(agent, Not(Box_a(agent2, Atom(p)))), True))
                    else:
                        raise Exception("Not supposed to be here ? Unknown " + X)

                elif len(res) == 5:
                    # Variable of an agent
                    a, pos, c, v, i = res
                    if agent == a:
                        formulas.append((Box_a(a, Atom(p)), False))
                        formulas.append((Pr(agent, Atom(p), ">", 0), True))
                        formulas.append((Pr(agent, Atom(p), ">=", seuil), True))
                        formulas.append((Pr(agent, Atom(p), ">=", seuil+0.01), False))
                        formulas.append((Box_a(agent, Not(Box_a(agent, Atom(p)))), True))
                    else:
                        formulas.append((Box_a(agent, Atom(p)), p in ks.pointed))
                        formulas.append((Box_a(agent, Not(Box_a(a, Atom(p)))), True))

                        formulas.append((Pr(agent, Pr(a, Atom(p), ">=", seuil-0.01), ">=", 1), True))
                        formulas.append((Pr(agent, Pr(a, Atom(p), "<=", seuil + 0.01), ">=", 1), True))

    return formulas

"""
formulas.append(Pr(a, Atom(p), ">=", seuil))
formulas.append(Pr(a, Atom(p), ">=", seuil + 0.1))
formulas.append(Pr("b", Pr(a, Atom(p), ">=", 0.33), ">=", 1))
formulas.append(Pr("b", Pr(a, Atom(p), ">=", 0.34), ">=", 1))
"""

if __name__ == "__main__":

    try:

        args = arg_parser()

        assert args.probability is True, "For testing SMCPDEL, we need probabilities : please use '-p'"

        hanabi_example = HanabiSMC(False, args.agents, args.nbCards, args.chands, args.random, inter=False, manager=ADDManager, prof=4,
                           order=args.order, cleaning=False,
                           proba=True, tokens=True, normalize=args.norm)

        ks = hanabi_example.getEpistemicModel()
        ts = hanabi_example.getEventModels()

        hanabi_example.check(ks)

        # TODO: after update !
        # actions = ["a-plays-pos0-R11:T", "Announce for b : 1 at [0]"]

    except SystemExit:
        # FIX PARAMETERS
        print("We will use fixed parameters.")

        normalize = False

        nbCards, nbCardsH = 6, 2
        hanabi_example, ks, kt = get_structure_and_transformers(nbCards, nbCardsH, normalize)
        hanabi_example.check(ks)

        #liste = ["a-plays-pos0-R11:T", "a-draws-D-R22"]

        hanabi_example.playable_actions(ks, kt, show=True)

        actions = hanabi_example.get_actions_to_apply(["Announce for b : 1 at [0]"])
        annonce = actions[0]

        formula = Box_a("b", MOr(Atom("b0-R11"), Atom("b0-R12"), Atom("b0-R13")))

        post_announcement = ks.apply(annonce)
        res = post_announcement.modelCheck(formula)
        assert res
        print(">>MC After(Announce for b : 1 at [0]) ", formula)
