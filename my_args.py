
import argparse

#####################################################################################
# args_use


def arg_parser(boucle=False):

    parser = argparse.ArgumentParser()

    #### boucle

    if boucle:
        parser.add_argument("-b", "--begin", type=int, choices=range(3, 51, 1),
                            help="BOUCLE begin number of cards", default=6)

        parser.add_argument("-e", "--end", type=int, choices=range(3, 52, 1),
                            help="BOUCLE ending number of cards", default=20)

        parser.add_argument("-step", "--step", type=int, choices=range(1, 6, 1),
                            help="BOUCLE step between begin and ending number", default=2)


    ### One run
    if not boucle:
        parser.add_argument("nbCards", type=int, help="number of cards in Hanabi", choices=range(3, 51, 1))

    parser.add_argument("-v", "--verbosity", action="store_true", default=False, help="verbose or not")

    parser.add_argument("-a", "--agents", type=int, choices=range(2, 6, 1),
                        help="change the number of agents. Default=2.", default=2)

    parser.add_argument("-ch", "--chands", type=int, choices=range(1, 6, 1), default=1,
                        help="change the number of number of cards in hands. Default=1.")

    parser.add_argument("-p", "--probability", action="store_true", default=False, help="use probability in structure")

    parser.add_argument("-s", "--symbolic", action="store_true", default=False, help="symbolic structure or not")

    parser.add_argument("-t", "--type", type=str, choices=["ADD", "SLDD", "CUDD", "add", "sldd", "cudd"],
                        default="ADD", help="change the type of symbolic repr. Default=ADD. Unused if not -s.")

    parser.add_argument("-xc", "--timeoutcreation", type=int, default=0, help="use a timeout")

    parser.add_argument("-lr", "--recursion", type=int, default=3, help="modify limit recursion : 10**lr")

    parser.add_argument("-mc", "--modelcheck", type=int, default=-1, help="test modelcheck or not. Defaut=-1, if 0: no timeout, other=with timeout")

    # ['cards', 'prime', 'position', 'agents', 'after']
    # ["cards", "agents", "position", "prime", "after"]
    parser.add_argument("-o", '--order', metavar='N', type=str, nargs='+', default=['cards', 'prime', 'position', 'agents', 'after'], help='list o string for order')

    parser.add_argument("-pu", "--productupdate", type=int, default=-1, help="test product update or not. Defaut=-1, if 0: no timeout, other=with timeout")

    parser.add_argument('-f', '--formulas', nargs='+', type=int, default=[])

    parser.add_argument("-m", "--memory", action="store_true", default=False, help="calculate memory used or not")

    parser.add_argument("-r", "--random", action="store_true", default=False, help="random or not")

    parser.add_argument("-c", "--centered", action="store_true", default=False, help="Symbolic formula centered on possible worlds")

    parser.add_argument("-i", "--input", action="store_true", default=False,
                        help="Require input in program or not")

    parser.add_argument("-kbp", type=int, default=-1,
                        help="test kbp or not. Defaut=-1, if 0: no timeout, other=with timeout")

    #parser.add_argument("-nstep", "--nstep", type=int,
    #                    help="Number of step in planification", default=1)

    #parser.add_argument("-bag", "--bag", action="store_true", default=False, help="bag in planification or not")

    #parser.add_argument("-cb", '--collapsebag', metavar='N', type=int, nargs='+', default=[], help='list o int for collapsing bag')
    #parser.add_argument("-ca", '--collapseall', metavar='N', type=int, nargs='+', default=[], help='list o iny for collasping all')

    parser.add_argument("-mal", "--malvin", action="store_true", default=True, help="")

    parser.add_argument("-s5", "--s5", action="store_true", default=False, help="")

    parser.add_argument("-prof", "--prof", type=int,
                        help="Planification depth for malvin historisation.", default=4)

    parser.add_argument("-n", "--norm", action="store_true", default=False, help="Normalise the structure or not ? Default=False.")

    parser.add_argument("-O", "--asserts", action="store_true", default=False,
                        help="Disable -O in python3.")

    args = parser.parse_args()

    if len(args.order) not in [0, 5]:
        parser.error(f"--order need to get 5 ordonned elements : agents cards prime positions after. Here : {args.order}")

    if args.timeoutcreation < 0:
        parser.error(f"creation time must be positive")

    # print(args)

    return args