import sys

from os import path, listdir, mkdir

import csv
from pprint import pprint
import statistics
import numpy as np

"""
Rend compte des fichiers générés par le lancement de:
python3.6 hanabi_loop.py de type :

[RES];Symb; False;type;None; proba; True; nbCards; 9; cardsHands; 4; ...
[OK]; MC errors;;Apply; OK;
...

"""

####################################################################
# READING PART

MY_DATA = {}

linestyles = ["solid", "dashed", "dotted", "dashdot", "solid", "dashed", "dotted", "dashdot"]


def search_data(string, row):
    for i, el in enumerate(row):
        if el.strip() == string:
            res = row[i + 1].strip()  # .replace(",", ".")
            if "," in res or "." in res:
                try:
                    return float(res.replace(",", "."))
                except ValueError:
                    return res
            elif "timeout" in res.lower():
                return res
            try:
                return int(res)
            except:
                return res
    return None


def search_multiples(string, row):
    liste = []
    for i, el in enumerate(row):
        name = el.strip()
        if string in name:
            res = row[i + 1].strip()  # .replace(",", ".")
            if "," in res or "." in res:
                try:
                    liste.append((name, float(res.replace(",", "."))))
                except ValueError:
                    liste.append((name, res))
            elif "timeout" in res.lower():
                liste.append((name, res))
            else:
                try:
                    liste.append((name, int(res)))
                except:
                    liste.append((name, res))
    return liste


def read_symb(row):
    # print("check", row[1])
    return row[2].strip() == "True"
    # return bool(row[FIELDS["symb"]].strip())


def store_single(dico_type, key, nbA, nbH, nbC, item, value):
    global MY_DATA

    if dico_type not in MY_DATA:
        MY_DATA[dico_type] = {}

    if key not in MY_DATA[dico_type]:
        MY_DATA[dico_type][key] = {}

    if nbA not in MY_DATA[dico_type][key].keys():
        MY_DATA[dico_type][key][nbA] = {}

    if nbH not in MY_DATA[dico_type][key][nbA].keys():
        MY_DATA[dico_type][key][nbA][nbH] = {}

    if nbC not in MY_DATA[dico_type][key][nbA][nbH].keys():
        MY_DATA[dico_type][key][nbA][nbH][nbC] = {}

    if item not in MY_DATA[dico_type][key][nbA][nbH][nbC].keys():
        MY_DATA[dico_type][key][nbA][nbH][nbC][item] = list()

    MY_DATA[dico_type][key][nbA][nbH][nbC][item].append(value)


def store_multiple(dico_type, key, nbA, nbH, nbC, keyword, row, erase=False):
    for item, t in search_multiples(keyword, row):
        if erase:
            item = item.replace(keyword, "")

        addon1 = search_data("Precompile PiTheta", row)
        addon2 = search_data("Precompile Marg(PiTheta)", row)


        if item == "K_a a":
            if addon1 not in [None, "None"]:
                t = t - float(addon1)
            if addon2 not in [None, "None"]:
                # print(addon2, None, not addon2 is None, type(addon2))
                t = t - float(addon2)

        if t not in [None, "None"]:
            store_single(dico_type, key, nbA, nbH, nbC, item, float(t))


def store_somme(dico_type, key, nbA, nbH, nbC, keyword1, keyword2, newkeyword, row):
    data1 = search_data(keyword1, row)
    data2 = search_data(keyword2, row)
    # print(nbC, keyword1, keyword2)
    t = 0
    if not (data1 is None or data1 == "None"):
        t += float(data1)
    if not (data2 is None or data2 == "None"):
        t += float(data2)
    if t == 0:
        return
    # print(data1, data2, data1 not in [None, "None"])
    store_single(dico_type, key, nbA, nbH, nbC, newkeyword, t)


def store_my_ratio(dico_type, key, nbA, nbH, nbC, keyword1, keywords_tot, newkeyword, row):
    data1 = search_data(keyword1, row)
    total = 0
    for subkey in keywords_tot:
        total += float(search_data(subkey, row))
    # print(nbC, keyword1, keyword2)
    if data1 is None or total == 0:
        return
    store_single(dico_type, key, nbA, nbH, nbC, newkeyword, float(data1) / total)


def get_key(malvin, type, symb, proba, norm):
    return (malvin, type, symb, proba, norm)


def read_key(key):
    return {"SMCDEL": key[0], "S5": False if not key[2] else key[1], "Symbolic": key[2], "Probabilistic": key[3],
            "Normalize": key[4]}


def fill(row):
    # print("FILL")

    global MY_DATA

    smcdel = search_data("SMCDEL", row) == "True"
    type = search_data("S5", row) == "True"
    symb = search_data("Symb", row) == "True"
    proba = search_data("proba", row) == "True"
    norm = search_data("normalize", row) == "True"

    # print(malvin, type, symb, proba)

    # key = "SMCDEL={}-S5={}-Symb={}-Proba={}".format(malvin, type, symb, proba)
    key = get_key(smcdel, type, symb, proba, norm)
    if key == (True, True, False, False, False):  # DIRTY but quick patch
        key = (True, False, False, False, False)  # Expl with S5 <=> Expl without S5

    nbA = search_data("nbAgents", row)
    nbA = 2 if nbA is None else int(nbA)

    nbH = int(search_data("cardsHands", row))
    nbC = int(search_data("nbCards", row))

    for search in ["total time MC", "create_epistemic_model", "create_event_models", "Apply time"]:
        res = search_data(search, row)
        # MY_DATA[key][nbA][nbH][nbC][search].append(res)
        # print(search, key, nbA, nbH, nbC, search, res)

        store_multiple(search, key, nbA, nbH, nbC, search, row, erase=False)

    # (dico_type, key, nbA, nbH, nbC, keyword1, keyword2, newkeyword, row)

    store_my_ratio("RatioEp/TOT", key, nbA, nbH, nbC, "create_epistemic_model",
                   ["create_epistemic_model", "create_event_models"], "RatioEp/TOT", row)

    store_my_ratio("RatioEv/TOT", key, nbA, nbH, nbC, "create_event_models",
                   ["create_epistemic_model", "create_event_models"], "RatioEv/TOT", row)

    store_somme("Creation time", key, nbA, nbH, nbC, "create_epistemic_model", "create_event_models", "Creation time",
                row)

    store_multiple("Model checking", key, nbA, nbH, nbC, "FMC", row, erase=True)

    store_multiple("Product Update", key, nbA, nbH, nbC, "PUi", row, erase=True)

    store_multiple("NbVars", key, nbA, nbH, nbC, "NbVars", row)

    store_multiple("Omega", key, nbA, nbH, nbC, "OmegaSize-a", row)

    store_multiple("Pi", key, nbA, nbH, nbC, "PiSize-a", row)
    store_multiple("Pib", key, nbA, nbH, nbC, "PiSize-b", row)

    store_multiple("Precompile PiTheta", key, nbA, nbH, nbC, "Precompile PiTheta", row)
    store_multiple("Precompile Marg(PiTheta)", key, nbA, nbH, nbC, "Precompile Marg(PiTheta)", row)

    store_somme("Precompile", key, nbA, nbH, nbC, "Precompile PiTheta", "Precompile Marg(PiTheta)", "Precompile",
                row)




def readfile(file):
    global MY_DATA

    with open(file, newline='') as csvfile:

        spamreader = csv.reader(csvfile, delimiter=';', quotechar='|')
        if True:
            for row in spamreader:
                if len(row) > 0 and row[0] == "[RES]":

                    row2 = []
                    for el in row:
                        if "trace" in el:
                            break
                        else:
                            row2.append(el)

                    fill(row2)
        # except:
        #    print("ERROR in ", file)
        #    sys.exit()


#############################################################################
# RENDERING PART

import matplotlib.pyplot as plt


def display_save(name, lg):
    global DISPLAY, to_store

    if DISPLAY:
        print("Display of " + name)
        plt.show()
    else:
        plt.savefig(to_store + "/" + name)  # , bbox_extra_artists=(lg,), )
        print("Creation of " + to_store + "/" + name)

    plt.clf()
    plt.close()


def pSymb(symb):
    return "Symb" if symb else "Expl"


def getMarker(nb):
    if nb == 1:
        return 'o'
    if nb == 2:
        return '^'
    if nb == 3:
        return "s"
    if nb == 4:
        return "x"
    raise ValueError("")


import matplotlib

# TO DEAL WITH BLACK AND WHITE PRINTERS

# viridi

cmap_type = 'Set1'
cmap = matplotlib.cm.get_cmap(cmap_type)

# black
explicit_color = cmap(0.0)

# yellow
symb_pdel_color_norm = cmap(0.2)

symb_pdel_color_2 = cmap(0.25)
symb_pdel_color = cmap(0.50)

symb_del_color_2 = cmap(0.75)
symb_del_color = cmap(0.90)

colors = ["tab:red", "tab:blue", "tab:orange", "tab:green", "tab:red", "tab:purple", "tab:brown", "tab:pink",
          "tab:cyan", "tab:cyan", "tab:gray"]
desired_colors = 3
cmap = matplotlib.cm.get_cmap(cmap_type)
range_float = range(0, 101, int(100 / (desired_colors - 1)))
# print([f for f in range_float])

# range_float = [0.0, 0.2, 0.4]
colors = [cmap(f / 100) for f in range_float]


def generic_draw(dico_names, title=None, label_function=None, timeout=None, log=False, filter_item=None,
                 filter_keys=None,
                 average=False, rename_item=None, dotdico=False, stddev=False, color_on_items=False, fixed_colors=None,
                 fixed_markers=None,
                 sort_legend=None, y=None, filter_data=None, SHOW=False, cols=1, autosort=None, loc=None,
                 bbox_to_anchor=None,
                 prop={'size': 9}, linewidth=0.65, markersize=14, markersize_except={}, line_except={},
                 label_nbh=True, show_n=False, ylim=None):

    if label_function is None:
        label_function = lambda key, nba, nbh: "-".join([f"{k}:{v}" for k, v in read_key(key).items()])

    if filter_item is None:
        # Keep all items (Differents formulas, for exemple)
        filter_item = lambda items: items

    if filter_keys is None:
        # All keys are accepted
        filter_keys = lambda key: True

    if filter_data is None:
        # All keys are accepted
        filter_data = lambda a, b: True

    if rename_item is None:
        rename_item = lambda s: s

    figure = plt.figure(dpi=200)
    from matplotlib.ticker import MaxNLocator
    ax = figure.gca()
    max_average = 0

    i_color = 0
    for iddico, dico_name in enumerate(dico_names):

        if dico_name not in MY_DATA:
            print(f"{dico_name} isn't initialized in MY_DATA")
            break
        my_dict = MY_DATA[dico_name]

        datas = {}

        for ikey, key in enumerate(sorted(my_dict.keys())):

            if not filter_keys(key):
                continue

            max_average = 0

            if key not in datas:
                datas[key] = {}

            for nbA in sorted(my_dict[key]):
                datas[key][nbA] = {}
                for nbH in sorted(my_dict[key][nbA]):

                    if not filter_data(nbA, nbH):
                        continue

                    datas[key][nbA][nbH] = {}

                    datas[key][nbA][nbH]["dates"] = {}
                    datas[key][nbA][nbH]["values"] = {}

                    for nbC in sorted(my_dict[key][nbA][nbH]):

                        # print("items", my_dict[key][nbA][nbH][nbC].items())
                        # print("filtered", filter_item( my_dict[key][nbA][nbH][nbC].items()))

                        for item, values_list in sorted(filter_item(my_dict[key][nbA][nbH][nbC].items())):

                            if item not in datas[key][nbA][nbH]["dates"]:
                                datas[key][nbA][nbH]["dates"][item] = []
                                datas[key][nbA][nbH]["values"][item] = []

                            # find, value

                            values_list = [v for v in values_list if not "timeout" in str(v).lower()]

                            if len(values_list) > 0:

                                max_average = max(max_average, len(values_list))

                                datas[key][nbA][nbH]["dates"][item].append(nbC)

                                # Mean of values
                                # print(values_list)
                                mean = statistics.mean(values_list)
                                datas[key][nbA][nbH]["values"][item].append(mean)

                                if len(values_list) > 1 and stddev:
                                    calcul_stdev = statistics.stdev(values_list)
                                    if calcul_stdev > 5:
                                        plt.errorbar(nbC, mean, yerr=calcul_stdev, color="Black")

        commun_label = []
        labels = []

        # fig, ax = plt.subplots()

        count = 0
        for key in sorted(datas):

            color = get_color(key)

            for nbA in sorted(datas[key]):
                for nbH in sorted(datas[key][nbA]):

                    dates_by_item = datas[key][nbA][nbH]["dates"]
                    values_by_item = datas[key][nbA][nbH]["values"]

                    bifurc = len(dates_by_item) != 1

                    items = dates_by_item.keys()
                    # filter = filter_item(items)
                    # print("items", items)
                    # print(filter)

                    for ii, item in enumerate(sorted(items)):
                        dates = dates_by_item[item]
                        values = values_by_item[item]

                        indexs_to_order_by = np.array(dates).argsort()
                        x_ordered = np.array(dates)[indexs_to_order_by]
                        y_ordered = np.array(values)[indexs_to_order_by]

                        label = "" if not show_n else str(count) + "-"
                        label += label_function(key, nbA, nbH) + " "
                        # label += f"av:{max_average}" if average else ""
                        label += "" if not bifurc else rename_item(item)

                        label += rename_item(dico_name) if len(dico_names) > 1 else ""
                        if label[-1] == " ":
                            label = label[:-1]
                        if label_nbh:
                            label += f", nbH:{nbH}"

                        # line = linestyles[dico_names.index(dico_name)] if dotdico else linestyles[ii]
                        line = '-'

                        if color_on_items:
                            color = colors[ii]

                        if fixed_colors is not None:
                            color = fixed_colors[i_color]
                            # print("Color", fixed_colors[i_color], i_color)

                        if fixed_markers is not None:
                            marker = fixed_markers[i_color]
                        else:
                            marker = getMarker(nbH - 1)

                        i_color += 1
                        # print("i_color", i_color, label)

                        count += 1
                        # print("Count", count)

                        m_changed = count in markersize_except.keys()
                        l_changed = count in line_except.keys()

                        # print(item, dates, values)
                        p2 = plt.scatter(dates, values, label=label,
                                         color=color, marker=marker, edgecolors="Black", ls=line,
                                         linewidth=linewidth / 1.75,
                                         s=markersize_except[count] if m_changed else markersize)

                        p1 = plt.plot(x_ordered, y_ordered, color=color, ls=line_except[count] if l_changed else line,
                                      linewidth=linewidth, zorder=0 if color != "red" else 10)  # , label=label

                        commun_label.append((p1, p2))
                        labels.append(label)

    tm = f"(timeout={timeout}s)" if timeout is not None else ""
    if not title is None:
        title = f'{title} {tm}'
    else:
        title = " ".join([rename_item(d) for d in dico_names]) + f'{tm}'

    if average:
        title += f" (average on {max_average} runs)"
    plt.title(title)

    plt.xlabel('Nombre total de cartes (nbC)', fontsize=12, color='Black')

    ax.xaxis.set_major_locator(MaxNLocator(integer=True))

    if ylim is not None:
        plt.ylim(*ylim)

    if y is None:
        if log:
            plt.yscale('log')
            y = "Temps (en secondes, échelle log.)"
        else:
            y = "Temps (en secondes)"

    if log:
        plt.yscale('log')

    plt.ylabel(y, fontsize=12, color='Black')

    # print(commun_label)

    lg = plt.legend(loc=loc, ncol=cols, bbox_to_anchor=bbox_to_anchor, prop=prop)

    if autosort is not None:
        sort_legend = autosort(labels)
        # print("SORT", sort_legend)

    if sort_legend is not None or autosort is not None:
        handles, labels = plt.gca().get_legend_handles_labels()
        # print("SORT", handles, labels)
        if len(sort_legend) == len(labels):
            plt.legend([handles[idx] for idx in sort_legend], [labels[idx] for idx in sort_legend],
                       loc=loc, ncol=cols, bbox_to_anchor=bbox_to_anchor, prop=prop)
        else:
            raise Exception(f"To sort, we need len(sort_legend) == len(labels). "
                            f"\nAutosort={autosort}"
                            f"\n labels ({len(labels)}) = {labels} "
                            f"\n sort_legend ({len(sort_legend)}) ={sort_legend}")

    plt.tight_layout()

    # from matplotlib.pyplot import figure

    name = f"{title.replace(' ', '-').replace('é', 'e').replace('è', 'e')}-log={log}.png"

    if SHOW:
        print("Display of " + name)

        canvas = figure.canvas
        canvas.get_default_filename = lambda: name
        plt.show()
    else:
        plt.savefig(to_store + "/" + name)  # , bbox_extra_artists=(lg,), )
        print("Creation of " + to_store + "/" + name)

    plt.clf()
    plt.close()


def key(cle, nbA, nbH):
    res = ""
    cle = read_key(cle)
    if cle["S5"]:
        res += "SK"  # KS
    elif not cle["Symbolic"] and not cle["Probabilistic"]:
        res += "Ex."  # Expl.
    elif not cle["Symbolic"] and cle["Probabilistic"]:
        res += "Ex Pr."  # Expl.
    elif cle["SMCDEL"] and not cle["Probabilistic"]:
        res += "SC"  # "BS
    elif cle["SMCDEL"] and cle["Probabilistic"]:
        res += "SP"  # "PS"
    else:
        pass

    if cle["Normalize"]:
        res += "-n"

    return res


def key_nba(cle, nbA, nbH):
    res = ""
    cle = read_key(cle)
    if not cle["Symbolic"] and not cle["Probabilistic"]:
        res += "Expl."
    elif not cle["Symbolic"] and cle["Probabilistic"]:
        res += "Ex. Pr."
    elif cle["SMCDEL"] and not cle["Probabilistic"] and cle["S5"]:
        res += "Croy.S5"
    elif cle["SMCDEL"] and not cle["Probabilistic"]:
        res += "Croy.KD45"
    elif cle["SMCDEL"] and cle["Probabilistic"]:
        res += "PDEL"
    else:
        pass

    # res += f" nbA:{nbA} "

    if cle["Normalize"]:
        res += "-Norm"

    return res


def rename_formulas(s):
    return s.replace("K_a", "□a").replace("Pr_a", "Pra").replace("K_b", "□b").replace("Pr_b", "Prb").replace("a0-R1i",
                                                                                                             "") \
        .replace("(", "").replace(")", "").replace(">=", "").replace("0.25", "")
    if "Pr_a Pr_b" in s:
        return "Pr Pr"
    if "knows" in s:
        return "□"
    if "Pr_a" in s:
        return "Pr"
    if "Pr_b" in s:
        return "Pr"
    if "Model checking" in s:
        return ""
    return s


# generic_draw(["Model checking"], label_function=lambda key: "",
#             filter_item=lambda items: [item for item in items if
#                                        "Pr_a" in str(item) or "a knows" in str(item)],
#             filter_keys=filter_smcpdel, log=log, dotdico=False, color_on_items=True,
#             title="Model checking (symbolic approach)", rename_item=rename_formulas,
#             sort_legend=[2, 5, 0, 3, 1, 4])


def rename_actions(s):
    if "Announce" in s:
        return "ann."
    if "plays" in s:
        return "j+p"
    if "Model checking" in s:
        return ""
    return s


def rename_create(s):
    if "create_epistemic_model" in s:
        return "Ep"
    if "create_event_models" in s:
        return "Ev"
    else:
        return ""


"""
cmap = matplotlib.cm.get_cmap('hot')
color_map = [cmap(0.0), cmap(0.8), cmap(0.0), cmap(0.8), cmap(0.3), cmap(0.6), cmap(0.3), cmap(0.6)]

color_map = [symb_pdel_color, symb_pdel_color_2, symb_pdel_color, symb_pdel_color_2,
             symb_del_color, symb_del_color_2, symb_del_color, symb_del_color_2]
"""


def get_copies(liste, n):
    res = []
    for el in liste:
        for i in range(0, n):
            res.append(el)
    return res


def get_color(key):
    dico_key = read_key(key)

    # TO DEAL WITH BLACK AND WHITE PRINTER

    # rgba = cmap(1.0)
    if dico_key["S5"]:  # SMCDEL S5
        return "limegreen"

    if not dico_key["Symbolic"] and dico_key["Probabilistic"]:  # Explicit
        return "red"
    if not dico_key["Symbolic"] and not dico_key["Probabilistic"]:  # Explicit
        return "orange"

    if dico_key["SMCDEL"] and dico_key["Probabilistic"] and not dico_key["Normalize"]:
        return "dodgerblue"  # SMCPDEL denormalized

    if dico_key["SMCDEL"] and dico_key["Probabilistic"] and dico_key["Normalize"]:
        return "darkviolet"  # SMCPDEL normalized

    if dico_key["SMCDEL"] and not dico_key["Probabilistic"]:
        return "green"  # SMCDEL

    return "Black"


def autosort(label_list):
    label_list2 = sorted(label_list)

    res_labels = []
    res_idx = []
    prior_list = ["Expl", "KS", "BS", "PS,", "PS ", "PS-n"]

    for prior_item in prior_list:
        for label in label_list2:
            if label not in res_labels:
                if prior_item in label:
                    res_labels.append(label)
                    res_idx.append(label_list.index(label))

    string = "Missing:\n"
    for el in label_list2:
        if el not in res_labels:
            string += "- '" + el + "'\n"

    assert len(label_list) == len(res_labels), f'{len(res_idx)} {res_idx} : missing idx. \n {string}' \
                                               f'\n{len(label_list2)} : {label_list2}' \
                                               f'\n{len(res_labels)} : {res_labels}'

    return res_idx


def get_colors_cmap(name, desired_colors, padding=0):
    n_colors = desired_colors + 2 * padding
    cmap = matplotlib.cm.get_cmap(name)
    range_float = range(0, 101, int(100 / (n_colors - 1)))
    set = [i for i in range_float][padding:len(range_float) - padding]
    print([i for i in range_float], set, padding)
    # print(f"get colormap '{name}', {desired_colors} colors (padding={set}) : {set}")
    return [cmap(f / 100) for f in range_float]


def get_colors_cmap_with_list(name, range_float):
    cmap = matplotlib.cm.get_cmap(name)
    return [cmap(f / 100) for f in range_float]


def feeding():
    for folder in sys.argv[1:-1]:
        print("> Search in", folder, end="...")
        assert path.exists(folder), f"{folder} doesn't exist."

        print(f"Searching data in {folder}", end="...")

        onlyfiles = [f for f in listdir(folder) if
                     path.isfile(path.join(folder, f)) and (f.endswith(".csv") or f.endswith(".txt"))]

        print(f"Files find : {len(onlyfiles)}.")

        for file in onlyfiles:
            # print(f" Read : {file}")
            readfile(folder + "/" + file)


def show(DATA_KEY):
    if DATA_KEY in MY_DATA.keys():
        print("=" * 60)
        print(DATA_KEY)
        print("=" * 60)
        for c in MY_DATA[DATA_KEY]:

            print(c)
            print(read_key(c))
            print(key_nba(c, None, None))
            for a, da in MY_DATA[DATA_KEY][c].items():
                print("Agents : ", a)
                for b, db in da.items():
                    print(" " * 4, "- Cartes en main : ", b)
                    liste_vus = []
                    mini, maxi = +99999, -99999
                    for c, dc in db.items():
                        liste_vus.append(c)
                        # print(dc)
                        mini = min(mini, len(dc[DATA_KEY]))
                        maxi = max(maxi, len(dc[DATA_KEY]))
                    print(" " * 8, sorted(liste_vus), mini, maxi)
            print()
    else:
        print("=" * 60)
        print(DATA_KEY, "not in MY_DATA.keys().")
        print("=" * 60)


if __name__ == '__main__':

    c_SP = ["dodgerblue"]
    c_SP_2 = ["steelblue"]
    c_SP_3 = c_SP
    c_SP_4 = ["darkblue"]

    c_SPn = ["violet"]
    c_SPn_2 = c_SPn
    c_SPn_3 = ["purple"]

    c_SK = ["limegreen"]
    c_SK_2 = ["lawngreen"]
    c_SK_3 = c_SK
    c_SK_4 = ["forestgreen"]
    c_SK_5 = ["darkgreen"]

    # python3.6 rendering_of_calculations.py input_folder output_folder

    DISPLAY = False

    assert len(sys.argv) > 1, "Two arguments are required : folders."

    import pickle
    filename = "histo.pickle"
    try:
        MY_DATA = pickle.load(open(filename, "rb"))
        print("Chargement de l'historique.")
    except (OSError, IOError) as e:
        print("Nouvel historique.")
        feeding()

        pickle.dump(MY_DATA, open(filename, "wb"))

    to_store = sys.argv[-1]
    if not path.exists(to_store):
        mkdir(to_store)

    print("DISPLAY = ", DISPLAY)
    print("Save in ", to_store)

    matplotlib.rcParams["savefig.directory"] = to_store

    print(MY_DATA.keys())
    print()

    show('create_epistemic_model')
    show('total time MC')
    show('Apply time')

    # MY_DATA[dico_type][key][nbA][nbH][nbC][item]

    filter_key_not_normalize = lambda key: not read_key(key)["Normalize"]  # or not read_key(key)["Symbolic"]

    filter_smcpdel = lambda key: not read_key(key)["Normalize"] and read_key(key)["Symbolic"] and read_key(key)[
        "Probabilistic"]

    filter_symb_proba = lambda key: read_key(key)["Symbolic"] and read_key(key)["Probabilistic"]

    log = True

    # markers = ["x", "D", "s", "x", "D", "s", "+", "^", "v", "+", "^", "v"]
    markers = ["x", "x", "+", "^", "v", "+", "^", "v"]


    def get_pr(items):
        return [item for item in items if "Pr" in str(item)]


    # colors = get_colors_cmap("viridis", 3, padding=1)
    colors = get_colors_cmap_with_list("viridis", [0, 60, 95])
    colors = [colors[0], colors[2], colors[1]]
    markersize = 40
    linewidth = 1

    colors = ["red", "orange", "blue", "green", "purple", "dodgerblue",]


    def creation_filter(key):
        #return True
        rk = read_key(key)
        symb, proba = rk["Symbolic"], rk["Probabilistic"]
        if symb: return True
        if not symb and proba: return True
        return False


    def lr(a, b):
        return list(range(a, b + 1))


    for log in [True, False]:

        for ag in [2, 3]:

            if ag == 2:
                sort_l = lr(0,2)+lr(15,18)+lr(3, 14)
            else:
                sort_l = lr(0,1)+lr(11,13)+lr(2, 10)

            generic_draw(["Creation time"],
                         title=f"Temps de création (nbA={ag})",
                         label_function=key,
                         filter_keys=creation_filter,
                         log=log,
                         filter_data=lambda nbA, nbH: nbH <= 5 and nbA == ag,
                         sort_legend=sort_l,
                         # fixed_colors=get_copies(colors, 2),
                         average=False,
                         stddev=False,
                         markersize=markersize,
                         linewidth=linewidth,
                         fixed_markers=["o", "^", "s"] + ["o", "^", "s", "v"]*4,
                         cols=1,
                         SHOW=False,
                         show_n=False,
                         bbox_to_anchor=(1.0, 1.0)
                         )

    generic_draw(["Creation time"],
                 title=f"Temps de création des structures de probabilités",
                 label_function=lambda k, nba, nbh: f"{key(k, None, None)}, nbA:{nba}, nbH:{nbh}",
                 filter_keys=filter_symb_proba,
                 log=False,
                 #filter_data=lambda nbA, nbH: nbH <= 5 and nbA == ag,
                 #sort_legend=sort_l,
                 fixed_colors=4*c_SP_2+3*c_SP_3+2*c_SP_4 + 4*c_SPn_2+3*c_SPn_3,
                 average=False,
                 stddev=False,
                 markersize=markersize,
                 linewidth=linewidth,
                 fixed_markers=["o", "^", "s", "v", "o", "^", "s", "o", "^"]*2,
                 cols=1,
                 SHOW=False,
                 show_n=False,
                 bbox_to_anchor=(1.0, 1.0),
                 label_nbh=False
                 )


    for agent in [2, 3]:

        if agent == 2:
            sort_l = lr(0, 2) + lr(15, 18) + lr(3, 14)
        else:
            sort_l = lr(0, 1) + lr(11, 13) + lr(2, 10)

        generic_draw(["RatioEv/TOT"],
                     title=f"Ratio du temps de création (nbA={agent})",
                     label_function=key,
                     filter_keys=creation_filter,
                     log=False,
                     filter_data=lambda nbA, nbH: nbA == agent,
                     sort_legend=sort_l,
                     # fixed_colors=get_copies(colors, 2),
                     average=False,
                     stddev=True,
                     markersize=markersize,
                     linewidth=linewidth,
                     fixed_markers=["o", "^", "s"] + ["o", "^", "s", "v"]*4,
                     cols=1,
                     y="Ratio temps création evénements / temps total",
                     SHOW=False,
                     bbox_to_anchor=(1.0, 1.0),
                     show_n=False
                     )

    for log in [True, False]:
        for nbA, nbH in [(2, 2), (2, 3), (2, 4), (3, 2), (3, 3)]:

            leg = lr(0,1)+lr(8,9)+lr(2,7) if nbA == 2 else lr(6,7)+lr(0,5)

            generic_draw(["Product Update"], label_function=key,
                         filter_keys=creation_filter,
                         log=log,
                         color_on_items=False,
                         title=f"Product update (nbA={nbA}, nbH={nbH})",
                         rename_item=rename_actions,
                         filter_data=lambda a, b: a == nbA and b == nbA,
                         # fixed_colors=get_copies(colors, 4),
                         fixed_markers=["o", "s"] * 6,
                         sort_legend=leg,
                         # average=True,
                         # sort_legend=[0, 2, 1, 3, 8, 10,9,11, 4, 6, 5, 7],
                         stddev=False,
                         cols=5 if nbA == 2 else 4,
                         markersize=markersize,
                         linewidth=linewidth,
                         label_nbh=False,
                         ylim=(-10,400)
                         )

    generic_draw(["NbVars"],
                 title="Nombre de variables en fonction de nbC",
                 label_function=lambda k, nba, nbh: f"nbA={nba}, nbH={nbh}",
                 filter_keys=filter_smcpdel, log=False, label_nbh=False,
                 fixed_colors=["r", "r", "r", "r", "b", "b", "b", "g", "g"],
                 y="Nombre de variables")

    colors = ["r"] * 4 + ["b"] * 3 + ["violet"] * 4 + ["purple"] * 3

    for log in [False]:
        generic_draw(["Pi"],
                     title="Taille des lois de probabilités normalisées et dénormalisées Π_a",
                     label_function=lambda k, nba, nbh: f"{key(k, None, None)}, nbA={nba}, nbH={nbh}",
                     filter_keys=filter_symb_proba, log=log, label_nbh=False, cols=2,
                     filter_data=lambda a, b: a <= 3,
                     y="Taille des ADDs, en nombre de noeuds",
                     fixed_colors=c_SP_3*4+c_SP_4*3+c_SPn_2*4+3*c_SPn_3,
                     fixed_markers=["o", "^", "s", "v", "o", "^", "s"]*2 ,
                     ylim=(-10, 700000))

    for log in [False]:
        generic_draw(["Pi"],
                     title="Taille des lois de probabilités dénormalisées Π_a ",
                     label_function=lambda k, nba, nbh: f"nbA={nba}, nbH={nbh}",
                     filter_keys=filter_smcpdel, log=log, label_nbh=False, cols=1,
                     filter_data=lambda a, b: a <= 3,
                     y="Taille des ADDs, en nombre de noeuds",
                     fixed_colors=c_SP_3*4+c_SP_4*3+c_SPn_2*4+3*c_SPn_3,
                     fixed_markers=["o", "^", "s", "v", "o", "^", "s"]*2 ,
                     ylim=(-10, 50000))


    liste_Ka = ["K_a a", "K_b a", "K_a K_b a", "K_b K_a a"]
    liste_Kb = ["K_a b", "K_b b", "K_a K_b b", "K_b K_a b"]

    agent = 'a'
    liste_Pra = [f"Pr_a {agent}", f"Pr_b {agent}", f"Pr_b Pr_a {agent}", f"Pr_a Pr_b {agent}"]
    agent = 'b'
    liste_Prb = [f"Pr_a {agent}", f"Pr_b {agent}", f"Pr_b Pr_a {agent}", f"Pr_a Pr_b {agent}"]

    agent = 'a'
    mixed_a = [
        f"Pr_b K_a {agent}",
        f"Pr_a K_b {agent}",
        f"K_b Pr_a {agent}",
        f"K_a Pr_b {agent}"]

    agent = 'b'
    mixed_b = [
        f"Pr_b K_a {agent}",
        f"Pr_a K_b {agent}",
        f"K_b Pr_a {agent}",
        f"K_a Pr_b {agent}"]


    def filter_formulas(items, liste):
        global liste_Ka
        res = []
        for item in items:
            # print(">", item[0], item)
            if item[0] in liste:
                # print("ADDED", item)
                res.append(item)
        # print("RES", res)
        return res

    filter_Ka = lambda items: filter_formulas(items, liste_Ka)
    filter_Kb = lambda items: filter_formulas(items, liste_Kb)
    filter_Ka_et_Kb = lambda items: filter_formulas(items, liste_Ka + liste_Kb)

    filter_Pra = lambda items: filter_formulas(items, liste_Pra)
    filter_Prb = lambda items: filter_formulas(items, liste_Prb)
    filter_Pra_et_Prb = lambda items: filter_formulas(items, liste_Pra + liste_Prb)


    def mc_filter(key):
        rk = read_key(key)
        symb, proba = rk["Symbolic"], rk["Probabilistic"]
        if symb and proba: return True
        if not symb and proba: return True
        return False


    # Variance sur Pr_ba Box_a , Pr_b

    # colors = get_colors_cmap("viridis", 4, padding=0)
    #colors = get_colors_cmap_with_list("viridis", [0, 45, 75, 95])
    #symboles = ["s", "^", "o", "v"]

    filter_symb_struc = lambda key: (not read_key(key)["Normalize"] and read_key(key)["Symbolic"] and read_key(key)[
        "Probabilistic"]) or (not read_key(key)["Symbolic"] and read_key(key)["Probabilistic"]) or (
                                            not read_key(key)["Normalize"] and read_key(key)["Symbolic"] and not
                                    read_key(key)[
                                        "Probabilistic"] and read_key(key)["S5"])

    filter_symb_struc2 = lambda key: (read_key(key)["Symbolic"] and read_key(key)[
        "Probabilistic"]) or (not read_key(key)["Symbolic"] and read_key(key)["Probabilistic"])

    """
    for nbA, nbH in [(2, 2), (2, 3), (2, 4), (3, 2), (3, 3)]:

        for filt, name, structs in [
            (filter_Ka_et_Kb, "K", filter_symb_struc),
            (filter_Pra_et_Prb, "Pr", filter_symb_struc2)
        ]:

            if name == "Pr":

                if nbA == 2:
                    colors = ["orange"] * 4 + ["red"] * 4 + ["dodgerblue"] * 4 + ["blue"] * 4 + ["violet"] * 4 + [
                        "purple"] * 4
                if nbA == 3:
                    cols = 2
                    colors = ["dodgerblue"] * 4 + ["blue"] * 4 + ["violet"] * 4 + ["purple"] * 4

            else:
                if nbA == 2:
                    colors = ["orange"] * 4 + ["red"] * 4 + ["dodgerblue"] * 4 + ["blue"] * 4 + ["limegreen"] * 4 + [
                        "darkgreen"] * 4
                    cols = 3
                else:
                    colors = ["dodgerblue"] * 4 + ["blue"] * 4 + ["limegreen"] * 4 + ["darkgreen"] * 4
                    cols = 2

            generic_draw(["Model checking"],
                         label_function=key,
                         filter_keys=structs,
                         log=False,
                         title=f"Model checking {name} (nbA={nbA}, nbH={nbH})",
                         cols=cols,
                         filter_data=lambda a, h: nbA == a and nbH == h,
                         filter_item=filt,
                         stddev=False,
                         average=False,
                         rename_item=rename_formulas,
                         fixed_colors=colors,
                         fixed_markers=symboles * 6,
                         label_nbh=False,
                         )
    """

    my_filter_k = lambda items: filter_formulas(items, ["K_b K_a a", "K_a a", "K_b a", "K_a K_b a"])
    my_filter_worst_k = lambda items: filter_formulas(items, ["K_b K_a a", "K_a a"])

    my_filter_pr = lambda items: filter_formulas(items, ["Pr_b Pr_a a", "Pr_a a", "Pr_a K_b a", "K_a Pr_b b"])
    my_filter_worst_pr = lambda items: filter_formulas(items, ["Pr_b Pr_a a", "Pr_a a"])

    SPn = lambda key : read_key(key)["Normalize"] and read_key(key)["Symbolic"] and read_key(key)["Probabilistic"]
    SP = lambda key : not read_key(key)["Normalize"] and read_key(key)["Symbolic"] and read_key(key)["Probabilistic"]
    SC = lambda key : read_key(key)["Symbolic"] and not read_key(key)["Probabilistic"] and not read_key(key)["S5"]
    SK = lambda key : read_key(key)["Symbolic"] and not read_key(key)["Probabilistic"] and read_key(key)["S5"]
    ExplPr = lambda key : not read_key(key)["Symbolic"] and read_key(key)["Probabilistic"]

    filter_symb_struc = lambda key: ExplPr(key) or SK(key) or SC(key)

    generic_draw(["Model checking"],
                 label_function=key,
                 filter_keys=ExplPr,
                 log=False,
                 title=f"Model checking Pr Expl (nbA={2}, nbH={2})",
                 cols=1,
                 #sort_legend=lr(0, 3) + lr(8, 11) + lr(4, 7),
                 filter_data=lambda a, h: a == 2 and h == 2,
                 filter_item=my_filter_pr,
                 stddev=True,
                 average=False,
                 rename_item=rename_formulas,
                 # fixed_colors=colors,
                 fixed_markers=["D", "s", "^", "v"] * 3,
                 label_nbh=False,
                 )


    generic_draw(["Model checking"],
                 label_function=key,
                 filter_keys=filter_symb_struc,
                 log=False,
                 title=f"Model checking K avec échantillon de formules (nbA={2}, nbH={2})",
                 cols=1,
                 sort_legend=lr(0,3)+lr(8,11)+lr(4,7),
                 filter_data=lambda a, h: a == 2 and h == 2,
                 filter_item=my_filter_k,
                 stddev=True,
                 average=False,
                 rename_item=rename_formulas,
                 # fixed_colors=colors,
                 fixed_markers=["D", "s", "^", "v"] * 3,
                 label_nbh=False,
                 )

    generic_draw(["Model checking"],
                 label_function=lambda k, nba, nbh: f"nbA:{nba}, nbH:{nbh}",
                 filter_keys=SC,
                 log=False,
                 title=f"Model checking K avec pires formules \n pour les structures de connaissances",
                 cols=1,
                 #sort_legend=lr(0,1)+lr(8,9)+lr(2,7),
                 filter_data=lambda a, h: a < 4 and 2 <= h <= 4 and (a,h) not in [(3,4)],
                 filter_item=my_filter_worst_k,
                 stddev=False,
                 average=False,
                 rename_item=rename_formulas,
                 fixed_colors=get_copies(colors, 2),
                 fixed_markers=["s", "^"] * 6,
                 label_nbh=False,
                 ylim=(-10, 200)
                 )

    filter_symb_struc = lambda key: ExplPr(key) or SP(key) or SPn(key)

    for l in [True, False]:
        generic_draw(["Model checking"],
                     label_function=key,
                     filter_keys=filter_symb_struc,
                     log=l,
                     title=f"Model checking Pr avec échantillon de formules (nbA={2}, nbH={2})",
                     cols=1,
                     sort_legend=lr(0, 3) + lr(8, 11) + lr(4, 7),
                     filter_data=lambda a, h: a == 2 and h == 2 ,
                     filter_item=my_filter_pr,
                     stddev=False,
                     average=False,
                     rename_item=rename_formulas,
                     # fixed_colors=colors,
                     fixed_markers=["D", "s", "^", "v"] * 3,
                     label_nbh=False,
                     ylim=(-10, 200) if not l else None
                     )

    generic_draw(["Model checking"],
                 label_function=lambda k, nba, nbh: f"nbA:{nba}, nbH:{nbh}",
                 filter_keys=SP,
                 log=False,
                 title=f"Model checking Pr avec pires formules \n pour les structures de probabilités",
                 cols=1,
                 # sort_legend=lr(0,1)+lr(8,9)+lr(2,7),
                 filter_data=lambda a, h: a < 4 and 2 <= h <= 4 and (a,h) not in [(3,4)],
                 filter_item=my_filter_worst_pr,
                 stddev=False,
                 average=False,
                 rename_item=rename_formulas,
                 fixed_colors=get_copies(colors, 2), #c_SP_2 * 2 + c_SP_3 * 2 + c_SP_4 * 2 + c_SPn_2 * 2 + 2 * c_SPn_3,
                 fixed_markers=["s", "^"] * 6,
                 label_nbh=False,
                 ylim=(-10, 200)
                 )

    generic_draw(["Product Update"],
                 label_function=lambda k, nba, nbh: f"nbA:{nba}, nbH:{nbh},",
                 filter_keys=SP,
                 log=False,
                 title=f"Product update pour les structures de probabilités",
                 cols=1,
                 # sort_legend=lr(0,1)+lr(8,9)+lr(2,7),
                 filter_data=lambda a, h: a < 4 and 2 <= h < 5 and (a,h) not in [(3,4)],
                 stddev=False,
                 average=False,
                 fixed_colors=get_copies(["red", "gold", "blue", "green", "purple"], 2),
                 # c_SP_2 * 2 + c_SP_3 * 2 + c_SP_4 * 2 + c_SPn_2 * 2 + 2 * c_SPn_3,
                 fixed_markers=["o", "s"] * 9,
                 label_nbh=False,
                 rename_item=rename_actions
                 )


    generic_draw(["Precompile"],
                 title="Temps de précompilation de Πxθ' et Marg(Πxθ') \n pour les structures de probabilités",
                 filter_keys=filter_smcpdel,
                 label_function=lambda k, nba, nbh: f"nbA:{nba}, nbH:{nbh}",
                 filter_data=lambda a, h: (a, h) not in [(3,4)],
                 #fixed_colors=["r"] * 3 + ["orange"] * 2,
                 fixed_colors=["red"]*4+["blue"]*3,
                 fixed_markers=["s", "D", "^", "v"] *2,
                 label_nbh=False,
                 ylim=(-10, 500))

    nbA = 2
    nbH = 2
    nbC = 50
    key = (True, False, True, True, False)

    print(read_key(key))

    agents = ["b", "a"]
    mods = ["K", "Pr"]
    liste = []

    table = [[" "] * 4 for i in range(0, 6)]

    colonne = 0
    ligne = 0
    for inner in agents:
        for i_first, first in enumerate(agents):
            for i_mod1, mod1 in enumerate(mods):
                subliste = []
                f1 = f"{mod1}_{first} {inner}"
                subliste.append(f1)
                table[ligne][colonne] = f1
                ligne += 1
                for i_second, second in enumerate(agents):
                    if first != second:
                        for i_mod2, mod2 in enumerate(mods):
                            f2 = f"{mod2}_{second} {f1}"
                            subliste.append(f2)
                            table[ligne][colonne] = f2
                            ligne += 1
                liste.append(subliste)

            colonne += 1
            ligne = 0

    from prettytable import PrettyTable

    x = PrettyTable()

    # x.field_names = ["a > b 1", "Temps 2 ", "a > b 3", "Temps 4", "a < b 5", "Temps 6", "a < b 7", "Temps 8"]

    for sublist in table:
        res = []
        for form in sublist:
            values = MY_DATA["Model checking"][key][nbA][nbH][nbC][form]
            res.append(form)
            res.append(round(sum(values) / len(values), 2))
        x.add_row(res)

    print(x)

    for fixed_nbH in range(2, 6):

        infos = {}
        dico = MY_DATA["Model checking"][key]
        for nbAgents in dico.keys():
           for nbH in dico[nbAgents].keys():
               for nbC in dico[nbAgents][nbH].keys():
                    for sublist in table:
                        res = []

                        for form in sublist:

                            if form not in infos:
                                infos[form] = {"x": [], "y": []}

                            values = dico[nbAgents][nbH][nbC][form]
                            temps_moyen = round(sum(values) / len(values), 2)
                            taille_a = MY_DATA["Pi"][key][nbAgents][nbH][nbC]["PiSize-a"]
                            taille_b = MY_DATA["Pib"][key][nbAgents][nbH][nbC]["PiSize-b"]
                            taille_moyenne_a = round(sum(taille_a) / len(taille_a), 2)
                            taille_moyenne_b = round(sum(taille_b) / len(taille_b), 2)

                            if taille_moyenne_a+taille_moyenne_b < 70000 and nbAgents == 2 and nbH == fixed_nbH:
                                infos[form]["x"].append(taille_moyenne_a + taille_moyenne_b)
                                infos[form]["y"].append(temps_moyen)

        plt.figure().clear()
        plt.close()
        plt.cla()
        plt.clf()

        count = 0
        color_i = 0
        markers = ["+", "D", "s", "x"]

        figure = plt.figure(dpi=200)

        for form in infos:
            if "Pr" in form:
                x, y = infos[form]["x"], infos[form]["y"]
                plt.scatter(x, y, label=form, c=colors[color_i], marker=markers[count], s=[10 for i in range(0, len(x))])

                count +=1
                if count == 4:
                    count = 0
                    color_i += 1

        plt.xlabel("Tailles des ADDs représentant Pi_a et Pi_b")
        plt.ylabel("Temps de model checking en secondes")
        plt.title(f'Temps de calcul en fonction de la taille des ADDs, avec nbA=2 et nbH={fixed_nbH}.')
        plt.legend(ncol=2)#ncol=4, bbox_to_anchor=(0.3, 1.0))
        plt.savefig(f"output/test_ratio_taille_temps_{fixed_nbH}.png")

        plt.figure().clear()
        plt.close()
        plt.cla()
        plt.clf()

    sys.exit()

    def filter_formulasK(items):
        res = []
        for item in items:
            # print(">", item[0], item)
            if item[0] in ["K_a", "K_b", "K_b K_a", "K_a K_b"]:
                res.append(item)
        # print("RES", res)
        return res


    generic_draw(["Model checking K"],
                 label_function=key_nba,
                 filter_keys=mc_filter,
                 log=True, title=f"Model checking K", cols=2,
                 # filter_data=lambda nbA, nbH: nbH == 2,
                 filter_item=filter_formulasK,
                 stddev=True,
                 average=True,
                 rename_item=rename_formulas,
                 fixed_colors=colors * 4,
                 fixed_markers=get_copies(symboles, 4),
                 markersize=markersize, linewidth=linewidth,
                 markersize_except={4: 20, 8: 20},
                 line_except={4: (0, (5, 5)), 8: (0, (5, 5))}  # (0,(5,10))
                 )

    """
    # colors = ["red", "blue", "green", "orange", "black", "yellow"]
    generic_draw(["NbVars"], label_function=key, filter_keys=filter_smcpdel, log=log, fixed_colors=colors)
    generic_draw(["Omega"], label_function=key, filter_keys=filter_smcpdel, log=log,
                 fixed_colors=colors)

    generic_draw(["NbVars", "Pi", "Omega"], label_function=key, filter_keys=filter_smcpdel, log=log, y="An integer",
                 fixed_colors=["red", "red", "green", "green", "blue", "blue"])

    generic_draw(["Pi"], label_function=key)#, filter_keys=filter_smcpdel, log=log)
    """
