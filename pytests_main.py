
#--->
# It's a dirty trick to deal with 'scripts' folder
# Otherwise, this file can be copy/paste near to 'src' folder
# and launched with 'python3 -O XXX.py'

import os, sys

p = os.path.abspath('scripts')
sys.path.insert(1, p)

#<-- End of the trick

from src.utils.timer import TimerData
from src.model.datastructure.real_function_tests import *
from src.model.datastructure.add.add_real_function import ADDManager


if __name__ == '__main__':

    n = 10
    nb_vars = 8
    print_time = True
    manager_liste = [ADDManager]

    particular_test = None # "test_marginalisation_in_fonction_hard_example2"#"test_not" #"test_substract"

    for manager in manager_liste:

        if particular_test is None:
            run_tests(manager, n=n, nb_vars=nb_vars, print_time=print_time)
        else:
            run_one_test(manager, particular_test, n, nb_vars)
